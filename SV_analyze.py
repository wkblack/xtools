#!/bin/python
# read in RM data from DES SV
# at SkyMaps/RM/SVA1

from matplotlib import pyplot as plt
from astropy.table import Table
from config import Aardvark as sim
rm = sim['rm_dir']
import numpy as np

fdir = rm + 'SVA1/'
fnames = ['redmapper_sva1_public_v6.3_area.fits.gz', 'redmapper_sva1_public_v6.3_catalog.fits.gz', 'redmapper_sva1_public_v6.3_members.fits.gz', 'redmapper_sva1_public_v6.3_randoms.fits.gz', 'redmapper_sva1_public_v6.3_zmask.fits.gz']
f = fdir + fnames[1]

# print 'keys:\n',df.keys()
cols=['ID','LAMBDA','Z_LAMBDA','Z_SPEC',
      'MAG_AUTO_G','MAG_AUTO_R','MAG_AUTO_I','MAG_AUTO_Z']
df = Table.read(f, format='fits')[cols].to_pandas()
print 'total cluster count:',len(df)

if 1: # vet down redshift range
  if 1:
    Z_LOW,Z_HIGH=.1,.3
  else:
    Z_LOW,Z_HIGH=.2,.25
    # Z_LOW,Z_HIGH=.1,.25
  print 'vetting redshift range: (%g,%g)' % (Z_LOW,Z_HIGH)
  mask = np.logical_or(np.logical_and(Z_LOW<df.Z_LAMBDA,df.Z_LAMBDA<Z_HIGH),
                       np.logical_and(Z_LOW<df.Z_SPEC,df.Z_SPEC<Z_HIGH))
  if sum(mask)<1:
    print "ERROR: Redshift mask null!" 
    raise SystemExit
  
  df = df[mask] # narrow down to my personal redshift range
  print 'reduced cluster count:',len(df)

hid_list = df.ID.values
z_list1 = df.Z_SPEC.values
z_list2 = df.Z_LAMBDA.values

if 0: # redshifts are pretty well aligned.
  if 0: 
    print 'hid list:',hid_list
    print 'z1 list:',z_list1
    print 'z2 list:',z_list2
  
  if 0: # compare the two redshifts
    MIN,MAX = min(min(z_list1),min(z_list2)),max(max(z_list1),max(z_list2))
    plt.plot([MIN,MAX],[MIN,MAX],'k')
    plt.scatter(z_list1,z_list2)
    plt.gca().set_aspect('equal')
    plt.xlabel("Z_SPEC"); plt.ylabel("Z_LABMDA");
    plt.show()
  
  if 0: # show redshift distribution
    plt.hist(z_list2,log=True)
    plt.xlabel("Z_LAMBDA"); plt.ylabel(r'$N$');
    plt.show()
  raise SystemExit


f = fdir + fnames[2]
cols=['ID','Z_SPEC','MAG_AUTO_G','MAG_AUTO_R','MAG_AUTO_I','MAG_AUTO_Z']
df_members = Table.read(f, format='fits')[cols].to_pandas()

if 1: # create colormap
  # puts more redshifted samples as redder
  from matplotlib import cm
  if 1:
    colormap = 'coolwarm'
  else:
    colormap = 'viridis'
  cw = cm.get_cmap(colormap)
  mini,maxi=min(z_list2),max(z_list2)
  colors = cw((z_list2-mini)/(maxi-mini))
  # norm = plt.Normalize(mini, maxi)

for ii,hid in enumerate(hid_list):
  # print "Redshift z=%g & %g" % (z_list2[ii],z_list2[ii])
  
  df_i = df_members[df_members.ID.values==hid]
  
  g=df_i.MAG_AUTO_G
  r=df_i.MAG_AUTO_R
  i=df_i.MAG_AUTO_I
  z=df_i.MAG_AUTO_Z
  
  sc = plt.scatter(i,g-r,1,c=colors[ii])
  plt.xlabel(r'$m_i$'); plt.ylabel(r'color: $g-r$');
  plt.xlim(15,20.6); plt.ylim(0,2.5);
  # plt.show()

if 1: # plot up Hao+ relation
  print "Plotting up Hao+ red sequence",
  from galaxy_id import gmr_line
  i_list = np.linspace(15,21,10) # more than spans range
  
  if 0: # use mean redshift
    print "using mean redshifts"
    z_val = np.mean(z_list2)
    gmr,wth = gmr_line(i_list,z_val)
    plt.plot(i_list,gmr[0],alpha=.5,color='k')
    plt.fill_between(i_list,gmr[0]+wth,gmr[0]-wth,
                     alpha=.125,color='r')
    plt.fill_between(i_list,gmr[0]+2*wth,gmr[0]-2*wth,
                     alpha=.125,color='r')
  else: # use min and max redshifts 
    print "using min and max redshifts"
    z_val_min,z_val_max = min(z_list2),max(z_list2)
    
    gmr,wth = gmr_line(i_list,z_val_min)
    plt.plot(i_list,gmr[0],alpha=.5,color='k')
    plt.fill_between(i_list,gmr[0]+wth,gmr[0]-wth,
                     alpha=.125,color='b')
    plt.fill_between(i_list,gmr[0]+2*wth,gmr[0]-2*wth,
                     alpha=.125,color='b')
    
    gmr,wth = gmr_line(i_list,z_val_max)
    plt.plot(i_list,gmr[0],alpha=.5,color='k')
    plt.fill_between(i_list,gmr[0]+wth,gmr[0]-wth,
                     alpha=.125,color='r')
    plt.fill_between(i_list,gmr[0]+2*wth,gmr[0]-2*wth,
                     alpha=.125,color='r')
    

plt.show()




print '~fin'
