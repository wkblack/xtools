#!/bin/python
# check CM distributions and radial profiles for buzzard flock haloes
########################################################################
import config as cfg
from glob import glob
from astropy.io import fits
import galaxy_id as gid
import pandas as pd
from os.path import isfile
import numpy as np

sim = cfg.Buzzard
gals = sim["glxy_dir"]+sim["glxy_fname"]
halos = sim["halo_dir"]+sim["halo_fname"]

f_gals = glob(gals)
f_halos = glob(halos)

print "len(f_gals)=%i" % len(f_gals)
print "len(f_halos)=%i" % len(f_halos)

# Now we've got a list of galaxy and halo files
# from which we can read in some data

########################################################################
# grab the most massive halo

f=f_halos[0]
print "processing halo file:",f
d=fits.open(f)[1].data
d=d[np.logical_and(.1<d['Z'],d['Z']<.3)]
hdl=d[max(d['MVIR'])==d['MVIR']][0]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def fix_fields(hdl):
  hdl_names = hdl.array.names; 
  hdl_values = [hdl.array[0][i] for i in range(len(hdl.array.names))]
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # approximate; based on Aardvark's largest halo
  r200=hdl['RVIR']/1.3007/1000. # convert to Mpc from kpc
  hdl_names.append("R200"); hdl_values.append(r200)
  Rs=hdl['RVIR']/4.8253 # keep in kpc
  hdl_names.append("Rs"); hdl_values.append(Rs)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # renaming...
  hdl_names.append("HALOPX"); hdl_values.append(hdl['PX'])
  hdl_names.append("HALOPY"); hdl_values.append(hdl['PY'])
  hdl_names.append("HALOPZ"); hdl_values.append(hdl['PZ'])
  
  return dict(zip(hdl_names, hdl_values))

hdl = fix_fields(hdl)
HID=hdl['HALOID']

########################################################################
# and get its galaxy members

fname='buzzard_galaxies_of_%i.csv' % HID

if isfile(fname):
  df = pd.read_csv(fname)
else:
  df_sum = pd.DataFrame(columns=gid.cols_glxy+gid.cols_tmag)
  
  # i=28 # only file for largest halo; contains 3033 galaxies 
  # if 1:
  for i in range(len(f_gals)):
    print "processing galaxy file %i/%i" % (i+1,len(f_gals))
    d = fits.open(f_gals[i])[1].data
    mask = d['HALOID']==hdl['HALOID']
    N = len(d[mask])
    if N<1:
      print "empty. continuing..."
      continue
    # else, process. 
    print "%i galaxies found" % N
    # ['PSIGMA5','RHALO','IVAR','ID','PX','MAG_R_EVOL','COMOVING_SIZE','SIZE','SEDID','Z_COS','Z','M200','AMAG','GAMMA1','TSIZE','FLUX','TDEC','GAMMA2','OMAGERR','PZ','TMAG','CENTRAL','EPSILON_IA','EPSILON','R200','DIST8','DEC','TRA','OMAG','VX','MAG_R','LMAG','KAPPA','VZ','SIGMA5','RA','TE','VY','W','MU','BAD_ASSIGN','PY','HALOID']
    df = gid.fits_to_pandas(d[mask])
    df_sum = df_sum.append(df,ignore_index=True,sort=False)
    pass
  
  df_sum.to_csv(fname,index=False)
  print "Saved %s" % fname
  
  df = df_sum


########################################################################
# create color-magnitude data for it

gid.color_plot(df,hdl)
print 'redshift:',hdl['Z']
dv = gid.vet(df,hdl)
gid.color_plot(dv,hdl,True)

########################################################################
# check the red fraction

dr = gid.select_ES0(dv,hdl)
print "Red fraction f_R=%g" % (1.0*len(dr)/len(dv))


########################################################################
# and create a radial profile for it
# to check whether it has the paucity at 0.1 R200.

gid.radial_profile(df,hdl)

