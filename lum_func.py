# fit m_star for Buzzard flock
# W.K.Black, Feb 2021

from scipy.optimize import curve_fit
from matplotlib import pyplot as plt
mpl_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
from kllr import kllr_model
import numpy as np
import h5py

h = .7 # Hubble constant

########################################################################
# functions for initial reading & analysis

fname_hist = 'lumfunc_hist.h5'
def get_hist(dZ=.005,subset=False,nbins=75):
  """
  generate histograms for Buzzard catalogue
  """
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # read in dataset
  print("Reading in dataset...") 
  if subset: # use low-Z dataset
    fname = '/global/cscratch1/sd/wkblack/SkyMaps/buzzard/vet/subset_low_Z.h5'
    loc_Z = 'redshift_cos'
    loc_i = 'mag_i_true'
    Z_lims = [.05, .32]
  else: # use full dataset
    fname = '/project/projectdirs/des/jderose/Chinchilla/Herd/Chinchilla-3/v2.0.0/sampleselection/Y3a/Buzzard-3_v2.0_Y3a_mastercat.h5'
    loc_Z = 'catalog/bpz/unsheared/redshift_cos'
    loc_i = 'catalog/gold/mag_i_true'
    Z_lims = [0, 2.35]

  with h5py.File(fname,'r') as ds:
    Z = ds[loc_Z][:]
    i = ds[loc_i][:]
  
  Z_bins = np.arange(Z_lims[0],Z_lims[1]+dZ-1e-10,dZ)
  Z_mids = (Z_bins[:-1]+Z_bins[1:])/2.
  print("Dataset processed.") 
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # store histogram values
  m_i,N = np.zeros((2,len(Z_mids),nbins))
  
  for ii in range(len(Z_bins)-1):
    # set up values for this bin
    Z_label = r'$z|[%0.3f,%0.3f)$' % (Z_bins[ii],Z_bins[ii+1])
    print(f"Analyzing {Z_label}")
    dZ = Z_bins[ii+1] - Z_bins[ii]
    mask_Z = (Z_bins[ii] <= Z) * (Z < Z_bins[ii+1])
    i_ii = i[mask_Z]
    if len(i_ii) < 10:
      print("ERROR: Insufficient data; skipping...") 
      continue
    # measure histogram
    n,b = np.histogram(i_ii,nbins)
    bin_mids = (b[1:]+b[:-1])/2.
    m_i[ii] = bin_mids
    N[ii] = n
  
  # save output
  with h5py.File(fname_hist,'w') as ds:
    ds.create_dataset('Z_bins',data=Z_bins)
    ds.create_dataset('Z_mids',data=Z_mids)
    ds.create_dataset('N',data=N)
    ds.create_dataset('m_i',data=m_i)
  print("Saved",fname_hist)


def flush_right(N):
  " fill in zeros from histogram by splitting into future bins "
  report = np.copy(N).astype(float)
  ii = 0
  while ii < len(N) - 1:
    jj = 1
    while N[ii+jj] == 0:
      if (ii + jj < len(N) - 1) and N[ii+jj+1] == 0:
        jj += 1
        continue
      else:
        report[ii:ii+jj+1] = N[ii]/(jj+1)
        ii += jj - 1
        break
    ii += 1
  return report


def get_max_mi(m_i,N,method='quick'):
  """ 
  Estimate the last safe m_i value, beyond which phi turns down 
  (Will always capture the last point, at least.)
  """
  mm = (m_i[1:]+m_i[:-1])/2. # midpoint m_i bins
  if method == 'quick':
    N_diff = N[1:] - N[:-1] # diff btw consecutive points
    return max(mm[N_diff/max(N_diff) > +.01]) # max ~safe m_i
  elif method == 'kllr':
    lm = kllr_model(kernel_width=.5)
    xr,yrm,icpt,sl,sig = lm.fit(m_i,N,nbins=250)
    if 0: # test out slopes
      x = (xr[1:]+xr[:-1])/2.
      y = -.4*np.log(10)*(np.log(yrm[1:])-np.log(yrm[:-1]))/(xr[1:]-xr[:-1])
      plt.plot(x,y); plt.ylim(-3,.5); plt.show()
    # mm = (xr[1:]+xr[:-1])/2.
    # return max(mm[yrm[1:] > yrm[:-1]])
    return max(xr[sl > 0])
  else:
    print("ERROR: Unset method!") 
    return -1


def plot_hist(m_i,N,fit_pars=None,use_BaBar=True,shade_bad=True):
  # set up errors
  if use_BaBar:
    err_low = -.5 + np.sqrt(N+.25)
    err_high = +.5 + np.sqrt(N+.25)
  else:
    err_low = err_high = np.sqrt(N)
  yerr = [err_low,err_high]
  # plot
  plt.errorbar(m_i,N,yerr,fmt='o',capsize=2,color='k')
  xlim = plt.xlim(plt.xlim())
  ylim = np.array(plt.ylim(.5,max(N)*2))
  if fit_pars is not None:
    x = np.linspace(xlim[0],xlim[1],10**3)
    if len(fit_pars) == 3:
      plt.plot(x,lum_func(x,*fit_pars),alpha=.5,color='cyan')
      plt.plot(np.ones(2)*fit_pars[-1],ylim,alpha=.5,color='cyan')
    elif len(fit_pars) == 5:
      plt.plot(x,lum_func5(x,*fit_pars),alpha=.5,color='cyan')
      plt.plot(np.ones(2)*fit_pars[-1],ylim,'cyan',alpha=.5)
      pass
    else:
      print("ERROR: Unknown parameter set!")
      return
  if shade_bad:
    # shade in areas which should be ignored in fitting
    mi_max = get_max_mi(m_i,N,method='kllr') # high end bound from turn down point
    plt.fill_between([mi_max,xlim[1]],ylim[1]*np.ones(2),
                     alpha=.125,lw=0,color='k')
    if 0: # unneeded---high end CAN be trusted!
      # low end bound from last zero
      try:
        mi_min = m_i[np.where(N==0)[0][-1]]
        plt.fill_between([xlim[0],mi_min],ylim[1]*np.ones(2),
                         alpha=.125,lw=0,color='k')
      except IndexError: # No zeros found; unlikely.
        pass
  plt.yscale('log')
  plt.xlabel(r'$m_i$')
  plt.ylabel(r'count')
  plt.tight_layout()
  plt.show()


def plot_all_hist(skip=1,fit_guess=True,use_BaBar=True,shade_bad=True,model=5):
  with h5py.File(fname_hist,'r') as ds:
    Z_bins,Z_mids,m_i,N = [ds[tag][()] for tag in ['Z_bins','Z_mids','m_i','N']]
    dZ = Z_bins[1:] - Z_bins[:-1]
  
  init = None
  
  for ii in range(len(Z_mids)):
    if ii % skip != 0:
      continue
    Z_label = r'$z|[%0.3f,%0.3f)$' % (Z_bins[ii],Z_bins[ii+1])
    plt.title(Z_label)
    if fit_guess:
      init,bounds = guess_pars(Z_mids[ii],dZ[ii],model)
    plot_hist(m_i[ii],N[ii],init,use_BaBar=use_BaBar,shade_bad=shade_bad)


########################################################################
# fitting functions

def m_star_fit_simple(Z,offset,slope_log,slope_linear):
  return offset + slope_log * np.log(Z) + slope_linear * Z
m_star_fit_simple.labels = ['offset','log slope','linear slope']


def m_star_fit2(Z,offset,slope_log,b,c):
  return offset + slope_log * np.log(Z) + b*Z + c*Z**2


def m_star_fit4(Z,offset,slope_log,b,c,d,e):
  return offset + slope_log * np.log(Z) + b*Z + c*Z**2 + d*Z**3 + e*Z**4

m_star_fit4.labels = ['offset','log slope','b','c','d','e']


def m_star_fit6(Z,offset,slope_log,b,c,d,e,f,g):
  return slope_log * np.log(Z) + np.poly1d([g,f,e,d,c,b,offset])(Z)

m_star_fit6.labels = ['offset','log slope','b','c','d','e','f','g']
m_star_fit6.pars = [21.246, 2.128, 8.3 , -30.809, 51.472, -38.507, 13.288, -1.736]
m_star_fit6.errs = [0.152, 0.044, 0.627, 1.475, 2.022, 1.499, 0.558, 0.082]
# the offset has the biggest error / covariance, followed by log slope


def m_star(Z):
  return m_star_fit6(Z,*m_star_fit6.pars)


def phi_fit_simple(Z,norm,a,b):
  return norm * Z**2 * (1 + a*Z + b*Z**2)
  # zp = 2.35/2.
  # return A * Z**2 + A * B * (zp**2 - (Z - zp)**2)


def guess_pars(Z,dZ=.0025,model=3):
  if model==3:
    # phi fitting
    A,phi_pars = 3.016e+07, [0.6862,-3.553,5.329,-2.185,-0.2808]
    phi = (A*Z**2*dZ) * np.exp(np.poly1d(phi_pars)(Z))
    # alpha = -1.449
    if Z > 1.38: 
      alpha = -0.9253 * Z + 0.908
    else:
      alpha = np.poly1d([ 0.564,-0.109,-1.506])(Z)
    # m_*
    m_star = m_star(Z)
    initial = [phi,alpha,m_star]
    bounds_low = [phi/30,-2,m_star-2-.05/Z]
    bounds_up = [phi*30,0,m_star+2+.05/Z]
  elif model == 5:
    # phi_1
    A1,phi1_pars = 2.423e7,[-5.0076,8.3903,-5.2849,-0.3904,0.4596]
    phi1_top = A1 * dZ * Z**2
    phi1 = phi1_top * np.exp(np.poly1d(phi1_pars)(Z))
    # phi_2
    A2,phi2_pars = 5.175e+07,[ 0.4781,-2.8004, 5.1568,-4.1553, 0.9881]
    phi2_top = A2 * dZ * Z**2
    phi2 = phi2_top * np.exp(np.poly1d(phi2_pars)(Z))
    # alphas
    alpha1 = -1.5 - .4 * Z**2
    alpha2 = -0.2079 + -0.2381 * Z
    # m_*
    m_star = m_star(Z)
    # output
    initial = [phi1,phi2,alpha1,alpha2,m_star]
    bounds_low = [0,0,-5,alpha2-1,m_star-2-.05/Z]
    bounds_up = [5*phi1_top,5*phi2_top,-1,0,m_star+5+.05/Z]
  else:
    print("ERROR: Unset model!")
    return -1
  return initial,[bounds_low,bounds_up]


########################################################################
# luminosity functions & their log forms

def lum_func(m_i,phistar,alpha,mstar):
  " three parameter luminosity function "
  return .4 * np.log(10) * phistar * 10**(.4*(alpha+1)*(mstar-m_i)) \
            * np.exp(-10**(.4*(mstar-m_i)))


def log_lum_func(m_i,phistar,alpha,mstar):
  " log of three-parameter lum func "
  return np.log(.4 * np.log(10) * phistar) \
         + (.4*(alpha+1)*(mstar-m_i))*np.log(10) \
         + -10**(.4*(mstar-m_i))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# five par fit
def lum_func5(m_i,phi1,phi2,alpha1,alpha2,mstar):
  return .4 * np.log(10) * np.exp(-10**(.4*(mstar-m_i))) \
            * (phi1 * 10**(.4*(alpha1+1)*(mstar-m_i)) \
             + phi2 * 10**(.4*(alpha2+1)*(mstar-m_i)))


def log_lum_func5(m_i,phi1,phi2,alpha1,alpha2,mstar):
  return np.log(.4 * np.log(10)) - 10**(.4*(mstar-m_i)) \
         + np.log((phi1 * 10**(.4*(alpha1+1)*(mstar-m_i)) \
                 + phi2 * 10**(.4*(alpha2+1)*(mstar-m_i))))


########################################################################
# master fitting function

fname_fitting = 'm_star__fitting.h5'
def fit_hist(skip=1,saving=True,printing=False,plotting=False):
  """
  fit histograms with luminosity function fits
  (skip = 1 doesn't skip)
  """
  with h5py.File(fname_hist,'r') as ds:
    Z_bins,Z_mids,N,m_i = [ds[tag][()] for tag in ['Z_bins','Z_mids','N','m_i']]
  
  v_pars3_lin,v_pars3_log = np.zeros((2,len(Z_mids),3))
  v_errs3_lin,v_errs3_log = np.zeros((2,len(Z_mids),3))
  v_pars5_lin,v_pars5_log = np.zeros((2,len(Z_mids),5))
  v_errs5_lin,v_errs5_log = np.zeros((2,len(Z_mids),5))
  
  for ii in range(len(Z_mids)):
    if ii % skip != 0:
      continue
    Z_label = r'$z|[%0.4f,%0.4f)$' % (Z_bins[ii],Z_bins[ii+1])
    if printing: 
      print("\nProcessing",Z_label)
    if np.sum(N[ii]) < 10:
      if printing:
        print(f"Low galaxy count ({np.sum(N[ii])}). Skipping...")
        continue
    Z = Z_mids[ii]
    dZ = Z_bins[ii+1] - Z_bins[ii]
    mi_max = get_max_mi(m_i[ii],N[ii],method='kllr') # ~ last safe m_i
    mi,en = [v[ii][m_i[ii] <= mi_max] for v in [m_i,N]] # vet down
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # three parameter fit (good for bright galaxies only)
    init,bounds = guess_pars(Z,dZ,3)
    err = np.sqrt(.25 + en) # semi BaBar errors -> generous at low N
    try: # linear fitting
      pars,covar = curve_fit(lum_func,mi,en,init,err,bounds=bounds)
      v_pars3_lin[ii] = pars # save current parameter set
      v_errs3_lin[ii] = np.sqrt(np.diag(np.abs(covar))) # one sigma errors
      if printing:
        print('linear fit:',pars)
      if plotting: # or ii==len(Z_mids)-2:
        plt.title(Z_label)
        plot_hist(m_i[ii],N[ii],pars)
    except RuntimeError as e:
      if printing:
        print(e)
        print("Skipping linear fit.")
    
    en = flush_right(en) # so we can safely take the log
    err = 1/np.sqrt(en) + .5/en # with second term from Taylor series
    try: # log fitting
      pars,covar = curve_fit(log_lum_func,mi,np.log(en),init,err,bounds=bounds)
      v_pars3_log[ii] = pars
      v_errs3_log[ii] = np.sqrt(np.diag(np.abs(covar))) # one sigma errors
      if printing:
        print('log fit:',pars)
      if plotting: # or ii==len(Z_mids)-2:
        plt.title(Z_label)
        plot_hist(m_i[ii],N[ii],pars)
    except RuntimeError as e:
      if printing:
        print(e)
        print("Skipping log fit.")
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # five parameter fit
    init,bounds = guess_pars(Z,dZ,5)
    mi,en = [v[ii][m_i[ii] <= mi_max] for v in [m_i,N]] # vet down
    err = np.sqrt(.25 + en) # semi BaBar errors -> generous at low N
    try: # linear fitting
      pars,covar = curve_fit(lum_func5,mi,en,init,err,bounds=bounds)
      v_pars5_lin[ii] = pars # save current parameter set
      v_errs5_lin[ii] = np.sqrt(np.diag(np.abs(covar))) # one sigma errors
      if printing:
        print('linear fit:',pars)
      if plotting: # or ii==len(Z_mids)-2:
        plt.title(Z_label)
        plot_hist(m_i[ii],N[ii],pars)
    except RuntimeError as e:
      if printing:
        print(e)
        print("Skipping linear fit.")
    except ValueError as e:
      if printing:
        print(e)
        print('init,bounds:',init,bounds)
        print("Skipping linear fit.")
    
    en = flush_right(en) # so we can safely take the log
    err = 1/np.sqrt(en) + .5/en # with second term from Taylor series
    try: # log fitting
      pars,covar = curve_fit(log_lum_func5,mi,np.log(en),init,err,bounds=bounds)
      v_pars5_log[ii] = pars
      v_errs5_log[ii] = np.sqrt(np.diag(np.abs(covar))) # one sigma errors
      if printing:
        print('log fit:',pars)
      if plotting: # or ii==len(Z_mids)-2:
        plt.title(Z_label)
        plot_hist(m_i[ii],N[ii],pars)
    except RuntimeError as e:
      if printing:
        print(e)
        print("Skipping log fit.")
    except ValueError as e:
      if printing:
        print(e)
        print('init,bounds:',init,bounds)
        print("Skipping linear fit.")
    
    pass
  
  # save results
  with h5py.File(fname_fitting,'w') as ds:
    ds.create_dataset('Z_bins',data=Z_bins)
    ds.create_dataset('Z_mids',data=Z_mids)
    ds.create_dataset('v_pars3_lin',data=v_pars3_lin)
    ds.create_dataset('v_errs3_lin',data=v_errs3_lin)
    ds.create_dataset('v_pars3_log',data=v_pars3_log)
    ds.create_dataset('v_errs3_log',data=v_errs3_log)
    ds.create_dataset('v_pars5_lin',data=v_pars5_lin)
    ds.create_dataset('v_errs5_lin',data=v_errs5_lin)
    ds.create_dataset('v_pars5_log',data=v_pars5_log)
    ds.create_dataset('v_errs5_log',data=v_errs5_log)
    ds.attrs['skip'] = skip
    # ds.create_dataset('min_dist',data=min_dist)
    # for ii in range(5):
    #   lbl = ['phi1','phi2','alpha1','alpha2','m*'][ii]
    #   ds.create_dataset(lbl,data=par_vals[:,ii])
  print("Saved",fname_fitting)


########################################################################
# subservent fitting functions

def fit_phi(fit_order=4):
  with h5py.File(fname_fitting,'r') as ds:
    Z_bins,Z_mids,v_pars3_lin,v_errs3_lin,v_pars3_log,v_errs3_log = [ds[tag][()] \
      for tag in ['Z_bins','Z_mids','v_pars3_lin','v_errs3_lin','v_pars3_log','v_errs3_log']]
    dZ = Z_bins[1:] - Z_bins[:-1]
    mask = (v_pars3_lin[:,0] > 0) * (v_pars3_log[:,0] > 0) # ignore empty regions
  # simplify parameters
  Z,dZ = [v[mask] for v in [Z_mids,dZ]]
  phiN,phiG,errN,errG = [v[mask,0] for v in [v_pars3_lin,v_pars3_log,
                                             v_errs3_lin,v_errs3_log]]
  # plot phi
  plt.scatter(Z,phiN/dZ,2,color='xkcd:lilac')
  plt.fill_between(Z,(phiN-errN)/dZ,(phiN+errN)/dZ,alpha=.25,lw=0,color='xkcd:lilac')
  plt.scatter(Z,phiG/dZ,2,color='g')
  plt.fill_between(Z,(phiG-errG)/dZ,(phiG+errG)/dZ,alpha=.25,lw=0,color='g')
  xlim = plt.xlim(1e-3,2.35)
  # fit phi
  # combine mean & errors with Gaussian weights
  phiC = (phiN/errN**2+phiG/errG**2)/(1/errN**2+1/errG**2)
  errC = np.sqrt(errN**2+errG**2)
  a,b = np.polyfit(np.log(Z[phiC>0]),np.log(phiC[phiC>0]/dZ),1,w=(phiC/errC/dZ)[phiC>0])
  print(f"phi(z)/dZ = {np.exp(b):0.4g} * z**{a:0.4g}")
  zz = 10**np.linspace(-3,np.log10(2.35),10**4)
  plt.plot(zz,np.exp(b) * zz**a,color='xkcd:battleship grey',alpha=.5)
  # more detailed fitting
  A = np.sum(phiC/dZ/Z**2/errC**2)/np.sum(1/errC**2)
  print(f"phi(z)/dZ ~ {A:0.4g} * z**2")
  plt.plot(zz,A*zz**2,'--',color='xkcd:battleship grey',alpha=.5)
  r,re = [v/dZ/Z**2/A for v in [phiC,errC]]
  zf = np.polyfit(Z,np.log(r),fit_order,w=r/re)
  print(f"correction:",zf)
  plt.plot(zz,A*zz**2*np.exp(np.poly1d(zf)(zz)),'--k')
  # set up axes
  plt.xlabel(r'Redshift')
  plt.xscale('log')
  plt.ylabel(r'$\phi / \Delta Z$')
  plt.yscale('log')
  plt.ylim(np.min((phiC/dZ)[phiC>0])/2,np.max(phiC/dZ)*2)
  plt.tight_layout()
  plt.show()
  return Z,phiC,errC


def fit_alpha():
  with h5py.File(fname_fitting,'r') as ds:
    Z_bins,Z_mids,v_pars3_lin,v_errs3_lin,v_pars3_log,v_errs3_log = [ds[tag][()] \
      for tag in ['Z_bins','Z_mids','v_pars3_lin','v_errs3_lin','v_pars3_log','v_errs3_log']]
    mask = (v_pars3_lin[:,0] > 0) * (v_pars3_log[:,0] > 0) # ignore empty regions
  Z = Z_mids[mask]
  alphaN,alphaG,errN,errG = [v[mask,1] for v in [v_pars3_lin,v_pars3_log,
                                                 v_errs3_lin,v_errs3_log]]
  # plot alpha
  plt.scatter(Z,alphaN,2,color='xkcd:lilac')
  plt.fill_between(Z,alphaN-errN,alphaN+errN,alpha=.25,lw=0,color='xkcd:lilac')
  plt.scatter(Z,alphaG,2,color='g')
  plt.fill_between(Z,alphaG-errG,alphaG+errG,alpha=.25,lw=0,color='g')
  xlim = plt.xlim(plt.xlim())
  ylim = plt.ylim()
  ylim = plt.ylim(None,min(0,ylim[1]))
  # fit alpha
  zv = np.append(Z,Z)
  av = np.append(alphaN,alphaG)
  ev = np.append(errN,errG)
  w = 1 / ev**2 # Gaussian weights
  mean = np.sum(av*w)/np.sum(w)
  std = np.sqrt(np.sum(1/w))
  print(f"alpha = {mean:0.4g} +/- {std:0.4g}")
  plt.plot(xlim,mean*np.ones(2),'grey')
  # for n_std in range(6):
  n_std = 5
  plt.fill_between(xlim,mean-n_std*std,mean+n_std*std,alpha=.125,color='k',lw=0)
  plt.xlabel(r'Redshift')
  plt.ylabel(r'$\alpha$')
  plt.tight_layout()
  plt.show()
  return zv,av,ev


def fit_mstar(which=3):
  with h5py.File(fname_fitting,'r') as ds:
    Z_bins,Z_mids,v_pars_lin,v_errs_lin,v_pars_log,v_errs_log = [ds[tag][()] \
      for tag in ['Z_bins','Z_mids',f'v_pars{which}_lin',f'v_errs{which}_lin',
                                    f'v_pars{which}_log',f'v_errs{which}_log']]
    mask = (v_pars_lin[:,0] > 0) * (v_pars_log[:,0] > 0) # ignore empty regions
  # simplify parameters
  Z = Z_mids[mask]
  vN,vG,errN,errG = [v[mask,-1] for v in [v_pars_lin,v_pars_log,
                                         v_errs_lin,v_errs_log]]
  # plot m*
  plt.scatter(Z,vN,2,color='xkcd:lilac')
  plt.scatter(Z,vG,2,color='g')
  xlim = plt.xlim(1e-3,2.35)
  ylim = plt.ylim(plt.ylim())
  plt.fill_between(Z,vN-errN,vN+errN,alpha=.25,lw=0,color='xkcd:lilac') 
  plt.fill_between(Z,vG-errG,vG+errG,alpha=.25,lw=0,color='g') 
  # fit m_star (simple)
  vC = (vN/errN**2+vG/errG**2)/(1/errN**2+1/errG**2)
  eC = np.sqrt(errN**2+errG**2)
  # dumb appending method
  zv = np.append(Z,Z)
  vv = np.append(vN,vG)
  ev = np.append(errN,errG)
  pars,covar = curve_fit(m_star_fit_simple,zv,vv,sigma=ev)
  sigmas = np.sqrt(np.abs(np.diag(covar)))
  N = (np.round(1.5-np.log10(np.abs(sigmas)))).astype(int)
  print(f"m*(z) ~ {pars[0]:0.{N[0]}f} + {pars[1]:0.{N[1]}f}*ln z + {pars[2]:0.{N[2]}f}*z")
  print(f"pars,errs: [{pars[0]:0.{N[0]}f}, {pars[1]:0.{N[1]}f}, {pars[2]:0.{N[2]}f}]," \
                 + f"[{sigmas[0]:0.2g}, {sigmas[1]:0.2g}, {sigmas[2]:0.2g}]")
  zz = np.linspace(xlim[0],xlim[1],10**4)
  plt.plot(zz,m_star_fit_simple(zz,*pars),'gray')
  # mid fitting
  # you can get a pretty great second-order fit using z<.84,
  # but for z>.84, you really need fourth-order. 
  pars,covar = curve_fit(m_star_fit2,zv[zv<.84],vv[zv<.84],sigma=ev[zv<.84])
  sigmas = np.sqrt(np.abs(np.diag(covar)))
  N = (np.round(1.5-np.log10(np.abs(sigmas)))).astype(int)
  print("quadratic model pars,sigmas:",np.round(pars,max(N)),np.round(sigmas,max(N)))
  plt.plot(zz,m_star_fit2(zz,*pars),color='xkcd:tomato')
  # fuller fitting
  pars,covar = curve_fit(m_star_fit4,zv,vv,sigma=ev)
  sigmas = np.sqrt(np.abs(np.diag(covar)))
  N = (np.round(1.5-np.log10(np.abs(sigmas)))).astype(int)
  print("quartic model pars,sigmas:",np.round(pars,max(N)),np.round(sigmas,max(N)))
  plt.plot(zz,m_star_fit4(zz,*pars),color='xkcd:sunflower')
  # touch up axes
  plt.xlabel(r'Redshift')
  plt.xscale('log')
  plt.ylabel(r'$m_*$')
  plt.tight_layout()
  plt.show()
  return zv,vv,ev


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# fitting functions for five-parameter fitting

def fit_phis(which=1,fit_order=4):
  with h5py.File(fname_fitting,'r') as ds:
    Z_bins,Z_mids,v_pars5_lin,v_errs5_lin,v_pars5_log,v_errs5_log = [ds[tag][()] \
      for tag in ['Z_bins','Z_mids','v_pars5_lin','v_errs5_lin','v_pars5_log','v_errs5_log']]
    dZ = Z_bins[1:] - Z_bins[:-1]
    mask = (v_pars5_lin[:,0] > 0) * (v_pars5_log[:,0] > 0) # ignore empty regions
  # simplify parameters
  Z,dZ = [v[mask] for v in [Z_mids,dZ]]
  phiN,phiG,errN,errG = [v[mask,which-1] for v in [v_pars5_lin,v_pars5_log,
                                             v_errs5_lin,v_errs5_log]]
  # combine mean & errors with Gaussian weights
  phiC = (phiN/errN**2+phiG/errG**2)/(1/errN**2+1/errG**2)
  errC = np.sqrt(errN**2+errG**2)
  # plot phi
  plt.scatter(Z,phiN/dZ,2,color='xkcd:lilac')
  plt.fill_between(Z,(phiN-errN)/dZ,(phiN+errN)/dZ,alpha=.25,lw=0,color='xkcd:lilac')
  plt.scatter(Z,phiG/dZ,2,color='g')
  plt.fill_between(Z,(phiG-errG)/dZ,(phiG+errG)/dZ,alpha=.25,lw=0,color='g')
  # fit phi simply, free exponent
  a,b = np.polyfit(np.log(Z[phiC>0]),np.log(phiC[phiC>0]/dZ),1,w=(phiC/errC/dZ)[phiC>0])
  print(f"phi(z)/dZ = {np.exp(b):0.4g} * z**{a:0.4g}")
  zz = 10**np.linspace(-3,np.log10(2.35),10**4)
  plt.plot(zz,np.exp(b) * zz**a,color='xkcd:battleship grey',alpha=.5)
  # fit phi simply, square law
  x,xe = [v/dZ/Z**2 for v in [phiC,errC]]
  A = np.sum((x/xe**2)[Z<.6])/np.sum(1/xe[Z<.6]**2) # simple z^2 rule
  print(f"phi(z)/dZ ~ {A:0.4g} * z**2")
  zz = 10**np.linspace(-3,np.log10(2.35),10**4)
  plt.plot(zz,A*zz**2,'--',color='xkcd:battleship grey',alpha=.5)
  # better fit phi
  # A = np.sum(phiC/dZ/Z**2/errC**2)/np.sum(1/errC**2)
  # print(f"phi(z)/dZ ~ {A:0.4g} * z**2")
  # plt.plot(zz,A*zz**2,'--',color='xkcd:battleship grey',alpha=.5)
  r,re = [v/dZ/Z**2/A for v in [phiC,errC]]
  zf = np.polyfit(Z,np.log(r),fit_order,w=r/re)
  print(f"correction:",np.round(zf,4))
  plt.plot(zz,A*zz**2*np.exp(np.poly1d(zf)(zz)),'--k')
  # set up axes
  plt.xlabel(r'Redshift')
  plt.xscale('log')
  plt.xlim(1e-3,2.35)
  plt.ylabel(r'$\phi / \Delta Z$')
  plt.yscale('log')
  plt.ylim(min(phiC[phiC>0])/2.,2*max(phiC))
  plt.ylim(np.min((phiC/dZ)[phiC>0])/2,np.max(phiC/dZ)*2)
  plt.tight_layout()
  plt.show()
  return Z,phiC,errC


def fit_alphas(which=1):
  with h5py.File(fname_fitting,'r') as ds:
    Z_bins,Z_mids,v_pars5_lin,v_errs5_lin,v_pars5_log,v_errs5_log = [ds[tag][()] \
      for tag in ['Z_bins','Z_mids','v_pars5_lin','v_errs5_lin','v_pars5_log','v_errs5_log']]
    mask = (v_pars5_lin[:,0] > 0) * (v_pars5_log[:,0] > 0) # ignore empty regions
  # simplify parameters
  Z = Z_mids[mask]
  valN,valG,errN,errG = [v[mask,which+1] for v in [v_pars5_lin,v_pars5_log,
                                                   v_errs5_lin,v_errs5_log]]
  # combine mean & errors with Gaussian weights
  valC = (valN/errN**2+valG/errG**2)/(1/errN**2+1/errG**2)
  errC = np.sqrt(errN**2+errG**2)
  # plot alpha
  plt.scatter(Z,valN,2,color='xkcd:lilac')
  plt.fill_between(Z,(valN-errN),(valN+errN),alpha=.25,lw=0,color='xkcd:lilac')
  plt.scatter(Z,valG,2,color='g')
  plt.fill_between(Z,(valG-errG),(valG+errG),alpha=.25,lw=0,color='g')
  # fit alpha (mean)
  mask = (np.abs(valC % 1) > 1e-10) # make sure it's not too close to a bound
  mean = np.sum(valC[mask]/errC[mask]**2)/np.sum(1/errC[mask]**2)
  std = np.sqrt(np.sum(errC[mask]**2))
  print(f"alpha = {mean:0.4g} +/- {std:0.2g}")
  xlim = plt.xlim(0,2.35)
  zz = 10**np.linspace(-3,np.log10(2.35),10**4)
  plt.plot(zz,zz*0+mean,'--',color='xkcd:battleship grey',alpha=.5)
  # fit alpha (linear)
  a,b = np.polyfit(Z[mask],valC[mask],1,w=1/errC[mask])
  print(f"alpha = {b:0.4g} + {a:0.4g} * z")
  plt.plot(zz,zz*a+b,color='k')
  # set up axes
  plt.xlabel(r'Redshift')
  plt.ylabel(r'$\alpha_%g$' % which)
  plt.ylim(-5,.5)
  plt.tight_layout()
  plt.show()
  return Z,valC,errC


def fit_mstars(plot_pars=False):
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # read in file
  with h5py.File(fname_fitting,'r') as ds:
    Z_bins,Z_mids,v_pars3_lin,v_errs3_lin,v_pars3_log,v_errs3_log, \
                  v_pars5_lin,v_errs5_lin,v_pars5_log,v_errs5_log = [ds[tag][()] \
      for tag in ['Z_bins','Z_mids','v_pars3_lin','v_errs3_lin','v_pars3_log','v_errs3_log',
                                    'v_pars5_lin','v_errs5_lin','v_pars5_log','v_errs5_log']]
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # simplify parameters
  Z = Z_mids[:]
  v3N,v3G,e3N,e3G = [v[:,-1] for v in [v_pars3_lin,v_pars3_log,
                                          v_errs3_lin,v_errs3_log]]
  v5N,v5G,e5N,e5G = [v[:,-1] for v in [v_pars5_lin,v_pars5_log,
                                          v_errs5_lin,v_errs5_log]]
  vals = np.array([v3N,v3G,v5N,v5G])
  errs = np.array([e3N,e3G,e5N,e5G])
  labels = ['lin 3','log 3','lin 5','log 5']
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # plot all m* measurements
  plt.figure(1)
  for ii in range(4):
    mask_ii = errs[ii] > 0
    plt.scatter(Z[mask_ii],vals[ii][mask_ii],2,label=labels[ii],color=mpl_colors[ii])
    plt.fill_between(Z[mask_ii],(vals[ii]-errs[ii])[mask_ii],(vals[ii]+errs[ii])[mask_ii],
                     alpha=.25,lw=0,color=mpl_colors[ii])
  # plot combined measurement
  mean = np.sum(vals/errs**2,axis=0)/np.sum(1/errs**2,axis=0)
  std = np.sqrt(np.sum(errs**2,axis=0))
  # plt.plot(Z,mean,'k',alpha=.125)
  plt.fill_between(Z,mean-std,mean+std,color='k',alpha=.125,lw=0)
  # plt.errorbar(Z,mean,std,color='k',capsize=2,fmt='o',ms=5)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # fit m*
  if plot_pars:
    # plot up fits to parameter values (linear)
    _,axs_simp = plt.subplots(1, 3, sharex=True) # , figsize=double_height)
    for ii in range(4):
      mask_ii = errs[ii] > 0
      m,e = [v[ii][mask_ii] for v in [vals,errs]]
      # simple fit
      pars,covar = curve_fit(m_star_fit_simple,Z[mask_ii],m,sigma=e)
      sigmas = np.sqrt(np.abs(np.diag(covar)))
      # print(ii,np.round(pars,4),np.round(sigmas,4))
      for jj in range(len(pars)):
        axs_simp[jj].errorbar(ii,pars[jj],sigmas[jj],capsize=2,color=mpl_colors[ii],fmt='o')
        axs_simp[jj].set_ylabel(m_star_fit_simple.labels[jj])
    # add combined fit
    mask_ii = (std > 0) * (~np.isnan(mean)) * (std < np.inf)
    pars,covar = curve_fit(m_star_fit_simple,Z[mask_ii],mean[mask_ii],sigma=std[mask_ii])  
    sigmas = np.sqrt(np.abs(np.diag(covar)))
    for jj in range(len(pars)):
        axs_simp[jj].errorbar(4,pars[jj],sigmas[jj],capsize=2,color='k',fmt='o')
    plt.tight_layout()
    plt.tight_layout()
    # plot up fits to parameter values (quartic) 
    _,axs_four = plt.subplots(1, 6, sharex=True) # , figsize=double_height)
    for ii in range(4):
      mask_ii = errs[ii] > 0
      m,e = [v[ii][mask_ii] for v in [vals,errs]]
      # 4-par correction
      pars,covar = curve_fit(m_star_fit4,Z[mask_ii],m,sigma=e)
      sigmas = np.sqrt(np.abs(np.diag(covar)))
      # print(ii,np.round(pars,4),np.round(sigmas,4))
      for jj in range(len(pars)):
        axs_four[jj].errorbar(ii,pars[jj],sigmas[jj],capsize=2,color=mpl_colors[ii],fmt='o')
        axs_four[jj].set_ylabel(m_star_fit4.labels[jj])
    # add combined fit
    mask_ii = (std > 0) * (~np.isnan(mean)) * (std < np.inf) * (Z > .02)
    pars,covar = curve_fit(m_star_fit4,Z[mask_ii],mean[mask_ii],sigma=std[mask_ii])  
    sigmas = np.sqrt(np.abs(np.diag(covar)))
    for jj in range(len(pars)):
        axs_four[jj].errorbar(4,pars[jj],sigmas[jj],capsize=2,color='k',fmt='o')
    plt.tight_layout()
    # return to base figure:
    plt.figure(1)
  # touch up axes
  plt.legend()
  plt.xlabel(r'Redshift')
  plt.xscale('log')
  plt.ylabel(r'$m_*$')
  plt.ylim(np.min(vals[:,Z>.01])-1,1+np.max(vals))
  plt.tight_layout()
  plt.show()


if __name__=='__main__':
  # get_hist(dZ=.0025,subset=True,nbins=75)
  # get_hist(dZ=.0025,subset=False,nbins=75)
  # plot_all_hist()
  # fit_hist(skip=3,printing=True) # ,plotting=True)
  fit_phi()
  fit_alpha()
  fit_mstar()
  print('~fin')


"""

par_vals = np.zeros((len(Z_mids),5))
min_dist,N = np.zeros((2,len(Z_mids)))
  
  # use scipy curve_fit
  if 0: # three parameter fit
    initial = [300,-1,20] # guess / starting points
    bounds_lower = [10,-3,10]
    bounds_upper = [1e5,.1,40]
  else: # five par fit
    phi1 = 10**6.6 * dZ * Z_mids[ii]**1.4 # low Z fit: 6e5 * Z_mids[ii]**3
    phi2 = 10**7.25 * dZ * Z_mids[ii]**1.5 # low Z fit: 17e5 * Z_mids[ii]**3
    alpha1 = -1.4 - .6*Z_mids[ii] # .133*Z_mids[ii]**2 - .627*Z_mids[ii] - 1.4
    alpha2 = -.46 # .4*Z_mids[ii]**2 - 1.44*Z_mids[ii] + .25
    Z9 = [ 2.93538791e-01,  4.24159852e-02, -1.65309126e+01,  8.44914891e+01,
       -1.88331895e+02,  2.17268535e+02, -1.31943694e+02,  4.25793955e+01,
       -7.88763326e+00,  4.20660492e-01] # fitting parameters for m*
    m_star = 22.8054 + 2.55972 * np.log(Z_mids[ii]) + np.poly1d(Z9)(Z_mids[ii])
    # 22 + 2.2 * np.log(Z_mids[ii])
    initial = [phi1,phi2,alpha1,alpha2,m_star] # guess / starting points
    bounds_lower = [phi1/1e6,phi2/1000,alpha1-1,-1,m_star-1]
    bounds_upper = [phi1*1000,phi2*1000,-1,alpha2+1.5,m_star+1]
  bounds = np.array([bounds_lower,bounds_upper])

"""
