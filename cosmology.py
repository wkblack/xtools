# Set of cosmology functions 
########################################################################

from astropy.coordinates import SkyCoord
from astropy import units as u

import numpy as np

########################################################################
# constants 
c=299792458/1000. # exact CODATA definition, in km/s
RAD2DEG=180./np.pi # ~60 deg/rad
m200_m500_factor = 0.714 # approximate; from Arya's XTRA

G_astro = 4.30091e-3 # Gravitational constant in pc(km/s)^2/Msol

########################################################################
# cosmologies

aardvark = {
  "H0": 73, # (km/s)/Mpc
  "WL": 0.77,
  "Wm": 0.23,
  "Wr": 0,
  "Wk": 0,
  "wDE": -1.0,
  "Wb": 0.047,
  "sig8" : 0.83,
  "ns" : 1.0,
  "url": "https://www.aanda.org/articles/aa/pdf/2015/04/aa25188-14.pdf",
}

buzzard = {
  "Wm":0.286, 
  "H0":70,
  "sig8":0.82, 
  "ns":0.96, 
  "Wb":0.046,
  "Neff":3.046,
  "url":"https://arxiv.org/abs/1901.02401",
  # from LCDM:
  "Wk":0,
  "Wr":0,
  "WL":1-.286,
  "wDE": -1.0,
}

planck2018 = { 
  "H0": 67.66,      # pm .42 (plus or minus .42)
  "WL": 0.6889,     # pm 0.0056
  "Wm": 0.3166,     # pm 0.0084
  "Wr": 9.23640e-5, # calculated
  "Wk": 0.001,      # pm 0.002
  "wDE": -1.03,     # pm 0.03
  "url": "https://arxiv.org/pdf/1807.06209.pdf"
}

"""
NOTE: Curvature Wk is defined in Dragan's way: -Wk = 1-Wtot
      This means that Wk>0 is positive curvature,
             and thus Wk<0 is negative curvature (while Wk=0 -> flat). 
"""

params = aardvark 

########################################################################
# functions 

# Evolutionary factor 
def E(z,params=aardvark): # input redshift and cosmology
  return np.sqrt(params["Wm"]*(1+z)**3 \
               + params["WL"]*(1+z)**(3*(1+params["wDE"])) \
               + params["Wr"]*(1+z)**4 \
               - params["Wk"]*(1+z)**2)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# critical density

def rho_crit(z=0,params=aardvark):
  # H0 -> km/s)/Mpc; Ga -> pc(km/s)^2/Msol
  # so H0^2/Ga -> Msol/Mpc^2/pc * (10^6 pc/Mpc) -> Msol/Mpc^3
  return 3*(params['H0']*E(z))**2/(8*np.pi*G_astro)*10**6

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# component evolution 

def Wm(z=0,params=aardvark):
  return params["Wm"]*(1+z)**3/E(z,params)**2

def WL(z=0,params=aardvark):
  return params["WL"]*(1+z)**(3*(1+params["wDE"]))/E(z,params)**2

def Wr(z=0,params=aardvark):
  return params["Wr"]*(1+z)**4/E(z,params)**2

def Wk(z=0,params=aardvark):
  return params["Wk"]*(1+z)**2/E(z,params)**2

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# FLRW coordinate, accounting for curvature of space
# used in tranverse comoving distance calculations 

def Sk(r,params=aardvark,tol=10**-5): 
  eta = (params['H0']/c)*np.sqrt(abs(params["Wk"])) # defined for convenience
  if params["Wk"]<0 and eta>tol: 
    return np.sin(eta*r)/eta
  if params["Wk"]>0 and eta>tol: 
    return np.sinh(eta*r)/eta
  else: 
    return r

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# comoving distance
from scipy import integrate

def r(z_final,params=aardvark): 
  # input redshift and cosmology; output comoving distance
  
  # if type(z_final) in {list,tuple,np.ndarray}: # hasattr(z_final,"__len__"):
  if hasattr(z_final,"__len__"):
    z_int = np.array([integrate.quad(lambda z: 1/E(z,params),0,z_i)[0] \
                      for z_i in z_final])
  else: 
    z_int = integrate.quad(lambda z: 1/E(z,params),0,z_final)[0]
  
  xi = np.sqrt(abs(params["Wk"])) # defining this for convenience
  H0inv=c/params["H0"] # Mpc (cosmology specific)
  tol = 10**-5 # tolerance for Wk being near zero
  if xi*np.sign(params["Wk"]) > tol:
    # positive curvature -> sin 
    r = H0inv/xi*np.sin(xi*z_int)
  elif xi*np.sign(params["Wk"]) < -tol:
    # negative curvature -> sinh
    r = H0inv/xi*np.sinh(xi*z_int)
  else: # Wk ~ 0
    # flat universe -> chi itself 
    r = H0inv*z_int
  return r # in Mpc, comoving

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# calculate a comoving volume, based on a solid angle 

sq_deg_in_sky = 4*np.pi*np.degrees(1)**2 # apx 41250 (40k)
sq_rad_in_sky = 4*np.pi

def coVol(z_min=0,z_max=1,Omega=4*np.pi,params=aardvark,per_h=False):
  R_i,R_f=r([z_min,z_max],params)
  if per_h: # return in (cMpc/h)^3
    return (Omega/3.*(R_f**3 - R_i**3))*(params["H0"]/100.)**3
  else:
    return Omega/3.*(R_f**3 - R_i**3) # cMpc^3

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# angular formulae

from numpy import sin as S
from numpy import cos as C

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
# rotations 

def rotate_about_y((phi,theta),beta): # all in radians
  # rotates input theta+phi about the y-axis by angle beta
  # based off of math.stackexchange.com/questions/1019910
  # the vector $a$ is the input, converted to Cartesian coordinates
  # the vector $a$ is the rotated result, which is converted to polar.
  # the resulting (phi,theta) is in radians
  
  ax, ay, az = (S(theta)*C(phi), S(theta)*S(phi), C(theta))
  bx, by, bz = (ax*C(beta)+az*S(beta), ay, az*C(beta)-ax*S(beta))
  return (np.arctan2(by,bx),np.arctan2(np.sqrt(bx**2+by**2),bz)) 

def rotate_to_north_pole((ra_i,dec_i),(ra_halo,dec_halo)): 
  # input: RA+DEC for each point to be rotated, and RA+DEC for center
  # return: (phi,theta) of points rotated s.t. center lies on N. pole
  # convert RA+DEC to theta+phi (radians) and rotate to the north pole:
  return rotate_about_y(((ra_i+180-ra_halo)/RAD2DEG,
                         (90-dec_i)/RAD2DEG),(90-dec_halo)/RAD2DEG)

def rotate_from_pole((phi,theta),(ra,dec)): 
  # rotates points of given phi+theta (rad) down to a given ra+dec (deg)
  # This is the inverse of rotate_to_north_pole() above. 
  alpha,delta = rotate_about_y((phi,theta),-(90-dec)/RAD2DEG)
  ra_i,dec_i = alpha*RAD2DEG+ra-180,-delta*RAD2DEG+90
  try: 
    ra_i[ra_i<0] += 360
  except TypeError: 
    if ra_i < 0: 
      ra_i += 360
  return (ra_i,dec_i)

def halo_points((ra,dec),theta,L=100): 
  # returns ring of L points a given central angle away from (ra,dec)
  # NOTE: all input angles are in degrees!
  phi,theta = np.linspace(0,2*np.pi,L), np.ones(L)*theta/RAD2DEG
  # from matplotlib import pyplot as plt
  # ra_i,dec_i = rotate_from_pole((phi,theta),(ra,dec))
  # plt.scatter(ra_i,dec_i,c=phi)
  # plt.colorbar()
  # plt.show()
  # NOTE: phi=3pi/2 isn't generally the max(ra)! 
  return rotate_from_pole((phi,theta),(ra,dec))

def ra_radius(dec,theta): # inputs all in degrees
  ra_i,dec_i = halo_points((180,dec),theta,L=1000)
  # return (max(ra_i)-min(ra_i))/2. # unneeded if hardcoded to 180 deg
  return max(ra_i)-180 # faster than above

theta_relative=5.
fname_ra_radius='ra_radius_scaled.ls'
def generate_ra_radius_file(N=100):
  # creates calculated relation for ra_radius_scaled from ra_radius
  # errors for radius < 10 deg are below one per cent
  declist = np.linspace(-90,90,N)
  radius = np.array([ra_radius(dec,theta_relative) for dec in declist])
  radius[radius>170]=-1 # to mark full sky coverage
  from pandas import DataFrame
  df = DataFrame({"DEC": declist,"radius": radius/theta_relative})
  df.to_csv(fname_ra_radius,index=False)
  print "Saved",fname_ra_radius

def ra_radius_scaled(dec,theta): # inputs: degrees
  # read from file pre-calculated relation and scale
  from pandas import read_csv
  try: 
    df = read_csv(fname_ra_radius)
  except IOError: 
    print "WARNING: %s does not exist. Creating..." % fname_ra_radius
    generate_ra_radius_file()
    df = read_csv(fname_ra_radius)
  radius = df.radius*theta
  radius[np.logical_or(radius<0,radius>180)]=180 # to mark full sky coverage
  return np.interp(dec,df.DEC,radius)

def square_selection((ra_i,dec_i),(ra_target,dec_target),theta): 
  # mask square patch @ (ra_target,dec_target) w/ radius theta
  
  # ensure RA in [0,360)
  if np.any(ra_i<0):
    ra_ii = np.copy(ra_i)
    ra_ii[ra_ii<0]+=360
  else: 
    ra_ii = ra_i
  
  # grab ra_extent (generally != dec_extent)
  dec_extent = theta; ra_extent = ra_radius(dec_target,theta)
  # make DEC mask
  dec_mask = np.logical_and(dec_i<dec_target+dec_extent,
                            dec_i>dec_target-dec_extent)
  # make RA mask
  if ra_target-ra_extent<0: # account for 0 deg = 360 deg
    # it overlaps from ~1deg onto ~359deg
    # include points with RA>ra_target-ra_extent+360 ~ 359
    ra_mask = np.logical_or(ra_ii<ra_target+ra_extent,
                            ra_ii>ra_target-ra_extent+360)
  elif ra_target+ra_extent>360: # account for 0 deg = 360 deg
    # include points with RA<ra+ra_extent-360 ~ 1
    ra_mask = np.logical_or(ra_ii>ra_target-ra_extent,
                            ra_ii<ra_target+ra_extent-360)
  else: # don't need to account for cycle
    ra_mask = np.logical_and(ra_ii>ra_target-ra_extent,
                             ra_ii<ra_target+ra_extent)
  
  return np.logical_and(ra_mask,dec_mask)

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
# central angles, from great circle distances

def cos_degrees_between((ra1,dec1),(ra2,dec2)): 
  # imports two spherical coordinates (degrees)
  # returns cosine of the "central angle"
  return S(dec1/RAD2DEG)*S(dec2/RAD2DEG) \
       + C(dec1/RAD2DEG)*C(dec2/RAD2DEG) \
       * C((ra1-ra2)/RAD2DEG)

def degrees_between((ra1,dec1),(ra2,dec2)): 
  # imports two spherical coordinates (degrees)
  # returns "central angle" between the two
  return np.arccos(cos_degrees_between((ra1,dec1),(ra2,dec2))) * RAD2DEG

def vet_theta_mask((ra1,dec1),(ra2,dec2),theta): 
  # returns mask to vet by angle
  return degrees_between((ra1,dec1),(ra2,dec2)) < theta

def vet_radius_mask((ra1,dec1),(ra2,dec2),radius,z):
  # returns mask to vet by radius
  return vet_theta_mask((ra1,dec1),(ra2,dec2),to_degrees(radius,z))

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
# plotting rotated coordinates --- this is abandoned for now

def project_to_xy_screen((ra_i,dec_i),(ra_halo,dec_halo)): 
  # returns (X,Y) projections onto a north pole screen distance D away
  phi_i,theta_i = rotate_to_north_pole((ra_i,dec_i),(ra_halo,dec_halo))
  # draw in halo here?
  X,Y = S(theta_i)*np.array([C(phi_i),S(phi_i)])
  
  # Re-scale: 
  scale = (max(ra_i)-ra_halo)/max(-Y)/RAD2DEG
  scale = 1
  print "maxes",scale*max(X),scale*max(Y),scale*min(X),-scale*min(Y) # (max(ra_i)-ra_halo),"bot",max(-Y)
  # maxes 0.00758472658506 0.00419617523585 -0.00540472188955 -0.0057838532508
  return -Y*scale,X*scale
  # return X+ra_halo,Y+dec_halo # TODO: determine why signs are wrong

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
# cosmology conversions between angles and (tangential) extents

def to_degrees(x,z):
  # imports physical distance x (Mpc) + redshift z;
  # returns corresponding theta.
  
  if np.any(z<1e-9): # has problem if z=0
    print "ERROR: ultra low-redshift object would consume sky!"
    raise SystemExit
  
  # NOTE: should technically be Sk(r(z)), but space is nearly flat.
  return x*RAD2DEG*(1+z)/r(z) 

def from_degrees(theta,z): 
  # imports angular extent theta + redshift z; 
  # returns corresponding physical transverse distance. 
  
  if np.any(z<1e-9): # check z > zero
    print "ERROR: ultra low-redshift object would consume sky!"
    raise SystemExit
  
  # NOTE: should technically be Sk(r(z)), but space is nearly flat.
  return theta*r(z)/(1+z)/RAD2DEG # Mpc, physical

if 0: # test limits of to_degrees
  from matplotlib import pyplot as plt
  z=np.array([10.**-i for i in np.arange(0,10)])
  y = to_degrees(1,z)
  plt.scatter(z,y)
  plt.xlabel(r'Redshift $z$'); plt.ylabel(r'$\theta(z)$')
  plt.xscale('log'); plt.yscale('log')
  plt.xlim(min(z),max(z)); plt.ylim(min(y),max(y))
  plt.show() 


def R_lambda(lam,params=aardvark): # input richness and cosmology
  # R_lambda = 1.0(lambda/100)**0.2 Mpc/h
  return (lam/100.)**0.2/(params["H0"]/100.) # Mpc

def theta_R_lambda(lam,z,params=aardvark):
  return to_degrees(R_lambda(lam,params),z)


# NOTE: not sure this is worthwhile or helpful or correct: 
def dx_from_delta_theta(ra_in,dec_in,z_in,ra_cf,dec_cf,z_cf): 
  # neoprogrammics.com/stars/distance_between_two_stars/
  # maybe only works for nearby? (no expansion of space?)
  X1=r(z_in)*C(ra_in/RAD2DEG)*C(dec_in/RAD2DEG)
  Y1=r(z_in)*S(ra_in/RAD2DEG)*C(dec_in/RAD2DEG)
  Z1=r(z_in)*S(dec_in/RAD2DEG)
  X2=r(z_cf)*C(ra_cf/RAD2DEG)*C(dec_cf/RAD2DEG)
  Y2=r(z_cf)*S(ra_cf/RAD2DEG)*C(dec_cf/RAD2DEG)
  Z2=r(z_cf)*S(dec_cf/RAD2DEG)
  return np.sqrt((X2-X1)**2+(Y2-Y1)**2+(Z2-Z1)**2) # comoving Mpc
# TODO: look into using astropy.coordinates.SkyCoord

def dx((ra1,dec1),z1,(ra2,dec2),z2):
  # returns distance between two points on the sky
  c1 = SkyCoord(ra=ra1*u.degree, dec=dec1*u.degree, distance=r(z1)/(1+z1)*u.Mpc, frame='icrs')
  c2 = SkyCoord(ra=ra2*u.degree, dec=dec2*u.degree, distance=r(z2)/(1+z2)*u.Mpc, frame='icrs')
  return c1.separation_3d(c2).value # Mpc


########################################################################
if __name__=='__main__': 
  HID = 11922429
  import galaxy_id as gid
  df = gid.members(HID)
  hdl = gid.halo_data_list(HID)
  ra,dec,z = hdl['ra'],hdl['dec'],hdl['z']
  dx = dx_from_delta_theta(df.TRA.values,df.TDEC.values,df.Z.values,ra,dec,z)
  r = df.RHALO/(1+z) # physical Mpc/h
  from matplotlib import pyplot as plt
  plt.scatter(r,dx,alpha=.25,lw=0)
  from numpy import linspace
  xx = linspace(min(r),max(r),100)
  plt.plot(xx,xx,'k')
  plt.xlabel("RHALO/(1+z)")
  plt.ylabel("dx")
  # plt.xscale('log'); plt.yscale('log')
  plt.show()
  print "~fin"
