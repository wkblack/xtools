" Select a portion of Buzzard Gold sky "
# Chun-Hao says I need: 
# ID, ra, dec, mag_{griz}, magerr_{griz}, flux_{griz}, IVAR_{griz}, Z, Z_cos, CENTRAL
# It would be nice to also have haloid and M200, so I can do a check myself.

import voxel
np,plt,h5py = voxel.np,voxel.plt,voxel.h5py

# keys in catalog/gold
keys1 = ['coadd_object_id', 'dec', 'flags_gold', 
         'flux_g', 'flux_i', 'flux_r', 'flux_z', 
         'haloid', 'hpix_16384', 'm200', 
         'ivar_g', 'ivar_i', 'ivar_r', 'ivar_z', 
         'mag_err_g', 'mag_err_i', 'mag_err_r', 'mag_err_z', 
         'mag_g', 'mag_g_lensed', 'mag_g_true', 
         'mag_i', 'mag_i_lensed', 'mag_i_true', 
         'mag_r', 'mag_r_lensed', 'mag_r_true', 
         'mag_z', 'mag_z_lensed', 'mag_z_true', 
         'px', 'py', 'pz', 
         'r200', 'ra', 'rhalo', 'sdss_sedid', 
         'vx', 'vy', 'vz']
keys2 = ['redshift_cos']

all_keys = ['catalog/gold/' + tag for tag in ['coadd_object_id','ra','dec',
            'mag_g_true','mag_r_true','mag_i_true','mag_z_true',
            'mag_err_g','mag_err_r','mag_err_i','mag_err_z',
            'flux_g', 'flux_i', 'flux_r', 'flux_z',
            'ivar_g', 'ivar_i', 'ivar_r', 'ivar_z']]
all_keys += ['catalog/bpz/unsheared/' + tag for tag in ['z','redshift_cos']]
all_keys += ['catalog/gold/' + tag for tag in ['rhalo','haloid','m200']]

all_keys_out = ['ID','ra','dec',
                'mag_g','mag_r','mag_i','mag_z',
                'magerr_g','magerr_r','magerr_i','magerr_z',
                'flux_g', 'flux_i', 'flux_r', 'flux_z',
                'IVAR_g', 'IVAR_i', 'IVAR_r', 'IVAR_z',
                'Z','Z_cos','CENTRAL','haloid','M200']

# read in ra & dec to get subsection
ra,dec = [voxel.f['catalog/gold/' + tag][()] for tag in ['ra','dec']]

# grab an ~500 square degree portion of the sky
print("creating mask")
mask = lowerleft = (ra>335)*(dec<-30)
print("mask created")

if 0:
  # test with a much smaller portion
  all_fields = [voxel.f[key][:10**4] for key in all_keys]
  outname = 'test_patch.h5'
  description = "collection of 10^4 galaxies pulled from Buzzard"
elif 1: 
  # test with a larger mask
  all_fields = [voxel.f[key][10**6:4*10**6] for key in all_keys]
  outname = 'larger_test_patch.h5'
  description = "collection of 3*10^6 galaxies pulled from Buzzard"
else: 
  # use the mask
  all_fields = [voxel.f[key][mask] for key in all_keys]
  out_dir = '/global/cscratch1/sd/wkblack/SkyMaps/buzzard/paintsplash/'
  outname = out_dir + 'patch_base.h5'
  description = "~500 sq deg patch of sky created by W.K.Black, pulled from Buzzard"

# TODO: change rhalo==0 to CENTER=1
idx_rhalo = np.where(np.array(all_keys)=='catalog/gold/rhalo')[0][0]
all_fields[idx_rhalo] = np.array(all_fields[idx_rhalo])==0 # select CENTRAL gals

print("Saving %s" % outname) 
with h5py.File(outname,'w') as ds:
  for ii,key in enumerate(all_keys_out):
    ds.create_dataset(key,data=all_fields[ii])
  ds.attrs['notes'] = description

elapsed_minutes = voxel.time()-voxel.start_time
print("Analysis took %g min" % elapsed_minutes)

print('~fin')

