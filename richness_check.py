#!/bin/python
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# check whether richness is accurate in Arya's dataset
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
from os.path import isfile
import config as cfg

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# cf input: 
fname_input = 'RM_training_fixed.csv' 
path_input = cfg.xtools + fname_input
assert(isfile(path_input))

# to the RM catalogue at: 
if 0: # old, FALSE catalogue: 
  RM = cfg.skymaps + 'RM/'
  fname_RM = 'redmapper_dr8_public_v6.3_catalog.fits'
else: # new, true catalogue?
  RM = cfg.skymaps + 'RM3/'
  fname_RM = 'aardvark-1.0-dr8_run_redmapper_v6.3.3_lgt20_catalog.fit'

path_RM = RM + fname_RM
assert(isfile(path_RM))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# grab the input data: 
from numpy import loadtxt

fields = ('CLUSTERID','M_HALOID1','M_HALOID2','M_HALOID3','M_HALOID4',
          'M_HALOIDN','STR1','STR2','STR3','STR4','STRO','STR_EXP',
          'LAMBDA','Z','CENTRAL_FLAG','CENTRAL_BCG_FLAG','RA','DEC',
          'R_LAMBDA','LAMBDA_ERR','M_1','M_2')
fmts = ('i','i','i','i','i','i','f','f','f','f','f',
        'f','f','f', 'i','i','f','f','f','f','f','f')
fmt_str = '%i,%i,%i,%i,%i,%i,%g,%g,%g,%g,%g,%g,%g,%g,%i,%i,%g,%g,%g,%g,%g,%g'
if 0: # new way
  fmts = ('i','i','i','i','i','i','f','f','f','f','f','f','f','f',
          'i','i','f','f','f','f','f','f')
  dataType={'names':fields,'formats':fmts}
  data_in = loadtxt(path_input,delimiter=',',dtype=dataType)
  cid_in = data_in['CLUSTERID']
  lam_in = data_in['LAMBDA']
else: # old way
  data_in = loadtxt(path_input,delimiter=',')
  [CLUSTERID,M_HALOID1,M_HALOID2,M_HALOID3,M_HALOID4,M_HALOIDN,
   STR1,STR2,STR3,STR4,STRO,STR_EXP,LAMBDA,Z,CENTRAL_FLAG,
   CENTRAL_BCG_FLAG,RA,DEC,R_LAMBDA,LAMBDA_ERR,M_1,M_2] = range(22)
  cid_in = data_in[:,CLUSTERID].astype(int)
  lam_in = data_in[:,LAMBDA]
  lam_in_err = data_in[:,LAMBDA_ERR]

# grab the RM catalogue
from astropy.io.fits import open as fitsopen
hdulist = fitsopen(path_RM)
data_RM = hdulist[1].data
# new: ['MEM_MATCH_ID', 'RA', 'DEC', 'MODEL_MAG', 'MODEL_MAGERR', 'IMAG', 'IMAG_ERR', 'ZRED', 'ZRED_E', 'ZRED_CHISQ', 'BCG_SPEC_Z', 'Z_SPEC_INIT', 'Z_INIT', 'Z', 'LAMBDA_CHISQ', 'LAMBDA_CHISQ_E', 'LAMBDA_ZRED', 'LAMBDA_ZRED_E', 'R_LAMBDA', 'SCALEVAL', 'MASKFRAC', 'C_LAMBDA', 'C_LAMBDA_ERR', 'MAG_LAMBDA_ERR', 'CHISQ', 'Z_LAMBDA', 'Z_LAMBDA_E', 'Z_LAMBDA_NITER', 'EBV_MEAN', 'LNLAMLIKE', 'LNBCGLIKE', 'LNLIKE', 'PZBINS', 'PZ', 'NCROSS', 'RMASK', 'RA_ORIG', 'DEC_ORIG', 'W', 'DLAMBDA_DZ', 'DLAMBDA_DZ2', 'DLAMBDAVAR_DZ', 'DLAMBDAVAR_DZ2', 'LAMBDA_CHISQ_C', 'LAMBDA_CHISQ_CE', 'NCENT', 'NCENT_GOOD', 'RA_CENT', 'DEC_CENT', 'ID_CENT', 'LAMBDA_CHISQ_CENT', 'ZLAMBDA_CENT', 'P_BCG', 'P_CEN', 'Q_CEN', 'P_FG', 'Q_MISS', 'P_SAT', 'P_C', 'BCG_ILUM', 'ILUM', 'Z_LAMBDA_RAW', 'Z_LAMBDA_E_RAW', 'LIM_EXPTIME', 'LIM_LIMMAG', 'LIM_LIMMAG_HARD']
cid_RM = data_RM['MEM_MATCH_ID']
lam_RM = data_RM['LAMBDA_CHISQ']
lam_RM_err = data_RM['LAMBDA_CHISQ_E'] # richness error (found that out in https://core.ac.uk/download/pdf/143477438.pdf)
# z_lambda_RM = data_RM['Z_LAMBDA']
# z_spec_RM = data_RM['Z_SPEC']
z_lambda_RM = data_RM['Z_LAMBDA']
z_spec_RM = data_RM['Z_SPEC_INIT']

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# compare richness values
import numpy as np
from numpy import array

if 0: 
  print "CID, Z (input), Z (spec), Z (lambda), lambda (input), lambda (RM):"
  N=15 # maximum check times
  for i in range(N): 
    # grab a cluster from input and compare lambdas
    # print cid_in[i],lam_in[i],lam_RM[cid_RM==cid_in[i]]
    mask = np.array(cid_RM==cid_in[i])
    # print 'mask=',mask
    # cf redshift
    if sum(mask)>=1: 
      print cid_in[i],data_in[i,Z],\
            z_spec_RM[mask][0],z_lambda_RM[mask][0],\
            lam_in[i],lam_RM[mask][0]
  
  # print 'minimum richness from training catalogue:',min(lam_in)
  # print 'minimum richness from cluster catalogue:',min(lam_RM)
  # from matplotlib import pyplot as plt
  # plt.scatter(lam_in,lam_RM)
  # plt.show() 

# If the richness values don't equate, we've got a problem. 
# I'd need to update bicycle.py to read in the RM catalogue
# to update all the values in the output csv. 


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# write out corrected file

# get new lambda list and mask for input set
mask = array([len(lam_RM[cid_RM==cid]) for cid in cid_in])>0
lambda_new = [lam_RM[cid_RM==cid][0] for cid in cid_in[mask]]
lambda_err_new = [lam_RM_err[cid_RM==cid][0] for cid in cid_in[mask]]
# print 'new lambda values:',lambda_new[:10]

# output file
fname_out = 'RM_training_corrected.csv'
path_output = cfg.xtools + fname_out
from numpy import savetxt

data_out = data_in[mask,:]
data_out[:,LAMBDA] = lambda_new
data_out[:,LAMBDA_ERR] = lambda_err_new

z_mask = np.logical_and(0.1<=data_out[:,Z],data_out[:,Z]<=0.3)
lam_mask = 20<=data_out[:,LAMBDA]
dat_mask = np.logical_and(z_mask,lam_mask)
data_out = data_out[dat_mask,:]

# print 'new data output lambda values:',data_out[:10,LAMBDA]

savetxt(fname_out,data_out,delimiter=',',
        comments='',header=','.join(fields),fmt=fmt_str)

hdulist.close()
print "~fin"





# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
if 0: # plot drift
  lambda_old = lam_in[mask]
  m1_in = data_in[:,M_1]
  m1_out = data_out[:,M_1]
  
  from matplotlib import pyplot as plt
  x1 = lambda_old
  x2 = lambda_new
  y1 = m1_in[mask]
  y2 = m1_out
  plt.title('Richness Drift')
  plt.quiver(x1,y1,x2-x1,y2-y1,(x2-x1),# cmap='coolwarm_r',
             angles='xy',scale_units='xy',scale=1)
  plt.xscale('log') 
  plt.xlabel(r'Richness $\lambda$')
  plt.xlim(15,300)
  plt.yscale('log') 
  plt.ylabel(r'$M_{{\rm VIR}}$')
  # plt.ylim(5.5e12,2e15)
  plt.show() 
if 0: # plot new M vs lam
  from matplotlib import pyplot as plt
  m1 = data_out[:,M_1]
  plt.scatter(lambda_new,m1,c='r',label='new',
              marker='.',s=1,alpha=0.5)
  # plt.scatter(lam_in[mask],data_in[mask,M_1],c='b',label='old',
              # marker='.',s=1,alpha=0.5)
  # plt.legend()
  
  plt.xscale('log') 
  plt.xlabel(r'Richness $\lambda$')
  plt.yscale('log') 
  plt.ylabel(r'$M_{{\rm VIR}}$')
  
  plt.show() 
