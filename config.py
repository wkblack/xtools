from glob import glob
host = 'NERSC'

########################################################################
# Universals
########################################################################

Aardvark = {
  "name":"Aardvark",
  # by default, have undefined location
  "exists":False,
  # redMaPPer file defaults
  "rm_dir":'',
  "clusters":None,
  "rm_catalog":'aardvark-1.0-dr8_run_redmapper_v6.3.3_lgt20_' \
             + 'catalog.fit',
  "members":None,
  "rm_members":'aardvark-1.0-dr8_run_redmapper_v6.3.3_lgt20_' \
             + 'catalog_members.fit',
  'rm_url':'des.ncsa.illinois.edu/releases/sva1/docs/docs-redmapper',
  # galaxy file defaults
  "galaxies":None,
  "glxy_dir":'',
  "glxy_fname":'Aardvark_v1.0c_truth_des_rotated.*.fit',
  "glxy_ftemp":'Aardvark_v1.0c_truth_des_rotated.%i.fit',
  "glxy_nest":False,
  "glxy_count":204,
  "pixels_dir":'',
  # other galaxy defaults
  "snips":'',
  "squares_dir":'',
  "blush_dir":'',
  # halo file defaults
  "halos":None,
  "halo_dir":'',
  "halo_fname":'Aardvark_v1.0_halos_r1_rotated.*.fit',
  "halo_ftemp":'Aardvark_v1.0_halos_r1_rotated.%i.fit',
  "halo_nest":False,
  "halo_count":15,
  # details
  "nest":False,
  "url":'slac.stanford.edu/~risa/des_bcc/aardvark_v1.0.html',
  "url_tags":'slac.stanford.edu/~risa/des_bcc/bcc_v1.0_tags.html'
}

Buzzard = {
  "name":"Buzzard",
  # by default, have undefined location
  "exists":False,
  # redMaPPer file defaults
  "rm_dir":'',
  "clusters":None,
  "rm_catalog":'y1a1_gold_1.0.3-d10-mof-001d_run_' \
             + 'redmapper_v6.4.17_lgt5_desformat_catalog.fit',
  "members":None,
  "rm_members":'y1a1_gold_1.0.3-d10-mof-001d_run_' \
             + 'redmapper_v6.4.17_lgt5_desformat_catalog_members.fit',
  # galaxy file defaults
  "galaxies":None,
  "glxy_dir":'',
  "glxy_fname":'Chinchilla-3_lensed_*', # generalized glob format
  "glxy_ftemp":'Chinchilla-3_lensed_rs_scat_cam.%i.fits', # template
  "glxy_nest":True,
  "glxy_count":204,
  "pixels_dir":'',
  # other galaxy defaults
  "snips":'',
  "squares_dir":'',
  "blush_dir":'',
  # halo file defaults
  "halos":None,
  "halo_dir":'',
  "halo_fname":'Chinchilla-3_halos.*.fits',
  "halo_ftemp":'Chinchilla-3_halos.%i.fits',
  "halo_nest":True,
  "halo_count":15,
  # details
  "nest":True,
  "url":'https://arxiv.org/abs/1901.02401',
}

# file directories
skymaps=''
xtools='' # this directory

# TODO: overwrite need for these!
halos = None
galaxies = None

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# default filenames
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

fname_galaxy_snips = 'galaxies_of_HID%i.csv'
fname_halo_sightline = 'galaxies_along_HID%i.csv'
fname_halo_R20 = 'galaxies_in_R20_of_HID%i.csv'
fname_galaxy_blush = 'blush_fR%g_pix%%i.fits'

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

mastercat = '/project/projectdirs/des/jderose/Chinchilla/Herd/' \
          + 'Chinchilla-3/v1.9.8/sampleselection/Y3a/mastercat/' \
          + 'Buzzard-3_v1.9.8_Y3a_mastercat.h5'
  # contains:
  ## Buzzard_v1.9.8_Y3a_bpz.h5  
  ## Buzzard_v1.9.8_Y3a_gold.h5  
  ## Buzzard_v2.9.8_Y3a_shape.h5  
  ## Y3_GOLD_2_2_1_maps_copy.h5  
  ## buzzard-1.9.8_3y3a_run_redmapper_v6.4.22.h5


########################################################################
# System specifics
########################################################################

if host=='NERSC':
  xtools = '/global/homes/w/wkblack/xproject/xtools/'
  xtra = '/global/homes/w/wkblack/xproject/XTRA/'
  skymaps = '/global/cscratch1/sd/wkblack/SkyMaps/'
  BCC = '/global/cscratch1/sd/jderose/BCC/'
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # Aardvark setup
  aardvark = skymaps + 'aardvark_v1.0/'
  Aardvark["exists"] = True
  Aardvark["dir"] = aardvark
  Aardvark["glxy_dir"] = aardvark + 'truth/'
  Aardvark["halo_dir"] = aardvark + 'halos/'
  Aardvark["squares_dir"] = aardvark + 'squares/'
  Aardvark["blush_dir"] = aardvark + 'blush/'
  Aardvark["snips"] = aardvark + 'galaxy_HID_snips/'
  Aardvark["rm_dir"] = skymaps + 'RM/'
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # Buzzard setup
  buzzard = BCC + 'Chinchilla/Herd/Chinchilla-3/addgalspostprocess/'
  Buzzard["exists"] = True
  Buzzard["dir"] = buzzard
  Buzzard["glxy_dir"] = buzzard + 'truth/'
  Buzzard["halo_dir"] = buzzard + 'halos/'
  Buzzard["rm_dir"] = skymaps + 'buzzard/'
  Buzzard["blush_dir"] = buzzard + 'blush/'
  Buzzard["pixels_dir"] = skymaps + 'buzzard/pixels/'
  
elif host=='Flux':
  xtools = '/home/wkblack/projects/xproject/xtools/'
  skymaps = '/home/wkblack/projects/xproject/SkyCatalogues/'
  xtra = '/home/wkblack/projects/xproject/XTRA/'
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # Aardvark setup 
  halos = skymaps + 'Aardvark/'
  Aardvark['exists'] = True
  Aardvark['halo_dir'] = skymaps + 'Aardvark/'
                         # or '/nfs/phys1/aryaf/aardvark_v1.0/halos/'
  Aardvark['rm_dir'] = skymaps + 'RM3/'
  Aardvark['snips'] = skymaps + 'galaxy_snips/'
  
elif host=='GreatLakes':
  skymaps = '/home/wkblack/SkyMaps/'
  
  Aardvark['exists'] = True
  Aardvark['rm_dir'] = skymaps + 'rm/'
  Aardvark["glxy_dir"] = skymaps + 'truth/'
  
  Buzzard['exists'] = False
else: 
  print "ERROR: Unrecognized host! Exiting."
  raise SystemExit

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# grab files

for sim in [Aardvark,Buzzard]:
  if sim['exists']:
    # set default names
    sim['halo_names']=sim['halo_dir']+sim['halo_fname']
    sim['glxy_names']=sim['glxy_dir']+sim['glxy_fname']
    sim['halo_temp']=sim['halo_dir']+sim['halo_ftemp']
    sim['glxy_temp']=sim['glxy_dir']+sim['glxy_ftemp']
    # forge filenames
    sim["clusters"] = sim["rm_dir"] + sim["rm_catalog"]
    sim["members"]  = sim["rm_dir"] + sim["rm_members"]
    sim["galaxies"] = glob(sim["glxy_names"])
    sim["halos"]    = glob(sim["halo_names"])
    
    # check halo files
    if len(sim["halos"]) < 1:
      print "Warning: %s halo files nonexistant! [config.py]" % sim['name']
    elif len(sim["halos"]) < sim["halo_count"]: 
      print "Warning: %s halo files incomplete! [config.py]" % sim['name']
  
    if len(sim["galaxies"]) < 1:
      print "Warning: %s galaxy files nonexistant! [config.py]" % sim['name']
    elif len(sim["galaxies"]) < sim["glxy_count"]: 
      print "Warning: %s galaxy files incomplete! [config.py]" % sim['name']
  else:
    print "%s not loaded on this system." % sim['name']


########################################################################
# debugging functions
########################################################################

import inspect

def lineNo():
    """Returns the current line number in our program."""
    return inspect.currentframe().f_back.f_lineno

########################################################################
# regex beauty

import numpy as np
import re
# from stackoverflow.com/questions/4703390
numeric_const_pattern = r"""
   [-+]? # optional sign
   (?:
       (?: \d* \. \d+ ) # .1 .12 .123 etc 9.1 etc 98.1 etc
       |
       (?: \d+ \.? ) # 1. 12. 123. etc 1 12 123 etc
   )
   # followed by optional exponent part if desired
   (?: [Ee] [+-]? \d+ ) ?
"""
rx = re.compile(numeric_const_pattern, re.VERBOSE)

def find_floats(string):
  return np.array(rx.findall(string)).astype(float)

"""
floats: [-+]?(\d+(\.\d*)?|\.\d+)([eE][-+]?\d+)?
from docs.python.org/3/library/re.html#simulating-scanf
"""

########################################################################
# future global variables

# ...
