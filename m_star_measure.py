import h5py
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit
import numpy as np
h = .7 # Hubble constant

########################################################################
# read in dataset

print("Reading in dataset...") 
if 0: # use low-Z dataset
  fname = '/global/cscratch1/sd/wkblack/SkyMaps/buzzard/vet/subset_low_Z.h5'
  
  with h5py.File(fname,'r') as ds:
    M = ds['m200'][:]/h # Msol
    px,py,pz = [ds[loc][:] for loc in ['px','py','pz']] # cMpc
    CEN = ds['rhalo'][:] == 0
    ID = ds['haloid'][:]
    Rvir = ds['r200'][:] # cMpc
    g,r,i,z = [ds['mag_'+c][:] for c in ['g','r','i','z']]
    Z = ds['redshift_cos'][:]
  
  Z_bins = np.arange(min(Z),max(Z),.01) # .3,.31,.001)
else: # use full dataset
  fname = '/project/projectdirs/des/jderose/Chinchilla/Herd/Chinchilla-3/v2.0.0/sampleselection/Y3a/Buzzard-3_v2.0_Y3a_mastercat.h5'
  loc_Z = 'catalog/bpz/unsheared/redshift_cos'
  loc_i = 'catalog/gold/mag_i_true'
  
  with h5py.File(fname,'r') as ds:
    Z = ds[loc_Z][:]
    i = ds[loc_i][:]
  
  # Z_bins = np.concatenate([np.arange(0,.01999,.005),np.arange(.02,.32,.001)]) 
  # Z_bins = np.arange(.32,.84+.099,.01)
  # Z_bins = np.arange(.84,2.35+.01,.01)
  Z_bins = np.arange(0,2.35+.003-1e-10,.003)

print("Dataset processed.") 

########################################################################
# measure m_* in a thin redshift slice

def lum_func(m_i,phistar=2.75e2,alpha=-1.25,mstar=18.85):
  return .4 * np.log(10) * phistar * 10**(.4*(alpha+1)*(mstar-m_i)) \
            * np.exp(-10**(.4*(mstar-m_i)))

def log_lum_func(m_i,phistar1,phistar2,alpha1,alpha2,mstar):
  return np.log(.4 * np.log(10) * np.exp(-10**(.4*(mstar-m_i))) \
            * (phistar1 * 10**(.4*(alpha1+1)*(mstar-m_i)) \
             + phistar2 * 10**(.4*(alpha2+1)*(mstar-m_i))))

Z_mids = (Z_bins[:-1]+Z_bins[1:])/2.
par_vals = np.zeros((len(Z_mids),5))
min_dist,N = np.zeros((2,len(Z_mids)))

for ii in range(len(Z_bins)-1):
  Z_label = r'$z|[%0.3f,%0.3f)$' % (Z_bins[ii],Z_bins[ii+1])
  dZ = Z_bins[ii+1] - Z_bins[ii]
  print(f"Analyzing {Z_label}")
  mask_Z = (Z_bins[ii] <= Z) * (Z < Z_bins[ii+1])
  i_ii = i[mask_Z]
  if len(i_ii) < 10:
    print(f"ERROR: Insufficient data ({len(i_ii)}); skipping.")
    # note enough data to analyze
    continue
  min_i = min(i_ii) + .2
  max_i = max(i_ii) - 2. * (1-Z_mids[ii]/2.35) # allow fainter magnitudes at high Z
  h_bins = np.linspace(min_i,max_i,100)
  n,b = np.histogram(i_ii,h_bins)
  b_mids = (b[1:]+b[:-1])/2.
  N[ii] = np.sum(n) # total galaxies in this bin
  
  # use scipy curve_fit
  if 0: # three parameter fit
    initial = [300,-1,20] # guess / starting points
    bounds_lower = [10,-3,10]
    bounds_upper = [1e5,.1,40]
  else: # five par fit
    phi1 = 10**6.6 * dZ * Z_mids[ii]**1.4 # low Z fit: 6e5 * Z_mids[ii]**3
    phi2 = 10**7.25 * dZ * Z_mids[ii]**1.5 # low Z fit: 17e5 * Z_mids[ii]**3
    alpha1 = -1.4 - .6*Z_mids[ii] # .133*Z_mids[ii]**2 - .627*Z_mids[ii] - 1.4
    alpha2 = -.46 # .4*Z_mids[ii]**2 - 1.44*Z_mids[ii] + .25
    Z9 = [ 2.93538791e-01,  4.24159852e-02, -1.65309126e+01,  8.44914891e+01,
       -1.88331895e+02,  2.17268535e+02, -1.31943694e+02,  4.25793955e+01,
       -7.88763326e+00,  4.20660492e-01] # fitting parameters for m*
    m_star = 22.8054 + 2.55972 * np.log(Z_mids[ii]) + np.poly1d(Z9)(Z_mids[ii])
    # 22 + 2.2 * np.log(Z_mids[ii])
    initial = [phi1,phi2,alpha1,alpha2,m_star] # guess / starting points
    bounds_lower = [phi1/1e6,phi2/1000,alpha1-1,-1,m_star-1]
    bounds_upper = [phi1*1000,phi2*1000,-1,alpha2+1.5,m_star+1]
  bounds = np.array([bounds_lower,bounds_upper])
  # fit and plot
  try:
    err = - np.log(1 - 1/np.sqrt(n[n>1]))
    pars,covar = curve_fit(log_lum_func,b_mids[n>1],np.log(n[n>1]),initial,err,bounds=bounds)
  except RuntimeError as e:
    print(e)
    # probably because "Optimal parameters not found"; abort and save.
    print("... breaking to end") 
    plt.scatter(b_mids,n,10,color='k')
    plt.plot(b,np.exp(log_lum_func(b,*initial)),'cyan')
    ylim = np.array(plt.ylim(plt.ylim()))
    plt.plot(np.ones(2)*m_star,ylim,'cyan',alpha=.5)
    plt.xlabel(r'$m_i$')
    plt.ylabel(r'count')
    plt.title("Attempted Fit")
    plt.tight_layout()
    plt.show()
    break
  dist = np.minimum(np.abs(pars-bounds[0]),np.abs(pars-bounds[1]))
  min_dist[ii] = np.min(dist)
  print('guess:',initial)
  print('pars:',pars)
  print('dist:',dist)
  print()
  
  par_vals[ii] = pars
  # m_star_vals[ii] = pars[2]
  
  if 0: # check plots
    plt.title(Z_label)
    # plot histogram and freeze vertical axis
    plt.scatter(b_mids,n,10,color='k')
    plt.yscale('log')
    ylim = np.array(plt.ylim(min(n[n>0])/1.1,1.1*max(n)))
    # plot fit
    i_vals = np.linspace(min_i,max_i,1000)
    plt.plot(i_vals,np.exp(log_lum_func(i_vals,*pars)),'r')
    plt.plot(i_vals,np.exp(log_lum_func(i_vals,*initial)),'cyan',alpha=.5)
    plt.plot(np.ones(2)*pars[-1],ylim,'r')
    plt.plot(np.ones(2)*m_star,ylim,'cyan',alpha=.5)
    plt.xlabel(r'$m_i$')
    plt.ylabel(r'count')
    plt.tight_layout()
    plt.show()

########################################################################
# save output

fname = 'm_fit_pars.npy'
np.save(fname,par_vals)
print("Saved",fname)


fname_out = 'm_fit_pars.h5'
with h5py.File(fname_out,'w') as ds:
  ds.create_dataset('Z_bins',data=Z_bins)
  ds.create_dataset('Z_mids',data=Z_mids)
  ds.create_dataset('min_dist',data=min_dist)
  ds.create_dataset('pars',data=par_vals)
  for ii in range(5):
    lbl = ['phi1','phi2','alpha1','alpha2','m*'][ii]
    ds.create_dataset(lbl,data=par_vals[:,ii])
print("Saved",fname_out)


if 0:
  # plot parameter evolution
  plt.scatter(Z_mids,par_vals[:,-1],2)
  plt.xlabel('Redshift')
  plt.ylabel(r'$m_*$')
  plt.tight_layout()
  plt.show()


