
import numpy as np

def g(z):
  return 0.206-0.371*z

def h(z):
  if np.any(z<3.6/25.8):
    print "ERROR: invalid redshift input"
  return -3.6+25.8*z

def f_R(N200,z):
  # scipy.special.erf
  return g(z)*np.erf(np.log10(N200)-np.log10(h(z)))+0.69

if __name__=='__main__':
  N200 = np.linspace(1,100,300)
  from matplotlib import pyplot as plt
  
  if 1: # test z=.1
    print 'h',h(.1)
    print 'g',g(.1)
    # print np.log10(N200)
    print np.log10(h(.1))
  
  for z in np.arange(.1,.35,.05):
    fR = f_R(N200,z)
    plt.plot(N200,fR,label="z=%g" % z)
  plt.xlabel(r'$N_{200}$')
  plt.ylabel(r'$f_R$')
  plt.xscale('log')
  plt.legend()
  plt.show() 
