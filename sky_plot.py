#!/bin/python
# created 14 May 2019 by W.K.Black
"""
TODO: 

functions to create: 
 * create 2d plotter
 * create "from fits" containers for the 2d and 3d plotters, 
   which reads in the fits file and calls the appropriate function

functionality to implement: 
 * more realistic scale: i.e.: using scale=R500, shoud look ~ reality
 * colorschemes based on mass

"""


import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from cosmology import r
from cosmology import to_degrees

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def axisEqual3D(ax):
    extents = np.array([getattr(ax, 'get_{}lim'.format(dim))() for dim in 'xyz'])
    sz = extents[:,1] - extents[:,0]
    centers = np.mean(extents, axis=1)
    maxsize = max(abs(sz))
    r = maxsize/2
    for ctr, dim in zip(centers, 'xyz'):
        getattr(ax, 'set_{}lim'.format(dim))(ctr - r, ctr + r)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def draw_sphere(ax,center=(0,0,0),radius=1,
                color="black",alpha=0.5,n_poly=5):
  u = np.linspace(0, 2 * np.pi, n_poly)
  v = np.linspace(0, np.pi, n_poly)
  x = 1 * np.outer(np.cos(u), np.sin(v))
  y = 1 * np.outer(np.sin(u), np.sin(v))
  z = 1 * np.outer(np.ones(np.size(u)), np.cos(v))
  ax.plot_surface(radius*x-center[0], 
                  radius*y-center[1], 
                  radius*z-center[2],  
                  # rstride=4, cstride=4, linewidth=0,
                  color=color, alpha=alpha)
  # I don't know what the r/c stride variables are... 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
null = np.array([-1])
def skyplot(z,RA,DEC,scale=null,coloring=null,colormap='coolwarm',
            title=False,plotsize=1,plot_all=False,cone=False,rescale=False):
  # read in redshift, right ascension, declination (degrees)
  
  # derived parameters
  ra_median = np.median(RA)
  dec_median = np.median(DEC)
  ra_shift = RA-ra_median
  dec_shift = DEC-dec_median
  
  # coloring
  if len(coloring)==1 and coloring[0]==-1: 
    coloring=z
  # else, use that variable to color
  
  if cone: 
    # rescale the ra and dec to be the x and y coords
    z = z # TODO: this would require rescaling z to comoving distance! 
    x = z*np.tan(ra_shift/180)
    y = z*np.tan(dec_shift/180)
    # TODO: change the x/y/z labels
  elif 0: 
    # keep ra and dec as their own coords (no cone) 
    x = ra_shift
    y = dec_shift
  else: 
    x = RA
    y = DEC
  
  if len(scale)==1 and scale[0]==-1: # if it's null, make all ones
    scale = np.ones(len(z))
  elif rescale: # rescale the scale to mean and sigma
    mean = 70
    sigma = 50
    MEAN = np.mean(scale) 
    SIGMA = np.std(scale)
    rescale = (scale-MEAN)/SIGMA*sigma+mean
    rescale[rescale<1]=rescale[rescale<1]/rescale[rescale<1]
    scale = rescale
  elif plot_all: # scale the radius up (~sky view)
    scale = 250*scale**4
    # scale = 1000*scale**2
  
  cf = plt.figure() 
  ax = Axes3D(cf)
  # scatter with approximate sizing
  if plot_all: 
    ax.scatter(z,x,y,s=scale,c=coloring,cmap=colormap) #,zdir='x')
  else: # draw individual spheres
    from sys import stdout
    rad_to_deg=180./np.pi
    theta = scale*rad_to_deg*(1+z)/r(z)
    print 'max :',max(theta)
    for i in range(len(z)): 
      print "\033[K" + "Drawing sphere %i/%i\r" % (i+1,len(z)),
      stdout.flush()
      # theta = scale[i]*(1+z[i])/r(z[i])
      # print "\033[K",scale[i],theta
      if theta[i]>.02: 
        draw_sphere(ax,(z[i],x[i],y[i]),theta[i])
    print "Finished drawing..."
  
  ax.set_xlabel(r'Redshift $z$',labelpad=10)
  if 1: # reverse redshift direction: 
    left,right = plt.xlim()
    ax.set_xlim(right,left)
    if 0: ax.set_xlim(max(z),min(z)) # messes things up...
  if 1: # set ra & dec lims: 
    # TODO: filter out points beyond these limits
    ax.set_ylim(-plotsize/2.0,plotsize/2.0)
    ax.set_zlim(-plotsize/2.0,plotsize/2.0)
  
  ax.set_ylabel(r'RA',labelpad=20)
  ax.set_zlabel(r'DEC',labelpad=20)
  
  ax.xaxis.set_major_locator(plt.MaxNLocator(5))
  ax.yaxis.set_major_locator(plt.MaxNLocator(5))
  ax.zaxis.set_major_locator(plt.MaxNLocator(5))
  
  ax.tick_params(axis='y', which='major', pad=10)
  ax.tick_params(axis='z', which='major', pad=10)
  
  ax.view_init(elev=45,azim=-90) #+5)
  if not title: 
    plt.title(r'RA=%g, DEC=%g' % (ra_median,dec_median))
  else: 
    plt.title(title)
  
  if 1: axisEqual3D(ax)
  plt.show() 
  

def fits_skyplot(fname): 
  # read in fits file, grab relevant data, and call the plotter
  from astropy.io import fits as pyfits
  h = pyfits.open(fname)[1].data[::]
  from numpy import log
  title = "(RA,DEC) = (%g,%g)" % (ra,dec)
  skyplot(h['z'],h['RA'],h['DEC'],scale=h['R500'],
          coloring=log(h['M500']),colormap='plasma',title=title)


########################################################################
if __name__=='__main__': 
  from sys import argv
  if len(argv)>1: 
    fname = argv[1] # path to fits file to plot 
    title = ""
  else: # test on premeditated slice
    import config as cfg
    # ra,dec = 34.672,-26.2455
    ra,dec = 6.76311,3.08443
    path = '/Catalog/Maps/Surface_Brightness/'
    template = 'sightseeing_RA%g_DEC%g_[0.5-2.0].fit'
    fname = cfg.xtra + path + template % (ra, dec)
    title = "(RA,DEC) = (%g,%g)" % (ra,dec)
  
  if 0: # test with random data
    from numpy import random as npr
    # set up data
    N=150
    z = npr.uniform(.1,.3,N)
    ra = npr.uniform(25,30,N)
    dec = npr.uniform(-30,-35,N)
    scale = npr.normal(500,100,N)
    mass = npr.normal(1e5,4e4,N)
    
    # skyplot(z,ra,dec,scale,mass,colormap='inferno')
    skyplot(z,ra,dec,scale)
    # skyplot(z,ra,dec)
    
  else: # test with actual slice
    from astropy.io import fits as pyfits
    try: 
      h = pyfits.open(fname)[1].data[::]
    except IOError: 
      print "ERROR: file %s does not exist! Exiting..."
      raise SystemExit 
    
    from numpy import log
    
    skyplot(h['z'],h['RA'],h['DEC'],scale=h['R500'],
            coloring=log(h['M500']),colormap='bone_r', # 'plasma'
            title=title,plot_all=False)
    
    
  print r(.2)
  print 'fin~'
  raise SystemExit
