#!/bin/python
# inspect most massive dataset, primarily based on radius cuts
import galaxy_id as gid

HID = 11922429
df = gid.members(HID)
hdl = gid.halo_data_list(HID)

df_20 = gid.vet_by_delta(df,hdl,20)
df_200 = gid.vet_by_delta(df,hdl,200)
df_500 = gid.vet_by_delta(df,hdl,500)
df_2500 = gid.vet_by_delta(df,hdl,2500)

initial_plots=False
if initial_plots: 
  gid.color_plot(df_20,hdl)
  gid.color_plot(df_200,hdl)
  gid.color_plot(df_500,hdl)
  gid.color_plot(df_2500,hdl)

triptych=False
for lim in [24,22,20]:
  dv_20 = gid.vet_by_r(df_20,r_limit=lim)
  print "R20 r<%g Ngal:" % lim, len(dv_20)
  if triptych: 
    gid.plot_shifted(dv_20)
  
  dv_200 = gid.vet_by_r(df_200,r_limit=lim)
  print "R200 r<%g Ngal:" % lim, len(dv_200)
  if triptych: 
    gid.plot_shifted(dv_200)
  else: 
    gid.radial_profile(dv_200,hdl,steps_per_decade=10)
  
  dv_500 = gid.vet_by_r(df_500,r_limit=lim)
  print "R500 r<%g Ngal:" % lim, len(dv_500)
  if triptych: 
    gid.plot_shifted(dv_500)
  
  dv_2500 = gid.vet_by_r(df_2500,r_limit=lim)
  print "R2500 r<%g Ngal:" % lim, len(dv_2500)
  if triptych: 
    gid.plot_shifted(dv_2500)

print '~fin'
