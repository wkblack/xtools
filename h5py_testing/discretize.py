#!/bin/python
# read in mastercat: catalogue from Buzzard simulation
# output numpy file stacking the galaxy colors into discretized bins
from m_star_model import m_star_cutoff as cutoff
from matplotlib import pyplot as plt
from time import time
from sys import argv

import numpy as np
import config
import h5py


########################################################################
# set up some global variables 

lim_red_low,lim_red_high = .05,.7 # redshift bounds
red_break = .375 # redshift where 4000A break moves into r-band
fname_vet = "vetted.h5"

fname_out = "stacked.npy"
fname_out_rmi = "stacked_rmi.npy"
f_template = "divided/stacked_Ndiv{:03d}_ii{:03d}.npy"
f_template_rmi = "divided/stacked_rmi_Ndiv{:03d}_ii{:03d}.npy"

########################################################################
# set up bins

# bin up by redshift
dz = .01
z_bins = np.arange(.05,.7+dz,dz)

# m_i bins
dmi = .0625
m_star_max = 23.1 # this is pretty generous, since cutoff(.75)<22.65
mi_bins = np.arange(13.5,m_star_max+dz,dmi)

# (g-r) bins
dgmr = .04
gmr_bins = np.arange(-.25,2.5+dz,dgmr)

# m_z bins?
# it's messier than m_i, so maybe I won't do it.....
# I think keeping it as m_i is best, 
# unless I figure out how to get m* for m_z. 

# (r-i) bins
# should range from about .25 to 1.6 to get the 1.5 sigma datapoints. 
# more liberal range: 0 to 1.8

########################################################################
# set up output file template

""" Structure: [redshift bin,ival bin, gmr bin]
    If I were to do it, I could have a high break going from .35 to .7,
    which would contain m_z mag vs r-i color, but that's more than I need. 
""" 
CM_report_template = np.zeros((len(z_bins),len(mi_bins),len(gmr_bins)))



def vet_mastercat(fname_in = config.mastercat, N=10**7):
  f = h5py.File(fname_in,'r')
  Ntot = f['catalog/gold/mag_g'].len()
  print(f'total galaxy count: {Ntot}')
  if N<0: N=Ntot # to capture all galaxies
  
  ######################################################################
  # grab variables from that version
  start_time = time()
  
  if N<Ntot: 
    print(f'Warning: using only {N}/{Ntot} galaxies') 
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # vet by redshift
  print("Vetting by redshift...")
  redshift = f['catalog/bpz/unsheared/redshift_cos'][:N]
  mask_red = (lim_red_low<=redshift)*(redshift<=lim_red_high)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # vet by 0.2L_*
  print("Trimming by luminosity...")
  m_i = f['catalog/gold/mag_i'][:N][mask_red]
  # calculate m* for each redshift
  mask_m_i = m_i<m_star_max
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # vet by 10^13 Msol/h
  print("Vetting by mass...")
  m200 = f['catalog/gold/m200'][:N][mask_red][mask_m_i]
  mask_mass = m200 > 10**13
  m200 = m200[mask_mass]
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # update magnitudes thus far
  m_i = m_i[mask_m_i][mask_mass]
  m_g = f['catalog/gold/mag_g'][:N][mask_red][mask_m_i][mask_mass]
  m_r = f['catalog/gold/mag_r'][:N][mask_red][mask_m_i][mask_mass]
  m_z = f['catalog/gold/mag_z'][:N][mask_red][mask_m_i][mask_mass]
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # vet, demanding magnitudes nonzero and finite
  print("Cleaning up magnitudes...")
  mask_mag = (0<m_i)*(m_i<99)
  for band in [m_g,m_r,m_z]:
    mask_mag *= (0<band)*(band<99)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # vet by 0.2L* for each individual galaxy 
  print("Vetting by 0.2L* for each galaxy") 
  # update redshift 
  redshift = redshift[mask_red][mask_m_i][mask_mass][mask_mag]
  mi_vals = cutoff(redshift)
  mask_Lstar = m_i[mask_mag]<mi_vals
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # update all values to most current mask
  
  redshift = redshift[mask_Lstar]
  m_g = m_g[mask_mag][mask_Lstar]
  m_r = m_r[mask_mag][mask_Lstar]
  m_i = m_i[mask_mag][mask_Lstar]
  m_z = m_z[mask_mag][mask_Lstar]
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # Double-check everything is working correctly: 
  LEN = len(redshift)
  assert(LEN==len(m_g))
  assert(LEN==len(m_r))
  assert(LEN==len(m_i))
  assert(LEN==len(m_z))
  
  elapsed = time()-start_time
  print(f"Created and vetted variables successfully ({elapsed:.03g}s).") 
  print(f"Remaining galaxies: {LEN}")
  
  ######################################################################
  # structure h5 file and save
  with h5py.File(fname_vet, 'w') as h5f:
    h5f.create_dataset('redshift', data=redshift)
    h5f.create_dataset('m_g', data=m_g)
    h5f.create_dataset('m_r', data=m_r)
    h5f.create_dataset('m_i', data=m_i)
    h5f.create_dataset('m_z', data=m_z)
  
  print(f"Saved {fname_vet}")
  return fname_vet # return filename to be loaded later



########################################################################
# stack data, discretizing the continuous into pixels in CM space

def stack_data(fname_in = fname_vet, printing=True, divisions=False, bool_rmi=False):
  """
  returns numpy array, giving counts in bins of
  [redshift,m_i,gmr or rmi]
  containing counts for each voxel
  """
  ######################################################################
  # read in data, save composite variables
  if printing: 
    print("Reading in data")
  
  with h5py.File(fname_in, 'r') as h5f:
    Z = h5f['redshift'][()]
    m_g = h5f['m_g'][()]
    m_r = h5f['m_r'][()]
    m_i = h5f['m_i'][()]
    m_z = h5f['m_z'][()]
  L = len(Z)
  
  if divisions==False:
    fname_save = fname_out if not bool_rmi else fname_out_rmi
  if divisions!=False:
    Ndiv,ii = divisions
    N_per_div = np.ceil(L/Ndiv).astype(int)
    ii_start = ii * N_per_div
    ii_end = ii_start + N_per_div
    fname_save = f_template if not bool_rmi else f_template_rmi
    fname_save = fname_save.format(Ndiv,ii)
    print(f"Save name: {fname_save}")
    
    # cut down the original inputs now
    Z = Z[ii_start:ii_end]
    L = len(Z)
    m_g = m_g[ii_start:ii_end]
    m_r = m_r[ii_start:ii_end] 
    m_i = m_i[ii_start:ii_end]
    m_z = m_z[ii_start:ii_end]
    print(f"Cut down to L={L}")
  
  # create secondary variables 
  gmr = m_g-m_r
  rmi = m_r-m_i
  if bool_rmi:
    gmr=rmi
  
  report = np.copy(CM_report_template)
  
  ######################################################################
  # set up boolean masks to iterate through
  if printing: 
    print("Creating boolean masks")
  
  subset_redshift = np.zeros((len(z_bins)-1,L))
  for i in range(len(z_bins)-1):
    subset_redshift[i] = np.logical_and(z_bins[i]<=Z,Z<z_bins[i+1])
  
  subset_mi = np.zeros((len(mi_bins)-1,L))
  for i in range(len(mi_bins)-1):
    subset_mi[i] = np.logical_and(mi_bins[i]<=m_i,m_i<mi_bins[i+1])
  
  subset_gmr = np.zeros((len(gmr_bins)-1,L))
  for i in range(len(gmr_bins)-1):
    subset_gmr[i] = np.logical_and(gmr_bins[i]<=gmr,gmr<gmr_bins[i+1])
  
  ######################################################################
  # outter product of boolean masks 
  for i in range(len(z_bins)-1):
    if printing:
      print("in z-bin [{:.2g},{:.2g})".format(z_bins[i],z_bins[i+1]))
    for j in range(len(mi_bins)-1):
      for k in range(len(gmr_bins)-1):
        report[i,j,k] += len(Z[(subset_redshift[i]*subset_mi[j]\
                                   *subset_gmr[k]).astype(bool)])
  np.save(fname_save,report)
  return report


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def combine_ds(Ndiv,max_val=-1,bool_rmi=False):
  ft = f_template_rmi if bool_rmi else f_template
  f_out = fname_out_rmi if bool_rmi else fname_out
  
  if max_val == -1: max_val = Ndiv
  ds = np.load(ft.format(Ndiv,0))
  for kk in range(1,max_val):
    ds += np.load(ft.format(Ndiv,kk))
  if max_val == Ndiv: 
    print('saving',f_out)
    np.save(f_out,ds)
  return ds


########################################################################
if __name__=='__main__':
  if 0: 
    print("Vetting mastercat")
    vet_mastercat(N=-1)
  
  if 0: 
    print("Stacking data")
    sample = 'vetted_sample.h5'
    if len(argv)<2:
      stack_data(bool_rmi=True)
    else:
      stack_data(divisions=(int(argv[1]),int(argv[2])),bool_rmi=True)

  if 1:
    print("Compiling stacked data")
    combine_ds(10,bool_rmi=True)

