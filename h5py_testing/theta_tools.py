# a fitting for the CM GMM+ parameter set 
# including masking for the ES0 break
import numpy as np
from m_star_model import m_star_cutoff

########################################################################
# parameter fits across a large redshift range
# Notes on a few key redshifts: 
#   z=.32 is where the simulations split (see Buzzard paper, Table 1)
#   z=.375 is where the 4kAA break leaves g-band
#   z={.25,.425,.5,.55} are also essentially shared in the fitting here

gmr_fit = { # updated as of 24 June 2020
  "Z_range":[.05,.25,.32,.375,.42,.49,.555],
  "params":['fR','aR','bR','lgsR','bB','lgsB'],
  "alpha_fR":[.50088,.4062011,None,None,None,None],
  "beta_fR":[.013783,.2795483,None,None,None,None],
  "alpha_aR":[-0.0097023,None,None,-0.07221,None,0.218296],
  "beta_aR":[-0.039855,None,None,.12390,None,-0.46444],
  "alpha_bR":[.62908,None,None,1.54183,None,2.29746],
  "beta_bR":[2.819327,None,None,.3954965,None,-1.13449],
  "alpha_lgsR":[-1.45719,None,None,-1.27156,None,-1.8925],
  "beta_lgsR":[1.39073,None,None,.92537555,None,2.215417],
  "alpha_bB":[.504486,None,.65958,None,1.25014,2.0109755],
  "beta_bB":[1.93804,None,1.37707,None,-.046046,-1.59966],
  "alpha_lgsB":[-0.847663,-0.72003,-0.51972,None,-0.6700786,None],
  "beta_lgsB":[.715859,.211631,-0.44496974,None,-0.07917,None]
}

rmi_fit = { # updated as of 23 June 2020
  "Z_range":[.05,.25,.33,.425,.5,.65,.7],
  "params":['fR','aR','bR','lgsR','bB','lgsB'],
  "alpha_fR":[.46282,None,None,None,.0584261,None],
  "beta_fR":[-.16188,None,None,None,.6101668,None],
  "alpha_aR":[-0.00966,0.01261,-0.01057,None,0,None],
  "beta_aR":[.03105,-.06133,.00305656,None,0,None],
  "alpha_bR":[.302401,None,None,-0.28655,None,.346424],
  "beta_bR":[.6965,None,None,2.077905,None,1.094378],
  "alpha_lgsR":[-1.5308,-2.0542,-1.64700,-1.877,-2.4224,None],
  "beta_lgsR":[-.24984,1.9157,.703732,1.2489,2.362750,None],
  "alpha_bB":[.3528,None,.35290,-0.055626,None,None],
  "beta_bB":[.144807,None,.144807,1.1172938,None,None],
  "alpha_lgsB":[-1.2003,-1.5767,-1.13381,None,-0.819638,None],
  "beta_lgsB":[.41882,1.9468,.612568,None,.0153955,None]
}

########################################################################
# vectorized fitting for theta over different redshifts


def gen_theta(Z,col='r-i',warnings=True):
  " generate theta values for a given (set of) redshift(s) "
  if col=='r-i':
    pars = rmi_fit
  elif col=='g-r':
    pars = gmr_fit
  else:
    print("ERROR: unset color")
    raise SystemExit
  
  # warn if extrapolating
  Zr = pars['Z_range']
  mask = np.logical_or(Z<Zr[0],Z>Zr[-1])
  if np.sum(mask)>0 and warnings:
    print("WARNING: Extrapolating!")
  
  # create report
  N_param = len(pars['params']) # number of parameters
  if hasattr(Z,'__len__'): # handle a series of redshifts
    report = np.zeros((N_param,len(Z)))
    indices = np.zeros(len(Z))
    masks = [Z>=z_i for z_i in Zr[:-1]] # set of masks; check if above
    masks[0] = True
    for idx,mask in enumerate(masks):
      indices[mask] = idx # if it's above that value, increment index
    
    for i,z_i in enumerate(Zr[:-1]):
      alpha_par = np.array([pars['alpha_'+tag][i] for tag in pars['params']])
      beta_par = np.array([pars['beta_'+tag][i] for tag in pars['params']])
      mask_par = alpha_par!=None
      
      for row in np.arange(N_param)[mask_par]:
        report[row][masks[i]] = alpha_par[row] + beta_par[row]*Z[masks[i]]
  else: # handle a single redshift
    report = np.zeros(N_param)
    # determine index of parameters to use
    if Z<Zr[1]: # use first values
      index = 0
    elif Z>Zr[-2]: # use last values
      index = len(Zr)-2
    else: # use middle values
      index = np.where([(Zr[i]<=Z)*(Z<Zr[i+1]) for i in range(len(Zr)-1)])[0][0]
    
    for i,z_i in enumerate(Zr[:index+1]):
      alpha_par = np.array([pars['alpha_'+tag][i] for tag in pars['params']])
      beta_par = np.array([pars['beta_'+tag][i] for tag in pars['params']])
      mask_par = alpha_par!=None
      report[mask_par] = alpha_par[mask_par] + beta_par[mask_par]*Z
  
  return report


def cm_line(Z,m_i,col='r-i',warnings=True):
  """
  cm_line(Z,m_i,col='g-r',warnings=True)
  return color line and width of red sequence
  """
  fR,aR,bR,lgsR,bB,lgsB = gen_theta(Z,col,warnings)
  return aR*(m_i-m_star_cutoff(Z)) + bR, 10**lgsR


def ES0_mask(Z,m_g,m_r,m_i,col='r-i',warnings=True):
  """
  ES0_mask(Z,m_g,m_r,m_i,col='r-i',warnings=True):
  input redshift (Z), colors {m_g,m_r,m_i}, & selection method (col)
  outputs mask, returning galaxies marked as red
  """
  c_line,wth = cm_line(Z,m_i,col,warnings)
  if col=='g-r':
    C = m_g-m_r
  elif col=='r-i':
    C = m_r-m_i
  else:
    print("ERROR: unset color")
  # Note: 2*sigma is too large with high redshift (r-i), otherwise good
  mask_under = C > c_line - 2*wth
  mask_above = C < c_line + 2*wth
  return np.logical_and(mask_under,mask_above)



if __name__=='__main__':
  # plot up (r-i) parameter fittings
  Z = np.linspace(.3,.5,20)
  Th = gen_theta(Z)
  from matplotlib import pyplot as plt
  plt.plot(Z,np.transpose(Th))
  plt.legend(rmi_fit['params'])
  plt.xlabel(r'Redshift')
  plt.ylabel(r'$(r-i)$ parameter values')
  plt.show()

