import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from m_star_model import m_star_cutoff as mi_max

from discretize import dz,z_bins,dmi,mi_bins,dgmr,gmr_bins
extents = [min(mi_bins),max(mi_bins),min(gmr_bins),max(gmr_bins)]
titular = r'$z|[%g,%g)$'

def cm_from_ds(ds,i=0,theta=None,tpcy=.2,cmap='bone_r',
               display=True,save=False,bool_rmi=False): 
  # ds as input dataset
  # i as redshift bin
  if i<0: # assume vetted by redshift already
    im = plt.imshow(np.transpose(ds[:,::-1]),extent=extents,cmap=cmap)
    # no title available without additional input
  else: 
    im = plt.imshow(np.transpose(ds[i,:,::-1]),extent=extents,cmap=cmap)
    Z = z_bins[i]+dz/2.
    plt.title(titular % (z_bins[i],z_bins[i+1]))
  plt.xlabel(r'$m_i$')
  ylabel = r'$r-i$' if bool_rmi else r'$g-r$' 
  plt.ylabel(ylabel)
  plt.xlim(extents[0],extents[1])
  plt.ylim(extents[2],extents[3])
  
  if (hasattr(theta,'__len__') or theta!=None) and i>=0:
    fR,aR,bR,lgsR,bB,lgsB = theta # lg = log_10
    # draw on 1-sigma & 2-sigma bounds
    mi_fits = np.linspace(extents[0],extents[1],10)
    max_i = mi_max(Z)
    # draw the red sequence
    red_line = (mi_fits-max_i) * aR + bR
    plt.fill_between(mi_fits,red_line - 2*10.**lgsR,
                             red_line + 2*10.**lgsR,
                     color='r',alpha=tpcy,linewidth=0)
    plt.fill_between(mi_fits,red_line - 10.**lgsR,
                             red_line + 10.**lgsR,
                     color='r',alpha=tpcy,linewidth=0)
    # draw the blue cloud
    blue_line = (mi_fits-max_i) * 0 + bB
    plt.fill_between(mi_fits,blue_line - 10.**lgsB,
                             blue_line + 10.**lgsB,
                     color='b',alpha=tpcy/2,linewidth=0)
    plt.fill_between(mi_fits,blue_line - 2*10.**lgsB,
                             blue_line + 2*10.**lgsB,
                     color='b',alpha=tpcy/2,linewidth=0)
  
  # create colorbar
  divider = make_axes_locatable(plt.gca())
  cax = divider.append_axes("right", size="2%", pad=0.05)
  plt.colorbar(im, cax=cax, label=r'Galaxy Count $N$')
  
  fig = plt.gcf()
  fig.subplots_adjust(top=1,bottom=0,left=.071,right=.916)
  fig.set_size_inches(8.25,3)
  
  if save:
    plt.savefig('slices/%02i.png' % i)
  if display:
    plt.show()
  else:
    plt.clf()

from matplotlib import cm
colormap = 'coolwarm'
cw = cm.get_cmap(colormap)
mini,maxi=min(z_bins),max(z_bins)
colors = cw((z_bins-mini)/(maxi-mini))

def hist_from_ds(ds,bool_rmi=False): # for all redshift bins
  plt.figure(figsize=(10,5))
  
  for i in np.flip(range(len(z_bins)-1)):
    plt.subplot(121)
    plt.plot(mi_bins,np.sum(ds[i],axis=1),
             color=colors[i],alpha=.75)
    plt.xlabel(r'$m_i$')
    plt.ylabel(r'$N$')
    plt.yscale('log')
    
    plt.subplot(122)
    plt.plot(gmr_bins,np.sum(ds[i],axis=0),
             color=colors[i],alpha=.75)
    if bool_rmi:
      plt.xlabel(r'$r-i$')
    else:
      plt.xlabel(r'$g-r$')
    plt.ylabel(r'$N$')
    plt.yscale('log')
  
  plt.tight_layout()
  plt.savefig('out/hist.pdf',format='pdf',dpi=1000)
  plt.show()

if __name__=='__main__':
  ii = 0
  from discretize import f_template 
  fname = f_template.format(10,ii)
  ds = np.load(fname)
  print("shape ds: {}".format(np.shape(ds)))
  
  from gmm_base import generate_theta as gTheta
  for i in range(15,len(z_bins)-1):
    cm_from_ds(ds,i,gTheta((z_bins[i]+z_bins[i+1])/2.))
  
  mi_ds = np.array([[mi_bins[i] for j in range(len(gmr_bins))]
                                for i in range(len(mi_bins))])
  gmr_ds = np.array([[gmr_bins[j] for j in range(len(gmr_bins))]
                                  for i in range(len(mi_bins))])
  coords = np.array([[(mi_bins[i],gmr_bins[j]) for j in range(len(gmr_bins))]
                                               for i in range(len(mi_bins))])
  print("shape mi_ds: {}".format(np.shape(mi_ds)))
  print("shape gmr_ds: {}".format(np.shape(gmr_ds)))
  print("shape coords: {}".format(np.shape(coords)))
  
  val = ds[0]*mi_ds*gmr_ds
  print('test: {}'.format(np.shape(val),np.sum(val)))
