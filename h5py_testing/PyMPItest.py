#!/usr/bin/env python
"""
run this program by executing (e.g.): 
srun -n 4 -c 1 PyMPItest.py
"""
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

print(f"Rank {rank} of {size}")
