# figure out general idea for the (r-i) vs Z relation for Bz v1.9.8

########################################################################
# imports
from matplotlib import pyplot as plt
from matplotlib import cm
import numpy as np
import h5py

########################################################################
# globals
fname = 'vetted.h5'

with h5py.File(fname,'r') as ds:
  m_g = ds['m_g'][()]
  m_r = ds['m_r'][()]
  m_i = ds['m_i'][()]
  m_z = ds['m_z'][()]
  Z = ds['redshift'][()]

rmi = m_r-m_i


########################################################################
# params from following function:
m = 2.03571
b = 0.23006

def max_hist(min_z=.375,max_z=.7,dz=.02,plotting=True):
  " plot up color evolution with redshift for Z>.375 the color (r-i) "
  cw = cm.get_cmap('coolwarm')
  z_vals = np.arange(min_z,max_z,dz)
  colors = cw((z_vals-min(z_vals))/(max(z_vals)-min(z_vals)))
  max_vals = np.zeros(np.shape(z_vals))
  for ii,z_ii in enumerate(z_vals):
    n,bins,patches = plt.hist(rmi[(z_ii<Z)*(Z<z_ii+dz)],alpha=.25,
                              bins=np.linspace(0,2,41),density=True,
                              log=True,color=colors[ii])
    jj = np.where(max(n)==n)[0]
    max_vals[ii] = np.mean([bins[jj],bins[jj+1]])
  
  if plotting:
    plt.xlabel(r'$(r-i)$')
    plt.ylabel(r'counts')
    plt.yscale('log')
    
    plt.tight_layout()
    plt.show()
  else:
    plt.clf()
  
  return z_vals,max_vals

import linmix

def plot_max(z_vals,max_vals):
  plt.scatter(z_vals,max_vals)
  mask = (.46<z_vals)*(z_vals<.66)
  m,b = np.polyfit(z_vals[mask],max_vals[mask],1)
  plt.plot(z_vals,m*z_vals+b)
  plt.title(r'$(r-i)_{\rm max} = %g Z %+g$' % (m,b))
  plt.xlabel(r'Redshift')
  plt.ylabel(r'$(r-i)_{\rm max}$')
  plt.show()

if __name__=='__main__':
  print("In main...")
  z_vals,max_vals = max_hist(plotting=False)
  plot_max(z_vals,max_vals)
