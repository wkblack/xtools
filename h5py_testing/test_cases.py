# test_cases.py
"""
Runs several test cases for the gmm:
0) Standard test
1) high fR (.999)
2) low fR (.25)
3) wide gap
4) positive aR slope
5) red sequence = blue cloud + offset
TODO? crossing red sequence

Prints distance from input values to calculated fits in
 * "MCMC sigmas"
   The MCMC finds a fit for mean value and std dev of that mean 
   this gives how many of those sigma the fit is from the input
 * "Data sigmas"
   This compares how far the fit is from the input in terms of 
   the apx error bars on the actual data (more generous than above)
If the data sigmas are on order unity, we start having problems.
"""

import numpy as np
import gmm_base
import plot_cm
import emcee

########################################################################

def check_theta(ds,jj,theta_in,Z_in):
  # how to do min/max_gmr?
  nwalkers, ndim = 32,len(theta_in)
  walkers = theta_in + gmm_base.error_sizes * np.random.randn(nwalkers,ndim)
  walkers[:,0] = np.clip(walkers[:,0],0.05,1) # fix fR|[0,1]
  sampler = emcee.EnsembleSampler(nwalkers,ndim,gmm_base.ln_prob,
                                  args=(ds[jj],Z_in))
  nsteps = 500
  sampler.run_mcmc(walkers,nsteps,progress=True)
  
  # check visually it's close
  samples = sampler.get_chain()
  theta_fit = [np.quantile(samples[-50:,:,ll],.5) for ll in range(ndim)]
  gmm_base.plot_chain_progression(samples,gmm_base.labels,theta_in)
  plot_cm.cm_from_ds(ds,i=jj,theta=theta_fit)
  
  # check numerically it's close
  flat_samples = sampler.get_chain(discard=400, flat=True) #thin=15
  errs = gmm_base.errors(flat_samples)
  # errs has structure [variable,[median/sigma+1/sigma-1]]
  print('\n')
  for kk in range(6): # for each variable
    med,sp1,sm1 = errs[kk]
    # report distance from actual
    dmu = med-theta_in[kk] # offset from actual
    sig = (sp1+sm1)/2. # mean std dev
    std_dev_away = dmu/sig # mean count of std dev away from true mean
    if np.abs(std_dev_away)>2:
      print("deviant value for {}:".format(gmm_base.labels[kk]))
      print(f"MCMC sigmas: {std_dev_away}")
      simulated_dev_away = dmu/gmm_base.error_sizes[kk]
      print(f"Data sigmas: {simulated_dev_away}")
      if simulated_dev_away>1: # if it's a big deal
        return 1
  print('~fin\n')
  return 0

########################################################################

if 1: # base theta values to work off of
  fR = .5
  aR = -.03
  bR = 1.2
  lgsR = np.log10(.05)
  bB = .5
  lgsB = np.log10(.25)
  theta0 = np.array([fR,aR,bR,lgsR,bB,lgsB])

if 1: # base redshift and count values to use 
  Z = .2
  ii_Z = 15
  N = 10**5 # 75400 in actual

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# cases run without arguments
# plot_cm.cm_from_ds(ds,ii_Z)

def case0():
  theta = np.copy(theta0) # [fR,aR,bR,lgsR,bB,lgsB]
  ds = gmm_base.generate_ds(theta,Z,N)
  print("#"*72)
  print("Standard test")
  return check_theta(ds,ii_Z,theta,Z)

def case1(): 
  theta = np.copy(theta0) # [fR,aR,bR,lgsR,bB,lgsB]
  theta[0]=.999
  ds = gmm_base.generate_ds(theta,Z,N)
  print("#"*72)
  print("Testing high fR")
  return check_theta(ds,ii_Z,theta,Z)

def case2():
  theta = np.copy(theta0) # [fR,aR,bR,lgsR,bB,lgsB]
  theta[0]=.25
  ds = gmm_base.generate_ds(theta,Z,N)
  print("#"*72)
  print("Testing low fR")
  return check_theta(ds,ii_Z,theta,Z)

def case3():
  theta = np.copy(theta0) # [fR,aR,bR,lgsR,bB,lgsB]
  theta[1]=0.001
  theta[2]=1.6
  theta[5]=np.log10(.15)
  ds = gmm_base.generate_ds(theta,Z,N)
  print("#"*72)
  print("Testing wide gap")
  return check_theta(ds,ii_Z,theta,Z)

def case4():
  theta = np.copy(theta0) # [fR,aR,bR,lgsR,bB,lgsB]
  theta[1]=+.03
  ds = gmm_base.generate_ds(theta,Z,N)
  # plot_cm.cm_from_ds(ds,ii_Z)
  print("#"*72)
  print("Testing positive aR slope")
  return check_theta(ds,ii_Z,theta,Z)

def case5():
  theta = np.copy(theta0) # [fR,aR,bR,lgsR,bB,lgsB]
  theta[1]=0
  theta[3]=np.log10(.15)
  theta[5]=np.log10(.15)
  ds = gmm_base.generate_ds(theta,Z,N)
  # plot_cm.cm_from_ds(ds,ii_Z)
  print("#"*72)
  print("Testing identical blue/red clouds")
  return check_theta(ds,ii_Z,theta,Z)

########################################################################

if __name__=='__main__':
  assert(case0()==0)
  # assert(case1()==0) # requires unrestricting gmr bounds in gmm_base
  assert(case2()==0)
  assert(case3()==0)
  assert(case4()==0)
  assert(case5()==0)
