import h5py
from matplotlib import pyplot as plt
import numpy as np
import config

# open the file
fname = config.mastercat
f = h5py.File(fname,'r')

Ntot = f['catalog/gold/mag_g'].len()
print(f'total galaxy count: {Ntot}')

N=10**7
m_g = f['catalog/gold/mag_g'][:N]
m_r = f['catalog/gold/mag_r'][:N]
m_i = f['catalog/gold/mag_i'][:N]
m_z = f['catalog/gold/mag_z'][:N]
gmr=m_g-m_r
rmi=m_r-m_i

redshift = f['catalog/bpz/unsheared/redshift_cos'][:N]

# grab 'good' data: nonzero and unsaturated galaxies
if 0: # thin strip
  mask = (.1<redshift)*(redshift<.11)
  mask *= (m_i<20) # z=.11 cutoff ~ 18.23
else: # all
  mask = (.05<redshift)*(redshift<.7)
  # mask = (.65<redshift)*(redshift<.7)
  mask *= (m_i<23.1) # equals the z=.7 magnitude cutoff

# get rid of under/over-saturated data
for band in [m_g,m_r,m_i,m_z]:
  mask *= (band>0)*(band<99)


if 0: # earlier portion
  plt.scatter(m_i[mask],gmr[mask],2,c=redshift[mask],cmap='coolwarm')
  plt.xlabel(r'$m_i$')
  plt.ylabel(r'$g-r$')
  plt.colorbar(label='Redshift $z$')
  plt.tight_layout()
  plt.show()
else: # latter portion
  if 0: 
    plt.scatter(m_i[mask],rmi[mask],2,c=redshift[mask],cmap='coolwarm',alpha=.125)
    plt.xlabel(r'$m_i$')
  else: 
    plt.scatter(m_z[mask],rmi[mask],2,c=redshift[mask],cmap='coolwarm',alpha=.125)
    plt.xlabel(r'$m_z$')
  plt.ylabel(r'$r-i$')
  plt.colorbar(label='Redshift $z$')
  plt.tight_layout()
  plt.show()
