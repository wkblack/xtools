# gaussian mixture model testing

########################################################################
from matplotlib import pyplot as plt
# default matplotlib colors
mpl_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

import corner
import linmix
import numpy as np
import numpy.random as npr

from m_star_model import m_star_cutoff as mi_max

########################################################################
# describe likelihood function
########################################################################

labels = ['$f_R$', '$a_R$', '$b_R$', '$\log_{10} \sigma_R$', 
                            '$b_B$', '$\log_{10} \sigma_B$']

def normal(theta,x): 
  mean,scatter = theta
  return (2*np.pi*scatter**2)**-.5 * np.exp(-(x-mean)**2/(2*scatter**2))

def line(theta,m_i,z):
  a,b,s = theta
  return (m_i-mi_max(z)) * a + b

def varying_normal(theta,m_i,gmr,z):
  a,b,s = theta
  # plt.plot(gmr,normal((line(theta,m_i,z),s),gmr)); plt.show() # test
  return normal((line(theta,m_i,z),s),gmr)

def likelihood(theta,m_i,gmr,z): 
  # unpack parameters from theta
  fR,ar,br,lgsr,bb,lgsb = theta # lg = log_10
  # return likelihood # FIXME: make ar instead of 0
  return fR*varying_normal((ar,br,10.**lgsr),m_i,gmr,z) \
       + (1-fR)*varying_normal((0,bb,10.**lgsb),m_i,gmr,z)
  # NOTE: force the blue cloud to have null slope

def ln_likelihood(theta,m_i,gmr,z,multiplicity=1):
  probs = likelihood(theta,m_i,gmr,z)
  if np.any(probs<=0):
    return -np.inf
  else:
    if 0:
      plt.figure(figsize=(10,5))
      plt.subplot(121)
      plt.imshow(multiplicity)
      plt.subplot(122)
      plt.imshow(np.log(probs))
      plt.tight_layout()
      plt.show()
    return np.sum(multiplicity*np.log(probs))

def ln_prior(theta,min_gmr=0,max_gmr=2.5):
  fR,ar,br,lgsr,bb,lgsb = theta
  sig = np.sqrt(10**(2*lgsr)+10**(2*lgsb)) # combined sigma
  if (fR<=1) * (fR>.25) * (np.abs(ar)<.2) \
    * (ar<0.2) * (ar>-.05) * (lgsr<lgsb+.3) \
    * (bb>.05) * (br+sig>bb) * (br<1.4) \
    * (bb>min_gmr) * (br<max_gmr) \
    * (lgsr<0) * (lgsr>-1.8) * (lgsb<0) * (lgsb>-2):
    return # -(ar/.01)**2/2.
    # * (bb>.4) * (br>bb) * (br<2.25) \ # for (g-r)
  else: 
    return -np.inf

def log_probability(theta,m_i,gmr,z):
  return ln_prior(theta) + ln_likelihood(theta,m_i,gmr,z)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# likelihood using ds
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
from discretize import dz,z_bins,dmi,mi_bins,dgmr,gmr_bins

mi_ds = np.array([[mi_bins[i] for j in range(len(gmr_bins))]
                              for i in range(len(mi_bins))]) + dmi/2.

gmr_ds = np.array([[gmr_bins[j] for j in range(len(gmr_bins))]
                                for i in range(len(mi_bins))]) + dgmr/2.

def ln_likelihood_ds(theta,ds,z):
  return ln_likelihood(theta,mi_ds,gmr_ds,z,ds)

def ln_prob(theta,ds,z):
  min_N = 10.**(min(z,.4)/.4+2)
  large_gmr = np.where(np.sum(ds,axis=0)>min_N)
  min_gmr,max_gmr = gmr_bins[[np.min(large_gmr),np.max(large_gmr)]]
  return ln_prior(theta,min_gmr,max_gmr) + ln_likelihood_ds(theta,ds,z)

########################################################################
# plotting for mcmc

# aka Alice
def plot_chain_progression(samples,labels,theta=None,end_N=200): 
  nsteps,nwalkers,ndim = np.shape(samples)
  fig, axes = plt.subplots(ndim, figsize=(9,6), sharex=True)
  medians = np.zeros(ndim)
  
  for i in range(ndim):
      ax = axes[i]
      ax.plot(samples[:, :, i], "k", alpha=0.3)
      ax.set_xlim(0,nsteps)
      ax.set_ylabel(labels[i])
      ax.yaxis.set_label_coords(-0.1, 0.5)
      if np.all(theta!=None): # plot actual
        ax.plot([0,nsteps],[theta[i],theta[i]],'r')
      if end_N>0: # plot end median for last end_N points
        med = np.median(samples[-end_N:, :, i])
        x = np.ones((end_N,nwalkers))*med
        ax.plot([nsteps-1-end_N,nsteps-1],[med,med],'b')
        medians[i] = med
  
  axes[-1].set_xlabel("step number")
  
  plt.show()
  return medians

def plot_corner(flat_samples,labels=labels,theta=None,size=(10,7)):
  fig = corner.corner(flat_samples, labels=labels, truths=theta,
                      quantiles = [.16,.50,.84], show_titles=True,
                      verbose=True);
  # TODO: figure out how to use label_kwargs={'labelpad':50}, 
  fig = plt.gcf()
  fig.set_size_inches(10,7)
  plt.tight_layout()
  fig.subplots_adjust(hspace=.25,wspace=.5,right=.961)
  plt.show()
########################################################################

########################################################################
from scipy.special import erf

one_sigma = sixty_eight = erf(1/np.sqrt(2))
two_sigma = ninety_five = erf(2/np.sqrt(2))

# approximate expected sizes for 2*sigma values
error_sizes = np.array([.04,.003,.004,.05,.015,.02])

def errors(flat_samples,printing=True,max_frac=.2):
  ndim = len(flat_samples[0,:])
  chains = [flat_samples[:,ii] for ii in range(ndim)]
  report = np.zeros((ndim,3))
  
  for kk,chain in enumerate(chains):
    bot,median,top = np.quantile(chain,.5+np.array([-1,0,+1])*one_sigma/2)
    sm1,sp1 = median-bot,top-median
    if printing: 
      print("{:s} = {:g}^{{+{:g}}}_{{-{:g}}}$".format(labels[kk][:-1],median,sp1,sm1))
      if np.abs(sm1/median) > max_frac or np.abs(sp1/median) > max_frac\
         or sm1>error_sizes[kk] or sp1>error_sizes[kk]:
        print("WARNING: large errors!")
    report[kk,:] = median,sm1,sp1
    
  return report

########################################################################
# plot up median values and errors, read from files

from glob import glob
import re

fname_err_temp = 'out/errors_zbin%02i.npy'
fname_err_glob = 'out/errors_zbin*.npy'

fname_err_temp_rmi = 'out/errors_rmi_zbin%02i.npy'
fname_err_glob_rmi = 'out/errors_rmi_zbin*.npy'

all_labels = [r'red fraction $f_R$',r'Slope $a_R$',r'Offset $b_R$',
              r'Scatter $\log_{10} \sigma_R$',r'Offset $b_B$', 
              r'Scatter $\log_{10} \sigma_B$'] # y-axis

red_labels = [r'Slope $a_R$',r'Offset $b_R$',
              r'Scatter $\log_{10} \sigma_R$'] # y-axis
H09 = [[-.003,-0.075], [.623,3.049], [.037,.136]]
H09_sig = [[.001,.005],[.002,.011],[.002,.010]]

def plot_values(file_nums=None,bool_rmi=False):
  if file_nums==None:
    out_files = sorted(glob(fname_err_glob_rmi if bool_rmi else fname_err_glob))
    file_nums = [int(re.findall(r'\d+',fname)[0]) for fname in out_files]
  
  z_v = np.linspace(.1,.3,100) # H09 range
  Z_vals = np.zeros(len(file_nums))
  R = np.zeros(len(file_nums))
  Rplus = np.zeros(len(file_nums))
  Rmin = np.zeros(len(file_nums))
  
  for ii in range(1,4): # for each red parameter
    # plot H09 results
    plt.xlabel(r'Redshift $z$')
    plt.ylabel(red_labels[ii-1])
    # plt.plot([0,1],H09[ii-1],color='r')
    b,a = H09[ii-1]
    b_sig,a_sig = H09_sig[ii-1]
    
    if ii==3: # log it
      plt.plot(z_v,np.log10(b+a*z_v),'r')
      plt.fill_between(z_v,np.log10((b-b_sig)+(a-a_sig)*z_v),
                           np.log10((b+b_sig)+(a+a_sig)*z_v),
                       alpha=.25,linewidth=0,color='r')
    else:
      plt.plot(z_v,b+a*z_v,'r')
      plt.fill_between(z_v,((b-b_sig)+(a-a_sig)*z_v),
                           ((b+b_sig)+(a+a_sig)*z_v),
                       alpha=.25,linewidth=0,color='r')
    
    for jj,fnum in enumerate(file_nums): # plot point from each file
      Z = z_bins[fnum]
      ft = fname_err_temp_rmi if bool_rmi else fname_err_temp
      dsv = np.load(ft % fnum)
      med,sm1,sp1 = dsv[ii]
      Z_vals[jj] = Z; R[jj] = med; Rmin[jj] = sm1; Rplus[jj] = sp1
      
    plt.xlim(0,max(Z_vals)+.05)
    plt.errorbar(Z_vals,R,[Rmin,Rplus],capsize=2,fmt='.',ls="",c='k')
    plt.tight_layout()
    plt.show()
  
  pass


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

blue_labels = [r'red fraction $f_R$',r'Offset $b_B$',
              r'Scatter $\log_{10} \sigma_B$'] # vertical-axis

def plot_values_blue():
  out_files = sorted(glob(fname_err_glob))
  file_nums = [int(re.findall(r'\d+',fname)[0]) for fname in out_files]
  
  z_v = np.array([.1,.3]) # H09 range
  z_v = np.linspace(.1,.3,100)
  Z_vals = np.zeros(len(file_nums))
  R = np.zeros(len(file_nums))
  Rplus = np.zeros(len(file_nums))
  Rmin = np.zeros(len(file_nums))
  
  for ii in [0,4,5]: # fR, bB, lgsB
    plt.xlabel(r'Redshift $z$')
    plt.ylabel(all_labels[ii])
    for jj,fnum in enumerate(file_nums): # plot point from each file
      Z = z_bins[fnum]
      dsv = np.load(fname_err_temp % fnum)
      med,sm1,sp1 = dsv[ii]
      Z_vals[jj] = Z; R[jj] = med; Rmin[jj] = sm1; Rplus[jj] = sp1
      
    plt.xlim(0,max(Z_vals)+.05)
    plt.errorbar(Z_vals,R,[Rmin,Rplus],capsize=2,fmt='.',ls="",c='k')
    plt.tight_layout()
    plt.show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# following values from previous linmix output of this method "out.ls"
# this is the z|[.1,.3] fit
_alpha = [0.5260148994095826,-0.010360268197827099,0.6060383303631377,
          -1.444547584393238,0.5037436485057589,-0.8326474338597026]
_alpha_sig = [0.00763538305397738,0.0008813443355373165,0.006275113932423528,
              0.0046875905729347675,0.006486220667143069,0.004428258649169916]
_beta = [-0.1375260684567094,-0.03477323473626794,2.948888464245325,
           1.3302888966416622,1.9423965604761209,0.62924138632724]
_beta_sig = [0.03604586999014797,0.00417993948254816,0.03006234075755101,
             0.02141876088989845,0.03093049256189453,0.021149842571539613]
_sigsqr = [6.895074148076171e-05,7.571632073193295e-07,6.032301644034658e-05,
           1.859754408662172e-05,5.818992161354702e-05,2.3694160892495755e-05]
_sig_sig = [3.290841722637918e-05,4.599791404757648e-07,2.5236206552104712e-05,
            1.1219136135838299e-05,2.513303731986256e-05,1.1689811403670805e-05]

Z_ranges_default = [[.1,.3],[.05,.375],[.05,.35],[.375,.5],[.303,.416]]
Zr = Z_transit_points_rmi = [.05,.1,.1725,.248,.3,.325,.345,.425,.5,.545,.7]
Z_ranges_rmi = [[Zr[i],Zr[i+1]] for i in range(len(Zr)-1)]
# good rmi Z_ranges: 
# Z_ranges = np.array([[.14,.61],[.14,.424],[.424,.61],[.14,.4835],[.4835,.61]])
# Z_ranges = np.array([[.33,.5],[.5,.63]])
def fit_lines(plotting=True,publishing=False,add_scatter=False,
              fit_pars=range(6),bool_rmi=False,linear_regress=False,
              Z_ranges=Z_ranges_default):
  """
  plotting=True
  publishing=True
  add_scatter=False
  fit_pars=range(6)
  bool_rmi=False
  linear_regress=False
  Z_ranges=Z_ranges_default
  """
  out_files = sorted(glob(fname_err_glob_rmi if bool_rmi else fname_err_glob))
  file_nums = [int(re.findall(r'\d+',fname)[0]) for fname in out_files]
  
  z_v = np.array([.1,.3]) # H09 range
  Z_vals = np.zeros(len(file_nums))
  R = np.zeros(len(file_nums))
  Rplus = np.zeros(len(file_nums))
  Rmin = np.zeros(len(file_nums))
  
  for ii in fit_pars:
    plt.xlabel(r'Redshift $z$')
    plt.ylabel(all_labels[ii])
    for jj,fnum in enumerate(file_nums): # plot point from each file
      Z = z_bins[fnum] + dz/2. # center on z-bin
      ft = fname_err_temp_rmi if bool_rmi else fname_err_temp
      dsv = np.load(ft % fnum)
      med,sm1,sp1 = dsv[ii]
      Z_vals[jj] = Z; R[jj] = med; Rmin[jj] = sm1; Rplus[jj] = sp1
      
    plt.xlim(0,max(Z_vals)+.05)
    plt.errorbar(Z_vals,R,[Rmin,Rplus],capsize=2,fmt='.',ls="",c='k')
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # if one of Hao's parameters, plot up its range
    if ii in range(1,4) and not bool_rmi:
      b,a = H09[ii-1]
      b_sig,a_sig = H09_sig[ii-1]
      if ii==3: # log it
        plt.plot(z_v,np.log10(b+a*z_v),'r')
        plt.fill_between(z_v,np.log10((b-b_sig)+(a-a_sig)*z_v),
                             np.log10((b+b_sig)+(a+a_sig)*z_v),
                         alpha=.25,linewidth=0,color='r')
      else: # linear fit
        plt.plot(z_v,b+a*z_v,'r')
        plt.fill_between(z_v,((b-b_sig)+(a-a_sig)*z_v),
                             ((b+b_sig)+(a+a_sig)*z_v),
                         alpha=.25,linewidth=0,color='r')
      
    
    if not publishing: 
      # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
      # fit values to line w/i different ranges
      print(all_labels[ii])
      Rmean = np.sqrt(Rmin*Rplus)
      
      # use default matplotlib colors
      colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
      Z_ranges = np.array(Z_ranges)
      for kk,_z in enumerate(Z_ranges):
        mask = (_z[0]<=Z_vals) * (Z_vals<=_z[1])
        if linear_regress: # linear regression
          m,b = np.polyfit(Z_vals[mask],R[mask],1,w=1/Rmean[mask])
          print(f"m,b [{_z[0]:g},{_z[1]:g}] = {m:g},{b:g}")
          plt.plot(_z,m*_z + b,colors[kk])
        else: # linmix
          lm = linmix.LinMix(Z_vals[mask],R[mask],ysig=Rmean[mask])
          lm.run_mcmc(silent=True)
          alpha,beta,sigsqr = [lm.chain[par].mean() for par in ['alpha','beta','sigsqr']]
          print(f"z|[{_z[0]:g},{_z[1]:g}]")
          print("alpha mean and stddev:  {}, {}".format(
                alpha,lm.chain['alpha'].std()))
          print("beta mean and stddev:   {}, {}".format(
                beta,lm.chain['beta'].std()))
          print("sigsqr mean and stddev: {}, {}".format(
                sigsqr, lm.chain['sigsqr'].std()))
          plt.plot(_z,alpha+beta*_z,colors[kk])
    else: # plot hardcoded fits
      # publishing ideas: 
      # https://python4astronomers.github.io/plotting/publication.html
      plt.rc('font', family='serif')
      _a,_b,_ss = _alpha[ii], _beta[ii], _sigsqr[ii]
      _as,_bs,_sss = _alpha_sig[ii],_beta_sig[ii],_sig_sig[ii]
      _s = np.sqrt(_ss) # spread
      _1s = np.sqrt(_ss+_sss) # 1sigma upper bound on spread
      # plot up a+bZ +- s
      plt.plot(z_v,_a + _b*z_v,'g') # mean relation
      plt.fill_between(z_v,(_a-_as)+(_b-_bs)*z_v,(_a+_as)+(_b+_bs)*z_v,
                       alpha=.25,linewidth=0,color='g') # mean + 1 sigma
      if add_scatter: # not included in plotting up the Hao parameters
        plt.fill_between(z_v,_a+_b*z_v-_s,_a+_b*z_v+_s, # mean + spread
                         alpha=.125,linewidth=0,color='g')
        plt.fill_between(z_v,(_a-_as)+(_b-_bs)*z_v-_1s,
                             (_a+_as)+(_b+_bs)*z_v+_1s,
                         alpha=.125,linewidth=0,color='g') # 1 sigma (all)
    
    plt.tight_layout()
    if plotting and not publishing: plt.show()
    if publishing: 
      plt.savefig(f'out/{ii:02}.pdf',format='pdf',dpi=1000)
      plt.clf()



def cf_pars(fit_pars=range(6),bool_rmi=False,draw_theta=True):
  """
  cf_pars(fit_pars=range(6),bool_rmi=False,draw_theta=True):
  """
  
  out_files = sorted(glob(fname_err_glob_rmi if bool_rmi else fname_err_glob))
  file_nums = [int(re.findall(r'\d+',fname)[0]) for fname in out_files]
  
  z_v = np.array([.1,.3]) # H09 range
  Z_vals = np.zeros(len(file_nums))
  R = np.zeros(len(file_nums))
  Rplus = np.zeros(len(file_nums))
  Rmin = np.zeros(len(file_nums))
  
  for ii in fit_pars:
    plt.xlabel(r'Redshift $z$')
    # compile data from each of the files
    for jj,fnum in enumerate(file_nums): 
      Z = z_bins[fnum] + dz/2. # center on z-bin
      ft = fname_err_temp_rmi if bool_rmi else fname_err_temp
      dsv = np.load(ft % fnum)
      med,sm1,sp1 = dsv[ii]
      Z_vals[jj] = Z; R[jj] = med; Rmin[jj] = sm1; Rplus[jj] = sp1
      
    # plot points from each file
    plt.xlim(0,max(Z_vals)+.05)
    plt.errorbar(Z_vals,R,[Rmin,Rplus],capsize=2,fmt='.',ls="",
                 c=mpl_colors[ii],label=all_labels[ii])
    
    theta_vals = [generate_theta(z_i,bool_rmi)[ii] for z_i in Z_vals]
    plt.plot(Z_vals,theta_vals,color=mpl_colors[ii])

  plt.legend()
  plt.tight_layout()
  plt.show()


########################################################################
# generate data

# some temporary parameters
slope_red = -.03
slope_blue = -.01
offset_red = 1.2 # from max_i
offset_blue = .5 # "
scatter_red = .05
scatter_blue = .25
red_fraction = .51
redshift_fake = .2
# ignore redshift evolution in this model; 
# do delta z and delta mu bins

theta_fake = (red_fraction,
              slope_red,offset_red,np.log10(scatter_red),
                         offset_blue,np.log10(scatter_blue))

def generate_data(theta=theta_fake,redshift=redshift_fake,N=200):
  # TODO: use likelihood to do this
  fR,ar,br,lgsr,bb,lgsb = theta
  min_i,max_i = 16,mi_max(redshift)
  sigma = (max_i-min_i)*.325
  m_i = max_i + dmi/2. - np.abs(npr.normal(0,sigma,N))
  m_i = np.clip(m_i,min_i-sigma,max_i)
  red_selection = npr.random(N)<fR
  gmr = np.empty(N)
  line_red = (m_i[red_selection]-max_i) * ar + br 
  gmr[red_selection] = npr.normal(line_red,10.**lgsr,sum(red_selection))
  line_blue = (m_i[~red_selection]-max_i) * 0 + bb
  gmr[~red_selection] = npr.normal(line_blue,10.**lgsb,sum(~red_selection))
  return m_i,gmr

from discretize import CM_report_template

def generate_ds(theta=theta_fake,redshift=redshift_fake,N=200,
                printing=False):
  # turn input parameter set into dataset
  m_i,gmr = generate_data(theta,redshift,N)
  assert(len(m_i)==N)
  dZ = np.abs(z_bins-redshift)
  ii_Z = np.where(dZ==min(dZ))[0][0]
  if printing: 
    print(f"Creating at redshift {redshift} -> index {ii_Z}")
  
  subset_mi = np.zeros((len(mi_bins)-1,N))
  for i in range(len(mi_bins)-1):
    subset_mi[i] = np.logical_and(mi_bins[i]<=m_i,m_i<mi_bins[i+1])
  
  subset_gmr = np.zeros((len(gmr_bins)-1,N))
  for i in range(len(gmr_bins)-1):
    subset_gmr[i] = np.logical_and(gmr_bins[i]<=gmr,gmr<gmr_bins[i+1])
  
  ######################################################################
  # outter product of boolean masks 
  report = np.copy(CM_report_template)
  for j in range(len(mi_bins)-1):
    for k in range(len(gmr_bins)-1):
      report[ii_Z,j,k] += len(gmr[(subset_mi[j]*subset_gmr[k]).astype(bool)])
  return report

# generate a semi-informed theta vector
def generate_theta(Z,bool_rmi=False): 
  """
  fit for theta based on results
  should be relatively accurate, but not to be taken too seriously
  used for a starting point for the MCMC
  """
  if bool_rmi: # (r-i) fit
    fR = .46282 - .16188*Z
    aR = -0.00966 + 0.03105*Z
    bR = .302401 + .6965*Z
    lgsR = -1.5308 - 0.24984*Z
    bB = .3528 + .144807*Z
    lgsB = -1.2003 + .41882*Z
    if Z>.25:
      aR = 0.01261 - .06133*Z
      lgsR = -2.0542 + 1.9157*Z
      lgsB = -1.5767 + 1.9468*Z
    if Z>.33:
      aR = -0.01057 + .00305656*Z
      lgsR = -1.64700 + .703732*Z
      lgsB = -1.13381 + .612568*Z
      bB = .35290 + .144807*Z
    if Z>.425:
      lgsR = -1.877 + 1.2489*Z
      bR = -0.28655 + 2.077905*Z
      bB = -0.055626 + 1.1172938*Z
    if Z>.5:
      fR = .0584261 + .6101668*Z
      aR = 0
      lgsR = -2.4224 + 2.362750*Z
      lgsB = -0.819638 + .0153955*Z
    if Z>.65:
      bR = 0.346424 + 1.094378*Z
    # 
  else: # (g-r) fit
    # use past results to inform estimated theta
    lgsR = -1.4524 + 1.362805*Z # good fit for all redshift
    if Z<.375:
      fR = .507 - .0450*Z
      aR = -.03992*Z - .0097
      bR = 2.7376*Z + .6511
      # lgsR = 1.391*Z - 1.457
      bB = .550 + 1.73*Z
      lgsB = -.8456 + .7046*Z
    if Z>.3: # overwrite a few, add end conditions to others
      fR = .4199 + .2524*Z
      lgsB = -.5126 - .4632*Z
    if Z>.375: # write end conditions
      aR = -0.0721551 + .1237874*Z
      bR = 1.542794 + .3932892*Z
      # lgsR = -1.271693 + .9257*Z
      bB = 1.347 - .3023*Z
    if Z>.416:
      lgsB = -.6522 - .1151*Z
    if Z>.49:
      aR = .2284 - .4828*Z
      bR = 2.3628 - 1.2555*Z
      # lgsR = -1.89644 + 2.22194*Z
  return np.array([fR,aR,bR,lgsR,bB,lgsB])


def plot_theta(bool_rmi=False):
  " bool_rmi = False "
  Z = np.linspace(.05,.7,500)
  vals = [generate_theta(z,bool_rmi) for z in Z]
  plt.plot(Z,vals)
  
  if 1: # plot other parameters
    out_files = sorted(glob(fname_err_glob_rmi if bool_rmi else fname_err_glob))
    file_nums = [int(re.findall(r'\d+',fname)[0]) for fname in out_files]
    
    z_v = np.array([.1,.3]) # H09 range
    Z_vals = np.zeros(len(file_nums))
    R = np.zeros(len(file_nums))
    Rplus = np.zeros(len(file_nums))
    Rmin = np.zeros(len(file_nums))
    for ii in range(6):
      for jj,fnum in enumerate(file_nums): # plot point from each file
        Z = z_bins[fnum] + dz/2. # center on z-bin
        ft = fname_err_temp_rmi if bool_rmi else fname_err_temp
        dsv = np.load(ft % fnum)
        med,sm1,sp1 = dsv[ii]
        Z_vals[jj] = Z; R[jj] = med; Rmin[jj] = sm1; Rplus[jj] = sp1
      plt.errorbar(Z_vals,R,[Rmin,Rplus],capsize=2,fmt='.',ls="",
                   c=mpl_colors[ii])
  
  plt.legend(labels)
  plt.xlabel(r'Redshift')
  plt.show()


########################################################################
# main method, testing emcee

if __name__=='__main__':
  ######################################################################
  # generate fake test data (for real: use actual halo(s))
  ######################################################################
  
  m_i,gmr = generate_data(theta_fake,redshift_fake,N=200)
  data = (m_i,gmr,redshift_fake)
  
  if 0: 
    plt.scatter(m_i[red_selection],gmr[red_selection],c='r')
    plt.scatter(m_i[~red_selection],gmr[~red_selection],c='b')
    plt.xlabel(r'$m_i$')
    plt.ylabel(r'$g-r$')
    plt.tight_layout()
    plt.show()
  
  ######################################################################
  # run emcee
  ######################################################################
  import emcee
  
  # FIXME: below here was just copied from another code:
  nwalkers, ndim = 32, len(theta_fake)
  walkers = np.array(theta_fake)*(1 + 1e-1 * np.random.randn(nwalkers,ndim))
  
  sampler = emcee.EnsembleSampler(nwalkers, ndim, log_probability, args=data)
  nsteps = 1000
  sampler.run_mcmc(walkers, nsteps, progress=True);
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # analyze mcmc chains
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  
  samples = sampler.get_chain()
  # tau = sampler.get_autocorr_time()

  plot_chain_progression(samples,labels,theta_fake,end_N=200)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # analyze data
  
  flat_samples = sampler.get_chain(discard=250, thin=15, flat=True)
  if 0: # forge corner plot
    fig = corner.corner(flat_samples, labels=labels, truths=theta_fake);
    plt.tight_layout()
    plt.show()
  
  if 1: # plot up fit
    medians = [np.percentile(flat_samples[:,i],50) for i in range(ndim)]
    # fR_med,aR_med,bR_med,lgsR_med,aB_med,bB_med,lgsB_med = medians
    fR_med,aR_med,bR_med,lgsR_med,bB_med,lgsB_med = medians
    # plot points
    plt.scatter(m_i[red_selection],gmr[red_selection],c='r')
    plt.scatter(m_i[~red_selection],gmr[~red_selection],c='b')
    # plot fit
    mi_fits = np.linspace(min(m_i)-1,max_i+.1,10)
    blue_line = (mi_fits-max_i) * aB_med + bB_med
    plt.fill_between(mi_fits,blue_line - 10.**lgsB_med,
                             blue_line + 10.**lgsB_med,color='b',alpha=.125)
    plt.fill_between(mi_fits,blue_line - 2*10.**lgsB_med,
                             blue_line + 2*10.**lgsB_med,color='b',alpha=.125)
    red_line = (mi_fits-max_i) * aR_med + bR_med
    plt.fill_between(mi_fits,red_line - 2*10.**lgsR_med,
                             red_line + 2*10.**lgsR_med,color='r',alpha=.125)
    plt.fill_between(mi_fits,red_line - 10.**lgsR_med,
                             red_line + 10.**lgsR_med,color='r',alpha=.125)
    # fix plot
    plt.xlabel(r'$m_i$')
    plt.ylabel(r'$g-r$')
    plt.tight_layout()
    plt.show()
