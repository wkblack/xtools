# main code to run gmm
import numpy as np
import gmm_base

bool_rmi = True

if 0: # generate fake data
  m_i,gmr = gmm_base.generate_data(N=1000)
  # FIXME: method in previous model
  ds = data_to_ds(.2,m_i,gmr,printing=False)
  # FIXME: get actual redshift value!
elif 0: # use actual dataset, reading in individual files
  print("Reading in partial dataset")
  from discretize import combine_ds
  ii_max = 6
  ds = combine_ds(10,ii_max)
else: # use pre-compiled dataset
  if bool_rmi: 
    print("Running (r-i) dataset")
    from discretize import fname_out_rmi as fname_out
  else:
    print("Running (g-r) dataset")
    from discretize import fname_out
  ds = np.load(fname_out)

from matplotlib import pyplot as plt
from discretize import dz,z_bins,dmi,mi_bins,dgmr,gmr_bins

########################################################################
import emcee
from gmm_base import ln_prob
from gmm_base import plot_chain_progression as alice

from scipy.special import erf
one_sigma = sixty_eight = erf(1/np.sqrt(2))
two_sigma = ninety_five = erf(2/np.sqrt(2))

import sys
if len(sys.argv)>1: # read in from command line
  jj = int(sys.argv[1])
else: # hardcode
  jj = 40 # z bin
  print("Using hardcoded pix index")

z = z_bins[jj]+dz/2.
theta0 = gmm_base.generate_theta(z,bool_rmi) # [fR,aR,bR,lgsR,bB,lgsB]


########################################################################
# fix offset guesses based on min & max: 
print("Redshift %g" % z)
min_N = 60 if bool_rmi else 10.**(min(z,.4)/.4+2)
large_gmr = np.where(np.sum(ds[jj],axis=0)>min_N)
min_gmr,max_gmr = gmr_bins[[np.min(large_gmr),np.max(large_gmr)]]
print(f"min & max gmr w/ N>{min_N:g}: {min_gmr:g},{max_gmr:g}")

if 0: # NOTE: removed for gmr and rmi with advent of superior theta generation
  offset_guess = (max_gmr+min_gmr)/2. # average of points
  spread_guess = (max_gmr-min_gmr)/4. # quarter of point spread
  print(f'guess: {offset_guess:g}')
  # guess theta=[fR,aR,bR,lgsR,bB,lgsB]
  theta0[2] = offset_guess + spread_guess
  theta0[3] = np.log10(spread_guess/4)
  theta0[4] = offset_guess - spread_guess
  theta0[5] = np.log10(spread_guess/2)

print('Total galaxy count in ds[{:d}]: {:d}'.format(jj,np.sum(ds[jj]).astype(int)))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

nwalkers, ndim = 32,len(theta0)

walkers = theta0 + gmm_base.error_sizes * np.random.randn(nwalkers,ndim)
walkers[:,0] = np.clip(walkers[:,0],0,1) # fix fR|[0,1]

sampler = emcee.EnsembleSampler(nwalkers,ndim,ln_prob,
                                args=(ds[jj],z_bins[jj]+dz/2.))
nsteps = 2000
sampler.run_mcmc(walkers,nsteps,progress=True)

# plot chain progression: 
samples = sampler.get_chain()
try:
  tau = sampler.get_autocorr_time()
  print(f"tau = {tau}")
  max_tau = np.ceil(np.max(tau)).astype(int)
except emcee.autocorr.AutocorrError as err:
  print("Chain too short!")
  print(err)
  max_tau = 200
  print(f"Using tau={max_tau:g}")

if 0: 
  theta_fit = alice(samples,gmm_base.labels,theta0) # plot input guess
elif 1: 
  theta_fit = alice(samples,gmm_base.labels,None) # no red lines
else: 
  print("Danger mode: ignoring the chains!")

theta_fit = [np.quantile(samples[-500:,:,ll],.5) for ll in range(ndim)]

# plot CM diagram
import plot_cm
if 1: plot_cm.cm_from_ds(ds,i=jj,theta=theta_fit,bool_rmi=bool_rmi)

# display corner plot
flat_samples = sampler.get_chain(discard=2*max_tau, flat=True) #thin=15
if 0: gmm_base.plot_corner(flat_samples)

print()
errs = gmm_base.errors(flat_samples)
if bool_rmi:
  np.save(gmm_base.fname_err_temp_rmi % jj,errs)
else: 
  np.save(gmm_base.fname_err_temp % jj,errs)
print(f'finished with ds[{jj}]')
print()
