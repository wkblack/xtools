# deciding which colors to use 
import h5py
import numpy as np

mastercat = '/project/projectdirs/des/jderose/Chinchilla/Herd/' \
          + 'Chinchilla-3/v1.9.8/sampleselection/Y3a/mastercat/' \
          + 'Buzzard-3_v1.9.8_Y3a_mastercat.h5'

f = h5py.File(mastercat,'r')

########################################################################

def grab_fields(N=-1,z_range=[.32,.7]):
  Z_loc = 'catalog/bpz/unsheared/redshift_cos'
  col_loc = 'catalog/gold/mag_%s_true'
  if N<0: N = len(f[Z_loc])
  
  # grab values
  Z = f[Z_loc][:N]
  m_g = f[col_loc % 'g'][:N]
  m_r = f[col_loc % 'r'][:N]
  m_i = f[col_loc % 'i'][:N]
  m_z = f[col_loc % 'z'][:N]
  
  # vet
  mask = ((z_range[0]<Z)*(Z<z_range[1])*(m_i>0)).astype(bool)
  Z = Z[mask]; m_g = m_g[mask]; m_r = m_r[mask]; m_i = m_i[mask]; m_z = m_z[mask]
  return [Z,m_g,m_r,m_i,m_z]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def analyze_color(col,Z):
  sig_low,sig_high = [np.std(col[Z<.51]),np.std(col[Z>.51])]
  sig_tot = np.std(col)
  print("std dev:",sig_low,sig_high)
  print("/std tot:",sig_low/sig_tot,sig_high/sig_tot)

########################################################################

if __name__=='__main__':
  Z,m_g,m_r,m_i,m_z = grab_fields()
  print("Analyzing (g-r):")
  analyze_color(m_g-m_r,Z)
  print("Analyzing (r-i):")
  analyze_color(m_r-m_i,Z)
  print("Analyzing (r-z):")
  analyze_color(m_r-m_z,Z)
  pass
