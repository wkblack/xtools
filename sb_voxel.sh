#!/bin/bash
#SBATCH --qos=low
##SBATCH --time=07:59:00
#SBATCH --time=02:59:00
##SBATCH --qos=debug
##SBATCH --time=5
#SBATCH --nodes=1
##SBATCH --tasks-per-node=68
#SBATCH --constraint=knl
#SBATCH --dependency=singleton
##SBATCH --dependency=after:32097497

# srun -n 2 -c 2 voxel.py
# srun -n 1 -c 1 voxel.py
# python voxel.py 2865 2866 2867 2868 2869 2870 2871 2872 2873 2874 2875 2876 2877 2878
python voxel.py
