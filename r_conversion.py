#!/bin/python
# go between r20,r200,r500,r2500

########################################################################
# the following follows Hu & Kravtsov 2002, Appendix C
# doi.org/10.1086%2F345846
# the goal is to take a halo and convert from e.g. r200 to r500
########################################################################

import cosmology as cosmo
import numpy as np

def x(z,params=cosmo.aardvark):
  return cosmo.Wm(z,params) - 1

def DeltaVir(z,params=cosmo.aardvark,mean=False): # virial overdensity
  # from Bryan & Norman 1998: arxiv.org/abs/astro-ph/9710107
  if mean:
    return (18*np.pi**2+82*x(z,params)-39*x(z,params)**2)/(1+x(z,params))
  else:
    return 18*np.pi**2+82*x(z,params)-39*x(z,params)**2

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

x_low,x_high = .0160182,12.3774 # 1% error bounds for apx fxns

def f(x): 
  if hasattr(x,'__len__'):
    report = np.zeros(len(x))
    report[x>x_high]= .5*x[x>x_high]-2./3.
    report[x<x_low]= -(1+np.log(x[x<x_low]))*x[x<x_low]**3
    mask = np.logical_and(x<=x_high,x>=x_low)
    report[mask] = x[mask]**3*(np.log(1+1./x[mask])-1./(1+x[mask]))
    assert(np.all(report>0))
    return report
  else:
    if x>x_high: 
      return .5*x-2./3.
    elif x<x_low:
      return -(1+np.log(x))*x**3
    else:
      return x**3*(np.log(1+1./x)-1./(1+x))

def fprime(x): 
  return x**2*((-4-3*x)/(1+x)**2 + 3*np.log(1+1./x))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
from scipy import optimize

def f_inv(f_val): 
  # inverse function from f(x) above
  
  if hasattr(f_val,'__len__'):
    N=len(f_val)
    report = np.zeros(N)
    easy = f_val>f(x_high)
    report[easy] = 2*(f_val[easy]+2./3.)
    for i in range(N):
      if not easy[i]:
        def f_solve(f_v): 
          return f(f_v)-f_val[i]
        try:
          report[i] = optimize.newton(f_solve, x0=0.01, fprime=fprime)
        except RuntimeError:
          print "RuntimeError: probably failed to converge... [r_conversion.f_inv]"
          print "input:",f_val[i]
    return report
  
  else: # there's only one object here
    if f_val>f(x_high):
      return 2*(f_val+2./3.)
    else: # it's not easily invertable, so root find
      def f_solve(f_v): 
        return f(f_v)-f_val
      return optimize.newton(f_solve, x0=0.01, fprime=fprime)
  
  report = np.zeros(N)
  for i in range(N):
    if f_val>f(x_high):
      report[i] = 2*(f_val+2./3.)
    else: # it's not easily invertable, so root find
      def f_solve(f_v): 
        return f(f_v)-f_val[i]
      report[i] = optimize.newton(f_solve, x0=0.01, fprime=fprime)
  
  if N==1: 
    return report[0]
  else:
    return report 
  # alternatively, an approximate function could be used,
  # as described in Hu & Kravtsov 2002, Appendix C, eqn C11

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def r_Delta(delta=500.,Rs=0.556282,R_ref=2.063662,delta_ref=200.,
            z=0,params=cosmo.aardvark): 
  # default values in physical Mpc/h for Aardvark's most massive halo
  """
  delta     :   goal r_Delta to achieve
  Rs        :   scale radius (same units as R_ref)
  R_ref     :   reference radius [e.g. R200C]
  delta_ref :   reference overdensity, matching R_ref [e.g. 200]
  z         :   redshift (needed for virial input/output)
  params    :   cosmology (needed for virial input/output)
  returns scaled r_Delta in same units as Rs and R_ref
  """
  if np.any(R_ref<1e-9): 
    print 'ERROR: Reference radius null! [r_conversion.py]'
    raise SystemExit
  if delta in {'vir','virial','v'}:
    delta = DeltaVir(z,params)
  if delta_ref in {'vir','virial','v'}:
    delta_ref = DeltaVir(z,params)
  assert(np.all(delta>0))
  assert(np.all(delta_ref>0))
  assert(np.min(Rs)>0)
  assert(np.min(R_ref)>0)
  assert(np.min(f(Rs/R_ref))>0)
  return Rs/f_inv(delta/delta_ref*f(Rs/R_ref))

########################################################################

if 0: # check f
  x=10.**np.arange(-16,16,.125)
  # x=np.array([1e-16,1e-10,1e-5,1e-3,1e-2,1e-1,1,1e1,1e2,1e3,1e5,1e16])
  from matplotlib import pyplot as plt
  plt.plot(x,f(x))
  plt.scatter(x,f(x))
  # plt.xlim(1e-6,1e6)
  plt.xscale('log')
  plt.yscale('log')
  plt.show()
  raise SystemExit

if 0: # check the inverse function of f
  print 'testing f_inv'
  x=[.001,.01,.1,1,5,10,100,1000,10000]
  for x_i in x: 
    print 'f_inv(%g)=%g\t' % (x_i,f_inv(x_i)),
    print 'f(f_inv(%f))=%f' % (x_i,f(f_inv(x_i)))
  raise SystemExit

if 0: # test different overdensities
  from matplotlib import pyplot as plt
  x=[20,200,500,2500]
  y=[r_Delta(d) for d in x]
  plt.scatter(x,y)
  plt.xscale('log')
  plt.xlabel(r'$\Delta$')
  plt.ylabel(r'$r_\Delta$ (Mpc/h, physical)')
  plt.show()
  for i in range(len(x)): 
    print 'r_%i = %g Mpc/h' % (x[i],y[i])

########################################################################
if __name__=='__main__':
  # start by looking at the largest halo
  HID = 11922429
  
  # read in its data
  from astropy.io import fits
  
  from config import Aardvark as sim
  fname = sim['halo_dir'] + sim['halo_ftemp'] % 1
  dat = fits.open(fname)[1].data
  # dat = dat[dat['HALOID']==HID]
  assert(HID in dat['HALOID'])
  
  if 0: # only do the most massive halo
    dat = dat[dat['HALOID']==HID]
  
  from matplotlib import pyplot as plt
  dlist = [20,200,500,2500]
  MAX_ITER = 1000
  for i in range(len(dat)): 
    r200 = dat['r200'][i]   # Mpc/h
    Rs = dat['rs'][i]/1000. # Mpc/h (converted from kpc/h)
    print "Halo %i/%i r200=%g and Rs=%g" % (i+1,len(dat),r200,Rs)
    if r200<=0: 
      print "bad r200; skipping..."
      continue
    # r_Delta(delta=500.,Rs=.556,R_ref=2.,delta_ref=200.): # distances in Mpc/h
    r20 = r_Delta(20,Rs,r200) # Mpc/h
    r500 = r_Delta(500,Rs,r200) # Mpc/h
    r2500 = r_Delta(2500,Rs,r200) # Mpc/h
    rlist = [r20/r200,r200/r200,r500/r200,r2500/r200]
    # print 'rlist:',rlist
    # print 'r200=%f'%r200; raise SystemExit
    plt.scatter(dlist,rlist,c='r',alpha=.01)
    if i>MAX_ITER: break
  
  plt.xlabel(r'$\Delta$')
  plt.ylabel(r'$r_\Delta/r_{200}$')
  plt.xscale('log')
  plt.yscale('log')
  plt.show()
