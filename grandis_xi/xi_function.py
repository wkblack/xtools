#!/bin/python

# import 
fname = 'pivot_points.tsv'
import numpy as np
z0,f0,xi0 = np.loadtxt(fname,unpack=True) # flux: [.5--2]keV rest frame
t0 = 400 # seconds

alpha_0,alpha_1,alpha_2,sigma_alpha = -0.12,1.23,.80,.370 # approximate!

# WARNING: only good for z in [0.15-0.9]! 
Z_MIN = 0.15
Z_MAX = 0.9

########################################################################
def plot_pivots(): 
  from matplotlib import pyplot as plt
  fig, ax1 = plt.subplots()
  
  color = 'tab:red'
  ax1.set_xlabel(r'Redshift $z$')
  ax1.set_ylabel(r'$f_0$',color=color)
  ax1.plot(z0,f0,color=color)
  ax1.tick_params(axis='y',labelcolor=color)
  # plt.yscale('log')
  
  ax2 = ax1.twinx()
  color = 'tab:blue'
  # don't need to re-label the x-axis
  ax2.set_ylabel(r'$\xi_0$',color=color)
  ax2.plot(z0,xi0,color=color)
  ax2.tick_params(axis='y',labelcolor=color)
  # plt.yscale('log')
  
  fig.tight_layout()
  plt.show() 

########################################################################
# internal functions: 

def log_uniform(low=.01, high=100., size=None):
  if low<=0 or high<=0: 
    print "ERROR: Cannot take log of non-positive number!"
    raise SystemExit
  if high<low: 
    low,high = high,low
  return np.exp(np.random.uniform(np.log(low),np.log(high),size))

OOB_WARNING = "WARNING: Data out of bounds [%g,%g]" % (Z_MIN,Z_MAX)

def f0_interp(z): 
  if np.any(z<Z_MIN) or np.any(z>Z_MAX): 
    print OOB_WARNING
  return np.interp(z,z0,f0) 

def xi0_interp(z): 
  if np.any(z<Z_MIN) or np.any(z>Z_MAX): 
    print OOB_WARNING
  return np.interp(z,z0,xi0) 

########################################################################
# external function: 

def xi_X(z,fx,t_exp=400): # 400s default scales texp/t0=1
  return xi0_interp(z) * np.exp(alpha_0) \
         * (fx/f0_interp(z))**alpha_1 \
         * (t_exp/t0)**alpha_2
  # times scatter np.exp(+/- sigma_alpha)


if __name__=='__main__': 
  if 1: plot_pivots() 
  
  # test the function
  N=10000 # number of points
  if 1: 
    z = log_uniform(Z_MIN,Z_MAX,N) 
    fx = log_uniform(1e-14,1e-10,N)
    # fx = log_uniform(1e-7,1e-2,N) 
  else: 
    z = np.linspace(Z_MIN,Z_MAX,N)
    fx = np.linspace(1e-7,1e-2,N)
  xi = xi_X(z,fx)
  
  from matplotlib import pyplot as plt
  plt.scatter(z,xi,c=np.log10(fx))
  plt.xlabel(r'Redshift $z$') 
  plt.ylabel(r'Significance $\xi_X$')
  plt.yscale('log') 
  plt.colorbar(label=r'$\log_{10} f_X$') 
  plt.hlines(6.5,Z_MIN,Z_MAX) # draw in the detection line
  plt.tight_layout() 
  plt.show()
  
  print "~fin"
