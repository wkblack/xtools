from matplotlib import pyplot as plt
import numpy as np
import galaxy_id as gid

z0=np.linspace(.05,.35,100); z1=np.linspace(.00,.45,200)

m0=gid.m_star(z0); m1=gid.m_star(z1)

if 1: 
  ax = plt.gca()
  ax.axvline(.1,color='gray',linewidth=1)
  ax.axvline(.3,color='gray',linewidth=1)
  ax.axhline(gid.m_star(.3)+1.75,color='gray',linewidth=1)
  ax.axhline(gid.m_star(.1)+1.75,color='gray',linewidth=1)
  
if 1: 
  plt.scatter(z1,m1,c='r',label=r'Extrapolation of $m_*$')
  plt.scatter(z0,m0,c='k',label=r'Characteristic magnitude $m_*$')
  plt.plot(z0,m0+1.75,'b',label=r'$L_{{\rm cut}} \rightarrow m_*+1.75$')
  plt.xlabel(r'Redshift $z$')
  plt.ylabel(r'Magnitude $m$')
  
  plt.legend()
  plt.show() 

if 0: # less than .2% error between the two
  m0_2014=gid.m_star(z0,params=gid.RykoffEtAl2014)
  plt.scatter(z0,(m0-m0_2014)/m0,c='k')
  plt.show()
