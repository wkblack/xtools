# read in halo and galaxy stats
# put out distance of galaxies from the halo center

########################################################################
# imports

import cosmology as c
from matplotlib import pyplot as plt
import numpy as np
import galaxy_id as gid

########################################################################
# function definitions

def xi(zH,z_i):
  # reads in galaxy redshift + halo redshift
  # spits out difference in line-of-sight distance
  return (c.r(z_i)-c.r(zH)) # comoving Mpc

def rho((raH,decH),(ra_i,dec_i),z_i):
  # returns transverse distance between galaxy and halo line of sight centers
  Theta = c.degrees_between((raH,decH),(ra_i,dec_i))
  return c.from_degrees(Theta,z_i)*(1+z_i) # comoving Mpc

def dist_to_halo((raH,decH),zH,(ra_i,dec_i),z_i):
  a=xi(zH,z_i);b=rho((raH,decH),(ra_i,dec_i),z_i);
  return np.sqrt(a**2+b**2) # comoving Mpc

########################################################################

if __name__=='__main__':
  print "testing distance to halo center"
  # calculate rsq, compare to RHALO
  HID=11922429
  df = gid.members(HID)
  hdl = gid.halo_data_list(HID)
  raH,decH,zH = hdl['ra'],hdl['dec'],hdl['z']
  ra_i,dec_i,z_i = df.TRA,df.TDEC,df.Z
  
  print "r200=%g" % hdl['r200']
  
  from time import time
  print "Calculating r_i"
  start = time()
  r_i = dist_to_halo((raH,decH),zH,(ra_i,dec_i),z_i) # comoving Mpc
  print "elapsed:",(time()-start)
  
  print "Grabbing RHALO"
  h = c.aardvark['H0']/100.
  rH = df.RHALO/h # comoving Mpc
  
  print "Calculating dx"
  start = time()
  r_dx = c.dx_from_delta_theta(ra_i,dec_i,z_i,raH,decH,zH)
  # it's within .005 of the original value, centered about null
  print "elapsed:",(time()-start)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  print "plotting..."
  if 0: # this is what I'd hope would match up... 
    plt.scatter(r_i,rH)
    plt.xlabel(r"newly calculated $r$")
    plt.ylabel(r"RHALO")
    plt.show()
  
  if 0: # compare to old method
    plt.scatter(r_i,(r_dx-r_i)/r_i)
    plt.xlabel(r"newly calculated $r$")
    plt.ylabel(r"reative error in oldly calculated $dx$")
    plt.show() # error is better than one part in 2000!
  
  if 0: # compare to redshift
    fig, axs = plt.subplots(2, 1)
    axs[0].scatter(df.Z,rH)
    axs[0].set_xlabel(r'Redshift $z$')
    axs[0].set_ylabel(r'RHALO')
    
    axs[1].scatter(df.Z,r_i)
    axs[1].set_xlabel(r'Redshift $z$')
    axs[1].set_ylabel(r'new $r$')
    
    fig.tight_layout()
    plt.show()
  
  if 0: # compare on sky
    fig, axs = plt.subplots(1, 2)
    axs[0].scatter(df.TRA,df.TDEC,c=r_i)
    axs[0].set_title(r'color=r_i')
    axs[1].scatter(df.TRA,df.TDEC,c=rH)
    axs[1].set_title(r'color=rH')
    fig.tight_layout()
    plt.show()
  
  if 1: # compare in 3D sky-like space
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    if 1: 
      ax.scatter(df.TRA,df.TDEC,c.r(df.Z),c=r_i)
      ax.set_title(r'newly calculated $r_i$')
    else:
      ax.scatter(df.TRA,df.TDEC,c.r(df.Z),c=rH)
      ax.set_title(r'RHALO $r_H$')
    ax.set_xlabel(r'RA')
    ax.set_ylabel(r'DEC')
    ax.set_zlabel(r'$z$')
    plt.show()
  
  if 0: # compare rho to rhalo
    rho_i = rho((raH,decH),(ra_i,dec_i),z_i)
    MAX = max(max(rho_i),max(rH))
    plt.plot([0,MAX],[0,MAX],'k')
    plt.scatter(rho_i,rH)
    plt.xlabel(r'$\rho_i$ (comoving Mpc)')
    plt.ylabel(r'RHALO (comoving Mpc)')
    plt.show()
  
  print '~fin'
