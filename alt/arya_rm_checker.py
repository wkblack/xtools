import pandas as pd
import matplotlib.pylab as plt
from astropy.table import Table

RM = '/home/wkblack/projects/xproject/SkyCatalogues/RM3/'
fname = 'aardvark-1.0-dr8_run_redmapper_v6.3.3_lgt20_catalog.fit'
fpath = RM + fname

dat = Table.read(fpath, format='fits')['MEM_MATCH_ID', 'LAMBDA_CHISQ']
dfRM = dat.to_pandas()
dfRM['CLUSTERID'] = dfRM['MEM_MATCH_ID']

fname = 'RM_training.csv'

df = pd.read_csv(fname)


df = df.join(dfRM.set_index('CLUSTERID'), on='CLUSTERID')

plt.plot([20, 200], [20, 200], c='k', lw=3.0)
plt.plot([20, 200], [20, 20], '--', c='k', lw=3.0)
plt.plot(df.LAMBDA, df.LAMBDA_CHISQ, '.')

plt.xlabel('LAMBDA [Arya]', size=20)
plt.ylabel('LAMBDA_CHISQ [RM]', size=20)
plt.show() 
