#!/bin/python
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Fits analysis program 
# looking at CRPERLx
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

print "Analyzing CR PER Lx cube"
from time import time
from sys import stdout

print "Importing libraries...\r",
stdout.flush(); start = time() 

from astropy.io.fits import open as fitsopen
from os.path import isfile, join
from os import listdir
from sys import argv
from numpy import log
from numpy import log10
from numpy import array
from numpy import arange
from matplotlib import pyplot as plt

end = time() 
print "Importing libraries took %g seconds" % (end-start) 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

files = ["/home/wkblack/projects/xproject/XTRA/" \
           + "parameters/Models/L_to_Flux/CRb05-2_overdl2.fits"]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
for f in files: 
  try: 
    hdulist = fitsopen(f) 
  except IOError: 
    print "Error: couldn't read in %s" % f
    continue
  
  hdulist.info() 
  
  # headers only show in interactive script
  for i in range(len(hdulist)): 
    print "list %i keys: %s" % (i,hdulist[i].header.keys())
  
  # new junk: 
  cube = hdulist[1].data
  ZCUBE = cube[0]['Z']
  lgTCUBE = log10(cube[0]['T'])
  L_CUBE = cube[0]['CR_PERL_TIMESD2']
  del cube 
  
  if 0: 
    print "z_cube",ZCUBE
    print "lg T_cube",lgTCUBE
    print "L_cube",L_CUBE
  
  if 0: 
    print "length of ZCUBE",len(ZCUBE)     # 300
    print "length of lgTCUBE",len(lgTCUBE) #  39 
    print "length of L_CUBE",len(L_CUBE)   # 300 
  
  if 0: 
    # plt.scatter(ZCUBE,L_CUBE)
    plt.scatter(range(len(ZCUBE)),ZCUBE)
    plt.show() 
    
    plt.scatter(range(len(lgTCUBE)),lgTCUBE)
    plt.show() 
    
    x=L_CUBE
    y=L_CUBE
    l=range(len(L_CUBE))
    plt.scatter(x,y)
    plt.show() 
    plt.scatter(l,y)
    plt.show() 
  
  raise SystemExit
  
  
  
  
  
  
  
  
  
  
  # my falsified input 
  z = arange(.1,1.3,.1)
  lnTx = arange(-2.5,0,.1)
  
  # code from theWheel
  iZ = array(float(len(ZCUBE) - 1) * (z - ZCUBE[0]) \
         / (ZCUBE[-1] - ZCUBE[0])).astype(int) # list of z-indices
  
  lg10Tx = lnTx/log(10.0)
  ilgT = array(float(len(lgTCUBE) - 1) * (lg10Tx - lgTCUBE[0]) \
         / (lgTCUBE[-1] - lgTCUBE[0])).astype(int) # list of lg10Tx-indices
  # iNH=0
  
  Z2 = ZCUBE[iZ + 1]
  Z1 = ZCUBE[iZ]
  
  T2 = lgTCUBE[ilgT + 1]
  T1 = lgTCUBE[ilgT]
  
  L11 = [L_CUBE[iZ[index]][ilgT[index]] for index in range(len(iZ))]
  L12 = [L_CUBE[iZ[index]+1][ilgT[index]] for index in range(len(iZ))]
  L21 = [L_CUBE[iZ[index]][ilgT[index]+1] for index in range(len(iZ))]
  L22 = [L_CUBE[iZ[index]+1][ilgT[index]+1] for index in range(len(iZ))]
  
  # count rates per Lx
  CRPERLx = (L11 * (Z2 - z) * (T2 - lg10Tx) 
           + L21 * (z - Z1) * (T2 - lg10Tx) 
           + L12 * (Z2 - z) * (lg10Tx - T1) 
           + L22 * (z - Z1) * (lg10Tx - T1)) / ((Z2 - Z1) * (T2 - T1))
  
  Fx = CRPERLx * Lx
  lnFx = log(Fx+1e-40)
  
  print "Fx calculated!   "

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
