#!/bin/python
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# theWheel.py
# William Black, Feb 2018
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Battleplan: 
# 1. Read in fits file with lambda &al vars
# 2. Get mass
# 3. Get Tx, Lx, Fx(z)
# 4. Plot Tx vs lambda for different flux values 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

print "the Wheel"
divider = "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # "
print divider

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# imports 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# from __future__ import print_function
from time import time
from sys import stdout #import flush # to overwrite lines

print "Importing libraries...\r",
stdout.flush(); start = time() 
from astropy.io import fits # to import data
import matplotlib
from matplotlib import pyplot as plt # to make plots 
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
from os import system as run # to run terminal commands 
import numpy as np
import numpy.random as npr

end = time() 
print "Importing libraries took %g seconds" % (end-start) 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


########################################################################
# 1. Read in fits file with lambda &al vars
########################################################################

print divider
print "Phase 1: Read in fits file and vet" 

fdir = '/home/wkblack/projects/xproject/SkyCatalogues/RM/'
fname = 'redmapper_dr8_public_v6.3_catalog.fits'
# columns: 
types = """
TTYPE1  = 'ID      '           /
TTYPE2  = 'NAME    ' (string)  /
TTYPE3  = 'RA      '           /
TTYPE4  = 'DEC     '           /
TTYPE5  = 'Z_LAMBDA'           /
TTYPE6  = 'Z_LAMBDA_ERR'       /
TTYPE7  = 'LAMBDA  '           /
TTYPE8  = 'LAMBDA_ERR'         /
TTYPE9  = 'S       '           /
TTYPE10 = 'Z_SPEC  '           /
TTYPE11 = 'OBJID   '           /
"""

# compile data
hdulist = fits.open(fdir+fname)
hdulist1data = hdulist[1].data
print "Read in %s" % fname

while 0: # create plots with the above axes 
  print types
  y_axis = raw_input("Which y-axis? ") 
  x_axis = 'Z_LAMBDA'
  if y_axis=='': # exit case 
    raise SystemExit
  plt.scatter(hdulist1data[x_axis],hdulist1data[y_axis],1,'k')
  plt.xlabel(x_axis) 
  plt.ylabel(y_axis) 
  plt.show() 

# vet fields 
print "Vetting fields...\r",
stdout.flush(); start = time() 

fields = ["ID","NAME","RA",'DEC','Z_LAMBDA','Z_LAMBDA_ERR', \
          'LAMBDA','LAMBDA_ERR','S','Z_SPEC','OBJID']
# data = np.array([[line[i] for i in fields] 
#                           for line in hdulist1data]) # TOO SLOW!
data = np.transpose(np.array(
            [hdulist1data[field] for field in fields]))
del hdulist1data
del hdulist

end = time() 
print "Vetting fields took %g seconds" % (end-start)




if 0: # grab masses from clusters
  print "Grabbing masses from clusters"
  cluster_fname = 'RM_training.csv'
  # list of columns: 
  # { CLUSTERID,M_HALOID1,M_HALOID2,M_HALOID3,M_HALOID4,M_HALOIDN,
  #   STR1,STR2,STR3,STR4,STRO,STR_EXP,LAMBDA,Z,
  #   CENTRAL_FLAG,CENTRAL_BCG_FLAG,RA,DEC,R_LAMBDA,LAMBDA_ERR,M_1,M_2 }
  desired_cols = (0,1,12,13,16,17) # (C_ID,H_ID,lam,z,ra,dec)
  cluster_data = np.loadtxt(cluster_fname,delimiter=',',
                            skiprows=1,usecols=desired_cols)
  # from this list, grab the halo to associate with the cluster ... 
  # match cluster_data[1] =? data[i] (match IDs), then get data[j] (mass)
  # FIXME: this can be vectorized, but for now... 
  new_data = 0 # TODO ...
  for cluster in clusters: 
    new_data.add_data(halos_data[cluster[1]])
  print "Exiting..."
  raise SystemExit




# vet z data 
print "Vetting by redshift...\r",
stdout.flush(); start = time() 

before=len(data)
[ID,NAME,RA,DEC,Z_LAMBDA,Z_LAMBDA_ERR,\
 LAMBDA,LAMBDA_ERR,S,Z_SPEC,OBJID] = range(len(fields))
mask = np.array([ (float(z)>=0.1 and float(z)<=0.3) \
                   for z in data[:,Z_LAMBDA] ] )
# mask_size = sum(mask)
data = data[mask] 
end = time() 
after=len(data)
print 'Vetting by redshift took %g seconds and compressed %.3g percent'\
      % ((end-start),((1-after/before)*100))


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# 2. Calculate mass using equation accompanying Fig 5 in Farahi 2016: 
# "Galaxy cluster mass estimation from stacked spectroscopic analysis"
# at https://academic.oup.com/mnras/article/460/4/3900/2609061
#
# <ln M|lambda,z> = pi_h + alpha_h log(lambda/lambda_p) 
#                        + beta_h log((1+z)/(1+z_p)) 
# units here are solar masses (or rather, ln Msol)
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

print divider 
print "Phase 2: Calculate mass"

MxS = { # mass scaling parameters
  "alpha_h":1.33,
  "beta_h":-0.48,
  # lambda_p and z_p are defined just under eqn 5 ibid
  # TODO: decide if these two should be calculated on the fly... 
  "lam_p":30, # approximate median cluster richness of our sample 
  "z_p":0.2, # approximate median cluster redshift of our sample
  # TODO: check formatting here: pi_h = np.log(1.26[e14 Msol]) 
  "pi_h":np.log(1.26e14),
  "sig":0.1,
  "adsurl":"https://academic.oup.com/mnras/article/460/4/3900/2609061"
  # Farahi 2016 
}

print "using pi_h: %g" % MxS["pi_h"]

lam = np.array(data[:,LAMBDA]).astype(np.float)
print "data median lambda: %g" % np.median(lam) 
print "using Arya's lambda: %g" % MxS["lam_p"]

z = np.array(data[:,Z_LAMBDA]).astype(np.float)
print "data median redshift: %g" % np.median(z) 
print "using Arya's redshift: %g" % MxS["z_p"]

n_data = len(z) 

# add a gaussian scatter ~ 1/sqrt lambda
scatter_sigma = MxS['sig']/np.sqrt(lam)
multiplier = 10
lnM = MxS["pi_h"] + MxS["alpha_h"]*np.log(lam/MxS["lam_p"]) \
                  + MxS["beta_h"]*np.log((1+z)/(1+MxS["z_p"])) # \
                  # + npr.normal(0.0, multiplier*scatter_sigma, n_data)
M = np.exp(lnM) 
print "M calculated!"


if 0: 
  plt.scatter(np.log(lam),lnM,1,'k')
  plt.xlabel('ln lambda') 
  plt.ylabel('ln M') 
  plt.title('sigma multiplier: %g' % multiplier)
  plt.show() 

if 0: 
  # create a new fits file - I'm foregoing this for now
  # I may just keep it all in this file, 
  # but it would be nice to eventually save it as a new fits or csv file
  
  # create mass header 
  hdulist[1].header.set('TTYPE31','MASS') 
  hdulist[1].header.set('TFORM31','E') 
  col_mass = fits.Column(name='MASS', format='E', array=M)
  
  # from https://github.com/astropy/astropy/issues/6649
  hdulist[1] = fits.BinTableHDU.from_columns(hdulist[1].columns + col_mass)
  hdulist.writeto('newtable.fits') 
  print 'created new table, including mass'
  print 'now call XTRA or write my own routines' 
  
  # data.concatenate('MASS',col_mass)
  
  # data['MASS'] = col_mass







# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# 3. Get Tx, Lx, Fx(z)
# this will likely be from XTRA, so I think I'll need to save as fits
# and then grab the fits output and re-load it. 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

print divider
print "Phase 3: Solving for Lx, Tx, Fx(z)"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# begin cosmology calculator and definitions 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# universal parameters
c=299792458/1000 # exact CODATA definition, in km/s
pc = 648000/np.pi # au
au = 149597870700 # m
m_in_Mpc = pc*au*10**6 # exact
s_in_Myr = 60*60*24*365.25*10**6 # apx

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# cosmological models

benchmark = {
  "H0": 70,
  "WL": 0.7,
  "Wm": 0.3,
  "Wr": 0,
  "Wk": 0,
  "wDE": -1
}

planck2018 = {
  # Planck 2018 parameters (https://arxiv.org/pdf/1807.06209.pdf):
  "H0": 67.66,  # pm .42 (plus or minus .42)
  "WL": 0.6889, # pm 0.0056
  "Wm": 0.3166, # pm 0.0084
  "Wr": 9.23640e-5, # calculated
  "Wk": 0.001,  # pm 0.002
  "wDE": -1.03  # pm 0.03
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# derived parameters

def d_H(params=planck2018): # Hubble distance
  return c/params["H0"] # Mpc

def t_H(params=planck2018): # Hubble time
  return m_in_Mpc/params["H0"]/10**6/s_in_Myr # Gyr

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def E(z,params=planck2018):
  # Evolutionary factor 
  return np.sqrt(params["Wm"]*(1+z)**3 \
               + params["WL"]*(1+z)**(3*(1+params["wDE"])) \
               + params["Wr"]*(1+z)**4 \
               - params["Wk"]*(1+z)**2)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Calculate Lx 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

LxS = { # Lx scaling parameters
  "M_p":6e14,
  "z_p":0.35,
  "a":1.087e45,
  "M_slope":1.26, # ln(erg s**-1)
  "E_slope":1.20, # ln(erg s**-1)
  "sig":0.24,
  "adsurl": "http://adsabs.harvard.edu/abs/2016MNRAS.456.4020M"
}

cosmology = planck2018

E_vals = E(z,cosmology) 
Ep_val = E(LxS["z_p"],cosmology)  
Mp = LxS["M_p"] * cosmology["H0"] 

# TODO: figure out what Halos.M500 corresponds to: 
#       is it log mass, mass, log10 mass, or something else?
#       for now, I'm just using "M". 
lnLx = np.log(LxS['a']) + LxS['M_slope'] * np.log(M/Mp) \
         + LxS['E_slope'] * np.log(E_vals/Ep_val) \
         + npr.normal(0.0, LxS['sig'], n_data) \
         - 44*np.log(10.0)
Lx = np.exp(lnLx)

print "Lx calculated!" 


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Calculate Tx 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

TxS = { # Tx scaling parameters
  "M_p":6e14,
  "z_p":0.35,
  "a":8.84,
  "M_slope":0.66, # kT/keV
  "E_slope":0.61, # kT/keV
  "sig":0.13,
  "adsurl":"http://adsabs.harvard.edu/abs/2016MNRAS.456.4020M"
}

Ep_val = E(TxS["z_p"],cosmology)  
Mp = TxS["M_p"] * cosmology["H0"] 

lnTx = np.log(TxS['a']) + TxS['M_slope']*np.log(M/Mp) \
            + TxS['E_slope'] * np.log(E_vals/Ep_val) \
            + npr.normal(0.0,TxS['sig'], n_data)
Tx = np.exp(lnTx) 


print "Tx calculated!" 


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Calculate Fx 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# computed in XTRA/source/Solver/LxTx_Solver.py +54
# github.com/afarahi/XTRA/blob/master/source/Solver/LxTx_Solver.py
# following from /XTRA/source/./Objects/XTRA_Xray_Band_Class.py +111

print "Calculating Fx...\r",
XTRAdir = '/home/wkblack/projects/xproject/XTRA/'
cubepath = 'parameters/Models/L_to_Flux/CRb05-2_overdl2.fits'
cube = fits.open(XTRAdir + cubepath)[1].data
ZCUBE = cube[0]['Z']
lgTCUBE = np.log10(cube[0]['T'])
L_CUBE = cube[0]['CR_PERL_TIMESD2']
del cube # to clear up some space # TODO: delete earlier fits files

iZ = np.array(float(len(ZCUBE) - 1) * (z - ZCUBE[0]) \
       / (ZCUBE[-1] - ZCUBE[0])).astype(int) # list of z-indices

lg10Tx = lnTx/np.log(10.0)
ilgT = np.array(float(len(lgTCUBE) - 1) * (lg10Tx - lgTCUBE[0]) \
       / (lgTCUBE[-1] - lgTCUBE[0])).astype(int) # list of lg10Tx-indices
# iNH=0

Z2 = ZCUBE[iZ + 1]
Z1 = ZCUBE[iZ]

T2 = lgTCUBE[ilgT + 1]
T1 = lgTCUBE[ilgT]

L11 = [L_CUBE[iZ[index]][ilgT[index]] for index in range(len(iZ))]
L12 = [L_CUBE[iZ[index]+1][ilgT[index]] for index in range(len(iZ))]
L21 = [L_CUBE[iZ[index]][ilgT[index]+1] for index in range(len(iZ))]
L22 = [L_CUBE[iZ[index]+1][ilgT[index]+1] for index in range(len(iZ))]

# count rates per Lx
CRPERLx = (L11 * (Z2 - z) * (T2 - lg10Tx) 
         + L21 * (z - Z1) * (T2 - lg10Tx) 
         + L12 * (Z2 - z) * (lg10Tx - T1) 
         + L22 * (z - Z1) * (lg10Tx - T1)) / ((Z2 - Z1) * (T2 - T1))

Fx = CRPERLx * Lx
lnFx = np.log(Fx+1e-40)
log10Fx = np.log10(Fx+1e-40)

print "Fx calculated!   "

if 0: # check CRPERLx cube
  print "Checking CRPERLx cube..."
  
  if 0: 
    count = range(len(lgTCUBE))
    plt.scatter(count,lgTCUBE)
    plt.title('lgTCUBE') 
    plt.show()
    # this is linear
  
  if 0: 
    count = range(len(ZCUBE))
    plt.scatter(count,ZCUBE)
    plt.title('ZCUBE') 
    plt.show()
    # also linear
  
  # L_CUBE shape: (300,39) 
  if 0: 
    # xx = np.array(L_CUBE) 
    # x_len = 300
    # y_len = 39
    # for i in range(y_len): 
    #   plt.scatter(range(x_len),i*np.ones(y_len),c=xx[:,i])
    # 
    # plt.title('L_CUBE') 
    # plt.show()
    plt.imshow(np.transpose(np.log(L_CUBE)))
    plt.title("log L_CUBE")
    plt.colorbar()
    plt.show() 
    # looks regular 
  

if 0: # plot Lx
  sc = plt.scatter(lam,Lx,s=1,c=log10Fx,alpha=.75)
  plt.colorbar(sc,label='log$_{10}$ $F_x$') 
  plt.xlabel('$\lambda$') 
  plt.xscale('log') 
  plt.xlim(min(lam),max(lam))
  plt.ylabel('$L_x$ (ergs/s)') 
  plt.yscale('log') 
  # plt.ylim(min(Lx),max(Lx))
  plt.ylim(10**-3,max(Lx))
  plt.tight_layout()
  plt.show() 

if 0: # plot Tx
  sc = plt.scatter(lam,Tx,s=1,c=log10Fx,alpha=.75)
  plt.colorbar(sc,label='log$_{10}$ $F_x$') 
  plt.xlabel('$\lambda$') 
  plt.xscale('log') 
  plt.xlim(min(lam),max(lam))
  plt.ylabel('$T_x$ (keV)') 
  plt.yscale('log') 
  plt.tight_layout()
  plt.show() 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Save data
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

print divider 
print "Phase 4: Save data"

newfields = ["MASS","Lx","Tx","Fx"] 
newdata = np.transpose([M,Lx,Tx,Fx])

# add new fields to data
outfields = np.concatenate((fields,newfields)) 
outhead = "  ".join(outfields) 
outdata = np.concatenate((data,newdata),axis=1)

outname = "output_theWheel.csv"
# TODO: fix so it can save text---or just omit some string fields
np.savetxt(outname,outdata,delimiter=',',header=outhead,fmt='%s') 
print "Saved %s" % outname

print '~fin~' 
