#!/bin/python
# create likelihood function P(Delta lambda_i)
########################################################################


########################################################################
# file paths 
from os.path import isfile

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
filedir = '/home/wkblack/projects/xproject/Farahi2019DES/'
filename1='chandra_rm_DESY1_ss2.csv'; f_Ch = filedir + filename1
filename2='xmm_rm_DESY1_ss2.csv'; f_XMM = filedir + filename2
assert(isfile(f_Ch)) # check Chandra file
assert(isfile(f_XMM)) # check XMM file

xtools = '/home/wkblack/projects/xproject/xtools/'
filename3 = 'output_bicycle.csv'; f_Aa = xtools + filename3
assert(isfile(f_Aa))


########################################################################
# read in files
import pandas as pd

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
df_Ch = pd.read_csv(f_Ch)   # Chandra dataset
df_XMM = pd.read_csv(f_XMM) # XMM dataset
df_Aa = pd.read_csv(f_Aa)   # Aardvark dataset

########################################################################
# bin data by lambda, see ratios of counts
import numpy as np

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
lambda_obs = np.concatenate([df_Ch['lambda'].values,
                            df_XMM['lambda'].values])
lambda_Aa = df_Aa['lambda'].values

from matplotlib import pyplot as plt
for i in np.arange(9,15,2): 
  counts, bins, bars = plt.hist(np.log(lambda_obs),bins=i,
                                alpha=.5,label="%i bins" % i)
if 0: 
  plt.xlabel(r'$\log \lambda$')
  plt.legend() 
  plt.show() 

# print '[counts] [bins] :',counts,bins
# print 'min/max :',min(np.log(lambda_obs)),max(np.log(lambda_obs))


prob = counts/sum(counts)

def find_nearest_idx(array, value):
    return (np.abs(array - value)).argmin()

def P(lam): 
  # reads in given lambda
  # returns ~probability of being in C+X
  
  # find out which bin I'm in (if I'm in the tails, set detection to zero)
  idx = np.array([find_nearest_idx(bins,np.log(ll)) for ll in lam])
  return np.where(np.logical_or(idx<=0,idx>=len(counts)),
                  0,(counts[idx-1]+counts[idx])/2.0)
  # 
  # if idx<=0 or idx>=len(counts): # it's in the tails, so throw it out
    # return 0
  # else: # return the value of that bin as a ratio of the total counts
    # return (counts[idx-1]+counts[idx])/2.0
  
l = np.arange(1,220,3)
plt.clf()
plt.plot(l,P(l))
plt.xlabel(r'$\log \lambda$')
plt.show() 

# plt.hist(lambda_Aa)
# plt.show() 




print "~fin"
