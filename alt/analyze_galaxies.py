# check field galaxy contents ultimately 
import pixel_analysis as pa
import galaxy_id as gid
import config as cfg
import pandas as pd
import numpy as np

from matplotlib import pyplot as plt
from astropy.io import fits
from time import time

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def field_fits_from_all(sim=cfg.Buzzard,printing=True,use_hid=False):
  gf = sim['galaxies']
  # initialize for gf[0]
  start=time()
  if printing: print "reading in 1/%g" % len(gf)
  data_gals = fits.open(gf[0])[1].data # read in all data
  if use_hid:
    data_gals = data_gals[data_gals['HALOID']<1] # select field gals
  else:
    data_gals = data_gals[data_gals['RHALO']>data_gals['R200']]
  data_gals = data_gals[np.logical_and(.1<data_gals['Z'],
                                          data_gals['Z']<.3)]
  if printing: print "elapsed time = %g s" % (time()-start)
  
  try:
    for i in range(1,len(gf)):
      start=time()
      if printing: print "reading in %i/%i" % (i+1,len(gf))
      # concatenate datasets for field galaxies onto gf[0]
      data_i = fits.open(gf[i])[1].data
      if use_hid:
        data_i = data_i[data_i['HALOID']<1] # select field gals
      else:
        data_gals = data_gals[data_gals['RHALO']>data_gals['R200']]
      data_i = data_i[np.logical_and(.1<data_i['Z'],
                                        data_i['Z']<.3)]
      data_gals = np.concatenate((data_gals,data_i)) # tack onto end
      if printing: print "elapsed time = %g s" % (time()-start)
  except KeyboardInterrupt:
    print "returning partial dataset"
    pass
  
  return data_i

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

if __name__=='__main__':
  z_mask=(.1,.3)
  mu_limit=None
  
  if 0: # make CM plots
    if 1:
      df = pa.df_for_CM_plot(32,range(2**7),z_mask,mu_limit,field=False,printing=True)
      # df = pa.df_for_CM_plot(32,0,z_mask,mu_limit,field=False,printing=True)
    else: # read in all field galaxies
      data = field_fits_from_all()
      print 'size:',len(data)
      df = gid.fits_to_pandas(data)
      df = df[np.logical_and(.1<df.Z,df.Z<.3)]
      df = gid.vet_by_m_star(df)
    
    # for a set of mu_limits, make these plots
    step = .5
    for ii in range(0,7):
      limit = (13+ii*step,13+(ii+1)*step)
      pa.CM_plot(df,z_mask,mu_limit=limit)
  
  else: # make HNN plots
    pa.plot_fR(32,range(2**7),printing=True,stacking=False)
    # plot
  
  print 'fin!!'
  raise SystemExit

