#!/bin/python
########################################################################
# testing matching file 
from time import time
from sys import stdout #import flush # to overwrite lines

print "Importing libraries...\r",
stdout.flush(); start = time() 

from os.path import isfile
from astropy.io import fits # to import data
import numpy as np

end = time() 
print "Importing libraries took %g seconds" % (end-start) 
########################################################################

########################################################################
# here are all the files 

import config as cfg
cluster_path = cfg.skymaps + 'Aa_truth_galaxy_catalogs/Aardvark_v1.0c_truth_des_rotated.0.fit'
matching_path = cfg.xtools + 'RM_training_fixed.csv'
halo_path = cfg.skymaps + 'RM/redmapper_dr8_public_v6.3_catalog.fits'
# halo_path = '/nfs/phys1/aryaf/aardvark_v1.0/halos' # here are all of the halo truth catalogs

assert isfile(cluster_path),"Error with cluster path"
assert isfile(matching_path),"Error with matching path"
assert isfile(halo_path),"Error with halo path"
# all seem okay :) 
########################################################################

########################################################################
# the goal here is to test the cluster truth catalog, 
# to see if I can match halos to clusters through the matching file. 
# since I'm missing the rest of the cluster truth catalog though, 
# I'll just see if I can find any matches. If yes, great! 
# Then I can check if the matching file already contains the information I need, 
# or if I really do need all the rest of the truth tables to do this. 
########################################################################

########################################################################
# import halo truth table
########################################################################

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
hdulist = fits.open(halo_path)
hdulist1data = hdulist[1].data
print "Read in %s" % halo_path
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
print "Vetting fields...\r",
stdout.flush(); start = time() 
fields = ["ID","NAME","RA",'DEC','Z_LAMBDA','Z_LAMBDA_ERR', \
          'LAMBDA','LAMBDA_ERR','S','Z_SPEC','OBJID']
halo_data = np.transpose(np.array(
            [hdulist1data[field] for field in fields]))
del hdulist1data
del hdulist
end = time() 
print "Vetting fields took %g seconds" % (end-start)
print "halo shape:",np.shape(halo_data)
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

########################################################################
# import cluster truth table
########################################################################

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
hdulist = fits.open(cluster_path)
hdulist1data = hdulist[1].data
print "Read in %s" % cluster_path
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
print "Vetting fields...\r",
stdout.flush(); start = time() 
all_fields = ['ID', 'INDEX', 'ECATID', 'COEFFS', 'TMAG', 'OMAG', 'FLUX', 'IVAR', 'OMAGERR', 'AMAG', 'RA', 'DEC', 'Z', 'HALOID', 'RHALO', 'M200', 'NGALS', 'R200', 'CENTRAL', 'TRA', 'TDEC', 'EPSILON', 'GAMMA1', 'GAMMA2', 'KAPPA', 'MU', 'LMAG', 'MAG_U', 'SIZE', 'ARBORZ', 'ARBORZ_ERR', 'ANNZ', 'ANNZ_ERR', 'PHOTOZ_GAUSSIAN', 'PX', 'PY', 'PZ', 'VX', 'VY', 'VZ', 'PSTAR', 'PQSO', 'TE', 'TSIZE']
fields = ['ID', 'INDEX','RA', 'DEC', 'Z', 'HALOID','M200']
cluster_data = np.transpose(np.array(
            [hdulist1data[field] for field in fields]))
del hdulist1data
del hdulist
end = time() 
print "Vetting fields took %g seconds" % (end-start)
print "cluster shape:",np.shape(cluster_data)
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

########################################################################
# import matching data

fields = (0,1,12,13,16,17) # (C_ID,H_ID,lam,z,ra,dec)
matching_data = np.loadtxt(matching_path,delimiter=',',skiprows=1) #,usecols=fields)
print "Read in %s" % matching_path
print "matching shape:",np.shape(matching_data)
print 'first line:',matching_data[0]

for match in matching_data: 
  cid=match[0]; hid=match[1]; lam=match[2]
  print "Cluster ID: %i" % cid
  print "Halo ID: %i" % hid
  print "Lambda: %g" % lam
  
  # find a halo with a cluster ID
  for halo in halo_data: 
    # print 'halo type:',type(halo[0])
    # print 'hid type:',type(hid)
    # raise SystemExit
    if int(halo[0])==int(hid): 
      print 'yipee!'
      raise SystemExit
    elif abs(float(halo[0])-float(hid))<.5: 
      print 'yay!'
      raise SystemExit
  
  if 0: # the cluster list is just fine
    for cluster in cluster_data: 
      if cluster[0]==cid: 
        print 'yipee!'
        raise SystemExit
