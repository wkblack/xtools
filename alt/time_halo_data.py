# time importing / masking data for halo

print "importing time"
from time import time
print "importing config"
import config as cfg
print "importing astropy"
from astropy.io import fits
# import galaxy_id as gid
# HID = gid.test_HID
print "importing HID"
from galaxy_id import test_HID2 as HID
print "finished imports"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def f1(): 
  for f in cfg.halo_files:
    IDs = fits.open(f)[1].data['HALOID']
    if HID not in IDs: continue
    dat = fits.open(f)[1].data[IDs==HID]
    return dat['Z'][0],dat['RA'][0],dat['DEC'][0],dat['R200'][0]
  return -1

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def f2(): 
  for f in cfg.halo_files:
    try: 
      dat = fits.open(f)[1].data
      dat = dat[dat['HALOID']==HID][0]
      return dat['Z'],dat['RA'],dat['DEC'],dat['R200']
    except IndexError: 
      continue
    # return dat[dat['HALOID']==HID]['Z'][0],dat[dat['HALOID']==HID]['RA'][0],dat[dat['HALOID']==HID]['DEC'][0],dat[dat['HALOID']==HID]['R200'][0]
  return -1

if __name__=='__main__': 
  print "starting time tests"
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  start=time(); f1(); t1=time()-start
  start=time(); f2(); t2=time()-start
  start=time(); f1(); t3=time()-start
  start=time(); f2(); t4=time()-start
  start=time(); f1(); t5=time()-start
  start=time(); f2(); t6=time()-start
  start=time(); f1(); t7=time()-start
  start=time(); f2(); t8=time()-start
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  
  print "f1:",t1,t3,t5,t7
  print "f2:",t2,t4,t6,t8
  
  from numpy import median
  print "f1 median time:",median([t1,t3,t5,t7])
  print "f2 median time:",median([t2,t4,t6,t8])
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # so they're really about the same... 
