#!/bin/python
########################################################################
# analyze output of theWheel.py
# take columns of data and find the slope and error
# wkblack (c) 2019.03.15
########################################################################

# for loading the file and computing slope & scatter
import numpy as np 
import linmix 

# for timing the process and printing prettily 
from time import time
from sys import stdout

fname="output_theWheel.csv"
silent_mcmc=1    # bool: silence mcmc output
plotting=0       # bool: make plots and exit 
subset=0         # bool: grab a subset of the data
subset_size=100  # int:  size of subset of points to grab

from os import sys
if len(sys.argv)>1: # read in flux (log10(Fx)) floor from command line 
  log10flux_floor=float(sys.argv[1])
else: # default flux limit below the lowest value in the set
  log10flux_floor=-23.0

########################################################################
# read in data and select columns 

# data has columns: 
# ID  NAME    RA     DEC   Z_LAMBDA  Z_LAMBDA_ERR  LAMBDA  LAMBDA_ERR  
#  S  Z_SPEC  OBJID  MASS  Lx        Tx            Fx
[ID,NAME,RA,DEC,Z_LAMBDA,Z_LAMBDA_ERR,LAMBDA,LAMBDA_ERR,
 S,Z_SPEC,OBJID,MASS,Lx,Tx,Fx] = range(15) 
grab = (RA,DEC,Z_LAMBDA,Z_LAMBDA_ERR,LAMBDA,LAMBDA_ERR,MASS,Lx,Tx,Fx) 
data = np.loadtxt(fname,skiprows=1,usecols=grab,delimiter=",")
# print np.shape(data) # = (6647, 10)
# relabel variables
[RA,DEC,Z_LAMBDA,Z_LAMBDA_ERR,
 LAMBDA,LAMBDA_ERR,MASS,Lx,Tx,Fx] = range(10)

########################################################################
# plotting

if plotting: 
  # Lx , Tx , lam
  print "importing...\r",
  start=time(); stdout.flush()
  import matplotlib
  from matplotlib import pyplot as plt
  end=time(); elapsed=end-start;
  print "Importing took %g seconds." % elapsed
  
  if 0: # plot Lx
    sc = plt.scatter(data[:,LAMBDA],data[:,Lx],s=1,
                     c=np.log10(data[:,Fx]),alpha=.75)
    plt.colorbar(sc,label='log$_{10}$ $F_x$') 
    plt.xlabel('$\lambda$') 
    plt.xscale('log') 
    plt.xlim(min(data[:,LAMBDA]),max(data[:,LAMBDA]))
    plt.ylabel('$L_x$ (ergs/s)') 
    plt.yscale('log') 
    # plt.ylim(min(data[:,Lx]),max(data[:,Lx]))
    plt.ylim(10**-3,max(data[:,Lx]))
    plt.ylim(min(data[:,Lx]),max(data[:,Lx]))
    plt.tight_layout()
    plt.show() 
  
  if 0: # plot Tx
    sc = plt.scatter(data[:,LAMBDA],data[:,Tx],s=1,
                     c=np.log10(data[:,Fx]),alpha=.75)
    plt.colorbar(sc,label='log$_{10}$ $F_x$') 
    plt.xlabel('$\lambda$') 
    plt.xscale('log') 
    plt.xlim(min(data[:,LAMBDA]),max(data[:,LAMBDA]))
    plt.ylabel('$T_x$ (keV)') 
    plt.yscale('log') 
    plt.ylim(min(data[:,Tx]),max(data[:,Tx]))
    plt.tight_layout()
    plt.show() 
  
  if 1: # plot both
    fig, axes = plt.subplots(nrows=2,ncols=1)
    [ax1,ax2] = axes.flat
    ax2.set_xlabel('$\lambda$') 
    im1 = ax1.scatter(data[:,LAMBDA],data[:,Tx],s=1,
                      c=np.log10(data[:,Fx]),alpha=.75)
    ax1.set_ylabel('$T_x$ (keV)') 
    im2 = ax2.scatter(data[:,LAMBDA],data[:,Lx],s=1,
                      c=np.log10(data[:,Fx]),alpha=.75)
    ax2.set_ylabel('$L_x$ (ergs/s)') 
    ax2.set_ylim(10**-3,max(data[:,Lx]))
    
    ax1.set_xscale('log') 
    ax2.set_xscale('log') 
    ax1.set_xlim(min(data[:,LAMBDA]),max(data[:,LAMBDA]))
    ax2.set_xlim(min(data[:,LAMBDA]),max(data[:,LAMBDA]))
    ax1.set_yscale('log') 
    ax2.set_yscale('log') 
    
    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
    fig.colorbar(im2, cax=cbar_ax,label='log$_{10}$ $F_x$') 
    # plt.tight_layout()
    plt.show() 
    plt.savefig('combined.png') 
  
  raise SystemExit




########################################################################
# set flux floor and plot log10Tx vs log10Lambda, 
# figure out how the slope changes with floor

x=np.log10(data[:,LAMBDA])
xsig=data[:,LAMBDA_ERR]/data[:,LAMBDA]/np.log(10) # from d(lnx)=dx/x
y=np.log10(data[:,Tx])
ysig=.1*np.ones(len(y)) # FIXME: get real value
log10flux=np.log10(data[:,Fx])

if len(sys.argv)>1: # vet data with flux floor
  mask = log10flux > log10flux_floor
  x=x[mask]
  xsig=xsig[mask]
  y=y[mask]
  ysig=ysig[mask]
  print "Masked below log10(Fx)=%g" % log10flux_floor

if subset: # grab a subset of the data
  indices = np.random.randint(0,len(x),subset_size)
  x=[x[i] for i in indices]
  xsig=[xsig[i] for i in indices]
  y=[y[i] for i in indices]
  ysig=[ysig[i] for i in indices]
  print "subset size :",len(x) 


if 0: # plot 
  from matplotlib import pyplot as plt
  plt.scatter(x,y,alpha=0.5)
  plt.errorbar(x,y,xerr=xsig,yerr=ysig,ls=" ",alpha=0.5)
  plt.show() 

print "####################################"

print "Mixing...\r",
start=time(); stdout.flush()

lm = linmix.LinMix(x,y,xsig,ysig,K=2) 
# set maxiter? takes at least 4000 steps or so to converge
# it's still changing in 1st decimal place after 8000 steps
# at 14000 it's at 1.325
lm.run_mcmc(silent=silent_mcmc)
end=time(); elapsed=end-start;
print "Mixing complete! Took %g seconds." % elapsed

print("alpha mean and stddev: {}, {}".format(
      lm.chain['alpha'].mean(), lm.chain['alpha'].std()))
print("beta mean and stddev: {}, {}".format(
      lm.chain['beta'].mean(), lm.chain['beta'].std()))
print("sigsqr mean and stddev: {}, {}".format(
      lm.chain['sigsqr'].mean(), lm.chain['sigsqr'].std()))


########################################################################
# write to file 

# flux floor, alpha, beta, sigsqr
report = "log10(Fx) floor: %g" % log10flux_floor
report += "\nalpha mean, stddev: %g, %g" % \
       (lm.chain['alpha'].mean(),lm.chain['alpha'].std())
report += "\nbeta mean, stddev: %g, %g" % \
        (lm.chain['beta'].mean(),lm.chain['beta'].std())
report += "\nsigsq mean, stddev: %g, %g" % \
        (lm.chain['sigsqr'].mean(), lm.chain['sigsqr'].std())

report_name = "mcmc_result_log10fxfloor%g.txt" % log10flux_floor

with open(report_name,"w") as text_file:
  text_file.write(report)

raise SystemExit


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# lm = linmix.LinMix(x, y, xsig=xsig, ysig=ysig, xycov=xycov, delta=delta, K=K, nchains=nchains)
# lm.run_mcmc(miniter=miniter, maxiter=maxiter, silent=silent)
# print("{}, {}".format(lm.chain['alpha'].mean(), lm.chain['alpha'].std()))
# print("{}, {}".format(lm.chain['beta'].mean(), lm.chain['beta'].std()))
# print("{}, {}".format(lm.chain['sigsqr'].mean(), lm.chain['sigsqr'].std()))
