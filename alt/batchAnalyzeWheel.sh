# execute analyzeWheel.py in a certain range of flux cuts 


# from -4.5 to -10 in steps of -.5
for i in $(seq -7.5 -.5 -10); do 
  echo 
  echo "python analyzeWheel.py $i"; 
  python analyzeWheel.py $i;
done
