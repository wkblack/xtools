import numpy as np
# from Objects import DtoR, RtoD

clusterEffRadCOEFF = 20.0
pixelSide = 30 # px
DECMapSize = .02 # deg

def flux2SurfaceBrightness():
  mapCenterShift = DECMapSize/2. # in degrees
  mapSize = DECMapSize # in degrees
  
  RA,DEC,Rc,beta = 0.,0.,.1,.6667 # from most massive halo
  
  RApix   = (RA+mapCenterShift)*pixelSide/mapSize
  DECpix  = (DEC+mapCenterShift)*pixelSide/mapSize
  rad     = Rc*pixelSide/mapSize
  effRad  = clusterEffRadCOEFF*rad
  thetac2 = rad**2
  
  bpower = -3.0*beta+0.5
  
  iMax = int(RApix+effRad);   iMin = int(RApix-effRad)
  jMax = int(DECpix+effRad);  jMin = int(DECpix-effRad)
  # if the halo wouldn't show on the plot; don't bother. 
  if (iMin>=pixelSide-1 or jMin>=pixelSide-1): return 0
  if (iMax<=0 or jMax<=0): return 0
  
  print 'grid parts:',iMin,iMax,jMin,jMax
  i,j = np.mgrid[iMin:iMax,jMin:jMax]
  print 'grid created.'
  if 0: # use original method of all circular halos
    mask = (i-RApix)**2 + (j-DECpix)**2 < effRad**2
    mapA = mask*( 1. + ((RApix - i)**2 + (DECpix - j)**2)/thetac2 )**bpower
  else: # generate random orientation and ellipticity for each halo
    print 'i,j parts:',iMax-iMin,jMax-jMin
    # thetaOrient = np.random.random((iMax-iMin,jMax-jMin))*2*np.pi
    thetaOrient = 2*np.pi*np.random.random()
    # ellipticity = np.random.random((iMax-iMin,jMax-jMin))*0.5
    # ellipticity = 0.5
    ellipticity = 0.9
    x = i-RApix; y = j-DECpix; 
    coords = (x*np.cos(thetaOrient)+y*np.sin(thetaOrient))**2 \
           + (x*np.sin(thetaOrient)-y*np.cos(thetaOrient))**2/(1-ellipticity**2)
    mask = coords < effRad**2
    mapA = mask*( 1. + coords/thetac2)**bpower
    del thetaOrient, ellipticity, x, y, coords
      
  return mapA
  
  # mapA *= 1. / sum(sum(mapA))
  # iMaxA = min(iMax,pixelSide)
  # iMinA = max(iMin,0)
  # jMaxA = min(jMax,pixelSide)
  # jMinA = max(jMin,0)
  # Map.MAP[iMinA:iMaxA,jMinA:jMaxA] += \
  #         mapA[(iMinA-iMin):(iMaxA-iMin),(jMinA-jMin):(jMaxA-jMin)]

if __name__=='__main__':
  # TODO: now plot this ... somehow
  from matplotlib import pyplot as plt
  mapA = flux2SurfaceBrightness()
  plt.imshow(mapA)
  plt.show()
