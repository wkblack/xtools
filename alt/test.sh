#!/bin/bash
#SBATCH -N 1
#SBATCH -C knl
#SBATCH -q debug
#SBATCH -J test
#SBATCH --mail-user=wkblack@umich.edu
#SBATCH --mail-type=ALL
#SBATCH -t 00:00:30

#OpenMP settings:
export OMP_NUM_THREADS=1
export OMP_PLACES=threads
export OMP_PROC_BIND=spread


#run the application:
srun -n 1 -c 272 --cpu_bind=cores /global/u1/w/wkblack/xproject/xtools/massive_subset.py
