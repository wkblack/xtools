import matplotlib.pyplot as plt
input_list = [{'x':100,'y':200,'radius':50, 'color':(0.1,0.2,0.3)}]    
output_list = []   
for point in input_list:
  output_list.append(plt.Circle((point['x'], point['y']), point['radius'], color=point['color'], fill=False, lw=3))
  output_list.append(plt.Circle((point['x'], point['y']), point['radius'], color='r', fill=False))
ax = plt.gca(aspect='equal')
ax.cla()
ax.set_xlim((0, 300))
ax.set_ylim((0, 300))
for circle in output_list:    
  ax.add_artist(circle)

if 0: # set axis ratio of data box
  ratio = 9./16.
  ax.set_aspect(ratio/ax.get_data_ratio())

plt.show() 
