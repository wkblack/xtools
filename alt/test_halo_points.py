# show how halo extent changes over dec
# looks like a cosh function
from cosmology import halo_points as hp
from matplotlib import pyplot as plt
import numpy as np

N=10**3
dec = np.linspace(-90,90,N)
val = np.zeros(N)

for i in range(N):
  ra_halo,dec_halo = hp((90,dec[i]),1)
  val[i] = max(ra_halo)-min(ra_halo)

plt.scatter(dec,val,c='k',marker='.',label='relative halo extent')
plt.scatter(dec,np.cosh(np.arccosh(360)/90.*dec),label='cosh(a*dec)')
plt.legend()
plt.show() 
