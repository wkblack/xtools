# Compare different <M|lambda> scaling relations
import voxel
np,plt = voxel.np,voxel.plt
m1,m2,m3 = voxel.Mlambda_Simet,voxel.Mlambda_DESY1,voxel.Mlambda_F16

lam = 10**np.linspace(np.log10(20),np.log10(350))

sm1,sim,sp1 = m1(lam,-1),m1(lam),m1(lam,+1)
plt.fill_between(lam,sm1,sp1,color='g',alpha=.25,linewidth=0)
plt.plot(lam,sim,label=r'M200m: Simet+18',color='g')
plt.fill_between(lam,m2(lam,.1),m2(lam,.3),label=r'M200m: DES Y1 z|[.1,.3]',color='b')
plt.fill_between(lam,m2(lam,.3),m2(lam,.7),label=r'M200m: DES Y1 z|[.3,.7]',color='r')
plt.plot(lam,m3(lam),label=r'M200c: Farahi+16',color='m')

plt.xlabel('Richness')
plt.ylabel('Mass')

plt.xscale('log')
plt.yscale('log')

plt.legend()
plt.tight_layout()
plt.show()
