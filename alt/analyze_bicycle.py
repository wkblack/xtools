#!/bin/python
########################################################################
# analyze output of bicycle.py
# take columns of data and find the slope and error
# wkblack (c) 2019.04.12
########################################################################

# for loading the file and computing slope & scatter
import numpy as np 
import linmix 

# for timing the process and printing prettily 
from time import time
from sys import stdout

fname="output_bicycle.csv"
silent_mcmc=1                 # bool: silence mcmc output
plotting=1                    # bool: make plots and exit 
subset=0                      # bool: grab a subset of the data
subset_size=100               # int:  size of subset of points to grab

from os import sys
if len(sys.argv)>1: # read in flux (log10(Fx)) floor from command line 
  log10flux_floor=float(sys.argv[1])
else: # default flux limit below the lowest value in the set
  log10flux_floor=-23.0

########################################################################
# read in data and select columns 

# data has columns: 
[HID1,HID2,STR1,STR2,LAMBDA,Z,CENTRAL_FLAG,CENTRAL_BCG_FLAG,RA,DEC,
 LAMBDA_ERR,M1,M2,Lx,Tx,Fx] = range(16) 
data = np.loadtxt(fname,skiprows=1,delimiter=",")
CEN = np.logical_and(CENTRAL_FLAG,CENTRAL_BCG_FLAG) # mask: only central

# missing errors: # these can be fabricated for now.
Lx_ERR=-1
Tx_ERR=-1

if 0: # print ra & dec of a certain halo
  # mask = data[:,Lx]==min(data[:,Lx])
  mask_Tx = data[:,Tx]>10 # ==min(data[:,Tx])
  mask_LAMBDA = data[:,LAMBDA]<50 # ==min(data[:,Tx])
  mask = np.logical_and(mask_Tx,mask_LAMBDA)
  print sum(mask),mask
  print "HID1 =",data[mask,HID1]
  print "[RA DEC] = [%g %g]" % (data[mask,RA],data[mask,DEC])
  raise SystemExit

########################################################################
# plotting

if plotting: # e.g.: Lx , Tx , lam
  ######################################################################
  print "importing...\r",
  start=time(); stdout.flush()
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  import matplotlib
  from matplotlib import pyplot as plt
  import config as cfg
  rm_fname = cfg.xtools + 'RM_training_fixed.csv'
  rm_data = np.loadtxt(rm_fname,delimiter=',',skiprows=1,usecols=(7,8))
  [STR1,STR2] = [rm_data[:,i] for i in range(2)] 
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  end=time(); elapsed=end-start;
  print "Importing took %g seconds." % elapsed
  ######################################################################
  
  if 1: # plot Lx & Tx
    fig, axes = plt.subplots(nrows=2,ncols=1)
    [ax1,ax2] = axes.flat
    
    # set up mutual colorbar 
    if 0: # Fx
      coloring = np.log10(data[:,Fx])
      colorlabel=r'$\log_{10}\,F_x\;$(ergs/s/cm$^2)$' 
      colormap = 'winter'
    elif 0: # clip M2/M1
      coloring = np.clip(np.log10(data[:,M2]/data[:,M1]+10**-10),-1,100)
      colormap = 'magma_r' # 'copper_r' # 'seismic'
      colorlabel=r'log$_{10} M2/M1$'
    else: # S1
      coloring = STR1
      colormap = 'coolwarm'
      colorlabel=r'Strength $S_1$'
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    im1 = ax1.scatter(data[:,LAMBDA],data[:,Tx],s=1,
                      c=coloring,cmap=colormap,alpha=.75)
    ax1.set_ylabel('$T_x$ (keV)') 
    ax1.set_yscale('log') 
    ax1.set_xscale('log') 
    ax1.set_xlim(min(data[:,LAMBDA]),max(data[:,LAMBDA]))
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    im2 = ax2.scatter(data[:,LAMBDA],data[:,Lx],s=1,
                      c=coloring,cmap=colormap,alpha=.75)
    ax2.set_xlabel('$\lambda$') 
    ax2.set_xscale('log') 
    ax2.set_xlim(min(data[:,LAMBDA]),max(data[:,LAMBDA]))
    ax2.set_ylabel('$L_x$ (ergs/s)') 
    ax2.set_yscale('log') 
    ax2.set_ylim(min(data[:,Lx]),max(data[:,Lx])) # lxmin prev: 1e-3
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  
    plt.tight_layout() # overwrites previous formatting
    cbar_ax = fig.add_axes([0.84, 0.15, 0.03, 0.785])
    fig.colorbar(im2, cax=cbar_ax,
                 label=colorlabel) 
    fig.subplots_adjust(right=0.8)
    
    plt.savefig('combined.png') 
    plt.show() 
  
  if 0: # plot other variables
    if 0: 
      pltlabel = 'Membership Strength vs Mass Ratio'
      x = np.log10(data[:,M2]/data[:,M1]+10**-10)
      xlabel = r'$\log_{10}(M_2/M_1)$'
      y = STR1
      ylabel = r'$S_1$'
      coloring = data[:,Z]
      clabel = r'Redshift $z$'
      plt.xlim(-4,3)
      colormap='coolwarm'
    elif 1: 
      pltlabel = 'Mass vs Richness'
      x = data[:,LAMBDA]
      xlabel = r'$\lambda$'
      plt.xscale('log')
      plt.xlim(10,200)
      y = data[:,M1]
      ylabel = r'$M_1$'
      plt.yscale('log') 
      plt.ylim(4e13,4e15)
      coloring = STR1
      clabel = r'Strength $S_1$'
      colormap='rainbow' # 'BuGn'
    
    sc = plt.scatter(x,y,s=1,c=coloring,cmap=colormap)
    plt.title(pltlabel)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.colorbar(sc,label=clabel)
    plt.tight_layout()
    plt.show() 
  
  if 0: # plot Luminosity and Temperature vs *Mass*
    fig, axes = plt.subplots(nrows=2,ncols=1)
    [ax1,ax2] = axes.flat
    
    # set up mutual colorbar
    coloring = np.log10(data[:,LAMBDA])
    colormap = 'rainbow'
    colorlabel = '$\log_{10}\,\lambda$'
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # set up the top plot 
    im1 = ax1.scatter(data[:,M1],data[:,Tx],s=1,
                c=coloring,cmap=colormap,alpha=.75)
    # don't set up x-axis label for top plot
    ax1.set_xscale('log') 
    # ax1.set_xlim(min,max) 
    ylabel = r'$T_X$ (keV)'
    ax1.set_ylabel(ylabel)
    ax1.set_yscale('log') 
    # ax1.set_ylim(min,max) 
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # set up the bottom plot 
    im2 = ax2.scatter(data[:,M1],data[:,Lx],s=1,
                c=coloring,cmap=colormap,alpha=.75)
    xlabel = r'$M_1$ (M$_\odot$)'
    ax2.set_xlabel(xlabel)
    ax2.set_xscale('log') 
    # ax1.set_xlim(min,max) 
    ylabel = r'$L_X$ (ergs/s)'
    ax2.set_ylabel(ylabel)
    ax2.set_yscale('log') 
    # ax1.set_ylim(min,max) 
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    plt.tight_layout()
    cbar_ax = fig.add_axes([0.84, 0.15, 0.03, 0.785])
    fig.colorbar(im2, cax=cbar_ax,
                 label=colorlabel) 
    fig.subplots_adjust(right=0.8)
    plt.savefig('vs_mass_combined.png') 
    plt.show() 
  
  raise SystemExit
# end plotting section 
########################################################################




########################################################################
# linmix! 

# set flux floor and plot log10Tx vs log10Lambda, 
# figure out how the slope changes with floor

x=np.log10(data[:,LAMBDA])
xsig=data[:,LAMBDA_ERR]/data[:,LAMBDA]/np.log(10) # from d(lnx)=dx/x
y1=np.log10(data[:,Tx])
y1sig=.1*np.ones(len(y1)) # TODO: get real value
y2=np.log10(data[:,Lx])
y2sig=.1*np.ones(len(y2)) # TODO: get real value
log10flux=np.log10(data[:,Fx])

if len(sys.argv)>1: # vet data with flux floor
  mask = log10flux > log10flux_floor
  x=x[mask]
  xsig=xsig[mask]
  y1=y1[mask]
  y1sig=y1sig[mask]
  y2=y2[mask]
  y2sig=y2sig[mask]
  print "Masked below log10(Fx)=%g" % log10flux_floor

if subset: # grab a subset of the data
  indices = np.random.randint(0,len(x),subset_size)
  x=[x[i] for i in indices]
  xsig=[xsig[i] for i in indices]
  y1=[y1[i] for i in indices]
  y1sig=[y1sig[i] for i in indices]
  y2=[y2[i] for i in indices]
  y2sig=[y2sig[i] for i in indices]
  print "subset size :",len(x) 


if 0: # plot 
  from matplotlib import pyplot as plt
  plt.scatter(x,y1,alpha=0.5)
  plt.errorbar(x,y1,xerr=xsig,yerr=y1sig,ls=" ",alpha=0.5)
  plt.show() 

print "####################################"

print "Mixing...\r",
start=time(); stdout.flush()

lm1 = linmix.LinMix(x,y1,xsig,y1sig,K=2) 
# set maxiter? takes at least 4000 steps or so to converge
# it's still changing in 1st decimal place after 8000 steps
# at 14000 it's at 1.325
lm1.run_mcmc(silent=silent_mcmc)
end=time(); elapsed=end-start;
print "Mixing complete! Took %g seconds." % elapsed

print("alpha mean and stddev:  {}, {}".format(
      lm1.chain['alpha'].mean(),  lm1.chain['alpha'].std()))
print("beta mean and stddev:   {}, {}".format(
      lm1.chain['beta'].mean(),   lm1.chain['beta'].std()))
print("sigsqr mean and stddev: {}, {}".format(
      lm1.chain['sigsqr'].mean(), lm1.chain['sigsqr'].std()))


########################################################################
# write to file 

# flux floor, alpha, beta, sigsqr
report = "log10(Fx) floor: %g" % log10flux_floor
report += "\nalpha mean, stddev: %g, %g" % \
       (lm.chain['alpha'].mean(),lm.chain['alpha'].std())
report += "\nbeta mean, stddev: %g, %g" % \
        (lm.chain['beta'].mean(),lm.chain['beta'].std())
report += "\nsigsq mean, stddev: %g, %g" % \
        (lm.chain['sigsqr'].mean(), lm.chain['sigsqr'].std())

report_name = "mcmc_result_log10fxfloor%g.txt" % log10flux_floor

with open(report_name,"w") as text_file:
  text_file.write(report)

raise SystemExit


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# lm = linmix.LinMix(x, y, xsig=xsig, ysig=ysig, xycov=xycov, delta=delta, K=K, nchains=nchains)
# lm.run_mcmc(miniter=miniter, maxiter=maxiter, silent=silent)
# print("{}, {}".format(lm.chain['alpha'].mean(), lm.chain['alpha'].std()))
# print("{}, {}".format(lm.chain['beta'].mean(), lm.chain['beta'].std()))
# print("{}, {}".format(lm.chain['sigsqr'].mean(), lm.chain['sigsqr'].std()))
