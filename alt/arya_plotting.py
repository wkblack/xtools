import numpy as np
import numpy.random as npr
import matplotlib.pylab as plt
from scipy import stats
import scipy.ndimage
from astropy.io import fits as pyfits # import pyfits
from matplotlib.patches import Ellipse
from sklearn import linear_model
import matplotlib
matplotlib.use('Agg')

estimators = [('OLS', linear_model.LinearRegression()),
              ('Theil-Sen', linear_model.TheilSenRegressor(random_state=42)),
              ('RANSAC', linear_model.RANSACRegressor(random_state=42)) ]
model = estimators[2][1]

def compute_sigma_level(trace1, trace2, nbins=20):
    """From a set of traces, bin by number of standard deviations"""
    L, xbins, ybins = np.histogram2d(trace1, trace2, nbins)
    L[L == 0] = 1E-16
    logL = np.log(L)

    shape = L.shape
    L = L.ravel()

    # obtain the indices to sort and unsort the flattened array
    i_sort = np.argsort(L)[::-1]
    i_unsort = np.argsort(i_sort)

    L_cumsum = L[i_sort].cumsum()
    L_cumsum /= L_cumsum[-1]
    

    xbins = 0.5 * (xbins[1:] + xbins[:-1])
    ybins = 0.5 * (ybins[1:] + ybins[:-1])

    return xbins, ybins, L_cumsum[i_unsort].reshape(shape)


def plot_MCMC_trace(ax, xdata, ydata, scatter=False, c='k', **kwargs):
    """Plot traces and contours"""
    xbins, ybins, sigma = compute_sigma_level(xdata, ydata)
    sigma = scipy.ndimage.gaussian_filter(sigma, sigma=1.0, order=0)
    xbins = scipy.ndimage.zoom(xbins, 3)
    ybins = scipy.ndimage.zoom(ybins, 3)
    sigma = scipy.ndimage.zoom(sigma, 3)
    #sigma = scipy.ndimage.gaussian_filter(sigma, sigma=1.0, order=0)
    ax.contour(xbins, ybins, sigma.T, interpolation='bicubic',\
               levels=[0.683, 0.955], **kwargs)
    if scatter:    ax.plot(xdata, ydata, ',', color=c, alpha=0.3)

def scatter_cal(x,y,slope,intercept,dof):
   sig2 = sum((np.array(y) - (slope*np.array(x)+intercept))**2) / dof
   return np.sqrt(sig2)

def median_conf(A):
   B = sorted(A) 
   lA = len(B)
   if(lA==0): return 0,0,0
   if(int(50.*lA/100.0)-1==-1): return B[0],B[0],B[0]
   m = B[int(50.*lA/100.0)]
   if(int(16.*lA/100.0)<0): return m,B[0],B[int(84.*lA/100.0)]
   return m,B[int(16.*lA/100.0)],B[int(84.*lA/100.0)]


def linregr(x,y,N=200):
   slope = np.zeros(N)
   intercept = np.zeros(N)
   ndata = len(x)

   index = range(0,ndata)

   for i in range(N):
      #print i
      indexNew = np.array([npr.choice(index) for _ in range(ndata)])

      xnew = np.array(x[indexNew])
      ynew = np.array(y[indexNew])

      slope[i], intercept[i], r_value, p_value, see =\
                        stats.linregress(xnew,ynew)

   print slope.mean(), intercept.mean()
   return slope , intercept

def coxbox(y,lambdaCB=1.0):
   return np.power(y,lambdaCB) / lambdaCB


def invcoxbox(y,lambdaCB=1.0):
   return np.power(lambdaCB*y , 1./lambdaCB)


if 0: # never used
  clusters =  pyfits.open('RM_Spec_full.fit')[1].header
  halos = pyfits.open('RM_halos.fit')[1].header

# MAIN RESULT OF THE PAPER
x = np.linspace(15,200,201)
M = np.log10(10**15 * ( 547.0 * (x/30.0)**.405 / (1082.0*1.025) )**3.0 )
x = np.log10(x/30.0)    

print 10**15 * ( 547.0 * (30.0/30.0)**.405 / (1082.0*1.025) )**3.0 

fig = plt.figure(0)
ax = fig.add_subplot(111)#,aspect='equal')

for CC in [True]:
   #print CC
   data = pyfits.open('RM_Spec_full.fit')[1].data
   halo = pyfits.open('RM_halos.fit')[1].data
   matched = pyfits.open('matched_catalog_1.0_may_2015.fit')[1].data
   cID = list(matched['CLUSTERID'][matched['STR1'] > 0.1])

   data = data[data['OMAGi']<19]
   data = data[abs(data['VEL'])<0.00001]
   mask = (data['Z'] < 0.22)*(data['Z'] > 0.18)
   data = data[mask]
   if CC: data = data[data['CENTRAL_FLAG']==1]
   if CC: data = data[data['CENTRAL_BCG_FLAG']==1]

   Mvir = np.zeros(len(data)); count = -1
   for i in range(len(data)):
      count += 1
      if ( data.CLUSTERID[i] in cID ): 
         iHalo = data.H1[i]      
         try:      Mvir[count] = halo['M200'][halo['HALOID'] == iHalo]
         except: print "ERROR"
   

   mask = Mvir > 0.0
   print sum(mask)

   lam = data.LAMBDA
   Mvir = np.log10(Mvir[mask])
   lam = np.log10(lam[mask]/30.0)

   """ slope, intercept, r_value, p_value, see = stats.linregress(lam,Mvir)
   sig = scatter_cal(lam,Mvir,slope,intercept,len(lam)-2)

   mx = lam.mean(); sx2 = ((lam-mx)**2).sum()
   sd_intercept = see * np.sqrt(1./len(lam) + mx*mx/sx2)
   sd_slope = see * np.sqrt(1./sx2)
   sd_intercept = 10**(intercept + see * np.sqrt(1./len(lam) + mx*mx/sx2)) - \
                  10**intercept
   sd_intercept /= 1e14 """

   lambdaCB = 1.
   Mnew = coxbox(Mvir,lambdaCB=lambdaCB)
   slope, intercept = linregr(lam,Mnew,N=500)

   Mass = 10**( invcoxbox( slope.mean()*x + intercept.mean() , lambdaCB=lambdaCB ) )

   slopeM = slope.mean()
   normM = intercept.mean() 
   if CC:
      plt.loglog(30.0*10**x,Mass,\
           '-b',label=r'$M_{200c}-\lambda_{RM}$ - Matched mass (CS)')
   else:
      #plt.loglog(10**x,10**(slope*x+intercept),\
      #     '-b',label=r'$M_{200c}-\lambda_{RM}$ - Matched mass (FS)')
      pass

plt.loglog(30.0*10**lam,10**Mvir,'y.',alpha=0.6)


fitL = np.zeros(len(x)); fitU = np.zeros(len(x))
fit = np.zeros(len(x))
for i in range(len(x)):
   xi = x[i]
   Max = (slope*xi+intercept)
   fitU[i] = np.percentile(Max, 97.5)
   fit[i] = np.percentile(Max, 50.0)
   fitL[i] = np.percentile(Max, 2.5)

#plt.loglog(30.0*10**x,10**fit,'ob',label=r'$M_{200c}-\lambda_{RM}$ - Matched mass')
plt.fill_between(30.0*10**x,10**fitL,10**fitU,color='b',alpha=0.3)

#print (fitU-fitL) / 1.96 / np.log(10.)

LAM_bins = [1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0]
lam = lam + np.log10(30.0)
for ilam in LAM_bins:
   mask = lam > ilam ;   M = Mvir[mask]; L = lam[mask]
   mask = L < ilam + 0.1;   M = M[mask]; L = L[mask]
   m,mn,mp = median_conf(M)
   errn = 10**m-10**mn
   errp = 10**mp-10**m
   print errn,m,errp
   err = np.array([[errn,errp]]).T
   #plt.errorbar(10**(ilam+0.05),10**m,yerr=err,c='r',fmt='o')

plt.xlabel(r'$\lambda_{RM}$',size=20)
plt.xlim([15.,200.])
plt.xticks([20,40,60,80,100,200],[20,40,60,80,100,200])
plt.ylim([10**12.75,10**15.25])
plt.ylabel(r'${\rm M}_{200c}~[h^{-1} {\rm M}_{\odot}]$',size=20)
plt.legend(loc=4,prop={'size':14})
plt.tick_params('both',length=10,width=1.25,which='major')
plt.tick_params('both',length=5,width=1.25,which='minor')


N = 8000
sigmav = npr.normal(547.0,3.0,N)
sigmam = npr.normal(1082.9,4.0,N)
alpha = npr.normal(0.3361,0.0026,N)
alpha = 1./alpha

normC  = np.log10( (sigmav / (sigmam*1.025) )**alpha ) + 15.0
slopeC = alpha * npr.normal(0.405,0.01,N)
M = normC.mean() + slopeC.mean() * x
plt.loglog(30.0*10**x, 10**M, 'k', linewidth=2.75,\
           label=r'$M_{\sigma}-\lambda_{RM}$ - Calibrated mass')

fitL = np.zeros(len(x)); fitU = np.zeros(len(x))
fit = np.zeros(len(x))
for i in range(len(x)):
   xi = x[i]
   Max = (slopeC*xi+normC)
   fitU[i] = np.percentile(Max, 97.5)
   fit[i] = np.percentile(Max, 50.0)
   fitL[i] = np.percentile(Max, 2.5)

#plt.loglog(30.0*10**x,10**fit,'ob',label=r'$M_{200c}-\lambda_{RM}$ - Matched mass')
plt.fill_between(30.0*10**x,10**fitL,10**fitU,color='k',alpha=0.3)

#print (fitU-fitL) / 1.96 / np.log(10.)


#plt.savefig('contour-scaling.pdf',bbox_inches='tight')
#plt.savefig(fdir+"lam-M200-calib.pdf",bbox_inches='tight')
plt.savefig("lam-M200-calib.pdf",bbox_inches='tight')
plt.show()
