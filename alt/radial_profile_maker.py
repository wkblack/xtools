# analyze radial profiles

########################################################################
# imports

import numpy as np
golden_ratio = (1+np.sqrt(5))/2.
golden_aspect = 5*np.array([golden_ratio,1])

import h5py

# plotting imports 
from matplotlib import pyplot as plt
mpl_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
plt.rcParams['figure.figsize'] = golden_aspect
plt.rcParams['font.size'] = 15


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# import h5py files
fname_RD = "out_voxel_0000000_to_0160000.h5"
fname_GM = "gmm_voxel_0000000_to_0160000.h5"

f_RD = h5py.File(fname_RD,'r')
f_GM = h5py.File(fname_GM,'r')

f_keys = ['Ngal', 'Nred', 'Z', 'mu', 'radial', 'rvir', 'vals']


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# copied from voxel: 
# xi=log10(r/r_vir)
# gets sketchy <-2 and loses relevance >~ 0, but I want the outliers.
dxi=.1
xi_bins = np.arange(-3,1,dxi)
x_bins = 10**xi_bins
_j = np.arange(len(x_bins)-1)
X = 4/3.*np.pi*(x_bins[_j+1]**3-x_bins[_j]**3) # volume element (unitless)
X0 = 4/3.*np.pi*x_bins[0]**3 # volume of interior bubble
x_midpts = 10.**((xi_bins[_j]+xi_bins[_j+1])/2.) # for plotting

h = .7 # Hubble unitless magnitude


########################################################################
# create radial profile

def get_rho(Z):
  " approximate "
  rho = .0539 * (1+Z)**.820 * np.exp(.55975*np.log(1+Z)**2) 
  return rho/(4*np.pi)


def get_N_vals(N):
  " pass in [each halo, each x bin] "
  N_mean = np.mean(N,axis=0)
  sigma_N = np.std(N,axis=0)
  N_sum = np.sum(N,axis=0)
  N_low = N_mean + .5 - np.sqrt(N_mean + .25)
  N_high = N_mean + .5 + np.sqrt(N_mean + .25)
  return N_mean,sigma_N,N_low,N_high


def remove_zeros(a):
  for ii in range(len(a)):
    jj = 1
    while a[ii] <= 0:
      vals = a[ii:ii+1+jj] * X[ii:ii+1+jj]
      a[ii:ii+1+jj] = np.mean(vals[~np.isnan(vals)] / np.sum(X[ii:ii+1+jj]))
      # if a[ii+1+jj] > 0: 
      #   a[ii:ii+2*jj] = np.mean(a[ii:ii+2*jj] * X[ii:ii+2*jj]) / np.sum(X[ii:ii+2*jj])
      jj += 1
  return a


def flush_left(a):
  a = np.array(a).flatten()
  for ii in range(len(a)):
    jj = 1
    while a[ii] <= 0 and ii+jj < len(a):
      a[ii:ii+jj] = a[ii+jj]
      jj += 1
  return a


def get_delta(N,rvir,Z,use_BaBar=False,count_interior=False,nix_nulls=False):
  """
  returns overdensity given number counts, halo radii, and redshift
  
  Parameters
  ----------
  N : array, size (N_halos,len(x_midpts))
    number counts for each halo for each radial bin
  rvir : array, size len(N_halos)
    virial radius of each halo in cMpc
  Z : array, size len(N_halos)
    redshift of each halo
  use_BaBar : bool, optional, default=False
    determines method for calculating Poisson errors
    the BaBar collaboration[1] statistics committee discussed alternate
    ways of portraying Poisson error[2], including for cases where N=0,
    which here sets an upper bound on possible measured error:
    N +- [+-1/2 + sqrt(N + 1/4)] = N + 1/2 +- sqrt(N + 1/4)
    * it gives non-zero error bars for N=0
    * it produces non-zero minimum for N=1
    * it yields asymmetric error bars, reflecting Poissonian asymmetry
  
  Returns
  -------
  mean delta, delta err mean, low Poisson error, high Poisson error
  
  [1] slac.stanford.edu/BFROOT (BaBar homepage) 
  [2] www-cdf.fnal.gov/physics/statistics/notes/pois_eb.txt
  """
  # calculate volume for each radial bin
  rho_mean = get_rho(Z)
  N_expected = np.outer(rvir**3 * rho_mean,X)
  delta = N/N_expected - 1 # delta; unitless
  if count_interior:
    # count the central galaxy in the density calculation as leftmost point
    N_interior_expected = rho_mean * X0 * rvir**3
    delta_interior = 1/N_interior_expected - 1
    delta[:,0] = (delta_interior + delta[:,0])/2.
  delta_mean = np.mean(delta,axis=0)
  delta_std = np.std(delta,axis=0)
  # calculate Poisson statistics
  N_tot = np.sum(N,axis=0)
  # delta_err_mean = delta_std/np.sqrt(N_tot)
  N_exp_tot = np.sum(N_expected,axis=0)
  N_mean = np.mean(N,axis=0)
  N_exp_mean = np.mean(N_expected,axis=0)
  if use_BaBar: # https://www.slac.stanford.edu/BFROOT/
    print("using BaBar Poisson error") 
    sig_low = (-.5 + np.sqrt(N + .25))/N_expected
    sig_high = (+.5 + np.sqrt(N + .25))/N_expected
  elif 0: # use standard Poisson error (mean of Poisson err)
    print("using standard Poisson error") 
    sig_low = sig_high = np.sqrt(N)/N_expected
  else: # use hybrid abomination
    print("WARNING: using abomination hybrid error bars!") 
    sig_low = (-.5 + np.sqrt(N + .25))/N_expected # BaBar
    sig_high = np.sqrt(N)/N_expected # standard 
  
  delta_low = np.mean(delta - sig_low,axis=0)
  delta_high = np.mean(delta + sig_high,axis=0)
  delta_sigx = delta_std/np.sqrt(.25 + N_tot)
  
  report = [delta_mean,delta_sigx,delta_low,delta_high]
  if nix_nulls:
    report = [remove_zeros(arr) for arr in report]
  return report


########################################################################
def get_delta2(N,rvir,mu,Z,use_BaBar=False,nix_nulls=False):
  " give rvir and mu in normal units, not per h! "
  # calculate volume for each radial bin
  rho_mean = get_rho(Z)
  N_expected = np.outer(rvir**3 * rho_mean,X)
  delta = N/N_expected - 1 # delta; unitless
  delta_mean = np.mean(delta,axis=0)
  if nix_nulls:
    delta_mean = remove_zeros(delta_mean)
  delta_std = np.std(delta,axis=0)
  N_tot = np.sum(N,axis=0)
  if use_BaBar:
    sig_x_low = -.5 + np.sqrt(N_tot + .25)
    sig_x_high = +.5 + np.sqrt(N_tot + .25)
  else:
    sig_x_low = sig_x_high = np.sqrt(N_tot)
  delta_low,delta_high = np.zeros((2,len(delta_mean))) # default to zero
  m = sig_x_low > 0
  delta_low[m] = delta_mean[m] - delta_std[m]/sig_x_low[m]
  m = sig_x_high > 0
  if nix_nulls: # upper bound data w/ null std
    delta_high[m] = delta_mean[m] + flush_left(delta_std[m]/sig_x_high[m])
  else:
    delta_high[m] = delta_mean[m] + delta_std[m]/sig_x_high[m]
  
  Z_mean,mu_mean,rvir_mean = np.mean([Z,mu,rvir],axis=1)
  delta_nfw_mean = get_NFW(mu_mean,Z_mean)
  # calculate number we'd expect if it were just matching the background (then delta would be null)
  N_exp = rvir_mean**3 * X * get_rho(Z_mean) # steadily rises from ~5e-11 to 15
  N_nfw = (delta_nfw_mean + 1) * N_exp
  if 0:
    plt.plot(x_midpts,N_nfw,label='N_nfw')
    plt.fill_between(x_midpts,N_nfw+.5-np.sqrt(N_nfw+.25),
                     N_nfw+.5+np.sqrt(N_nfw+.25),alpha=.1,lw=0)
    plt.plot(x_midpts,N_exp,label='N_exp')
    plt.xscale('log')
    plt.yscale('log')
    plt.legend()
    plt.show()
  if use_BaBar:
    sig_nfw_low = (-.5+np.sqrt(N_nfw+.25))/N_exp
    sig_nfw_high = (+.5+np.sqrt(N_nfw+.25))/N_exp
  else:
    sig_nfw_low = sig_nfw_high = np.sqrt(N_nfw)/N_exp
  delta_nfw_low = delta_nfw_mean - sig_nfw_low
  delta_nfw_high = delta_nfw_mean + sig_nfw_high
  
  delta_data = [delta_low, delta_mean, delta_high]
  delta_nfw_data = [delta_nfw_low, delta_nfw_mean, delta_nfw_high]
  return delta_data, delta_nfw_data


def plot_delta_x3(f,reds=False,use_BaBar=False,nix_nulls=False,
                  mu_bins=[13.75,14,14.5,15,15.5]):
  Ngal,Nred,Z,mu,radial,rvir,vals = [f[key][:] for key in f_keys]
  rvir /= h # get out of /h units, into cMpc
  N = radial[:,:,1 if reds else 0]
  
  # iterate over bins
  for ii in range(len(mu_bins)-1):
    mask_ii = (mu_bins[ii] <= mu) * (mu < mu_bins[ii+1])
    label_ii = r'$\mu|[%0.2f,%0.2f)$' % (mu_bins[ii],mu_bins[ii+1])
    col_ii = mpl_colors[ii]
    dd,dnd = get_delta3(N[mask_ii],rvir[mask_ii],mu[mask_ii],Z[mask_ii],
                        use_BaBar=use_BaBar,count_interior=False,nix_nulls=nix_nulls)
    dl,dm,dh = dd # unpack delta data
    dnl,dnm,dnh = dnd # unpack nfw data
    
    if 1: # plot measured profiles + error of the mean
      plt.plot(x_midpts,dm,col_ii,label=label_ii,ls=':')
      plt.fill_between(x_midpts,dl,dh,alpha=.125,lw=.5,color=col_ii)
    if 0:  # plot nfw terms + expected Poisson scatter
      plt.plot(x_midpts,dnm,col_ii)
      plt.fill_between(x_midpts,dnl,dnh,alpha=.125,lw=0,color=col_ii)
  
  # set up axes
  plt.xlabel(r'Cluster-centric distance $r/r_{\rm vir}$')
  plt.xscale('log')
  plt.xlim(min(x_bins),max(x_bins))
  plt.ylabel(r'Overdensity $\delta$')
  plt.yscale('log')
  # tidy up & display
  plt.legend()
  plt.tight_layout()
  plt.show()


########################################################################
def get_delta3(N,rvir,mu,Z,use_BaBar=False,count_interior=False,nix_nulls=False):
  " fix: use per halo instead of per sum "
  rho_mean = get_rho(Z)
  N_expected = np.outer(rvir**3 * rho_mean,X)
  delta = N/N_expected - 1 # delta; unitless
  delta_mean = np.mean(delta,axis=0)
  if nix_nulls:
    delta_mean = remove_zeros(delta_mean)
  delta_std = np.std(delta,axis=0)
  N_halo = len(Z) if hasattr(Z,'__len__') else 1
  if use_BaBar:
    sig_x_low = -.5 + np.sqrt(N_halo + .25)
    sig_x_high = +.5 + np.sqrt(N_halo + .25)
  else:
    sig_x_low = sig_x_high = np.sqrt(N_halo)
  delta_low,delta_high = np.zeros((2,len(delta_mean))) # default to zero
  # set error of the mean
  ml = sig_x_low > 0
  mh = sig_x_high > 0
  if nix_nulls: # upper bound data w/ null std
    delta_high[mh] = delta_mean[mh] + flush_left(delta_std[mh]/sig_x_high[mh])
    delta_low[ml] = delta_mean[ml] - flush_left(delta_std[ml]/sig_x_low[ml])
  else:
    delta_high[mh] = delta_mean[mh] + delta_std[mh]/sig_x_high[mh]
    delta_low[ml] = delta_mean[ml] - delta_std[ml]/sig_x_low[ml]
  
  Z_mean = np.mean(Z)
  mu_mean = np.mean(mu)
  rvir_mean = np.mean(rvir)
  delta_nfw_mean = get_NFW(mu_mean,Z_mean)
  # calculate number we'd expect if it were just matching the background (then delta would be null)
  N_exp = rvir_mean**3 * X * get_rho(Z_mean) # steadily rises from ~5e-11 to 15
  N_nfw = (delta_nfw_mean + 1) * N_exp
  if use_BaBar:
    sig_nfw_low = (-.5+np.sqrt(N_nfw+.25))/N_exp/np.sqrt(N_halo)
    sig_nfw_high = (+.5+np.sqrt(N_nfw+.25))/N_exp/np.sqrt(N_halo)
  else:
    sig_nfw_low = sig_nfw_high = np.sqrt(N_nfw)/N_exp/np.sqrt(N_halo)
  delta_nfw_low = delta_nfw_mean - sig_nfw_low
  delta_nfw_high = delta_nfw_mean + sig_nfw_high
  
  delta_data = [delta_low, delta_mean, delta_high]
  delta_nfw_data = [delta_nfw_low, delta_nfw_mean, delta_nfw_high]
  return delta_data, delta_nfw_data


########################################################################
# this should account better for redshift drift & reduce scatter

def get_delta4(N,rvir,mu,Z,nix_nulls=False):
  """ 
  calculate mean overdensity & error of the mean relative to NFW, 
  along with the non-relative terms, normalized to mean mass,
  including expected Poisson noise on the NFW expected
  """ 
  # calculate sample size
  N_halo = len(Z) if hasattr(Z,'__len__') else 1
  if N_halo > 1:
    diff = np.abs(mu-np.mean(mu))
    index_mean_mu = np.where(diff==min(diff))[0][-1]
  
  # calculate number counts expected in random area of space
  rho = get_rho(Z)
  N_expected = np.outer(rvir**3 * rho,X)
  # calculate overdensity from number counts excess
  delta = N/N_expected - 1 # delta; unitless
  
  nfw = np.array([get_NFW(mu[ii],Z[ii]) for ii in range(N_halo)])
  N_nfw = (nfw + 1) * N_expected
  nfw_sig = np.sqrt(N_nfw)/N_expected/np.sqrt(N_halo)
  
  rel = delta/nfw
  rel_sig = np.std(rel,axis=0)
  rel_sigx = rel_sig/np.sqrt(N_halo)
  
  delta_mean = np.mean(rel,axis=0) * nfw[index_mean_mu]
  delta_sigx = rel_sigx * nfw[index_mean_mu]
  
  if nix_nulls:
    delta_mean = remove_zeros(delta_mean)
  
  return delta_mean, delta_sigx, nfw[index_mean_mu], nfw_sig[index_mean_mu]


########################################################################
def plot_delta_x_heaviest(f,N_heavy=1,N_start=0,N_sig=1,
                          reds=False,nix_nulls=True):
  " plot heaviest `N_heavy` halos, starting with `N_start=0` "
  Ngal,Nred,Z,mu,radial,rvir,vals = [f[key][N_start:N_start + N_heavy] for key in f_keys]
  rvir /= h # get out of /h units, into cMpc
  N = radial[:,:,1 if reds else 0]
  relatives = np.zeros((len(x_midpts),N_heavy))
  # plot first one
  dd,dnd = get_delta3(N[0],rvir[0],mu[0],Z[0],nix_nulls=nix_nulls)
  dl,dm,dh = dd # unpack delta data
  dnl,dnm,dnh = dnd # unpack nfw data
  relatives[:,0] = dm/dnm
  col_ii = mpl_colors[0]
  label_ii = r'$\mu=%0.2f$' % mu[0]
  # plot measured profiles + error of the mean
  plt.scatter(x_midpts,dm,10,color=col_ii,label=label_ii,zorder=3)
  # plt.fill_between(x_midpts,dl,dh,alpha=.125,lw=.5,color=col_ii) # no scatter for single guy
  # plot nfw terms + expected Poisson scatter
  plt.plot(x_midpts,dnm,col_ii)
  plt.fill_between(x_midpts,dnl,dnh,alpha=.125,lw=0,color=col_ii)
  
  for ii in range(1,N_heavy):
    dd,dnd = get_delta3(N[ii],rvir[ii],mu[ii],Z[ii],nix_nulls=nix_nulls)
    relatives[:,ii] = dd[1]/dnd[1]
  rel = np.mean(relatives,axis=1) * dnm
  lbl_all = r'$\mu|[%0.2f,%0.2f)$' % (min(mu),max(mu))
  plt.plot(x_midpts[rel>0],rel[rel>0],'k',label=lbl_all)
  # add error of the mean
  std = np.std(relatives,axis=1) * dnm
  errmean = N_sig * std/np.sqrt(N_heavy)
  plt.fill_between(x_midpts,rel-errmean,rel+errmean,alpha=.125,color='k',lw=0)
  
  # reduce nfw errors by sqrt N_halos
  nfw_low = dnm - (dnm-dnl)/np.sqrt(N_heavy)
  nfw_high = dnm + (dnh-dnm)/np.sqrt(N_heavy)
  plt.fill_between(x_midpts,nfw_low,nfw_high,alpha=.125,color='k',lw=0)
  
  # set up axes
  plt.xlabel(r'Cluster-centric distance $r/r_{\rm vir}$')
  plt.xscale('log')
  plt.xlim(min(x_midpts),max(x_midpts))
  plt.ylabel(r'Overdensity $\delta$')
  plt.yscale('log')
  # tidy up & display
  plt.legend()
  plt.tight_layout()
  plt.show()


########################################################################


def plot_delta_x(f,reds=False):
  " plot_delta_x(f,reds=False) "
  Ngal,Nred,Z,mu,radial,rvir,vals = [f[key][:] for key in f_keys]
  rvir /= h # get out of /h units, into cMpc
  N = radial[:,:,1 if reds else 0]
  
  dm,ds,dl,dh = get_delta(N,rvir,Z)
  plt.plot(x_midpts,dm,'k')
  plt.fill_between(x_midpts,dm-ds,dm+ds,alpha=.125,lw=0,color='b') # intrinsic scatter
  plt.fill_between(x_midpts,dl,dh,alpha=.125,lw=0,color='r') # error of mean
  
  plt.xlabel(r'Cluster-centric distance $r/r_{\rm vir}$')
  plt.xscale('log')
  plt.xlim(min(x_bins),max(x_bins))
  plt.ylabel(r'Overdensity $\delta$')
  plt.yscale('log')
  plt.tight_layout()
  plt.show()


def plot_delta_x_mu(f,reds=False,mu_bins=[13.75,14,14.5,15.5],
                    mask_x=False,plot_nfw=True,use_BaBar=False,
                    count_interior=False,nix_nulls=False):
  " plot_delta_x(f,reds=False) "
  Ngal,Nred,Z,mu,radial,rvir,vals = [f[key][:] for key in f_keys]
  rvir /= h # get out of /h units, into cMpc
  N = radial[:,:,1 if reds else 0]
  
  # iterate over bins
  for ii in range(len(mu_bins)-1):
    mask_ii = (mu_bins[ii] <= mu) * (mu < mu_bins[ii+1])
    Omega_m = Wm(np.mean(Z[mask_ii]))
    if 0: # use older method
      dm,ds,dl,dh = get_delta(N[mask_ii],rvir[mask_ii],Z[mask_ii],
                              use_BaBar,count_interior,nix_nulls)
      nfw = get_NFW(np.mean(mu[mask_ii]),np.mean(Z[mask_ii]))
    else: # use newer method
      dm,ds,dnm,dns = get_delta4(N[mask_ii],rvir[mask_ii],mu[mask_ii],
                                 Z[mask_ii],nix_nulls=nix_nulls)
      dl,dh = dm-ds,dm+ds
      dnl,dnh = dnm-dns,dnm+dns
      nfw = dnm
    
    label_ii = r'$\mu|[%0.2f,%0.2f)$' % (mu_bins[ii],mu_bins[ii+1])
    if mask_x: # only plot positive values
      mask_iix = dm > 0
      plt.plot(x_midpts[mask_iix],dm[mask_iix],
               mpl_colors[ii],label=label_ii) # mean overdensity
      plt.fill_between(x_midpts[mask_iix],(dm-ds)[mask_iix],
                       (dm+ds)[mask_iix],alpha=.125,
                       lw=0,color=mpl_colors[ii]) # intrinsic scatter
      plt.fill_between(x_midpts[mask_iix],dl[mask_iix],dh[mask_iix],
                       alpha=.125,lw=2,color=mpl_colors[ii]) # err of mean
    else: # don't mask; plot null / negative values
      plt.plot(x_midpts,dm,mpl_colors[ii],label=label_ii) # mean overdensity
      plt.fill_between(x_midpts,dm-ds,dm+ds,alpha=.125,
                       lw=0,color=mpl_colors[ii]) # intrinsic scatter
      plt.fill_between(x_midpts,dl,dh,
                       alpha=.125,lw=2,color=mpl_colors[ii]) # err of mean
    if plot_nfw:
      plt.plot(x_midpts,nfw/Omega_m,mpl_colors[ii],linestyle=':')
      plt.plot(x_midpts,nfw,mpl_colors[ii],linestyle=(0, (1, 3)))
  
  # set up axes
  plt.xlabel(r'Cluster-centric distance $r/r_{\rm vir}$')
  plt.xscale('log')
  plt.xlim(min(x_bins),max(x_bins))
  plt.ylabel(r'Overdensity $\delta$')
  plt.yscale('log')
  # tidy up & display
  plt.legend()
  plt.tight_layout()
  plt.show()


def plot_delta_x_Z(f,reds=False,Z_bins=[.1,.3,.5,.7],mask_x=False):
  " plot_delta_x(f,reds=False) "
  Ngal,Nred,Z,mu,radial,rvir,vals = [f[key][:] for key in f_keys]
  rvir /= h # get out of /h units, into cMpc
  N = radial[:,:,1 if reds else 0]
  
  # iterate over bins
  for ii in range(len(Z_bins)-1):
    mask_ii = (Z_bins[ii] <= Z) * (Z < Z_bins[ii+1])
    dm,ds,dl,dh = get_delta(N[mask_ii],rvir[mask_ii],Z[mask_ii])
    label_ii = r'$Z|[%0.2f,%0.2f)$' % (Z_bins[ii],Z_bins[ii+1])
    if mask_x: # only plot positives
      mask_iix = dm > 0
      plt.plot(x_midpts[mask_iix],dm[mask_iix],
               mpl_colors[ii],label=label_ii) # mean overdensity
      plt.fill_between(x_midpts[mask_iix],(dm-ds)[mask_iix],
                       (dm+ds)[mask_iix],alpha=.125,
                       lw=0,color=mpl_colors[ii]) # intrinsic scatter
      plt.fill_between(x_midpts[mask_iix],dl[mask_iix],dh[mask_iix],
                       alpha=.125,lw=0,color=mpl_colors[ii]) # err of mean
    else: # plot all values
      plt.plot(x_midpts,dm,mpl_colors[ii],label=label_ii) # mean overdensity
      plt.fill_between(x_midpts,(dm-ds),(dm+ds),alpha=.125,
                       lw=0,color=mpl_colors[ii]) # intrinsic scatter
      plt.fill_between(x_midpts,dl,dh,
                       alpha=.125,lw=0,color=mpl_colors[ii]) # err of mean
  
  # set up axes
  plt.xlabel(r'Cluster-centric distance $r/r_{\rm vir}$')
  plt.xscale('log')
  plt.xlim(min(x_bins),max(x_bins))
  plt.ylabel(r'Overdensity $\delta$')
  plt.yscale('log')
  # tidy up & display
  plt.legend()
  plt.tight_layout()
  plt.show()


########################################################################
# set up NFW grabber 

W0,WR,WL = .286,0,1-.286 # Buzzard parameters

def E_sq(z): 
  " expansion factor (unitless Hubble) "
  return W0*(1+z)**3 + WR*(1+z)**2 + WL

def Wm(z):
  " Omega_matter "
  return W0*(1+z)**3 / E_sq(z)

def Dc(z): # Delta_vir
  """
  calculate virial Delta at a given redshift for a given cosmology
  i.e.: overdensity of virialized region as cf critical density
  using original form from Bryan and Norman 1997
  DOI:10.1086/305262 URL:arxiv.org/abs/astro-ph/9710107
  """
  def x_Dv(z):
    return Wm(z)-1
  return 18*np.pi**2 + 82*x_Dv(z) - 39*x_Dv(z)**2
  # NOTE: ranges from 110 to 125 over z=[.1,.3]
  #       ranges from 100 to 170 over z=[0.,2.]
  #       So it's always larger than R200 in our case


########################################################################
########################################################################
# colossus cosmology: 
from colossus.cosmology import cosmology
params = {'flat':True,'H0':70.,'Om0':.286,'Ob0':.046,'sigma8':.82,'ns':.96}
cosmology.addCosmology('buzzard',params)
cosmo = cosmology.setCosmology('buzzard')
# see https://bdiemer.bitbucket.io/colossus/cosmology_cosmology.html 

from colossus.halo import concentration
# for concentration docs, see bdiemer.bitbucket.io/colossus/
# halo_concentration.html#halo.concentration.concentration

def get_c(mass,Z,paper='bhattacharya13'):
  """
  input mass (Msol) and redshift to calculate halo concentration 
  redshift range of [0,2], mass range of [2e12,2e15] Msol/h
  Bhattacharya+13 uses WMAP7 as a base cosmology, and allows vir input
  --> can only take singleton input; doesn't handle vector input :/
  """
  return concentration.concentration(mass*h,'vir',Z,paper)
  # function requires Msol/h input; said to return c,c_mask; returns c


def get_NFW(mu,Z):
  " return NFW profile relative to critical density "
  Delta_vir = Dc(Z) # virial overdensity ~ 178
  c_vir = get_c(10**mu,Z,'bhattacharya13') # concentration
  A_NFW = np.log(1+c_vir)-c_vir/(1.+c_vir) # concentration-dependent prefactor
  return Delta_vir/(3*A_NFW)/x_midpts/(1./c_vir+x_midpts)**2


def plot_rel_NFW(f,reds=False,mu_bins=[13.75,14,14.25,14.5,14.75,15,15.25]):
  Ngal,Nred,Z,mu,radial,rvir,vals = [f[key][:] for key in f_keys]
  rvir /= h # get out of /h units, into cMpc
  N = radial[:,:,1 if reds else 0]
  
  for ii in range(len(mu_bins)-1):
    mask_ii = (mu_bins[ii] <= mu) * (mu < mu_bins[ii+1])
    dm,ds,dl,dh = get_delta(N[mask_ii],rvir[mask_ii],Z[mask_ii])
    label_ii = r'$\mu|[%0.2f,%0.2f)$' % (mu_bins[ii],mu_bins[ii+1])
    nfw = get_NFW(np.mean(mu[mask_ii]),np.mean(Z[mask_ii]))
    # plotting
    if 0:
      plt.plot(x_midpts,dm,label=label_ii)
      plt.plot(x_midpts,dm+1,'grey',lw=.1)
      plt.plot(x_midpts,nfw,'k',lw=.1)
      plt.plot(x_midpts,nfw/Wm(np.mean(Z[mask_ii])),'r',lw=.1)
      plt.plot(x_midpts,nfw*Wm(np.mean(Z[mask_ii])),'y',lw=.1)
    else: # plot relative
      enneff = nfw/Wm(np.mean(Z[mask_ii]))
      plt.plot(x_midpts,dm/enneff,label=label_ii)
      plt.plot(x_midpts,np.ones(len(x_midpts))*Wm(np.mean(Z[mask_ii])),'r',lw=.1)
      plt.fill_between(x_midpts,dl/enneff,dh/enneff,alpha=.125,lw=0)
  
  plt.plot(x_midpts,np.ones(len(x_midpts)),'k')
  
  # fix up axes
  plt.xlabel(r'Cluster-centric distance $r/r_{\rm vir}$')
  plt.xscale('log')
  plt.xlim(min(x_bins),max(x_bins))
  plt.ylabel(r'Relative overdensity $\delta/\delta_{\rm NFW}$')
  plt.yscale('log')
  # tidy up & display 
  if len(mu_bins) <= 6:
    plt.legend()
  plt.tight_layout()
  plt.show()


########################################################################
def make_picture(f,reds=False,nix_nulls=True,mu_range=[14.75,15],Z_range=[.1,.7],Npix=500,R=.25):
  """
  make picture of radial profile as cf NFW expectations
  Npix = side length of picture to generate; should be even preferably
  R = fraction of side length that the virial radius should be plotted
      should probably be less than 1/2
  """
  mask_mu = (mu_range[0] <= f['mu'][:]) * (f['mu'][:] < mu_range[1])
  mu_label = r'$\mu|[%0.2f,%0.2f)$' % (mu_range[0],mu_range[1])
  mask_Z = (Z_range[0] <= f['Z'][:]) * (f['Z'][:] < Z_range[1])
  Z_label = r'$Z|[%0.2g,%0.2g)$' % (Z_range[0],Z_range[1])
  mask = np.logical_and(mask_mu,mask_Z)
  Ngal,Nred,Z,mu,rvir = [f[key][mask] for key in ['Ngal','Nred','Z','mu','rvir']]
  radial = f['radial'][:]
  radial = radial[mask]
  rvir /= h # get out of /h units, into cMpc
  N = radial[:,:,1 if reds else 0]
  # grab overdensity data
  if 0: # older get delta method
    dd,dnd = get_delta3(N[0],rvir[0],mu[0],Z[0],nix_nulls=nix_nulls)
    dl,dm,dh = dd # unpack delta data
    dnl,dnm,dnh = dnd # unpack nfw data
  else: # newer get delta method
    dm,ds,dnm,dns = get_delta4(N,rvir,mu,Z,nix_nulls=nix_nulls)
    dl,dh = dm-ds,dm+ds
    dnl,dnh = dnm-dns,dnm+dns
  
  if 0: # make radial plot
    plt.plot(x_midpts,dm,mpl_colors[0])
    plt.fill_between(x_midpts,dl,dh,color=mpl_colors[0],alpha=.125,lw=0)
    plt.plot(x_midpts,dnm,'k')
    plt.fill_between(x_midpts,dnl,dnh,color='k',alpha=.125,lw=0)
    # fix up axes
    plt.xlabel(r'Cluster-centric distance $r/r_{\rm vir}$')
    plt.xscale('log')
    plt.xlim(min(x_midpts),max(x_midpts))
    plt.ylabel(r'Relative overdensity $\delta/\delta_{\rm NFW}$')
    plt.yscale('log')
    # tidy up & display 
    plt.tight_layout()
    plt.show()
  
  # make picture
  points = np.zeros((Npix,Npix))
  x_pts = .5 + np.arange(Npix).astype(float) - Npix/2.
  x_pts /= Npix*R
  y_pts = .5 + np.arange(Npix).astype(float) - Npix/2.
  y_pts /= Npix*R
  x,delta = np.zeros((2,Npix,Npix))
  for ii in range(Npix):
    for jj in range(Npix):
      x[ii,jj] = np.sqrt(x_pts[ii]**2 + y_pts[jj]**2)
  delta = np.interp(x,x_midpts,dm)
  delta_nfw = np.interp(x,x_midpts,dnm)
  
  print('\nmin x: %0.6f' % np.min(x))
  if 0: # check radial variable set up correctly
    plt.figure(figsize=(7,6.5))
    plt.imshow(x,extent=np.array([-1,1,-1,1])/R/2.)
    t = np.linspace(0,2*np.pi,100)
    plt.plot(np.cos(t),np.sin(t),'r',lw=1,dashes=[14,10])
    plt.xlabel(r'$x/r_{\rm vir}$')
    plt.ylabel(r'$y/r_{\rm vir}$')
    plt.tight_layout()
    plt.show()
  
  # plot comparison 
  t = np.linspace(0,2*np.pi,100)
  if 0: print('dels:',delta,'\n',delta_nfw)
  dels = np.log10([delta,delta_nfw])
  vmn,vmx = np.min(dels),np.max(dels)
  
  from mpl_toolkits.axes_grid1 import AxesGrid
  fig = plt.figure(figsize=(10,4.75))
  grid = AxesGrid(fig, 111,  # similar to subplot(142)
                  nrows_ncols=(1, 2),
                  axes_pad=0.0,
                  share_all=True,
                  label_mode="L",
                  cbar_location="right",
                  cbar_mode="single")
  
  for ii in range(2):
    im = grid[ii].imshow(dels[ii],extent=np.array([-1,1,-1,1])/R/2.,
                    vmin=vmn,vmax=vmx,cmap='cividis') 
    grid[ii].set_xlim(grid[ii].get_xlim())
    grid[ii].set_ylim(grid[ii].get_ylim())
    grid[ii].plot(np.cos(t),np.sin(t),'r',lw=1) #,dashes=[14,10])
    grid[ii].plot(np.cos(t)/10,np.sin(t)/10,'purple',lw=.1)
    grid[ii].plot(np.cos(t)/100,np.sin(t)/100,'blue',lw=.1)
    if not ii: # left plot
      grid[ii].set_title(mu_label) # + " & " + Z_label)
      grid[ii].set_ylabel(r'$y/r_{\rm vir}$')
    else: # right plot
      grid[ii].set_title('NFW expectations')
    grid[ii].set_xlabel(r'$x/r_{\rm vir}$')
  grid.cbar_axes[0].colorbar(im)
  cax = grid.cbar_axes[0]
  axis = cax.axis[cax.orientation]
  axis.label.set_text(r'$\log_{10} \delta$')
  plt.tight_layout()
  plt.show()


########################################################################
if __name__=='__main__':
  # plot_delta_x(f_RD)
  plot_delta_x_mu(f_RD,mu_bins=np.arange(13.75,15.5,.25),use_BaBar=False)
  # plot_delta_x_Z(f_RD,Z_bins=np.arange(.05,.75+.05,.1))
  # plot_delta_x_mu(f_RD,mu_bins=[13.75,14,14.5,15.5],use_BaBar=False,count_interior=False,nix_nulls=False)
  # plot_delta_x_Z(f_RD,Z_bins=[.1,.35,.7])
  # plot_rel_NFW(f_RD,mu_bins=[13.75,14,14.5,15,15.5])
  # plot_rel_NFW(f_RD,mu_bins=np.linspace(13.75,15.5,9))
  # plot_delta_x3(f_RD,use_BaBar=False,nix_nulls=True,mu_bins=np.linspace(15,15.4,5))
  # plot_delta_x_heaviest(f_RD,N_heavy=136,N_start=0,nix_nulls=0)
  # plot_delta_x_heaviest(f_RD,N_heavy=14572,N_start=5515,nix_nulls=0)
  
  if 0: # images need ~1600 pixels to get to x=1e-3
    make_picture(f_RD,R=.425,Npix=280,mu_range=[14.25,14.5],Z_range=[.1,.7],nix_nulls=True)
    make_picture(f_RD,R=.425,Npix=280,mu_range=[15.0,15.4],Z_range=[.1,.7],nix_nulls=True)
  
  if 0: 
    Ngal,Nred,Z,mu,radial,rvir,vals = [f_RD[key][:136] for key in f_keys]
    rvir /= h # get out of /h units, into cMpc
    N = radial[:,:,1 if False else 0]
    get_delta4(N,rvir,mu,Z)
  print('~fin')
