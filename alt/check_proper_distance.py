import cosmology as cgy
from config import xtra # dir to XTRA

fname = xtra + 'Output/tabulated_data/Proper_Distance.txt'

import pandas as pd

df = pd.read_table(fname,sep=' ',skiprows=1,names=['Z','PD'])
# redshift and proper distance (Mpc/h)

# cosmology returns values in Mpc (not per h) since H0 is an input
prop_dist = cgy.r(df.Z)
h = cgy.aardvark["H0"]/100.

from matplotlib import pyplot as plt
# plt.scatter(df.PD/h,prop_dist)
from numpy import arange
plt.scatter(df.Z,(df.PD-prop_dist*h)/df.PD)
# plt.plot([0,1e4],[0,1e4],c='k')
plt.xlabel(r'Redshift $z$')
plt.ylabel(r'error in prop_dist')
plt.show()
