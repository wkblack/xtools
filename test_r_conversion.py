#!/bin/python
########################################################################
# use aardvark dataset to check whether r_vir to r200 to ... works well

from matplotlib import pyplot as plt
from astropy.io import fits
import r_conversion
import numpy as np

import config as cfg
sim = cfg.Aardvark
hfs = sim['halos'] # Aardvark halos

if 0: # def check_nonzero():
  from sys import stdout
  for f in hfs:
    data = fits.open(f)[1].data
    mask = data.R500>0 # there are NO 2500 or 500 radii!!!
    print sum(mask),
    stdout.flush()
  raise SystemExit
    
f=hfs[0]

########################################################################
# load and mask data
print "loading and masking data"

data = fits.open(f)[1].data
# demand radii be nonzero
mask = np.logical_and(data.RS>0,data.R200>0)
mask = np.logical_and(mask,data.RVIR>0)
data = data[mask]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# grab specific variables

rs=data.RS/1000. # convert to Mpc
rvir=data.RVIR
r200=data.R200

if 0:
  plt.hist(rs,bins=50,label='rs')
  plt.hist(rvir,bins=50,label='rvir')
  plt.hist(r200,bins=50,label='r200')
  plt.legend()
  plt.show()

r500=data.R500 # NULL
r2500=data.R2500 # NULL
mu=np.log10(data.M200)
mu_mask=mu>13
z=data.Z
z_mask=np.logical_and(.1<z,z<.3)

joint_mask = np.logical_and(z_mask,mu_mask)

print "min//max z : %g//%g" % (min(z),max(z))

########################################################################

if 0: # slope is about .85
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  plt.plot([.05,2],[.05,2],'k')
  plt.plot([.05,2],[.05*.9,2*.9],'grey')
  plt.plot([.05,2],[.05*.8,2*.8],'grey')
  plt.plot([.05,2],[.025,1],'grey')
  plt.xscale('log')
  plt.yscale('log')
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  plt.scatter(rvir,r200,1)
  plt.xlabel(r'$r_{{\rm VIR}}$')
  plt.ylabel(r'$r_{200}$')
  plt.show()
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  raise SystemExit

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
print "Calculating radii"

from sys import stdout

# """
print "calculating r200\r",;stdout.flush()
if 0:
  r200_calc = r_conversion.r_Delta(delta=200,Rs=rs,
                R_ref=rvir,delta_ref='vir',z=z)
if 1:
  import HK02
  Dv = HK02.Dc(z)
  r200_calc2 = HK02.rh(rs,200,Dv,rvir)
  if 0: # plot diff btw actual and calculated r200
    plt.plot([.1,2],[.1,2],'k')
    if 0: 
      plt.scatter(r200,r200_calc,1,label='r200_calc1',c=joint_mask,alpha=.5)
      plt.scatter(r200,r200_calc2,1,label='r200_calc2',c=joint_mask,alpha=.5)
      plt.legend()
    else:
      plt.scatter(r200[~joint_mask],r200_calc2[~joint_mask],1,c='purple',alpha=.0625)
      plt.scatter(r200[joint_mask],r200_calc2[joint_mask],1,c='yellow',alpha=1)
    plt.ylabel(r'calculated $r_{200}$')
  elif 0: # compare the two methods
    err=(r200_calc-r200_calc2)/r200_calc2
    plt.scatter(r200[~joint_mask],err[~joint_mask],1,c='purple',alpha=.0625)
    plt.scatter(r200[joint_mask],err[joint_mask],1,c='yellow',alpha=1)
    plt.ylabel(r'fractional error between calculated models')
    # |error|<10%, and for the most part, |error|<5%
  else: # plot error in r200 calculation
    err=(r200_calc2-r200)/r200
    plt.plot([.05,2],[0,0],'k')
    plt.scatter(r200[~joint_mask],err[~joint_mask],1,c='purple',alpha=.0625)
    plt.scatter(r200[joint_mask],err[joint_mask],1,c='yellow',alpha=1)
    plt.ylabel(r'fractional error') # was 'frctional'
  plt.xlabel(r'$r_{200}$')
  plt.show()
  raise SystemExit

print "r200 calculated \r",;stdout.flush()

print "calculating r500\r",;stdout.flush()
r500_calc = r_conversion.r_Delta(delta=500,Rs=rs,
             R_ref=rvir,delta_ref='vir',z=z)
print "r500 calculated \r",;stdout.flush()

print "calculating r2500\r",;stdout.flush()
r2500_calc = r_conversion.r_Delta(delta=2500,Rs=rs,
             R_ref=rvir,delta_ref='vir',z=z)
# r2500_calc_from_r200 = r_conversion.r_Delta(delta=2500,Rs=rs,
#                        R_ref=r200,delta_ref=200,z=z)
print "r2500 calculated \r",;stdout.flush()
# """

print "calculating rvir \r",;stdout.flush()
rvir_calc = r_conversion.r_Delta(delta='vir',Rs=rs,
             R_ref=r200,delta_ref=200,z=z)
print "rvir calculated  \r",;stdout.flush()

deltaVir = r_conversion.DeltaVir(z)

print "Radii and deltaVir calculated."

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# other tests
if 0:
  print "checking stuff"
  print min(rs),min(r200),min(z)
  print "min(Dvir):",min(deltaVir)
  
  f200=r_conversion.f(rs/r200)
  print "f(Rs/R_200):",min(f200),max(f200)
  fvir=r_conversion.f(rs/rvir)
  print "f(Rs/R_vir):",min(fvir),max(fvir)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
print "Plotting..."

err_r200=(r200-r200_calc)/r200
err_rvir=(rvir-rvir_calc)/rvir
N=10**5; imask=np.random.choice(sum(mask),N)
imask = imask[mu_mask[imask]]

if 1: # plot differences between r2500 calculated from vir vs 200
  plt.scatter(r2500_calc,r2500_calc_from_r200,1,deltaVir)
  plt.scatter(r2500_calc[mu>13],r2500_calc_from_r200[mu>13],1,deltaVir[mu>13])
  plt.xlabel(r'from $r_{{\rm VIR}}')
  plt.ylabel(r'from $r_{200}$')
  plt.xscale('log')
  plt.yscale('log')
  plt.show()

if 0:
  plt.plot([0,3],[0,3],'k')
  plt.plot([0,2.5],[0,5],'gray')
  plt.plot([0,3],[0,1.5],'gray')
  plt.scatter(rvir[imask],rvir_calc[imask],1,label=r'$r_{{\rm VIR}}$')
  plt.scatter(r200[imask],r200_calc[imask],1,label=r'$r_{200}$')
  print "Delta_VIR range: [%g,%g]" % (min(deltaVir),max(deltaVir))
  plt.xlabel(r'actual')
  plt.ylabel(r'calculated')
  plt.legend()
  plt.gca().set_aspect('equal')
  # plt.colorbar()
  plt.title('All values should hug middle line!')
  plt.tight_layout()
  plt.show()

if 0:
  plt.plot([0,3],[0,0],'k')
  plt.scatter(rvir[imask],err_rvir[imask],1,
              label=r'$r_{{\rm VIR}}$',c=deltaVir[imask])
  plt.scatter(r200[imask],err_r200[imask],1,
              label=r'$r_{200}$',c=deltaVir[imask])
  plt.xscale('log')
  plt.xlabel('Actual radii')
  plt.ylabel('Fractional error')
  plt.title(r'Error in $r_\Delta$ conversion')
  plt.colorbar(label=r'$\Delta_v$')
  plt.tight_layout()
  plt.show()

if True:
  if 0:
    plt.plot([0,.5],[0,.5],'k')
    plt.plot([0,2.5],[0,5],'gray')
    plt.plot([0,3],[0,1.5],'gray')
    plt.scatter(r200[imask],r200_calc[imask],label=r'$r_{200}$')
    plt.scatter(r500[imask],r500_calc[imask],label=r'$r_{500}$')
    plt.scatter(r2500[imask],r2500_calc[imask],label=r'$r_{2500}$')
    plt.xlabel(r'Actual radii')
  elif 0:
    plt.hist(np.log10(r200_calc[mu>13]),bins=100,log=True)
    plt.hist(np.log10(r500_calc[mu>13]),bins=100,log=True)
    plt.hist(np.log10(r2500_calc[mu>13]),bins=100,log=True)
    plt.xlabel(r'$r_\Delta$')
  elif 1:
    plt.hist((r500_calc[mu>13]/r200_calc[mu>13]),bins=50,log=True)
    plt.hist((r2500_calc[mu>13]/r200_calc[mu>13]),bins=50,log=True)
    plt.xlabel(r'$r1\Delta/r_{200}$')
  # plt.ylabel(r'Calculated radii')
  # plt.legend()
  # plt.gca().set_aspect('equal')
  plt.tight_layout()
  plt.show()

if 0: # plotmass vs deltaVir
  plt.scatter(mu[mu>13],deltaVir[mu>13],1,c=np.log10(-err_r200[mu>13])) # ,vmin=-2,vmax=-.5)
  plt.xlabel(r'$\mu$')
  plt.ylabel(r'$\Delta_{{\rm VIR}}$')
  plt.colorbar(label='log10 error')
  plt.tight_layout()
  plt.show()
if 0: # try to correct using DeltaVir
  plt.scatter(r200_calc[imask]/r200[imask],deltaVir[imask]/200.,1,alpha=.125,c=mu[imask])
  plt.xlabel(r'$r_{200,{\rm calc}}/r_{200}$')
  plt.ylabel(r'$\Delta_{{\rm VIR}}/\Delta_{200}$')
  plt.xscale('log')
  plt.yscale('log')
  plt.tight_layout()
  plt.show()
if 0: # try to correct using DeltaVir
  plt.plot([.05,2.5],[.05,2.5],'k')
  plt.scatter(r200[mu>13],r200_calc[mu>13],1,alpha=.125,label=r'calculated')
  plt.scatter(r200[mu>13],r200_calc[mu>13]*200./deltaVir[mu>13],1,label=r'reduced',alpha=.25)
  plt.xlabel(r'Actual radius $r_{200}$')
  plt.ylabel(r'Calculated radius $r_{200,{\rm calc}}$')
  plt.xscale('log')
  plt.yscale('log')
  plt.legend()
  plt.tight_layout()
  plt.show()

if 0:
  # plot guidelines
  plt.plot([.35,2],[.70,4],'grey')
  plt.plot([.5,2],[.5,2],'k')
  # plot up calcualted r200 values
  plt.scatter(r200[imask],r200_calc[imask],1,c=mu[imask],alpha=.5)
  plt.colorbar(label=r'$\mu \equiv \log_{10}(M_{200})$')
  plt.xlabel(r'$r_{200}$')
  plt.ylabel(r'Calculated $r_{200}$')
  plt.xscale('log')
  plt.yscale('log')
  plt.show()

print '~fin'
