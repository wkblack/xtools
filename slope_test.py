#!/bin/python
# check whether the values of slope in my data 
# are consistent with Arya's 201? paper

########################################################################
# files 
########################################################################

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# file declarations
from os.path import isfile
import config as cfg
fname = 'output_bicycle.csv'
bicycle = cfg.xtools + fname
assert(isfile(bicycle))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# read in bicycle output
from pandas import read_csv
df_bi = read_csv(bicycle)
# header: HID1,HID2,STR1,STR2,LAMBDA,z,CENTRAL_FLAG,CETNRAL_BCG_FLAG,RA,DEC,LAMBDA_ERR,M1,M2,Lx,Tx,Fx

from numpy import all as np_all
assert(np_all(df_bi.z<=0.3))
assert(np_all(df_bi.z>=0.1))
assert(np_all(df_bi.LAMBDA>=20))
from numpy import logical_and
CEN = logical_and(df_bi.CENTRAL_FLAG,df_bi.CENTRAL_BCG_FLAG)

########################################################################
# calculations 
########################################################################

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# calculate the slope using linmix
from numpy import log as ln
from time import time
from sys import stdout
import linmix 
silent_mcmc = False

# linmix inputs
x,xsig = ln(df_bi.LAMBDA)[CEN] , (df_bi.LAMBDA_ERR/df_bi.LAMBDA)[CEN]
y,ysig = ln(df_bi.M1)[CEN] , (df_bi.M1/df_bi.M1/10)[CEN]
# TODO: check whether it'd be better to fabricate mass errors

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# plot up mass vs richness (for the visual check) 
if 0: 
  print "plotting"
  # df_bi.plot('LAMBDA','M1',kind='scatter',c='w',
  #             marker='o',alpha=.25,edgecolors='y',facecolors='none')
  from matplotlib import pyplot as plt
  plt.errorbar(x,y,xerr=xsig,yerr=ysig,ls=" ",c='k',alpha=.25)
  plt.xlabel(r'$\log \lambda$'); plt.ylabel(r'$\log M_1$')
  # plt.xscale('log'); plt.yscale('log') 
  plt.show()
  print "exiting..."
  raise SystemExit
  
# linmix these guys
print "Mixing...\r",
start=time(); stdout.flush()
##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##
lm1 = linmix.LinMix(x,y,xsig,ysig,K=2)
lm1.run_mcmc(silent=silent_mcmc)
##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##
end=time(); elapsed=end-start;
print "Mixing complete! Took %g seconds." % elapsed

# print results
print("alpha mean and stddev:  {}, {}".format(
      lm1.chain['alpha'].mean(),  lm1.chain['alpha'].std()))
print("beta mean and stddev:   {}, {}".format(
      lm1.chain['beta'].mean(),   lm1.chain['beta'].std()))
print("sigsqr mean and stddev: {}, {}".format(
      lm1.chain['sigsqr'].mean(), lm1.chain['sigsqr'].std()))







########################################################################
print "~fin"
