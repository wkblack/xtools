#!/bin/python
# import M200 and other parameters from other files into a new group, 
# based off of Arya's RM_training catalog

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up file read in 
from os.path import isfile # to validate
import config as cfg

# matching dataset 
match_name = 'RM_training_corrected.csv'
match_path = cfg.xtools + match_name
assert(isfile(match_path))

# halo dataset
sim = cfg.Aardvark
halo_path = sim['halo_dir'] + sim['halo_fname']
from glob import glob
halo_files = glob(halo_path)
for f in halo_files: 
  assert(isfile(f))

assert(len(halo_files) > 0)

########################################################################
# imports

from time import time
programstart = time() 
from sys import stdout
print "Importing libraries...\r",
stdout.flush(); start = time() 
import numpy as np
from astropy.io import fits # to import data

end = time() 
print "Importing libraries took %g seconds" % (end-start) 


########################################################################
# read in matching btw halo & cluster 
import pandas as pd

print "Reading in matching dataset...\r",
stdout.flush(); start = time() 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# item list:  0--4  : CLUSTERID,M_HALOID1,M_HALOID2,M_HALOID3,M_HALOID4,
#             5--9  : M_HALOIDN,STR1,STR2,STR3,STR4,
#            10--14 : STRO,STR_EXP,LAMBDA,Z,CENTRAL_FLAG,
#            15--19 : CENTRAL_BCG_FLAG,RA,DEC,R_LAMBDA,LAMBDA_ERR,
#            20--21 : M_1,M_2
data = np.loadtxt(match_path,delimiter=',',skiprows=1)
[CID,HID1,HID2,HID3,HID4,
 HIDN,STR1,STR2,STR3,STR4,
  STRO]=range(11)
df_RM = pd.read_csv(match_path)

print "Reading in matching dataset took %g seconds" % (time()-start) 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# compile list of halos 

HID = []            # list of halo IDs from halo files
M200 = []           # list of halo M200 from halo files

start = time() 
for i,f in enumerate(sorted(halo_files)):
  print "Reading in halo file %i/%i\r" % (i+1,len(halo_files)),
  stdout.flush()
  new_data = fits.open(f)[1].data
  # mask = [ID in data[:,CID] for ID in new_data['HALOID']]
  HID += list(new_data['HALOID'])
  M200 += list(new_data['M200']) # *0.714 to get to ~ M500
  # This is M200,critical/h (just to be clear). 

print "Reading in all halo files took %g seconds" % (time()-start) 

HID = np.array(HID)

df_halo = pd.DataFrame({'HID':HID,'M200':M200})

# now we should have full HID and M200 lists we can match with
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# iterate over the matching list, use HID to grab M200


if 0: # test pandas union
  # take df_RM and join with df_halo, using HID
  # print df_RM.keys() 
  # print df_halo.keys() 
  df_out = df_RM.set_index('M_HALOID1').join(df_halo.set_index('HID'),
                                             rsuffix='_1')
  print "one done"
  df_out = df_RM.set_index('M_HALOID2').join(df_halo.set_index('HID'),
                                             rsuffix='_2')
  df_out = df_RM.set_index('M_HALOID3').join(df_halo.set_index('HID'),
                                             rsuffix='_3')
  df_out = df_RM.set_index('M_HALOID4').join(df_halo.set_index('HID'),
                                             rsuffix='_4')
  with pd.option_context('display.max_rows', 20, 'display.max_columns', None):
    print(df)
  print "~fin"
  raise SystemExit


N = len(data[:,HID1])

m200_lists = np.zeros((N,4))

# match_list_m200 = []

for j in range(4): 
  start = time() 
  print "Analyzing HID%i\r" % (j+1),
  stdout.flush()
  for i,halo_id in enumerate(data[:,HID1+j]):
    # print "Analyzing HID%i for halo %i/%i\r" % (j+1,i+1,N),
    stdout.flush()
    m200_lists[i,j] = M200[np.where(HID==int(halo_id))[0][0]]
    # match_list_m200 += M200[np.where(HID==halo_id)[0][0]]
  print "Analyzing HID%i took %g seconds" % (j+1,time()-start)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# save data 

outfile = 'halo_M200.csv'
outdata = np.concatenate((data[:,HID1:HID4+1],m200_lists),axis=1)
head = 'HID1,HID2,HID3,HID4; M1,M2,M3,M4 (M200/h)'
np.savetxt(outfile,outdata,header=head,fmt='%i,%i,%i,%i,%g,%g,%g,%g')

print 'Process in total took %g minutes.' % ((time()-programstart)/60.0)
print '~fin'
raise SystemExit
