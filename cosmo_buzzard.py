# Cosmology methods for the Buzzard parameter set
########################################################################
# imports
import numpy as np

from scipy import integrate

from astropy.cosmology import LambdaCDM

# fundamental constants
c = 299792458/1000. # km/s
yr = 365.25*24*60*60 # seconds in a year
Myr = 10**6 * yr # seconds in a Megayear
AU = 149597870700 * 10**-3 # km in an astronomical unit
pc = 648000/np.pi * AU # km in a parsec
Mpc = 10**6 * pc # km in a Megaparsec
G_astro = 4.30091e-3 # Gravitational constant in pc(km/s)^2/Msol

# input cosmology
buzzard = {
  "Wm":0.286, 
  "H0":70, # (km/s)/Mpc
  "sig8":0.82, 
  "ns":0.96, 
  "Wb":0.046,
  "Neff":3.046,
  "url":"https://arxiv.org/abs/1901.02401",
  # from LCDM:
  "Wk":0,
  "Wr":0,
  "WL":1-.286,
  "wDE": -1.0,
}
h = buzzard['H0']/100.

# generate cosmology object for astropy uses
cosmo_astropy = LambdaCDM(buzzard["H0"],buzzard["Wm"],buzzard["WL"],
                          Neff=buzzard["Neff"],name='buzzard')


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# sky coverage
buzzard_sky_deg = 10313 # square degrees (about a quarter sky)
buzzard_sky_str = buzzard_sky_deg/np.degrees(1.)**2 # in steradians (apx pi)
buzzard_sky = buzzard_sky_deg # default

DES_footprint = 5000 # approximately

full_sky_str = 4*np.pi
full_sky_deg = full_sky_str*np.degrees(1.)**2 # in degrees (apx 41253)
full_sky = full_sky_deg

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# functions


def E(Z): 
  " expansion factor for buzzard cosmology: flat LCDM universe "
  return np.sqrt(buzzard["Wm"]*(1+Z)**3 + buzzard["WL"]) # unitless


def Omega_m(Z):
  " at redshift Z, give current matter density "
  return (buzzard["Wm"]*(1+Z)**3)/E(Z)**2 # unitless


def d_comov(Z):
  """
  input redshifts and cosmology; output comoving distance to redshifts
  assume Buzzard cosmology: flat LCDM universe
  """
  z_int = [integrate.quad(lambda z: 1/E(z),0,z_ii)[0] for z_ii in Z]
  return c/buzzard['H0']*np.array(z_int) # cMpc


def coVol(z_min=0,z_max=.7,Omega=4*np.pi):
  """ 
  coVol(z_min=0,z_max=.7,Omega=4*np.pi)
  return a comoving volume between two radii 
  """
  R_i,R_f=d_comov([z_min,z_max])
  return Omega/3.*(R_f**3 - R_i**3) # cMpc^3


def rho_crit(z=0):
  """
  returns critical density in units of Msol/Mpc^3
  
  H0 -> km/s)/Mpc; Ga -> pc(km/s)^2/Msol
  so H0^2/Ga -> Msol/Mpc^2/pc * (10^6 pc/Mpc) -> Msol/Mpc^3
  """
  return 3*(buzzard['H0']*E(z))**2/(8*np.pi*G_astro)*10**6


########################################################################
# colossus cosmology: 
from colossus.cosmology import cosmology
params = {'flat':True,'H0':70.,'Om0':.286,'Ob0':.046,'sigma8':.82,'ns':.96}
cosmology.addCosmology('buzzard',params)
cosmo = cosmology.setCosmology('buzzard')
# see https://bdiemer.bitbucket.io/colossus/cosmology_cosmology.html 

from colossus.halo import concentration
# for concentration docs, see bdiemer.bitbucket.io/colossus/
# halo_concentration.html#halo.concentration.concentration

from colossus.halo import mass_defs 
# https://bdiemer.bitbucket.io/colossus/halo_mass_defs.html
from colossus.halo import mass_so
# https://bdiemer.bitbucket.io/colossus/halo_mass_so.html

def Delta_vir(Z):
  """
  calculate virial Delta at a given redshift for a given cosmology
  i.e.: overdensity of virialized region as cf critical density
  using original form from Bryan and Norman 1997
  DOI:10.1086/305262 URL:arxiv.org/abs/astro-ph/9710107
  """
  def x_Dv(Z):
    return Omega_m(Z)-1
  return 18*np.pi**2 + 82*x_Dv(Z) - 39*x_Dv(Z)**2
  # NOTE: ranges from 110 to 125 over z=[.1,.3], and
  #       ranges from 100 to 170 over z=[0.,2.], so
  #       R_vir > R_200 for all redshift here


def get_c(mass,Z,paper='bhattacharya13'):
  """
  input mass (Msol) and redshift to calculate halo concentration 
  redshift range of [0,2], mass range of [2e12,2e15] Msol/h
  Bhattacharya+13 uses WMAP7 as a base cosmology, and allows vir input
  """
  return concentration.concentration(mass*h,'vir',Z,paper)
  # function requires Msol/h input; said to return c,c_mask; returns c


def A_NFW(c):
  " concentration-dependent prefactor for NFW profile "
  return np.log(1+c)-c/(1.+c)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# mass conversion

def get_M_X(Mvir,Z,X='500c'):
  """
  wrapper for COLOSSUS's change mass definition
  https://bdiemer.bitbucket.io/colossus/halo_mass_defs.html
  
  Parameters
  ----------
  Mvir, Z (redshift), X
  X = new format, e.g. '500c', '200m'
  """
  Mvir = np.array(Mvir)
  cvir = get_c(Mvir*h,Z) # use Mvir in Msol/h units
  M_X,R_X,c_X = mass_defs.changeMassDefinition(Mvir*h,cvir,Z,'vir',X)
  return M_X/h # return in Msol (remove per h units)


def get_Mvir(M_X,Z,X='200m',paper='bhattacharya13'):
  """
  wrapper for COLOSSUS's change mass definition
  change some other definition of mass into virial
  """
  M_X = np.array(M_X)
  c_X = concentration.concentration(M_X*h,X,Z,paper)
  M_vir,R_vir,c_vir = mass_defs.changeMassDefinition(M_X*h,c_X,Z,X,'vir')
  return M_vir/h # return in Msol (remove per h units)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def NFW(r,Delta, r_Delta=None, M_Delta=None, rho_c=None,
        c_Delta=None, Rs=None):
  """
  return NFW profile (relative to rho_crit) for given set of radii r
  must have either {r_Delta or {rho_c and M_Delta}} 
        and either {c_Delta or Rs} defined
  
  NFW(r,Delta,...)
  """
  
  # handle alternate inputs: [cD or Rs] and [rD or MD]
  
  if r_Delta==None:
    if M_Delta!=None:
      # convert M_Delta to r_Delta; requires critical density
      if rho_c==None:
        print("ERROR: Must define rho_c! [voxel.NFW]")
        return -1
      else:
        r_Delta = (3*M_Delta/(4*np.pi*rho_c*Delta))**(1/3.)
    else:
      print("ERROR: Must define either r_Delta or M_Delta! [voxel.NFW]")
      return -1
  
  if c_Delta==None:
    if Rs!=None:
      # convert Rs to c_Delta
      c_Delta = r_Delta/Rs
    else:
      print("ERROR: Must define either c_Delta or Rs! [voxel.NFW]")
      return -1
  
  return Delta/(3*A_NFW(c_Delta))/(r/r_Delta)/(1./c_Delta+r/r_Delta)**2


# fin


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def x_to_R(Z,x,Rvir):
  """
  convert from r/r_vir to R (arcmin)
  
  Parameters
  ------
  Z : redshift of each halo
  x : r/r_vir for each halo
  Rvir : virial radius for each halo in cMpc (convert out of 'per h'!)
  """
  return Rvir * x / cosmo_astropy.kpc_comoving_per_arcmin(Z) * 1000
