#!/bin/python
print "importing..."
import matplotlib
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True

from matplotlib import pyplot as plt 
from numpy import loadtxt
print "finished importing" 

fname = 'mcmc_result_combined.dat'
data = loadtxt(fname,delimiter=',')
[FLOOR,ALPHA,ALPHA_SIG,BETA,BETA_SIG,SIGMA,SIGMA_SIG] = range(7)

limits = {"fx_min" : -23, # ln limits 
          "fx_max" : -10,
          "b_min"  : 0.725,
          "b_max"  : 0.925, 
          "b_label" : r"Slope $\left(\frac{\ln L_x}{\ln \lambda} \right)$",
          "s_min"  : 0,
          "s_max"  : 0.018,
          "fx_label" : r"$\ln(F_x)$ floor"} 
limits = {"fx_min" : -10,
          "fx_max" : -4.7,
          "b_label" : r"Slope $\left(\frac{\log_{10} L_x}{\log_{10} \lambda} \right)$",
          "fx_label" : r"$\log_{10}(F_x)$ floor"} 

combined=1 # make all three plots together (alpha,beta,sigma) 
           # else just plot slope (beta) 

if combined: 
  # make a triple plot: 
  f, axarr = plt.subplots(3, sharex=True)
  # r"$y=\alpha x + \beta$" 
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  axarr[0].errorbar(data[:,FLOOR],data[:,ALPHA],
                    xerr=None,yerr=data[:,ALPHA_SIG],
                    fmt="o",c='k')
  axarr[0].set_xlim(limits["fx_min"],limits["fx_max"])
  axarr[0].set_title("Slope varying with flux floor") 
  axarr[0].set_ylabel(r'Intercept $\alpha$') 
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  axarr[1].errorbar(data[:,FLOOR],data[:,BETA],
                    xerr=None,yerr=data[:,BETA_SIG],
                    fmt="o",c='k')
  axarr[1].set_xlim(limits["fx_min"],limits["fx_max"])
  # axarr[1].set_ylim(0.725,0.975)
  axarr[1].set_ylabel(limits["b_label"])
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  axarr[2].errorbar(data[:,FLOOR],data[:,SIGMA],
                    xerr=None,yerr=data[:,SIGMA_SIG],
                    fmt="o",c='k')
  axarr[2].set_xlim(limits["fx_min"],limits["fx_max"])
  # axarr[2].set_ylim(0,0.018)
  axarr[2].set_ylabel(r'Scatter $\sigma$') 
  # axarr[2].set_xlabel(r"$\ln(F_x)$ floor")
  axarr[2].set_xlabel(limits["fx_label"])
  
  ########################################################################
  # axarr[2].set_aspect('equal') # trying to fix aspect ratio...
  
  plt.tight_layout() 
  # f.subplots_adjust(hspace=0.02) # also left, right, bottom, top, wspace
  f.subplots_adjust(hspace=0)
  
  plt.savefig('mcmc_result_combined.png')
  plt.show() 
  raise SystemExit

else: # just plot slope (beta) 
  plt.errorbar(data[:,FLOOR],data[:,BETA],
               xerr=None,yerr=data[:,BETA_SIG],
               fmt="o",c='k')
  plt.xlim(limits["fx_min"],limits["fx_max"]) # -23,-10
  plt.xlabel(limits["fx_label"]) # r"$\ln(F_x)$ floor")
  plt.ylabel(limits["b_label"]) # r"Slope $\left(\frac{\ln L_x}{\ln \lambda}\right)$")
  plt.title("Slope varying with flux floor") 
  plt.tight_layout()
  plt.savefig('mcmc_beta_result.png')
  plt.show() 
  
  
  plt.errorbar(data[:,FLOOR],data[:,ALPHA],
               xerr=None,yerr=data[:,ALPHA_SIG],
               fmt="o",c='k')
  plt.tight_layout() 
  plt.show() 
