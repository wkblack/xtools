#!/usr/bin/env python
########################################################################
# program for analyzing mastercat catalogues
########################################################################
" requires: {ES0_model.py, m_star_model.py, cosmo_buzzard.py} "

from time import time
start_time = time()
if __name__=='__main__': print("[timer start]")

from sys import argv
from sys import stdout
from glob import glob
from os.path import isfile
from m_star_model import m_star_cutoff
from scipy.special import erf
from scipy import interpolate

from matplotlib import pyplot as plt
from matplotlib import cm # for iterating through colormaps 
from matplotlib.colors import LinearSegmentedColormap as LSC
mpl_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

import h5py
import linmix
import numpy as np
import healpy as hp
import pandas as pd
import matplotlib as mpl
import cosmo_buzzard as cosmo

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up KLLR 

from kllr import kllr_model
from kllr.regression_model import calculate_weigth as weights
def get_counts(x,xr,kw=.2,kt='gaussian'):
  """ return pseudo counts for each xr bin (sum of all weights at point)
  
  Parameters
  ----------
  x : data input x-values
  xr : KLLR sampled output x-points
  kw : kernel width used in KLLR analysis (default: .2)
  kt : kernel type used in KLLR analysis (default: 'gaussian')
  
  Returns
  -------
  pseudocount N, size=len(xr)
  """
  w,N = np.zeros((2,len(xr)))
  for ii in range(len(xr)):
      w[ii] = np.sum(weights(x,kernel_type=kt,mu=xr[ii],width=kw))
  return w # number of points in analysis bin


def KLLR_bootstrap(x, y, xrange=None, nbins=25, 
                   kernel_type='gaussian', kernel_width=0.2,
                   N_resample=50, printing=False):
  """
  x, y, xrange, 
  nbins=25, kernel_type=None, kernel_width=None,
  N_resample=50 : number of times to resample bootstrap. 
  """
  # set up default x-range
  if xrange is None:
    xrange = np.quantile(x,[0,1])
  
  # 'truth' from using *all* datapoints
  lm = kllr_model(kernel_type,kernel_width)
  KLLR_all = lm.fit(x,y,xrange,nbins)
  
  # set up reports
  KLLR_pars, KLLR_par_err = np.zeros((2,5,N_resample,nbins))
  _xr,_yrm,_icpt,_sl,_sig = KLLR_pars
  
  if printing:
    print("Running bootstrap, step ",end='')
    stdout.flush()
  
  for jj in range(N_resample):
    mask = np.random.choice(len(x),len(x))
    xr,yrm,icpt,sl,sig = lm.fit(x[mask],y[mask],xrange,nbins)
    _xr[jj] = xr
    _yrm[jj] = yrm
    _icpt[jj] = icpt
    _sl[jj] = sl
    _sig[jj] = sig
    
    if printing:
      print(jj+1,end=', ')
      stdout.flush()
  
  if printing:
    print('fin~')

  KLLR_par_mn = np.mean(KLLR_pars,axis=1)
  KLLR_par_err = np.std(KLLR_pars,axis=1)
  
  return KLLR_all, KLLR_par_mn, KLLR_par_err


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# determine red sequence selector 

bool_use_GMMp = False # use GMM+ for selection, else use Red Dragon
if bool_use_GMMp:
  # import P_red from theta tools (GMM+)
  from theta_tools import P_red
else:
  # import P_red from red dragon (3G:6C)
  import sys
  rd_path = '/global/u1/w/wkblack/xproject/red-dragon'
  try:
    sys.path.index(rd_path)
  except ValueError:
    sys.path.append(rd_path)
  from rd_beta import P_red

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# define one and two sigma values
sv3 = [erf(ii/np.sqrt(2)) for ii in range(1,1+3)]
one_sigma = sixty_eight = sv3[0]
two_sigma = ninety_five = sv3[1]
three_sig = ninety_nine = sv3[2]
# combine sigma values: [-2,-1,0,+1,+2]
sv = sigma_vals = [1-two_sigma,1-one_sigma,.5,one_sigma,two_sigma]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up golden aspect ratio
golden_ratio = (1+np.sqrt(5))/2.
golden_aspect = 5*np.array([golden_ratio,1])
double_height = np.array([1,2]) * golden_aspect
double_width = np.array([2,1]) * golden_aspect
six_tall = np.array([3,3]) * golden_aspect


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up default plotting paradigms (mpl)
plt.rcParams['figure.figsize'] = golden_aspect
plt.rcParams['font.size'] = 15
ones = np.ones(2)
# Other options to consider:
# python4astronomers.github.io/plotting/publication.html
# towardsdatascience.com/matplotlib-styles-for-scientific-plotting-d023f74515b4

#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  
# plot labels
label__mu_to_M = r'$\log_{10} \left( M_{\rm vir} / M_{\odot} \right)$'
# label_mu = f'$M|[10^{mu_bins[ii]:0.4g},10^{mu_bins[ii+1]:0.4g})\,M_{\odot}$'
label__M = r'Mass ($M_{\rm vir} / M_{\odot}$)'
label__M_bins = r'$M|[10^{%0.2f},10^{%0.2f}) \, M_{\odot}$'


########################################################################
########################################################################
# import master catalogue
########################################################################
########################################################################

version = 198
# need '/global' at front? 
dir_Herd = '/project/projectdirs/des/jderose/Chinchilla/Herd/'
if version==198:
  mastercat = dir_Herd \
            + 'Chinchilla-3/v1.9.8/sampleselection/Y3a/mastercat/' \
            + 'Buzzard-3_v1.9.8_Y3a_mastercat.h5'
  halocat = '/global/project/projectdirs/des/jderose/Chinchilla/Herd/' \
          + 'Chinchilla-3/v1.9.9/addgalspostprocess/halos/'
  # The halo files have nest ordering, with NSIDE=2.
elif version==200: # DOESN'T HAVE RM RUN ON IT YET
  mastercat = '/global/homes/j/jderose/cscratch/BCC/Chinchilla/Herd/' \
            + 'Chinchilla-3/sampleselection/Y3a/' \
            + 'Buzzard-3_v2.0_Y3a_mastercat.h5'
else:
  mastercat = dir_Herd + '*/sampleselection/Y3{a,b}/*v2.0*mastercat*'
  print("ERROR: Unknown version!")

mcat_200 = dir_Herd + 'Chinchilla-3/v2.0.0/sampleselection/Y3a/Buzzard-3_v2.0_Y3a_mastercat.h5'
fname_Lvet = '/global/cscratch1/sd/wkblack/SkyMaps/buzzard/vet/subset_new_L.h5'
fname_new = 'out_voxel_0000000_to_0160000.h5' # for new analysis

########################################################################
# add locations for each of the tags

Z_loc = 'catalog/bpz/unsheared/redshift_cos' # cosmological redshift
Z_spec_loc = 'catalog/bpz/unsheared/z' # spectroscopic redshift
gold_loc = 'catalog/gold/'
col_loc = gold_loc + 'mag_%s_true' # true magnitudes
col_loc_obs = gold_loc + 'mag_%s' # observed magnitudes
col_loc_err = gold_loc + 'mag_err_%s' # error on magnitudes
hpix_loc = gold_loc + 'hpix_16384' # healpix index
r200_loc = gold_loc + 'r200' # R_vir ckpc/h # NOTE: divide by 1000
m200_loc = gold_loc + 'm200' # M_vir Msol/h
rhalo_loc = gold_loc + 'rhalo' # distance from nearest halo in cMpc/h
P_loc = gold_loc + 'p%s' # 3D position
V_loc = gold_loc + 'v%s' # 3D velocity
SED_loc = gold_loc + 'sdss_sedid' # SED 
ra_loc = gold_loc + 'ra' # right ascension
dec_loc = gold_loc + 'dec' # declination
flux_loc = gold_loc + 'flux_%s'
ivar_loc = gold_loc + 'ivar_%s'
ID_loc = gold_loc + 'coadd_object_id'
HID_loc = gold_loc + 'haloid'


########################################################################
# read in the master file

f = h5py.File(mastercat,'r')

LENGTH = 1504210222 # all galaxies in Bz 2.0.0

if __name__=='__main__':
  # only grab the dataset if we're making masks
  LENGTH = 10**8
  LENGTH = -1
  
  if LENGTH<1: # grab all galaxies
    hpix_16384 = f[hpix_loc][()]
    LENGTH = len(hpix_16384)
  else: # grab a few positions (for testing purposes)
    hpix_16384 = f[hpix_loc][:LENGTH] 
else:
  hpix_16384 = None

########################################################################
# convert between NSIDE
# (see healpy/rotator.py +208 for details)
def convert_index(NSIDE_in,NSIDE_out,index,nest=True):
  " convert a series of pixels from one NSIDE to another "
  assert(np.all(np.array(index)>=0)) # else this will have an error.
  theta,phi = hp.pix2ang(NSIDE_in,index,nest=nest)
  return hp.ang2pix(NSIDE_out,theta,phi,nest=nest)

# set defaults
comm = None
size,rank = 1,0
hpix_8 = hpix_16 = None
hpix_fname = 'hpix_buzzard.h5'
if __name__=='__main__':
  if 0:
    # import MPI
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
  
  # grab hpix
  start_hpix = time()
  if 0: # create anew (~21 min on interactive node)
    hpix_8 = convert_index(16384,8,hpix_16384) # TODO: have this pre-made!
    hpix_16 = convert_index(16384,16,hpix_16384)
    print("took %0.2g min to create hpix" % ((time()-start_hpix)/60.))
  else: # read from file
    with h5py.File(hpix_fname,'r') as ds:
      hpix_8 = ds['hpix_8'][()]
      hpix_16 = ds['hpix_16'][()]
    print("took %0.2g min to read in hpix" % ((time()-start_hpix)/60.))


def pix(NSIDE):
  " returns square degrees area of a pix for a given NSIDE "
  return hp.nside2pixarea(NSIDE,degrees=True)


sky = cosmo.buzzard_sky # sq deg of Buzzard sky (pi str)

########################################################################
# get masks for selecting galaxies and their halos

NSIDE_MAX=128
def get_masks(NSIDE=32,index=0,z_range=[.1,.7],z_buffer=.05,mi=0.2,
              nest=True,mu=13.5,printing=False,timing=False):
  """
  reads in vetting calls and returns a set of masks 
  to select galaxies and halos
  """
  if timing: start = time()
  assert(NSIDE<NSIDE_MAX) # to avoid taking too small a region
  # grab pix ids
  if NSIDE == 8:
    hpix_NSIDE = hpix_8
  elif NSIDE == 16:
    hpix_NSIDE = hpix_16
  else:
    if printing: print("Converting to new hpix...")
    hpix_NSIDE = convert_index(16384,NSIDE,hpix_16384)
  
  if np.all(hpix_NSIDE)==None:
    print("ERROR: no hpix stored! Run as main.")
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # galaxy masks
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  
  # grab neighboring pix
  neighbors = np.array(hp.get_all_neighbours(NSIDE,index,nest=nest))
  neighbors = neighbors[neighbors>-1] # get rid of empty non-existence
  
  # pull values from base hpix, returning subset mask
  mask_pix = hpix_NSIDE==index # central index
  for neighbor in neighbors: # neighbors
    mask_pix += hpix_NSIDE==neighbor
  mask_pix = mask_pix.astype(bool)
  if printing:
    print("len mask_pix:",len(mask_pix))
    print("sum mask_pix:",np.sum(mask_pix))
  
  # redshift mask 
  Z = f[Z_loc][:LENGTH][mask_pix]
  mask_z = ((z_range[0]-z_buffer)<Z)*(Z<(z_range[1]+z_buffer)) # generous
  if printing:
    print('len mask_z:',len(mask_z))
    print('sum mask_z:',np.sum(mask_z))
  
  # 0.2L* lim
  mi_lim = m_star_cutoff(Z[mask_z],cutoff=mi)
  m_i = f[col_loc % 'i'][:LENGTH][mask_pix][mask_z]
  mask_mi = m_i < mi_lim
  if printing:
    print('len mask_mi:',len(mask_mi))
    print('sum mask_mi:',np.sum(mask_mi))
  
  # consolidate
  if printing: print("expect",len(hpix_NSIDE[mask_pix][mask_z][mask_mi]))
  mask_z[mask_z==True] = mask_mi
  mask_pix[mask_pix==True] = mask_z
  glxys = mask_pix
  if printing: print("glxies",len(hpix_NSIDE[glxys]))
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # halo masks
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  
  # grab central
  rhalo = f[rhalo_loc][:LENGTH][glxys]
  mask_rhalo = rhalo==0
  # narrow redshift range
  mask_Zhalo = (z_range[0]<Z[mask_z][mask_rhalo]) \
                        * (Z[mask_z][mask_rhalo]<z_range[1])
  # select only massive halos
  m_vir = f[m200_loc][:LENGTH]\
          [glxys][mask_rhalo][mask_Zhalo]/cosmo.h # Msol
  mask_mu = m_vir > 10.**mu
  
  # select only halos in the center pix
  hpix_curr = hpix_NSIDE[glxys][mask_rhalo][mask_Zhalo][mask_mu]
  if printing: print("before pix mask:",len(hpix_curr))
  # plt.hist(hpix_curr,log=True,bins=50); plt.show()
  # print("includes points:",set(hpix_curr))
  mask_hpix = hpix_curr==index
  if printing: print("after pix mask:",len(hpix_curr[mask_hpix]))
  
  # consolidate
  if printing: print("expect:",len(rhalo[mask_rhalo][mask_Zhalo]\
                                        [mask_mu][mask_hpix]))
  mask_mu[mask_mu==True] = mask_hpix
  mask_Zhalo[mask_Zhalo==True] = mask_mu
  mask_rhalo[mask_rhalo==True] = mask_Zhalo
  halos = mask_rhalo
  if printing: 
    print("halos",len(rhalo[halos]))
    print("checking...")
    print("len:",len(hpix_NSIDE[glxys][halos]))
  if len(rhalo[halos])<1:
    print("Empty masks!")
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # return set of masks to be used one after the other, e.g.:
  # total_data[mask1][mask2][mask3][...]
  if timing: 
    elapsed = time()-start # in seconds
    print(f"Elapsed time: {elapsed:0.3g} s = {elapsed/60.:0.3g} min")
  return [glxys,halos]

########################################################################
# generate subset datafile

fname_subset = 'analyzed/subset.h5'

def grab_subset(fname_out=fname_subset,N=10**4,Z_bounds=[.05,.75],
                mu_limit=13.5,r_limit=True):
  # output at least ['m_g', 'm_i', 'm_r', 'm_z', 'redshift']
  # TODO: Add all tags needed for analysis!! 
  Z_all = f[Z_loc][:N]
  mask_Z = (Z_bounds[0]<=Z_all) * (Z_all<Z_bounds[1])
  Z = Z_all[mask_Z]
  mi_lim = m_star_cutoff(Z,cutoff=0.2)
  M = f[m200_loc][:N][mask_Z]/cosmo.h # Msol
  mu = np.log10(M)
  # observed galaxy colors
  m_go,m_ro,m_io,m_zo = [f[col_loc_obs%tag][:N][mask_Z] \
                         for tag in ['g','r','i','z']] 
  if 0: # ignore oversaturated galaxies
    mask_col = np.ones(np.shape(m_go))
    for col in [m_go,m_ro,m_io,m_zo]:
      mask_col *= col<98 # 99 is oversaturated
    mask_col = mask_col.astype(bool)
  # true galaxy colors
  m_g,m_r,m_i,m_z = [f[col_loc%tag][:N][mask_Z] \
                     for tag in ['g','r','i','z']] 
  rhalo = f[rhalo_loc][:N][mask_Z] # Mpc/h
  rvir = f[r200_loc][:N][mask_Z]/1000 # Mpc/h
  mask_tot = (m_i<mi_lim) * (mu>mu_limit)
  if r_limit:
    mask_tot *= rhalo<rvir
  print("Saving...")
  with h5py.File(fname_out,'w') as ds:
    for lbl,dta in zip(
      ['m_g','m_r','m_i','m_z',
       'm_go','m_ro','m_io','m_zo',
       'redshift','mu','rhalo','r200'],
      [m_g,m_r,m_i,m_z,
       m_go,m_ro,m_io,m_zo,
       Z,mu,rhalo,rvir]):
      ds.create_dataset(lbl,data=dta[mask_tot])
    if r_limit:
      ds.attrs['notes'] = 'subset with r<rvir'
    ds.attrs['m_i limit'] = "L > 0.2 L_*"
    ds.attrs['mu limit'] = "mu > %g" % mu_limit
    ds.attrs['N'] = N
    ds.attrs['Z_bounds'] = Z_bounds
  print("Saved '%s'" % fname_out)


def subset_maker(fname_in=mcat_200,fname_out='subset_new.h5',
                 L_lim=None,mu_lim=None,r_lim=None,
                 N=10**4,Z_bounds=[.05,.75],true_colors=True):
  " create subset of dataset based on a few key keys "
  f_in = h5py.File(fname_in,'r')
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # apply various masks
  
  # vet by redshift
  Z = f_in[Z_loc][:N]
  mask_tot = (Z_bounds[0] < Z) * (Z < Z_bounds[1])
  
  if L_lim is not None:
    # vet by Luminosity
    mi_lim = m_star_cutoff(Z[mask_tot],cutoff=L_lim)
    cl = col_loc if true_colors else col_loc_obs
    m_i = f_in[cl % 'i'][:N][mask_tot]
    mask_tot[mask_tot] = m_i < mi_lim
  
  if mu_lim is not None:
    # vet by mass
    M = f_in[m200_loc][:N][mask_tot]/cosmo.h
    mu = np.log10(M)
    mask_tot[mask_tot] = mu > mu_lim
  
  if r_lim is not None:
    # vet by radial distance to nearest halo
    rhalo = f_in[rhalo_loc][:N][mask_tot] # Mpc/h
    rvir = f_in[r200_loc][:N][mask_tot]/1000 # Mpc/h
    mask_tot[mask_tot] = rhalo < r_lim * rvir
  
  with h5py.File(fname_out,'w') as ds:
    # save data
    for loc in ['catalog/gold/','catalog/bpz/unsheared/']:
      for key in f_in[loc].keys():
        try:
          ds.create_dataset(key,data=f_in[loc+key][:N][mask_tot])
        except RuntimeError as e:
          if key=='coadd_object_id':
            # print(e,end=' ')
            # print('for',key)
            pass
          else:
            print(e)
    # save attributes
    ds.attrs['L_lim'] = L_lim if L_lim is not None else "None"
    ds.attrs['mu_lim'] = mu_lim if mu_lim is not None else "None"
    ds.attrs['r_lim'] = r_lim if r_lim is not None else "None"
    ds.attrs['N'] = N
    ds.attrs['Z_bounds'] = Z_bounds
    ds.attrs['true_colors'] = true_colors
  print(f"Saved {fname_out}")


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# analyze above

def subset_analyzer(N=10,N_init=0,true_colors=True):
  " analyze first (+N_init) N heaviest halos "
  # read in data
  f_in = h5py.File(fname_Lvet,'r')
  mask_cen = f_in['rhalo'][:]==0
  mu = np.log10(f_in['m200'][mask_cen]/cosmo.h)
  Z = f_in['redshift_cos'][mask_cen]
  rvir = f_in['r200'][mask_cen]
  heaviest = np.flip(np.argsort(mu))
  px,py,pz = [f_in[p][:] for p in ['px','py','pz']]
  cl = 'mag_%s_true' if true_colors else 'mag_%s' # location of color
  griz = np.array([f_in[cl % c][:] for c in ['g','r','i','z']])
  
  # set up reports
  rv_labels = ['Z','mu','rvir','Ngal','Nred'] # labels for above
  report_vals = np.zeros((N,len(rv_labels))) # Z,mu,rvir,Ngal,Nred
  radial_report = np.zeros((N,len(x_bins)-1,2)) # rho_gal,rho_red
  
  for ii in range(N_init, N_init+N):
    mu_ii = mu[heaviest[ii]]
    print(f'Processing halo {ii+1-N_init}/{N} (mu={mu_ii:0.5g})',end='\r')
    # set up halo-specific data
    d = np.sqrt((px-px[mask_cen][heaviest[ii]])**2 \
              + (py-py[mask_cen][heaviest[ii]])**2 \
              + (pz-pz[mask_cen][heaviest[ii]])**2 ) 
    rvir_ii = rvir[heaviest[ii]]
    x = d/rvir_ii
    Z_ii = Z[heaviest[ii]]
    N_gal_ii = len(x[x<1])
    # grab RS/BC membership
    if bool_use_GMMp:
      P_red_ii = P_red(Z_ii,griz[:3,x<10],warnings=False)
    else: # use red dragon
      P_red_ii = P_red(Z_ii*np.ones(len(x[x<10])),griz[:,x<10]) # TODO: consider moving without for loop!
    N_red_ii = sum(P_red_ii[x[x<10]<1])
    report_vals[ii-N_init] = Z_ii,mu_ii,rvir_ii,N_gal_ii,N_red_ii
    # add to stacked radial profiles
    radial_report[ii-N_init,:,0],_ = np.histogram(x[x<10],x_bins)
    radial_report[ii-N_init,:,1],_ = np.histogram(x[x<10][P_red_ii>.5],x_bins)
  
  # create output HDF5 file
  if bool_use_GMMp:
    outname = f'gmm_voxel_{N_init:07g}_to_{N_init+N:07g}.h5'
  else: 
    outname = f'out_voxel_{N_init:07g}_to_{N_init+N:07g}.h5'
  # save
  with h5py.File(outname,'w') as ds:
    for ii,lbl in enumerate(rv_labels):
      ds.create_dataset(lbl,data=report_vals[:,ii])
    ds.create_dataset('vals',data=report_vals)
    ds.create_dataset('radial',data=radial_report)
    ds.attrs['N'] = N
    ds.attrs['N_init'] = N_init
    ds.attrs['true_colors'] = true_colors
  print(f'\nSaved {outname}')


def combine_sets(remove_inputs=False):
  " combine outputs from 'subset_analyzer' "
  # collect files
  if bool_use_GMMp:
    globname = 'gmm_voxel_0*.h5'
  else:
    globname = 'out_voxel_0*.h5'
  files = sorted(glob(globname))
  h5_files = [h5py.File(f,'r') for f in files]
  
  # set up base key vectors
  main_keys = ['Ngal','Nred','Z','mu','radial','rvir','vals']
  attr_keys = ['N','N_init','true_colors']
  
  # read off values
  Ngals,Nreds,Zs,mus,radials,rvirs,vals = [[f_N[key][:] 
    for f_N in h5_files] for key in main_keys]
  Ns,N_inits,true_colorses = [[f_N.attrs[key]
    for f_N in h5_files] for key in attr_keys]
  
  # Error check 1: assert files sequential
  N_tot = sum(Ns)
  N_ii = N_inits[0]
  for ii in range(len(Ns)-1):
    assert(N_ii+Ns[ii] == N_inits[ii+1])
    N_ii += Ns[ii]
  
  # Error check 2: assert colors identical
  assert(np.all(np.array(true_colorses)==true_colorses[0]))
  
  # join fields together
  Ngal_tot = np.concatenate(Ngals)
  Nred_tot = np.concatenate(Nreds)
  Z_tot = np.concatenate(Zs)
  mu_tot = np.concatenate(mus)
  radial_tot = np.concatenate(radials)
  rvir_tot = np.concatenate(rvirs)
  vals_tot = np.concatenate(vals)
  fields = [Ngal_tot,Nred_tot,Z_tot,mu_tot,radial_tot,rvir_tot,vals_tot]
  
  # Error check 3: assert mu values descend
  assert(np.all(mu_tot[:-1]>=mu_tot[1:]))
  assert(sum(~(mu_tot[:-1]>=mu_tot[1:]))==0)
  
  # output summed file
  if bool_use_GMMp:
    outname = f'gmm_voxel_{N_inits[0]:07g}_to_{N_inits[0]+N_tot:07g}.h5'
  else:
    outname = f'out_voxel_{N_inits[0]:07g}_to_{N_inits[0]+N_tot:07g}.h5'
  with h5py.File(outname,'w') as ds:
    for ii,lbl in enumerate(main_keys):
      ds.create_dataset(lbl,data=fields[ii])
    ds.attrs['N'] = N_tot
    ds.attrs['N_init'] = N_inits[0]
    ds.attrs['true_colors'] = true_colorses[0]
  print(f'\nSaved {outname}')
  
  # close files
  for f in h5_files:
    f.close()
  
  if remove_inputs:
    # remove input files
    from os import system as run
    run("rm "+" ".join(files))
    print('Removed composite files')


########################################################################
# calculate number density (vetted set)

fname_rho = 'buzzard2_rho.npy' # [Z midpoint values,N,dV,rho]
fname_rho_coeff = 'buzzard2_rho_log_coeffs.npy' # [a,b]

def calculate_rho(Z_bins=np.linspace(.05,.75,351)): # use <= 701 bins
  " measure number density for Buzzard v2.0.0 "
  f_in = h5py.File(fname_Lvet,'r')
  N,_ = np.histogram(f_in['redshift_cos'][:],Z_bins)
  Z_mids = (Z_bins[1:]+Z_bins[:-1])/2.
  Omega = cosmo.DES_footprint/cosmo.full_sky_deg * 4*np.pi
  dV = np.array([cosmo.coVol(Z_bins[ii],Z_bins[ii+1],Omega) \
                 for ii in range(len(Z_mids))])
  rho = N/dV
  distances = cosmo.d_comov(Z_mids)
  # account for low redshift limited volume via Poisson error
  sigma = rho/np.sqrt(N)
  # make linear fit in log space
  coeffs = np.polyfit(np.log(1+Z_mids),np.log(rho),2,w=1/sigma)
  # save outputs
  np.save(fname_rho,[Z_mids,N,dV,rho])
  np.save(fname_rho_coeff,coeffs)


def print_rho(nice=True):
  coeffs = np.flip(np.load(fname_rho_coeff))
  order = len(coeffs) - 1
  if not nice:
    # rough formatting:
    print("ln[rho(z)/cMpc**-3] = ",end='')
    for ii,c in enumerate(coeffs):
      print("%0.3g" % c,end='')
      print("* [ln(1+z)]**%g" % ii,end='')
      if ii < order:
        print(" + ",end='')
      else:
        print()
  else:
    # nicer formatted:
    print("rho(z) = ",end='')
    for ii,c in enumerate(coeffs):
      if ii==0:
        print("(%0.3g cMpc**-3)" % np.exp(c),end='')
      elif ii==1:
        print("(1+z)**%0.3g" % c,end='')
      else:
        print("exp{%0.3g [ln(1+z)]**%g}" % (c,ii),end='')
      # connect terms
      if ii < order:
        print(" * ",end='')
      else:
        print()


def get_rho(Z):
  p = np.poly1d(np.load(fname_rho_coeff))
  x = np.log(1+np.array(Z))
  return np.exp(p(x)) # galaxies per cMpc^3


def plot_rho(sig_vals=sv3,saving=False):
  " plot_rho(sig_vals=sv3,saving=False) "
  # load input data
  [Z,N,dV,rho] = np.load(fname_rho)
  # plt.scatter(Z,rho,10)
  plt.errorbar(Z,rho,5*rho/np.sqrt(N),fmt='o',markersize=1,color='b',
               linewidth=.5,ecolor='r') #,zorder=-1)
  # fix xlim & ylim
  xlim = plt.xlim(plt.xlim())
  ylim = plt.ylim(plt.ylim())
  # plot fit & error
  Z_interp = np.linspace(xlim[0],xlim[1],100)
  rho_interp = get_rho(Z_interp)
  plt.plot(Z_interp,rho_interp,'k')
  # plot cosmic variance (ish; not really quantifiable)
  if sig_vals is not None:
    # get scaling so this reflects X sigma spread
    resid = np.abs(rho-get_rho(Z))
    ds = cosmo.d_comov(Z)
    wr = resid * ds**2 * np.sqrt(N) # weighted residuals
    d_H = cosmo.d_comov([30]) # apx out to last galaxy formed
    dist = cosmo.d_comov(Z_interp)
    f_err_base = (dist/d_H)**-3 - 1 # zero error at z=30
    for q in sig_vals: # ~ [65%, 95%, 99%]
      idx = np.argmin(np.abs(np.abs(wr)-np.quantile(np.abs(wr),q))) 
      f_err_idx = (ds[idx]/d_H)**-3 - 1 
      fac = resid[idx]*(1+1/np.sqrt(N[idx]))/f_err_idx
      # plot
      f_err = f_err_base * fac # normalize
      plt.fill_between(Z_interp,rho_interp-f_err,rho_interp+f_err,
                       linewidth=0,alpha=.0625,color='k')
  plt.xlabel('Redshift')
  plt.ylabel(r'Number density (cMpc$^{-3}$)')
  plt.tight_layout()
  if saving:
    fname_out = 'rho_of_z.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


########################################################################

fname = 'analyzed/pix%02i/pix%05i.hdf5'
fname_glob = 'analyzed/pix%02i/pix*.hdf5'

def unique_identifier(NSIDE,index):
  # eqn 1 of https://healpix.sourceforge.io/pdf/intro.pdf
  return np.array(index)+4*np.array(NSIDE)**2

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# xi=log10(r/r_vir)
# gets sketchy <-2 and loses relevance >~ 0, but I want the outliers.
dxi=.1
xi_bins = np.arange(-3,1,dxi)
x_bins = 10**xi_bins
_j = np.arange(len(x_bins)-1)
X = 4/3.*np.pi*(x_bins[_j+1]**3-x_bins[_j]**3) # volume element (unitless)
x_midpts = 10.**((xi_bins[_j]+xi_bins[_j+1])/2.) # for plotting

def analyze_masks(masks,out=None,timing=False): # out=(NSIDE,index) 
  """
  read in pix masks for galaxies and halos from get_masks(NSIDE,index)
  for each halo, analyze its contents: {Ngal, Nred; radial profile}
    radial profile: for each {Z,mu,r/r_vir}:{N_halo,N_gal,Volume}
  output files with HNN + RP data (including Z, m_vir, ... ?)
  """
  if timing: start = time()
  [glxys,halos]=masks # unpack masks
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # galaxy info 
  Z = f[Z_loc][glxys] # grab redshifts in pix
  Px,Py,Pz = [f[P_loc%tag][glxys] \
                  for tag in ['x','y','z']] # glxy positions
  m_g,m_r,m_i,m_z = [f[col_loc%tag][glxys] \
                  for tag in ['g','r','i','z']] # glxy colors
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # halo info 
  r_vir = f[r200_loc][glxys][halos] # grab halo dimensions (cMpc/h)
  m_vir =  f[m200_loc][glxys][halos] # Msol/h
  N_halos = len(r_vir) # total count of halos in pix
  Ngal,Nred = np.zeros((2,N_halos))
  radial_report = np.zeros((N_halos,len(x_bins),2))
  
  if bool_use_GMMp:
    Prob_red = P_red(Z,[m_g,m_r,m_i],warnings=False)
  else:
    Prob_red = P_red(Z,[m_g,m_r,m_i,m_z])
  
  for ii in range(N_halos):
    zh = Z[halos][ii] # halo redshift
    pxh,pyh,pzh = [pp[halos][ii] 
                   for pp in [Px,Py,Pz]] # halo position (cMpc/h)
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # count (red) galaxies within r_vir (halo quantity)
    dist = np.sqrt((Px-pxh)**2 + (Py-pyh)**2 + (Pz-pzh)**2) # cMpc/h
    mask_inside = dist<r_vir[ii]
    mask_xbins = dist<10*r_vir[ii] # implement in radial profile?
    Ngal[ii] = len(dist[mask_inside])
    # NOTE: to be more precise, I should calculate P_red based on halo redshift zh
    Nred[ii] = sum(Prob_red[mask_inside])
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # calculate radial profiles
    x = dist/r_vir[ii]
    for jj in range(len(x_bins)-1):
      mask_in_bin = (x_bins[jj]<x)*(x<x_bins[jj+1])
      radial_report[ii,jj,0] = len(x[mask_in_bin])
      radial_report[ii,jj,1] = sum(Prob_red[mask_in_bin])
  
  # put these values into an h5
  if out==None:
    return Z,m_vir,Ngal,Nred
  else:
    outname = fname % (out[0],unique_identifier(out[0],out[1]))
    print("Creating",outname)
    with h5py.File(outname,'w') as outfile:
      outfile.create_dataset('Z',data=Z[halos])
      outfile.create_dataset('m200',data=m_vir)
      outfile.create_dataset('r200',data=r_vir)
      outfile.create_dataset('Ngal',data=Ngal)
      outfile.create_dataset('Nred',data=Nred)
      outfile.create_dataset('radial',data=radial_report)
      outfile.attrs['NSIDE'] = out[0]
      outfile.attrs['index'] = out[1]
      # TODO: add radial profile data here
      print("Nred:",sum(Nred))
  
  if timing: 
    elapsed = time()-start # in seconds
    print(f"Elapsed time: {elapsed:0.3g} s = {elapsed/60.:0.3g} min")


########################################################################
########################################################################
# analysis methods

fname_agg = 'analyzed/sum_%03i.hdf5' # % NSIDE
tags = ['Z','m200','r200','Ngal','Nred','radial']

def aggregate(NSIDE=16): 
  """
  in verb command, to collect and combine all together.
  run after analysis [$py run voxel.py [indices]], before plotting
  """
  files = sorted(glob(fname_glob % NSIDE))
  # analyze first file to set everything up
  ff = files[0]
  ds = h5py.File(ff,'r')
  assert(NSIDE==ds.attrs['NSIDE'])
  for tag in tags:
    exec(f"{tag} = ds['{tag}']")
  
  N_empty = 0
  for ff in files:
    with h5py.File(ff,'r') as ds:
      assert(NSIDE==ds.attrs['NSIDE'])
      # read in data & coagulate
      for tag in tags:
        exec(f"{tag} = np.append({tag},ds['{tag}'],axis=0)")
      if np.sum(ds['Ngal'])<1:
        print(f"Warning: adding empty file: {ff}")
        N_empty += 1
  
  if N_empty>0: 
    print(f"Warning: added {N_empty} empty files (of %i)" % len(files))
  sqdeg = (len(files)-N_empty) * pix(NSIDE)
  print("Approximate sky area: %i sq deg ~ %0.2g%% of Buzzard sky" \
        % (sqdeg,100*sqdeg/sky))
  
  # create output file
  fname_out = fname_agg % NSIDE
  with h5py.File(fname_out,'w') as output:
    output.attrs['indices'] = len(files)
    output.attrs['NSIDE'] = NSIDE
    for tag in tags:
      exec(f"output.create_dataset('{tag}',data={tag})")
  ds.close()
  return fname_out


########################################################################
########################################################################

########################################################################
########################################################################
# analysis functions

def fR_mu_Z(fname=fname_agg % 16,backdrop=False,cmap='coolwarm',
            z_bins=None,mu_bins=None,saving=False,ylim=[0,1]):
  """
  fname=fname_agg % 16
  backdrop=False
  cmap='coolwarm'
  z_bins=None
  mu_bins=None
  """
  # grab values
  with h5py.File(fname,'r') as ds:
    Z = ds['Z'][()]
    Nred = ds['Nred'][()]
    Ngal = ds['Ngal'][()]
    if 'mu' in ds.keys():
      mu = ds['mu'][:]
      m200 = 10**mu
    else:
      m200 = ds['m200'][()]/cosmo.h # to convert out of per h units
      mu = np.log10(m200)
  fR = Nred/Ngal
  
  # fix inputs
  if mu_bins is None:
    mu_bins = np.linspace(min(mu),max(mu),12)
  else: 
    mu_bins = np.array(mu_bins)
  if z_bins is None:
    mn,mx = np.round_([min(Z),max(Z)],2)
    z_bins = [mn,(mx+mn)/2.,mx]
  else:
    z_bins = np.array(z_bins)
  
  
  # plotting 
  if backdrop: 
    plt.scatter(m200,fR,1,c=Z,cmap=cmap)
    plt.colorbar(label='Redshift')
  def m_vals(shift=0):
    return 10.**(mu_bins[:-1]+.25/2.+shift*.015)
  m200_vals = m_vals()
  
  # get colors
  cw = cm.get_cmap(cmap)
  if len(z_bins)>2: # more than one bin
    colors = cw((z_bins[:-1]-z_bins[0])/(z_bins[-2]-z_bins[0]))
  else: # single color for single bin
    colors = [cw[0]]
  
  for ii in range(len(z_bins)-1):
    # z_mask = z_bins[ii]>0 # to select all
    z_mask = (z_bins[ii]<=Z) * (Z<z_bins[ii+1])
    z_label = f"$z|[{z_bins[ii]:0.3g},{z_bins[ii+1]:0.3g})$"
    # plot up bins
    means,stds,hi,lo,err_mean,N_jj = np.zeros((6,len(m200_vals)))
    for jj in range(len(mu_bins)-1):
      mu_mask = (10.**mu_bins[jj]<=m200[z_mask]) \
              * (m200[z_mask]<10.**mu_bins[jj+1])
      N_jj[jj] = len(fR[z_mask][mu_mask])
      if N_jj[jj]<1: continue
      means[jj] = np.mean(fR[z_mask][mu_mask])
      stds[jj] = np.std(fR[z_mask][mu_mask])
      lo[jj] = np.quantile(fR[z_mask][mu_mask],.05)
      hi[jj] = np.quantile(fR[z_mask][mu_mask],.95)
      err_mean[jj] = stds[jj]/np.sqrt(N_jj[jj])
    
    plt_mask = (N_jj-1)>0 # .astype(bool)
    plt.errorbar(m_vals(ii-1)[plt_mask],means[plt_mask],
                 ((means-lo)[plt_mask],(hi-means)[plt_mask]),capsize=2,
                 fmt='.-',label=z_label,c=colors[ii])
                 # label='%s quartiles' % z_label
    if 0: # nonsense to use sigma for non-Gaussian
      plt.fill_between(m_vals(ii-1)[plt_mask],(means-stds)[plt_mask],
                       (means+stds)[plt_mask],alpha=.125,color=colors[ii],
                       linewidth=0) # label=r'%s $\mu \pm \sigma$' % z_label)
    plt.fill_between(m_vals(ii-1)[plt_mask],(means-err_mean)[plt_mask],
                     (means+err_mean)[plt_mask],alpha=.25,color=colors[ii],
                     linewidth=1) # label=r'%s $\mu \pm \sigma_{\bar \mu}$' % z_label)
  
  plt.xscale('log')
  MIN = min(min(m200),m200_vals[0])
  MAX = max(max(m200),m200_vals[-2])
  plt.xlim(MIN/1.1,MAX*1.1)
  plt.xlabel(r"$M_{\rm vir} ($M$_\odot)$")
  plt.ylabel(r"$f_R$")
  plt.ylim(ylim)
  
  plt.legend()
  plt.tight_layout()
  if saving:
    fname_out = 'fR_mu_Z.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()



def plot_fR_cf_H09(fname=fname_new,saving=True,warnings=False,
                   plot_points=False,hand_bins=False,plot_residue=True):
  " cf to Hansen+09 "
  # grab values
  with h5py.File(fname,'r') as ds:
    mask = ds['Nred'][:] > .1
    Z = ds['Z'][mask]
    Nred = ds['Nred'][mask]
    Ngal = ds['Ngal'][mask]
    fR = Nred/Ngal
    if 'mu' in ds.keys():
      mu = ds['mu'][mask]
      m200 = 10**mu
    else:
      m200 = ds['m200'][mask]/cosmo.h # to convert out of per h units
      mu = np.log10(m200)
  
  # set up constants
  ell = np.log(Nred)
  l_bins = np.exp(np.linspace(np.log(.5),np.log(200),14)) 
  l_mids = np.sqrt(l_bins[1:]*l_bins[:-1])
  nr = np.exp(np.linspace(np.log(3),np.log(120),100))
  
  z_bins_H09 = [.1,.25,.3]
  z_medians = [0,0]
  for jj in range(len(z_bins_H09)-1):
    mask_Z = (z_bins_H09[jj] <= Z) * (Z < z_bins_H09[jj+1])
    z_medians[jj] = np.median(Z[mask_Z])
    z_label = f"$z|[{z_bins_H09[jj]:0.2f},{z_bins_H09[jj+1]:0.2f})$"
    c_jj = ['b','r'][jj]
    
    if plot_points: # set up background points
      plt.scatter(Nred[mask_Z],fR[mask_Z],3,color=c_jj,alpha=.125,lw=0)
    
    avg,std,N,err_mean = np.zeros((4,len(l_mids)))
    if hand_bins:
      for ii in range(len(l_bins)-1):
        mask_l = (l_bins[ii]<Nred[mask_Z]) * (Nred[mask_Z]<l_bins[ii+1])
        if sum(mask_l)<2: 
          continue
        avg[ii] = np.average(fR[mask_Z][mask_l])
        std[ii] = np.std(fR[mask_Z][mask_l])
        N[ii] = sum(mask_l)
      err_mean[N>0] = std[N>0]/np.sqrt(N[N>0])
      mask = avg>0
    else: # do kllr analysis
      # define avg,std,N,err_mean,mask (sqrt(N)>2)
      kw = .20
      lm = kllr_model(kernel_width=kw)
      xr,avg,icpt,sl,std = lm.fit(ell[mask_Z],fR[mask_Z],xrange=[-1,5.3],nbins=100)
      l_mids = np.exp(xr)
      N = get_counts(ell[mask_Z],xr,kw)
      err_mean = std/np.sqrt(N)
      # mask = (N > 1) # beyond this, the mean isn't trustworthy
      mask = (N > 2) # beyond this, scatter can't be (normally) calculated
      # mask = (N > 4) # beyond this, the scatter isn't trustworthy
    
    if plot_residue: # plot points beyond KLLR analysis
      ell_max = max(l_mids[mask])
      mask_resid = (Nred[mask_Z] > ell_max)
      print(f'Residue count ({z_label}):',len(mask_resid[mask_resid]))
      plt.scatter(Nred[mask_Z][mask_resid],fR[mask_Z][mask_resid],10,color=c_jj)
    
    plt.plot(l_mids[mask],avg[mask],c_jj,linewidth=1,label=z_label)
    plt.fill_between(l_mids[mask],(avg-std)[mask],(avg+std)[mask],color=c_jj,lw=0,alpha=.125)
    for n_sig in range(1,2): # previously 4, to get +/- 3 sigma
      plt.fill_between(l_mids[mask],avg[mask]-n_sig*err_mean[mask],
                                    avg[mask]+n_sig*err_mean[mask],
                                    alpha=.125,color=c_jj,linewidth=0)
    
    y = [fR_Hp09(z_medians[jj],n_i,warnings) for n_i in nr]
    label_H09 = r'H09 $z_{\rm med}=%0.2f$' % z_medians[jj]
    # plt.plot(nr[1:-1],y[1:-1],'k',linewidth=3)
    plt.plot(nr,y,c_jj+'--',linewidth=2,label=label_H09)
  
  # set up axes
  plt.xscale('log')
  plt.xlabel(r'Richness')
  plt.ylabel(r'$f_R$')
  xlim = np.array(plt.xlim(1,150)) # H09 uses (1,180)
  if plot_points: # hardcode ylims
    plt.ylim(0,1)
  else: # H09 uses (.65,.9), with all markers > .67
    plt.ylim(.3,.85)
  if 0:
    # could show horizontal, i.e. bottom line of H09 plot
    H09_plot_bounds = 0.6571711848291456,.9
    plt.plot(xlim,H09_plot_bounds[0]*np.ones(2),'k',linewidth=.125)
  
  plt.legend()
  plt.tight_layout()
  if saving:
    fname_out = 'fR_cf_H09.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


def plot_fR_x_cf_H09(f=fname_new,Z_lim=[.1,.3],printing=False,
                     N_bins = [3,6,10,20,30,40,60,90,200], # default from Hansen+09
                     cmap='autumn',saving=True,colorbar=True):
  """
  compare radial red fraction to results from Hansen+09 (fig 12, left)
  
  Params
  ------
  f=fname_new
  Z_lim=[.1,.3] # to match range in Hansen+09
  printing=False
  N_bins = [3,6,10,20,30,40,60,90,200] # default from Hansen+09
  cmap='autumn'
  saving=True
  colorbar=True
  """
  if printing:
    print("Scanning in file...")
  with h5py.File(f,'r') as ds:
    mask_Z = (Z_lim[0] <= ds['Z'][:]) * (ds['Z'][:] < Z_lim[1])
    Z,mu,Nred = [ds[key][mask_Z] for key in ['Z','mu','Nred']]
    radial = ds['radial'][()]
    assert(np.all(ds['Ngal'][:] > 0)) # demand there be >= 1 galaxy
    radial_gal = radial[mask_Z,:,0]
    radial_red = radial[mask_Z,:,1]
    fR = radial_red/radial_gal
  # set up x-variable for ease of access
  x = x_midpts
  cw = cm.get_cmap(cmap)
  
  # set up H09 variables
  x_pts = [0.1650,0.4157,0.6252,0.8706,1.4925,2.4811,3.4818,4.4893]
  fR_sig_low = [.76921,.72125,.71362,.69292,.59482,.41717,.40300,.49564]
  fR_pts_low = [.77684,.72997,.72016,.71253,.60136,.51526,.51308,.54360]
  fR_pts_high = [.91853,.86076,.80736,.78011,.70708,.66567,.60790,.8259]
  fR_sig_high = [.94469,.88256,.85204,.80409,.75395,.71253,.80954,1.000]
  fR_global = 0.569210 # estimated global field value
  plt.fill_between(x_pts,fR_pts_low,fR_pts_high,alpha=.25,color='g',lw=0)
  plt.fill_between(x_pts,fR_sig_low,fR_sig_high,alpha=.125,color='g',lw=0)
  plt.plot([.1,7],np.ones(2) * fR_global,'--g',lw=3)
  
  # plot our data, bin by bin
  for ii in range(len(N_bins)-1):
    mask_ii = (N_bins[ii] <= Nred) * (Nred < N_bins[ii+1])
    label_ii = r'$N_{\rm red}|[%0.2f,%0.2f)$' % (N_bins[ii],N_bins[ii+1])
    c_ii = cw(1-ii/(len(N_bins)-2))
    N_halo = np.sum(radial_gal[mask_ii]>0,axis=0)
    fR_mean = np.nanmean(fR[mask_ii],axis=0)
    fR_sig = np.nanstd(fR[mask_ii],axis=0)
    fR_err = fR_sig/np.sqrt(N_halo)
    plt.plot(x,fR_mean,color=c_ii,label=label_ii)
    plt.fill_between(x,fR_mean-fR_err,fR_mean+fR_err,
                     alpha=.125,color=c_ii,lw=0)
  # plot aggregate
  c_ii = 'k'
  N_halo = np.sum(radial_gal>0,axis=0)
  fR_mean = np.nanmean(fR,axis=0)
  fR_sig = np.nanstd(fR,axis=0)
  fR_err = fR_sig/np.sqrt(N_halo)
  plt.plot(x,fR_mean,color=c_ii)
  plt.fill_between(x,fR_mean-fR_err,fR_mean+fR_err,
                   alpha=.25,color=c_ii,lw=0)
  
  # use same bounds as H09:
  plt.xlabel(r'Cluster-centric distance $r/r_{\rm vir}$')
  plt.xlim(.1,7)
  plt.xscale('log')
  plt.ylabel('Red fraction')
  plt.ylim(.4,1)
  if True: # use colorbar for binning
    # create dumb colormap
    cnc = [cw(1-ii/(len(N_bins)-2)) for ii in range(len(N_bins)-1)]
    c_vals = (np.array(N_bins)-min(N_bins)) / (max(N_bins[:-1])-min(N_bins))
    cmap_N = LSC.from_list('H09_Fig12',list(zip(c_vals,cnc)))
    # use in colorbar
    norm = mpl.colors.Normalize(N_bins[0],N_bins[-1])
    sm = mpl.cm.ScalarMappable(norm,cmap_N)
    plt.colorbar(sm,boundaries=N_bins,label=r'Richness')
  else: # legend (too large; pretty ugly)
    plt.legend()
  plt.tight_layout()
  if saving:
    fname_out = 'fR_x_cf_H09.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


def plot_fR_Z(fname=fname_agg % 16,mu_bins=[13.75,14,14.5,15.5]):
  # grab values
  with h5py.File(fname,'r') as ds:
    Ngal = ds['Ngal'][()]
    Nred = ds['Nred'][()]
    fR = Nred/Ngal
    Z = ds['Z'][()]
    if 'mu' in ds.keys():
      mu = ds['mu'][:]
      m200 = 10**mu
    else:
      m200 = ds['m200'][()]/cosmo.h # to convert out of per h units
      mu = np.log10(m200)
  # iterate over mass bins
  for ii in range(len(mu_bins)-1):
    mask_mu = (mu_bins[ii]<=mu)*(mu<mu_bins[ii+1])
    if np.sum(mask_mu)<1:
      continue
    plt.subplot(len(mu_bins)-1,1,1+ii)
    # calculate medians
    z_vals = np.linspace(min(Z[mask_mu]),max(Z[mask_mu]),25)
    dz = z_vals[1]-z_vals[0]
    qtls = np.zeros((len(z_vals)-1,len(sigma_vals)))
    z_med = np.zeros(len(z_vals)-1)
    for jj in range(len(z_vals)-1):
      mask_Z_jj = (z_vals[jj]<=Z[mask_mu]) * (Z[mask_mu]<z_vals[jj+1])
      z_med[jj] = np.median(Z[mask_mu][mask_Z_jj])
      if len(fR[mask_mu][mask_Z_jj])<2: # set to overall value
        qtls[jj] = np.quantile(fR[mask_mu],sigma_vals)
      else: 
        qtls[jj] = np.quantile(fR[mask_mu][mask_Z_jj],sigma_vals)
    # plot red fraction as a function of redshift
    plt.scatter(Z[mask_mu],fR[mask_mu],2,linewidth=0,alpha=.125,color='xkcd:evergreen')
    pm = qtls[:,3]-qtls[:,2],qtls[:,2]-qtls[:,1]
    plt.plot(z_med,qtls[:,2],'k')
    plt.fill_between(z_med,qtls[:,1],qtls[:,3],color='k',linewidth=0,alpha=.5)
    plt.xlabel('Redshift')
    plt.xlim(min(Z)-.025,max(Z)+.025)
    plt.ylabel('Red fraction')
    plt.ylim(0,1)
    # wrap up and display
    plt.tight_layout()
  plt.show()


def plot_fR_Z_mu(fname=fname_agg % 16,mu_bins=[13.75,14,14.5,15.5],
                 cmap='copper',saving=False):
  " plot fR(Z) binned by mu "
  # grab values
  with h5py.File(fname,'r') as ds:
    Ngal = ds['Ngal'][()]
    Nred = ds['Nred'][()]
    fR = Nred/Ngal
    Z = ds['Z'][()]
    if 'mu' in ds.keys():
      mu = ds['mu'][:]
      m200 = 10**mu
    else:
      m200 = ds['m200'][()]/cosmo.h # to convert out of per h units
      mu = np.log10(m200)
  
  cw = cm.get_cmap(cmap)
  # iterate over mass bins
  for ii in range(len(mu_bins)-1):
    mask_mu = (mu_bins[ii]<=mu)*(mu<mu_bins[ii+1])
    label_mu = label__M_bins % (mu_bins[ii],mu_bins[ii+1])
    if np.sum(mask_mu)<1:
      continue
    # calculate medians
    z_vals = np.linspace(min(Z[mask_mu]),max(Z[mask_mu]),25)
    dz = z_vals[1]-z_vals[0]
    qtls = np.zeros((len(z_vals)-1,len(sigma_vals)))
    z_med = np.zeros(len(z_vals)-1)
    for jj in range(len(z_vals)-1):
      mask_Z_jj = (z_vals[jj]<=Z[mask_mu]) * (Z[mask_mu]<z_vals[jj+1])
      z_med[jj] = np.median(Z[mask_mu][mask_Z_jj])
      if len(fR[mask_mu][mask_Z_jj])<2: # set to overall value
        qtls[jj] = np.quantile(fR[mask_mu],sigma_vals)
      else: 
        qtls[jj] = np.quantile(fR[mask_mu][mask_Z_jj],sigma_vals)
    # plot red fraction as a function of redshift
    mx = len(mu_bins)-2
    col = cw(1-ii/mx)
    plt.plot(z_med,qtls[:,2],color=col,label=label_mu)
    plt.fill_between(z_med,qtls[:,1],qtls[:,3],color=col,linewidth=0,alpha=.125)
  plt.xlabel('Redshift')
  plt.xlim(.1,.7)
  plt.ylabel('Red fraction')
  plt.ylim(.45,1)
  # wrap up and display
  plt.legend()
  plt.tight_layout()
  if saving: 
    fname_out = 'fR_Z_mu.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# d_mu = .550001; np.arange(13.75,15.4+d_mu,d_mu)

def plot_fR_Z_mu2(f=fname_new,Z_lim=[.1,.32,.7],cmap='copper',saving=True,
                  printing=False,mu_bins=np.linspace(13.75,15.4,4),
                  bootstrap=False):
  with h5py.File(f,'r') as ds:
    mask_Z = (Z_lim[0] <= ds['Z'][:]) * (ds['Z'][:] < Z_lim[-1])
    Z,mu,Ngal,Nred = [ds[key][mask_Z] for key in ['Z','mu','Ngal','Nred']]
    fR = Nred/Ngal
  
  ylim = np.array(plt.ylim(.5,.725))
  cw = cm.get_cmap(cmap); mx = len(mu_bins)-2 # set up colorscheme
  # iterate over mass bins
  for ii in range(len(mu_bins)-1):
    mask_ii = (mu_bins[ii]<=mu)*(mu<mu_bins[ii+1])
    N_ii = len(mask_ii[mask_ii])
    col_ii = cw(1-ii/mx)
    label_mu = label__M_bins % (mu_bins[ii],mu_bins[ii+1])
    if printing:
      print('Processing',label_mu,'...')
    if np.sum(mask_ii)<1:
      continue
    nb = 100
    kw = .0375
    for jj in range(len(Z_lim)-1):
      mask_jj = (Z_lim[jj] <= Z) * (Z < Z_lim[jj+1])
      if jj > 0 and jj < len(Z_lim)-1:
        # plot vertical line at transition redshift
        plt.plot(Z_lim[jj]*np.ones(2),ylim,'k--',alpha=.5)
      if not bootstrap:
        lm = kllr_model(kernel_width=kw) #0.025)
        xr, yrm, icpt, sl, sig = lm.fit(Z[mask_ii * mask_jj], 
                                        fR[mask_ii * mask_jj], nbins=nb)
        sqt = np.sqrt(get_counts(Z,xr,kw))
        # plot results
        plt.plot(xr,yrm,label=label_mu if jj==0 else None,color=col_ii)
        plt.fill_between(xr,yrm-5*sig/sqt,yrm+5*sig/sqt,alpha=.25,linewidth=0,color=col_ii)
      else:
        K_all, K_mn, K_err = KLLR_bootstrap(Z[mask_ii * mask_jj], 
                                            fR[mask_ii * mask_jj], 
                                            [Z_lim[jj],Z_lim[jj+1]],
                                            nbins=nb,kernel_width=kw,
                                            printing=printing)
        xr,yrm,icpt,sl,sig = K_all # KLLR 'truth', sampling all points
        xrm,yrmm,icptm,slm,sigm = K_mn # mean from bootstrap
        xre,yrme,icpte,sle,sige = K_err # st dev from bootstrap
        # plot results
        plt.plot(xr,yrm,label=label_mu if jj==0 else None,color=col_ii)
        plt.fill_between(xr,yrmm-yrme,yrmm+yrme,alpha=.25,linewidth=0,color=col_ii)
  
  # tidy up & display
  plt.xlabel('Redshift')
  plt.xlim(Z_lim[0],Z_lim[-1])
  plt.ylabel('Red fraction')
  plt.legend(loc='lower right')
  plt.tight_layout()
  if saving:
    fname_out = 'fR_Z_mu2.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def plot_fR_mu_Z(f=fname_new,cmap='coolwarm',saving=True,plot_scatter=True,
                 mu_max_method='err',printing=False,Z_bins=[.1,.32,.7]):
  with h5py.File(f,'r') as ds:
    mask_Z = (Z_bins[0] <= ds['Z'][:]) * (ds['Z'][:] < Z_bins[-1])
    Z,mu,Ngal,Nred = [ds[key][mask_Z] for key in ['Z','mu','Ngal','Nred']]
    fR = Nred/Ngal
  
  cw = cm.get_cmap(cmap); mx = len(Z_bins)-2 # set up colorscheme
  # iterate over mass bins
  for ii in range(len(Z_bins)-1):
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # set up work in this bin
    mask_ii = (Z_bins[ii]<=Z)*(Z<Z_bins[ii+1])
    label_ii = f'$z|[{Z_bins[ii]:0.4g},{Z_bins[ii+1]:0.4g})$'
    col_ii = cw(ii/mx)
    if printing:
      print('Processing',label_ii,'...')
    if np.sum(mask_ii)<1:
      continue
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # figure out the largest mu to trust
    kw = .075
    nb = 2 * (max(mu[mask_ii])-min(mu[mask_ii])) // kw
    lm = kllr_model(kernel_width=kw)
    xr, yrm, icpt, sl, sig = lm.fit(mu[mask_ii], fR[mask_ii], nbins=nb)
    sqt = np.sqrt(get_counts(mu[mask_ii],xr,kw))
    # maximum mu at which to trust KLLR: (default: sqt<2)
    try:
      # mu_max_trust = mu[np.where(sqt<=2)[0][0]] # to trust scatter
      mu_max_trust = mu[np.where(sqt<np.sqrt(2))[0][0]] # for scatter
      # mu_max_trust = mu[np.where(sqt<=1)[0][0]] # to calculate mean
    except IndexError:
      mu_max_trust = max(mu)
    
    # set up maximum mu to use:
    if mu_max_method=='err':
      mu_max = mu_max_trust
    elif mu_max_method < 0: # negative (integer) input
      mu_max = sorted(mu[mask_ii])[mu_max_method] # largest to include
    else: # assume real number input as hardcoded max
      mu_max = mu_max_method
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # use KLLR on the trustworthy part
    nb,kw = 66,.125
    lm = kllr_model(kernel_width=kw)
    xr, yrm, icpt, sl, sig = lm.fit(mu[mask_ii], fR[mask_ii], nbins=nb, xrange=(13.75,mu_max))
    sqt = np.sqrt(get_counts(mu[mask_ii],xr,kw))
    plt.plot(10**xr,yrm,label=label_ii,color=col_ii)
    plt.fill_between(10**xr,yrm-sig/sqt,yrm+sig/sqt,alpha=.25,linewidth=0,color=col_ii)
    if plot_scatter:
      if np.any(sqt<=2):
        print(f"WARNING: Untrustworthy KLLR beyond mu={mu_max_trust:0.5g}")
      plt.fill_between(10**xr,yrm-sig,yrm+sig,alpha=.125,lw=0,color=col_ii)
      # plt.plot(xr,yrm-sig,'--',color=col_ii)
      # plt.plot(xr,yrm+sig,'--',color=col_ii)
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # plot those not included in the KLLR regression
    mask = mask_ii * (mu > mu_max)
    if len(mask[mask])<1:
      continue # we don't have to worry about the remainder
    elif len(mask[mask])<10: 
      # not enough points for great statistics
      plt.scatter(10**mu[mask],fR[mask],10,color=col_ii)
    else:
      mn,sig = np.mean(fR[mask]), np.std(fR[mask])
      mn *= np.ones(2)
      mu_range = np.array([mu_max,max(mu[mask])])
      plt.plot(10**mu_range,mn,color=col_ii)
      plt.fill_between(10**mu_range,mn-sig,mn+sig,alpha=.125,lw=0,color=col_ii)
      # plt.plot(mu_range,mn-sig,'--',color=col_ii)
      # plt.plot(mu_range,mn+sig,'--',color=col_ii)
      sx = sig/np.sqrt(len(mask[mask])) # error of the mean
      plt.fill_between(10**mu_range,mn-sx,mn+sx,alpha=.25,lw=0,color=col_ii)
  
  # tidy up & display
  plt.xlabel(label__M)
  plt.xlim(10**13.75,10**15.4)
  plt.xscale('log')
  plt.ylabel('Red fraction')
  ylim = plt.ylim()
  plt.ylim(min(ylim[0],.5),None)
  plt.legend(loc='upper right') #loc='lower right')
  plt.tight_layout()
  if saving:
    fname_out = 'fR_mu_Z.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def plot_PDF(fname=fname_agg % 16,mu_bins=[13.75,14,14.5,15.5],
             z_bins=np.linspace(.1,.375,5),legend=True):
  # Make PDFs of fR in mass-limited bins: {13.5-14,14-14.5,>14.5}
  # grab values
  with h5py.File(fname,'r') as ds:
    Z = ds['Z'][()]
    Nred = ds['Nred'][()]
    Ngal = ds['Ngal'][()]
    if 'mu' in ds.keys():
      mu = ds['mu'][:]
      m200 = 10**mu
    else:
      m200 = ds['m200'][()]/cosmo.h # to convert out of per h units
      mu = np.log10(m200)
  fR = Nred/Ngal
  
  mu_masks = [(10**mu_bins[ii]<=m200)*(m200<10**mu_bins[ii+1]) \
              for ii in range(len(mu_bins)-1)]
  Nz = len(z_bins)-1 # number of z-bins
  Nrows = np.floor(np.sqrt(Nz)).astype(int)
  Ncols = np.ceil(Nz/Nrows).astype(int)
  for jj in range(Nz):
    plt.subplot(Nrows,Ncols,jj+1)
    mask_z = (z_bins[jj]<=Z)*(Z<z_bins[jj+1])
    plt.title(r'$z|[%0.3g,%0.3g)$' % (z_bins[jj],z_bins[jj+1]))
    for ii in range(len(mu_bins)-1):
      label_ii = r'$\mu|[%0.3g,%0.3g)$' % (mu_bins[ii],mu_bins[ii+1])
      plt.hist(fR[np.logical_and(mask_z,mu_masks[ii])],density=True,
               alpha=.5,bins=np.linspace(0,1,21),label=label_ii)
    if jj==0 and legend: 
      plt.legend()
  
  fig = plt.gcf() # get current figure to add labels & formatting
  fig.text(.5,.015,r'$f_R$')
  fig.text(.015,.5,r'Density',rotation='vertical')
  fig.subplots_adjust(top=.95,bottom=0.091,left=0.075,right=.95,
                      hspace=.30,wspace=.15)
  plt.show()


def plot_HOD(fname=fname_agg % 16,printing=False,Nred_min=0,N_iter=100,red_only=False):
  """
  plot_HOD(fname=fname_agg % 16,printing=False,Nred_min=0,N_iter=100)
  """
  with h5py.File(fname,'r') as ds:
    Z = ds['Z'][()]
    Nred = ds['Nred'][()]
    Ngal = ds['Ngal'][()]
    if 'mu' in ds.keys():
      mu = ds['mu'][:]
      m200 = 10**mu
    else:
      m200 = ds['m200'][()]/cosmo.h # to convert out of per h units
      mu = np.log10(m200)
  mask = Nred>=Nred_min
  x,y1,y2 = np.log10([m200[mask],Ngal[mask],Nred[mask]])
  x_vals = np.linspace(min(x)/1.01,1.01*max(x),100)
  
  if not red_only:
    # run for gals
    if printing: print("Running first linmix")
    lm1 = linmix.LinMix(x,y1)
    lm1.run_mcmc(silent=not printing,maxiter=N_iter)
    alpha1,beta1,sigsqr1 = [lm1.chain[key].mean() \
                         for key in ['alpha','beta','sigsqr']]
  # run for reds
  if printing: print("Running second linmix")
  lm2 = linmix.LinMix(x,y2)
  lm2.run_mcmc(silent=not printing,miniter=100,maxiter=N_iter)
  alpha2,beta2,sigsqr2 = [lm2.chain[key].mean() \
                       for key in ['alpha','beta','sigsqr']]
  # plot up HOD and display
  if printing: print("plotting...")
  
  if not red_only:
    # plot gals
    plt.subplot(211)
    plt.scatter(10**x,10**y1,1,c='g',label=r'$N_{\rm gal}$')
    plt.plot(10**x_vals,10**(alpha1+x_vals*beta1),'k')
    plt.fill_between(10**x_vals,10**(alpha1+x_vals*beta1-np.sqrt(sigsqr1)),
                                10**(alpha1+x_vals*beta1+np.sqrt(sigsqr1)),
                     alpha=.125,color='g',linewidth=0)
    plt.xscale('log')
    plt.xlim(.95*10**min(x),10**max(x)/.95)
    plt.yscale('log')
    plt.ylabel(r'Galaxy count')
    plt.title(r'${\rm lg} N = %g \mu + %g$' % (beta1,alpha1))
  
  # plot reds
  if not red_only:
    plt.subplot(212)
  plt.scatter(10**x,10**y2,1,c='r',label=r'$N_{\rm red}$')
  plt.plot(10**x_vals,10**(alpha2+x_vals*beta2),'k')
  plt.fill_between(10**x_vals,10**(alpha2+x_vals*beta2-np.sqrt(sigsqr2)),
                              10**(alpha2+x_vals*beta2+np.sqrt(sigsqr2)),
                   alpha=.125,color='r',linewidth=0)
  plt.xscale('log')
  plt.xlabel(r'Mass ($M_{\rm vir}/M_\odot$)')
  plt.xlim(min(10**x_vals),max(10**x_vals))
  plt.yscale('log')
  plt.ylabel(r'Galaxy count')
  plt.title(r'${\rm lg} N = %g \mu + %g$' % (beta2,alpha2))
  
  plt.tight_layout()
  plt.show()


########################################################################
# HOD alternate: 
# plt.scatter(mu-voxel.np.log10(.7),Nred,1,c=Z,cmap='coolwarm'); plt.xlabel(r'$\mu$'); plt.xlim(13.5,15.05); plt.yscale('log'); plt.ylim(.1,150); plt.ylabel(r'$N_{\rm red}$'); plt.colorbar(label='Redshift'); plt.tight_layout(); plt.show()

pars_B20 = {
  "url":"https://doi.org/10.3847/1538-4365/ab6993", 
  "Mp":3e14, # pivot mass
  "Zp":.6, # pivot redshift
  "A":76.9, "A_err":8.2, # richness normalization
  "B":1.020, "B_err":.080, # log slope
  "C":.29, "C_err":.27, # temporal evolution strength
  "sig":.23, "sig_err":.16, # scatter
}

def lambdaM_Bleem(M=3e14,Z=.6,idx=None,p=pars_B20):
  """
  returns <lambda|M500c,z>
  
  idx=None: [-1,+1] give scatter
  """
  lnreport = np.log(p['A'])
  lnreport += p['B'] * np.log(M/p['Mp'])
  lnreport += p['C'] * np.log(cosmo.E(Z)/cosmo.E(p['Zp']))
  if idx==-1:
    lnreport -= p['sig']
  elif idx==+1:
    lnreport += p['sig']
  return np.exp(lnreport)


def Mlambda_Simet(lam=40,idx=None):
  " <M200m|lambda> from Simet+18 (correction of Simet+16) "
  url = "https://doi.org/10.1093/mnras/sty2318"
  report = 10**14.369*(lam/40.)**1.3/cosmo.h # Msol
  
  if idx==-1: # report low bound
    report *= 10**(-.021-.023)
    report[lam>40] *= (lam[lam>40]/40.)**(-.09)
    report[lam<40] *= (lam[lam<40]/40.)**(.10)
  elif idx==+1: # report high bound
    report *= 10**(.021+.023)
    report[lam>40] *= (lam[lam>40]/40.)**(.10)
    report[lam<40] *= (lam[lam<40]/40.)**(-.09)
  
  return report


def Mlambda_DESY1(lam=40,Z=.35):
  """
  <M200m|lambda,z> from McClintock+18: DES Y1 results
  lambda >= 20, .2 <= Z <= .65
  url = "https://doi.org/10.1093/mnras/sty2711"
  """
  M0,F,G = 3.081e14,1.356,-.30 # Msol, unitless, unitless
  return M0*(lam/40.)**F*((1+Z)/(1+.35))**G # Msol


def Mlambda_F16(lam=30):
  " <M200c|lambda> from Farahi+16: Galaxy cluster mass estimation "
  url = "https://doi.org/10.1093/mnras/stw1143"
  M30,alpha_m = 1.56e14,1.31 # Msol, unitless
  return M30*(lam/30.)**alpha_m # Msol



def M_N200_Jp07(N200=20,mass_def='Mvir',cautious=True,warnings=True):
  """ 
  <M|N200> from Johnston+07 table 10: WL mass--richness relation 
  It calls M200 'the average halo virial mass', but it's M200c (see eq4)
  """
  url = "https://arxiv.org/pdf/0709.1159.pdf"
  
  N200_lim = [3,92.18,200] # table 1; table 8 for cautious bound
  if warnings and (np.any(N200<N200_lim[0]) or \
     np.any(N200>N200_lim[1 if cautious else 2])):
    print("WARNING: Extrapolating!")
  
  if mass_def=='M200c': 
    M20,alpha_N = 8.794e13/cosmo.h,1.28
    err_M,err_alpha = .4+1.1,.04  # relative errors are the same (eqn 26)
  elif mass_def=='M180b':
    M20,alpha_N = 1.204e14/cosmo.h,1.30 
  elif mass_def=='Mvir':
    M20,alpha_N = 1.055e14/cosmo.h,1.29
  elif mass_def=='M500':
    M20,alpha_N = 6.069e13/cosmo.h,1.25
  else:
    print("Unset mass definition")
    raise SystemExit
  
  return M20*(N200/20)**alpha_N # Msol



def fR_Hp09(z,N200,warnings=True):
  """
  red fraction estimation as a function of redshift and N200
  as given by Hansen+09
  """
  url = "https://doi.org/10.1088%2F0004-637x%2F699%2F2%2F1333"
  N200_lim = [3,200] # loose bounds
  if warnings and (np.any(N200<N200_lim[0]) or np.any(N200>N200_lim[1])):
    print("WARNING: Extrapolating!")
  def _g(z):
    return .206 - .371*z
  def _h(z):
    # check if it's zero:
    assert(z>3.6/25.8) # must have z>~.14
    return -3.6 + 25.8*z
  return _g(z)*erf(np.log10(N200/_h(z))) + .69








def plot_HOD_flop(fname=fname_agg % 16,printing=False,hardcode=False,
                  Nred_min=20,N_iter=500,normalize_std=True,Z_max=.33,
                  red_only=False,add_error=False,save_output=False,
                  only_first=False):
  """
  fname=fname_agg % 16
  printing=False
  hardcode=False
  Nred_min=20
  N_iter=500
  normalize_std=True
  Z_max=.33
  red_only=False
  add_error=False
  save_output=False
  only_first=False
  """
  with h5py.File(fname,'r') as ds:
    Z = ds['Z'][()]
    mask_Z = Z<=Z_max
    Z = Z[mask_Z]
    if printing:
      print(f"Vetted down to z<={Z_max:0.3g}")
    Nred = ds['Nred'][mask_Z]
    Ngal = ds['Ngal'][mask_Z]
    if 'mu' in ds.keys():
      mu = ds['mu'][mask_Z]
      m200 = 10**mu
    else:
      m200 = ds['m200'][mask_Z]/cosmo.h # to convert out of per h units
      mu = np.log10(m200)
  mask = Nred>=Nred_min
  x,y1,y2 = np.log10([m200[mask],Ngal[mask],Nred[mask]])
  
  if not hardcode:
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # run for gals
    if not red_only: 
      if printing: print("Running first linmix")
      lm1 = linmix.LinMix(y1,x)
      lm1.run_mcmc(silent=not printing,miniter=100,maxiter=N_iter)
      alpha1,beta1,sigsqr1 = [lm1.chain[key].mean() \
                              for key in ['alpha','beta','sigsqr']]
      a_sig1,b_sig1,ss_sig1 = [lm1.chain[key].std() \
                               for key in ['alpha','beta','sigsqr']]
      sigma1 = np.sqrt(sigsqr1)
      sigma1_max = np.sqrt(sigsqr1+ss_sig1)
      # this then gives x = alpha + y1*beta +/- sigma,
      # so ignoring sigma, this gives y1 = (x-alpha)/beta, 
      # or y1 = (1/beta)*x - alpha/beta
      if printing:
        print(f"alpha = {alpha1:0.5g} +/- {a_sig1:0.5g}")
        print(f"beta  = {beta1:0.5g} +/- {b_sig1:0.5g}")
        print(f"sigma = {sigma1:0.5g} +/- {ss_sig1:0.5g}")
    
    if 1: # always do this; just put "if 1" for formatting nicety
      # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
      # run for reds
      if printing: print("Running second linmix")
      lm2 = linmix.LinMix(y2,x)
      lm2.run_mcmc(silent=not printing,miniter=100,maxiter=N_iter)
      alpha2,beta2,sigsqr2 = [lm2.chain[key].mean() \
                              for key in ['alpha','beta','sigsqr']]
      a_sig2,b_sig2,ss_sig2 = [lm2.chain[key].std() \
                               for key in ['alpha','beta','sigsqr']]
      sigma2 = np.sqrt(sigsqr2)
      sigma2_max = np.sqrt(sigsqr2+ss_sig2)
      # similarly, this gives y2 = (x-alpha2)/beta2
      if printing:
        print(f"alpha = {alpha2:0.5g} +/- {a_sig2:0.5g}")
        print(f"beta  = {beta2:0.5g} +/- {b_sig2:0.5g}")
        print(f"sigma = {sigma2:0.5g} +/- {ss_sig2:0.5g}")
  else: # use hardcoded values
    if 0:
      print("Using hardcoded (10% of Bz sky) values")
      alpha1,beta1,sigma1 = 13.21,.8611,.1167
      alpha2,beta2,sigma2 = 13.45,.7502,.1438
    else:
      print("Using hardcoded (25% of Bz sky) values")
      alpha1,beta1,sigma1 = 13.14,.9056,.1173
      alpha2,beta2,sigma2 = 13.39,.7960,.1470
    # these weren't set
    a_sig1,b_sig1,ss_sig1,sigma1_max = 0,0,0,0
    a_sig2,b_sig2,ss_sig2,sigma2_max = 0,0,0,0
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # plot up HOD and display
  if printing: 
    print("plotting...")
  
  plt.figure(1) # HOD
  y_vals = np.linspace(np.log10(Nred_min),1.01*max(y1),100)
  if not red_only:
    # plot gals
    plt.subplot(121)
    plt.scatter(10**y1,10**x,1,c='g',label=r'${\rm lg}~N_{\rm gal}$')
    plt.plot(10**y_vals,10**(alpha1+beta1*y_vals),'k')
    if add_error: # add error to Buzzard relation
      # plot given scatter
      plt.fill_between(10**y_vals,10**(alpha1+beta1*y_vals-sigma1),
                                  10**(alpha1+beta1*y_vals+sigma1),
                                  color='k',alpha=.25,linewidth=0)
      # plot maximum scatter
      plt.fill_between(10**y_vals,
                       10**((alpha1-a_sig1)+(beta1-b_sig1)*y_vals), #-sigma1_max),
                       10**((alpha1+a_sig1)+(beta1+b_sig1)*y_vals), #+sigma1_max),
                       color='k',alpha=.25,linewidth=0)
    plt.ylabel(r'Mass ($M_{\rm vir} / M_\odot$)')
    plt.yscale('log')
    ylim = plt.ylim(.90*10**min(x),10**max(x)/.90)
    plt.xlim(Nred_min,max(10**y_vals))
    plt.xscale('log')
    plt.xlabel(r'$N_{\rm gal}$')
    plt.title(r'$\mu = %0.4g \,{\rm lg}~N_{\rm gal} + %0.4g \pm %0.4g$' \
              % (beta1,alpha1,sigma1))
  if 1: 
    # plot reds
    if red_only:
      plt.ylabel(r'Mass ($M_{\rm vir} / M_\odot$)')
    else:
      plt.subplot(122)
    plt.scatter(10**y2,10**x,1,c='r') # ,label=r'$N_{\rm red}$')
    plt.plot(10**y_vals,10**(alpha2+beta2*y_vals),'k',label=r'Buzzard')
    if add_error: # add error to Buzzard relation
      # plot given scatter
      plt.fill_between(10**y_vals,10**(alpha2+beta2*y_vals-sigma2),
                                  10**(alpha2+beta2*y_vals+sigma2),
                                  color='k',alpha=.25,linewidth=0)
      # plot maximum scatter
      plt.fill_between(10**y_vals,
                       10**((alpha2-a_sig2)+(beta2-b_sig2)*y_vals), #-sigma2_max),
                       10**((alpha2+a_sig2)+(beta2+b_sig2)*y_vals), #+sigma2_max),
                       color='k',alpha=.25,linewidth=0)
    plt.plot(10**y_vals,Mlambda_Simet(10**y_vals),label=r'Simet+18',color='c')
    if add_error: # add error to Simet relation
      plt.fill_between(10**y_vals,Mlambda_Simet(10**y_vals,-1),
                       Mlambda_Simet(10**y_vals,+1),color='c',
                       alpha=.25,linewidth=0)
    plt.legend(loc='lower right')
    plt.xscale('log')
    plt.xlim(Nred_min,max(10**y_vals))
    plt.xlabel(r'Richness')
    plt.yscale('log')
    plt.title(r'$\mu = %0.4g \,{\rm lg}~N_{\rm red} + %0.4g \pm %0.4g$' \
              % (beta2,alpha2,sigma2))
    if not red_only: 
      plt.subplots_adjust(wspace=.2)
      plt.ylim(ylim)
      yt,yl = plt.yticks() # y-ticks and y-labels
      plt.yticks(yt,"")
      # apparently changing yticks resets ylim and visa vers :C
    
    plt.ylim(.90*10**min(x),10**max(x)/.90)
    plt.tight_layout()
  # fig.subplots_adjust(left=.085) # FIXME: maybe uncomment? figure out spacing
  # top=.920,bottom=0.131,left=0.110,right=.977, hspace=.251,wspace=.2)
  if save_output is not False:
    fname_out = 'flopfig.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print("Saved",fname_out)
  
  if only_first or red_only:
    plt.show()
    return
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  plt.figure(2) # Plot of residuals
  if 0: # abs val of resid
    if not red_only:
      residuals_gal = np.abs(x-(alpha1+beta1*y1))
    residuals_red = np.abs(x-(alpha2+beta2*y2))
    plt.yscale('log')
    plt.ylim(1e-5,2)
    plt.ylabel(r'Residuals $|\log_{10}\,M_{\rm vir}-\log_{10}\,M_{\rm fit}|$')
  else: # relative val of resid
    if not red_only:
      residuals_gal = x-(alpha1+beta1*y1)
    residuals_red = x-(alpha2+beta2*y2)
    if normalize_std:
      # plt.ylabel(r'Normalized Residuals $(\log_{10}\,M_{\rm vir}-\log_{10}\,M_{\rm fit})/\sigma$')
      plt.ylabel(r'Normalized Residuals')
      plt.ylim(-4,+4)
    else:
      plt.ylabel(r'Residuals ($\log_{10}\,M_{\rm vir}-\log_{10}\,M_{\rm fit}$)')
      plt.ylim(-.8,.8)
    plt.plot([19,350],[0,0],'k') # baseline (zero residual)
    
    # collect -1sigma,median,+1sigma values for N-bins
    N_y_bins = 8
    y_vals = np.linspace(np.log10(19),np.log10(350),N_y_bins+1)
    y_med_vals = (y_vals[:-1]+y_vals[1:])/2
    N_ii_gals,N_ii_reds = np.zeros((2,N_y_bins))
    scatters_gals = np.zeros((len(sigma_vals),N_y_bins)) # want 1,2,3 for sig-1,median,+1
    scatters_reds = np.zeros((len(sigma_vals),N_y_bins)) # want 1,2,3 for sig-1,median,+1
    stddev_gals_ii,stddev_reds_ii = np.zeros((2,N_y_bins)) # scatter should be Normal
    stddev_gals,stddev_reds = [np.std(res) for res in \
                               [residuals_gal,residuals_red]] # scatter should be Normal
    for ii in range(N_y_bins):
      # all galaxies: 
      mask_gal = (y_vals[ii]<y1)*(y1<y_vals[ii+1])
      N_ii_gals[ii] = len(residuals_gal[mask_gal])
      if len(residuals_gal[mask_gal])<1:
        continue
      scatters_gals[:,ii] = np.quantile(residuals_gal[mask_gal],sigma_vals)
      stddev_gals_ii[ii] = np.std(residuals_gal[mask_gal])
      # just reds: 
      mask_red = (y_vals[ii]<y2)*(y2<y_vals[ii+1])
      N_ii_reds[ii] = len(residuals_red[mask_red])
      if len(residuals_red[mask_red])<1:
        continue
      scatters_reds[:,ii] = np.quantile(residuals_red[mask_red],sigma_vals)
      stddev_reds_ii[ii] = np.std(residuals_red[mask_red])
    
    # plot up the sigma values
    # plt.plot(10**y_med_vals,scatters_gals[2,:],'go',mfc='none') # median dots
    div_fac = stddev_gals if normalize_std else 1 # 0 flag undoes division by STDEV
    plt.plot(10**y_med_vals,scatters_gals[2,:]/div_fac,'g') # median line
    plt.fill_between(10**y_med_vals[N_ii_gals>1],
                     (scatters_gals[2,:]-stddev_gals_ii)[N_ii_gals>1]/div_fac,
                     (scatters_gals[2,:]+stddev_gals_ii)[N_ii_gals>1]/div_fac,
                     # scatters_gals[1,N_ii_gals>1]/div_fac,
                     # scatters_gals[3,N_ii_gals>1]/div_fac,
                     color='g',alpha=.25,linewidth=0) # +/- 1 sigma range
    plt.scatter(10**y1,residuals_gal/div_fac,1,color='g',alpha=.125)
    # plt.plot(10**y_med_vals,scatters_reds[2,:],'ro',mfc='none') # median dots
    div_fac = stddev_reds if normalize_std else 1 # 0 flag undoes division by STDEV
    plt.plot(10**y_med_vals,scatters_reds[2,:]/div_fac,'r') # median line
    plt.fill_between(10**y_med_vals[N_ii_reds>1],
                     (scatters_reds[2,:]-stddev_reds_ii)[N_ii_reds>1]/div_fac,
                     (scatters_reds[2,:]+stddev_reds_ii)[N_ii_reds>1]/div_fac,
                     # scatters_reds[1,N_ii_reds>1]/div_fac,
                     # scatters_reds[3,N_ii_reds>1]/div_fac,
                     color='r',alpha=.25,linewidth=0) # +/- 1 sigma range
    plt.scatter(10**y2,residuals_red/div_fac,1,color='r',alpha=.125)
  
  plt.xscale('log')
  plt.xlim(19,350)
  plt.xlabel(r'Galaxy Count')
  plt.tight_layout()
  
  if 1: # plot stdev of residuals vs galaxy counts
    plt.figure(3)
    hriz = [10**y_med_vals[N_ii_gals>1],10**y_med_vals[N_ii_reds>1]]
    vert = np.array([stddev_gals_ii[N_ii_gals>1],
                     stddev_reds_ii[N_ii_reds>1]]) * np.log(10)
    plt.plot(hriz[0],vert[0],'g'); plt.scatter(hriz[0],vert[0],color='g')
    plt.plot(hriz[1],vert[1],'r'); plt.scatter(hriz[1],vert[1],color='r')
    plt.xscale('log')
    plt.xlabel(r'Galaxy Count')
    plt.ylabel(r'Standard Deviation ($\ln\,M$)')
    plt.tight_layout()
  
  plt.show()

#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  # 

def plot_HOD_pretty(f=fname_new,Z_lim=[.1,.33],Nred_min=10,
                    printing=True,saving=True):
  """
  f=fname_new
  Z_lim=[.1,.33]
  Nred_min=10
  printing=True
  saving=True
  """
  with h5py.File(f,'r') as ds:
    mask_Nred = ds['Nred'][:] >= Nred_min
    mask_Z = (Z_lim[0] <= ds['Z'][:]) * (ds['Z'][:] < Z_lim[1])
    mu,Nred,Ngal,Z = [ds[tag][mask_Z * mask_Nred] \
                      for tag in ['mu','Nred','Ngal','Z']]
    ell,ellg = np.log10([Nred,Ngal])
  
  if printing:
    print(f"{len(mu)} halos")
  
  # plot points to be fitted; fix plot limits
  sizing = 15**ell / 5
  plt.scatter(10**ell,10**mu,sizing,lw=0,alpha=.5,color=mpl_colors[1])
  plt.xscale('log')
  xlim = np.array(plt.xlim(plt.xlim(Nred_min,None)))
  plt.yscale('log')
  ylim = np.array(plt.ylim(plt.ylim(10**min(mu),None)))
  
  # find mean and scatter using linmix
  lm = linmix.LinMix(ell,mu)
  lm.run_mcmc(silent=not printing, miniter=100, maxiter=300)
  # evaluate mean values
  keys = ['alpha','beta','sigsqr'] # intercept, slope, variance
  a,b,v = [lm.chain[key].mean() for key in keys]
  sig = np.sqrt(v)
  sa,sb,sv = [lm.chain[key].std() for key in keys]
  if printing:
    N = [int(np.round(1.5-np.log10(np.abs(x)))) for x in [sa,sb,sv]]
    print(f"alpha: {a:0.{N[0]}f} +/- {np.round(sa,N[0]):0.{N[0]}f}")
    print(f"beta: {b:0.{N[1]}f} +/- {np.round(sb,N[1]):0.{N[1]}f}")
    print(f"sigsq: {v:0.{N[2]}f} +/- {np.round(sv,N[2]):0.{N[2]}f}")
  
  ell_v = np.linspace(np.log10(xlim[0]),np.log10(xlim[1]),100)
  if 0:
    lines = lm.chain['alpha'] + np.outer(ell_v,lm.chain['beta'])
    mean_v = 10**np.mean(lines,axis=1)
    sig = np.std(lines,axis=1) # This is too teeny to be important. 
  else:
    mean_v = 10**(a + b*ell_v)
  plt.plot(10**ell_v,mean_v,color='k',label='linmix fit')
  plt.fill_between(10**ell_v,mean_v*10**-sig,mean_v*10**sig,
                   color='k',alpha=.25,lw=0)
  
  # find mean and scatter using KLLR
  kw = np.log10(ylim[1]/ylim[0])/25
  print(f"kernel width: {kw:0.3g}")
  lm = kllr_model(kernel_width=kw)
  xr,yrm,icpt,sl,sig = lm.fit(ell,mu,xrange=np.log10(xlim),nbins=100)
  N = get_counts(ell,xr,kw)
  err_mean = sig/np.sqrt(N)
  mask_KLLR = (N > 4) # so we can trust the scatter 
  plt.plot(10**xr[mask_KLLR],10**yrm[mask_KLLR],'--k',label='KLLR fit')
  plt.fill_between(10**xr[mask_KLLR],10**(yrm-sig)[mask_KLLR],
                                     10**(yrm+sig)[mask_KLLR],
                                     color='k',alpha=.125,lw=0)
  plt.fill_between(10**xr[mask_KLLR],10**(yrm-err_mean)[mask_KLLR],
                                     10**(yrm+err_mean)[mask_KLLR],
                                     color='k',alpha=.125,lw=0)
  
  # plot mean and scatter from Simet+18
  S_vals = [Mlambda_Simet(10**ell_v, tag) \
                           for tag in [-1, None, +1]] # M200m
  S_min, mean_S, S_plus = cosmo.get_Mvir(S_vals,np.mean(Z),X='200m')
  plt.plot(10**ell_v,mean_S,color=mpl_colors[4],label='Simet+18')
  plt.fill_between(10**ell_v, S_min, S_plus,
                   color=mpl_colors[4], alpha=.25, lw=0)
  
  # set up axes
  plt.xlabel(r'Richness')
  plt.ylabel(r'Mass ($M_{\rm vir} / M_{\odot}$)')
  # tidy up and display
  plt.legend(loc='lower right')
  plt.tight_layout()
  if saving:
    fname_out = 'HOD_pretty.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


def plot_HOD_unflip(f=fname_new,saving=True):
  with h5py.File(f,'r') as ds:
    mu,Nred,Ngal,Z = [ds[tag][:] for tag in ['mu','Nred','Ngal','Z']]
    ell,ell_g = np.log10(np.array([Nred,Ngal]))
  
  # plot existing points
  sizing = 10**(mu-14.75)
  plt.scatter(10**mu,Nred,sizing,alpha=.5,lw=0,color=mpl_colors[3])
  # plt.scatter(10**mu,Ngal,1,label=r'$N_{\rm gal}$',alpha=.125) 
  
  # run and display KLLR analysis
  kw = .15
  lm = kllr_model(kernel_width=kw)
  # plot Ngal fit
  xrg,yrmg,icptg,slg,sigg = lm.fit(mu,ell_g,nbins=100)
  plt.plot(10**xrg,10**yrmg,color='k',lw=2)
  plt.plot(10**xrg,10**yrmg,color=mpl_colors[0],label=r'$N_{\rm gal}$')
  plt.fill_between(10**xrg,10**(yrmg-sigg),10**(yrmg+sigg),
                   alpha=.25,lw=0,color=mpl_colors[0])
  # plot lambda fit 
  xr,yrm,icpt,sl,sig = lm.fit(mu,ell,nbins=100)
  plt.plot(10**xr,10**yrm,color='k',lw=2)
  plt.plot(10**xr,10**yrm,color=mpl_colors[3],label=r'$N_{\rm red}$')
  plt.fill_between(10**xr,10**(yrm-sig),10**(yrm+sig),
                   alpha=.25,lw=0,color=mpl_colors[3])
  
  if 0: # plot pseudo f_R
    plt.clf()
    plt.plot(xr,yrm/yrmg)
    plt.show()
  
  # plot Bleem fit
  M_500c = cosmo.get_M_X(10**xr,Z=.1,X='500c')
  lo,lo_md,_ = [lambdaM_Bleem(M_500c,Z=.1,idx=ii) for ii in [-1,0,+1]]
  M_500c = cosmo.get_M_X(10**xr,Z=.7,X='500c')
  _,hi_md,hi = [lambdaM_Bleem(M_500c,Z=.7,idx=ii) for ii in [-1,0,+1]]
  plt.fill_between(10**xr,lo_md,hi_md,color=mpl_colors[8],lw=0,label='Bleem+20')
  plt.fill_between(10**xr,lo,hi,alpha=.25,color=mpl_colors[8],lw=0)
  
  # set up axes
  plt.xlabel(r'Mass ($M_{\rm vir} / M_{\odot}$)')
  plt.xscale('log')
  plt.xlim(10**min(mu),10**max(mu))
  plt.ylabel(r'Richness')
  plt.yscale('log')
  # tidy up and display
  plt.legend(loc='lower right')
  plt.tight_layout()
  if saving:
    fname_out = 'HOD_unflip.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()



lm_keys = ['alpha','beta','sigsqr'] # intercept, slope, variance
lm_lbls = [r'$\alpha$',r'$\beta$',r'$\sigma^2$']
fname_abv = 'abv.npy'
Z_abv = np.linspace(.1,.7,13)
Z_abv[4] = .32 # to set edge at simulation edge

def calc_HOD_evolution(f=fname_new,Z_bins=np.linspace(.1,.7,20),
                       mu_min=14.2,printing=True,
                       pivot_lambda=1,pivot_mu=15.5):
  with h5py.File(f,'r') as ds:
    mask_Z = (Z_bins[0] <= ds['Z'][:]) * (ds['Z'][:] < Z_bins[-1])
    mask_mu = (ds['mu'][:] >= mu_min)
    mu,Nred,Ngal,Z = [ds[tag][mask_Z * mask_mu] \
                      for tag in ['mu','Nred','Ngal','Z']]
    if printing:
      print(f"min(lambda): {min(Nred):0.3g}")
    ell = np.log10(Nred/pivot_lambda)
    myu = mu - pivot_mu
  
  abv_vals,abv_sig_vals = np.zeros((2,len(Z_bins)-1,3))
  
  for ii in range(len(Z_bins)-1):
    mask_ii = (Z_bins[ii] <= Z) * (Z < Z_bins[ii+1])
    label_ii = f'$z|[{Z_bins[ii]:0.2f},{Z_bins[ii+1]:0.2f})$'
    if printing:
      print('Processing',label_ii)
    # run linmix on that subset
    lm = linmix.LinMix(myu[mask_ii],ell[mask_ii])
    lm.run_mcmc(silent=True, miniter=200, maxiter=1000)
    abv_vals[ii] = [lm.chain[key].mean() for key in lm_keys]
    abv_sig_vals[ii] = [lm.chain[key].std() for key in lm_keys]
    
    if 0: # plot fit
      plt.scatter(myu[mask_ii],ell[mask_ii],1,alpha=.125)
      myu_v = np.array([min(myu),max(myu)])
      lines = lm.chain['alpha'] + np.outer(myu_v,lm.chain['beta'])
      mean = np.mean(lines,axis=1)
      stds = np.std(lines,axis=1) # spread in fit lines (mean)
      plt.plot(myu_v,mean,'k')
      plt.fill_between(myu_v,mean-stds,mean+stds,
                       alpha=.125,lw=0,color='k')
      plt.show()
  
  np.save(fname_abv,[Z_bins,abv_vals,abv_sig_vals])


def plot_abv(printing=True,saving=True):
  Z_bins,abv_vals,abv_sig_vals = np.load(fname_abv,allow_pickle=True)
  Z_mids = (Z_bins[1:] + Z_bins[:-1])/2.
  Z_err = (Z_bins[1:] - Z_bins[:-1])/2.
  Z_err /= 5 # have 5 sigma inside bin
  Z_err *= 0 # use no x-error
  
  # put data into more cosmological space
  def f(Z,Zp=.4):
    return np.log((1+Z)/(1+Zp))
  
  x = f(Z_mids)
  x_err = Z_err/(1+Z_mids)
  xx = f(Z_bins) # for plotting
  
  # plot results
  fig,axs = plt.subplots(3, 1, sharex=True, figsize=double_height)
  
  for ii in range(3):
    axs[ii].set_ylabel(lm_lbls[ii])
    val,val_sig = abv_vals[:,ii],abv_sig_vals[:,ii]
    if printing:
      print('#'*72)
      # calculate weighted mean
      sig_0 = np.std(val)
      w = 1/len(val) + (sig_0/val_sig)**2 # Gaussian weights
      w = 1/val_sig**2
      mean_ii = np.sum(val*w)/np.sum(w)
      std_ii = np.sqrt(1/np.sum(w))
      N = int(np.round(1.5-np.log10(np.abs(std_ii))))
      print(lm_keys[ii],f"= {mean_ii:0.{N}f} +/- {std_ii:0.{N}f}")
      if ii==2: # calculate uneven error bars for sigma from sigma^2
        sig = np.sqrt(mean_ii)
        lo = sig - np.sqrt(mean_ii-std_ii)
        hi = np.sqrt(mean_ii+std_ii) - sig
        N = int(np.round(1.5-np.log10(np.abs(min(lo,hi)))))
        if np.round(lo,N)==np.round(hi,N):
          print(f"(sigma = {sig:0.{N}f} +/- {hi:0.{N}f})")
        else:
          print(f"(sigma = {sig:0.{N}f} +{hi:0.{N}f} / -{lo:0.{N}f})")
    # begin plot
    axs[ii].errorbar(Z_mids,val,val_sig,Z_err,'.',capsize=2)
    
    # run linmix to measure significance of redshift evolution
    lm = linmix.LinMix(x,val,xsig=x_err,ysig=val_sig)
    lm.run_mcmc(silent=True, miniter=2000, maxiter=5000)
    a,b,v = [lm.chain[key].mean() for key in lm_keys]
    sa,sb,sv = [lm.chain[key].std() for key in lm_keys]
    N = [int(np.round(1.5-np.log10(np.abs(x)))) for x in [sa,sb,sv]]
    if printing:
      print(f"> alpha: {a:0.{N[0]}f} +/- {np.round(sa,N[0]):0.{N[0]}f}")
      print(f"> beta: {b:0.{N[1]}f} +/- {np.round(sb,N[1]):0.{N[1]}f}")
      print(f"> sigsq: {v:0.{N[2]}f} +/- {np.round(sv,N[2]):0.{N[2]}f}")
      print()
    lines = lm.chain['alpha'] + np.outer(xx,lm.chain['beta'])
    mean = np.mean(lines,axis=1)
    stds = np.std(lines,axis=1)
    axs[ii].plot(Z_bins,mean,'k')
    axs[ii].fill_between(Z_bins,mean-stds,mean+stds,
                         alpha=.125,lw=0,color='k')
    if printing:
      mn,sg = np.mean(lines), np.std(lines)
      N = int(np.round(1.5-np.log10(np.abs(sg))))
      print(f"mean value: {mn:0.{N}f} +/- {sg:0.{N}f}")
      print(f"slope is {b/sb:0.3g} sigma from zero\n")
  
  # set axes and tidy up
  axs[2].set_xlabel(r'Redshift $z$')
  axs[2].set_xlim(Z_bins[0],Z_bins[-1])
  plt.tight_layout()
  if saving:
    fname_out = 'abv.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()



def plot_HOD_Z(f=fname_new,Z_bins=np.linspace(.1,.7,4),Nred_min=10,
                 printing=True,cmap='coolwarm',pivot=40,saving=True):
  with h5py.File(f,'r') as ds:
    mask_Nred = ds['Nred'][:] >= Nred_min
    mask_Z = (Z_bins[0] <= ds['Z'][:]) * (ds['Z'][:] < Z_bins[-1])
    mu,Nred,Ngal,Z = [ds[tag][mask_Z * mask_Nred] \
                      for tag in ['mu','Nred','Ngal','Z']]
    ell,ell_g = np.log10(np.array([Nred,Ngal])/pivot)
  
  # prep plots
  cw = cm.get_cmap(cmap); mx = len(Z_bins) - 2 # set up colormap
  colors = cw(np.arange(mx + 1) / mx) # (mx,-1,-1) for flip
  
  fig,axs = plt.subplots(3, 1, sharex=True, figsize=double_height)
  xlim = plt.xlim(Nred_min,max(Ngal))
  
  for ii in range(len(Z_bins)-1):
    mask_ii = (Z_bins[ii] <= Z) * (Z < Z_bins[ii+1])
    label_ii = f'$z|[{Z_bins[ii]:0.2f},{Z_bins[ii+1]:0.2f})$'
    if printing:
      print("Processing",label_ii,"...")
    
    kw = .15 # use in range .05 to .15
    lm = kllr_model(kernel_width=kw)
    xr,yrm,icpt,sl,sig = lm.fit(ell[mask_ii],mu[mask_ii],nbins=100)
    xrg,yrmg,icptg,slg,sigg = lm.fit(ell_g[mask_ii],mu[mask_ii],nbins=100)
    N = get_counts(ell[mask_ii],xr,kw)
    # set up variables
    mask = (N > 4) # to trust scatter
    lam = pivot*10**xr[mask]
    lamg = pivot*10**xrg
    mean = 10**yrm[mask]
    std = sig[mask]
    poi = 1/np.sqrt(N[mask]) # Poisson error
    err = std*poi # standard error (of the mean)
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # plot fit
    axs[0].set_ylabel(r'Mass ($M_{\rm vir}/M_{\odot}$)')
    # axs[0].scatter(Nred[mask_ii],10**mu[mask_ii],2,
    #                color=colors[ii],alpha=.25,lw=0)
    axs[0].plot(lam,mean,color='k',lw=2)
    axs[0].plot(lam,mean,color=colors[ii],label=label_ii)
    axs[0].fill_between(lam,mean*10**-err,mean*10**+err,
                     color=colors[ii],alpha=.125,lw=0)
    axs[0].fill_between(lam,mean*10**-std,mean*10**+std,
                     color=colors[ii],alpha=.125,lw=0)
    
    axs[0].set_yscale('log')
    axs[0].legend()
    
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # plot slopes
    axs[1].set_ylabel(r'Slope')
    axs[1].plot(lam,sl,color=colors[ii])
    axs[1].plot(lamg,slg,'--',color=colors[ii])
    axs[1].plot(xlim,np.ones(2),'k')
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # plot scatter
    axs[2].set_ylabel(r'Scatter')
    axs[2].plot(lam,std,color=colors[ii])
    axs[2].plot(lamg,sigg,'--',color=colors[ii])
    axs[2].set_ylim(.075,.175)
    axs[2].plot(lam,poi,'k',lw=1)
    axs[2].plot(lam,poi,':',color=colors[ii])
  
  # set up axes and display
  axs[2].set_xlabel(r'Richness')
  axs[2].set_xscale('log')
  plt.tight_layout()
  if saving:
    fname_out = 'HOD_Z.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


def plot_HOD_KLLR(f=fname_new,Z_bins=np.linspace(.1,.7,4),max_safe_N=4,
                  add_expectations=False,
                  printing=True,cmap='coolwarm',saving=True,mu_max=15.35):
  """
  max_safe_N=4, where to stop displaying KLLR fit; suggested values:
    10 : below which statistics are poor
     4 : below which scatter shouldn't be trusted
     2 : below which scatter can't traditionally be calculated
     1 : below which mean can't traditionally be calculated
  """
  with h5py.File(f,'r') as ds:
    mask_Z = (Z_bins[0] <= ds['Z'][:]) * (ds['Z'][:] < Z_bins[-1])
    mu,Nred,Ngal,Z = [ds[tag][mask_Z] \
                      for tag in ['mu','Nred','Ngal','Z']]
    ell,ell_g = np.log10(np.array([Nred,Ngal]))
  
  # prep plots
  fsize = six_tall/2 # hardcoded mini version
  fig,axs = plt.subplots(3, 3, sharex='all', sharey='row', figsize=fsize)
  axs[0][0].set_ylabel(r'Richness')
  axs[0][0].set_yscale('log')
  axs[0][0].set_ylim(2,300)
  axs[1][0].set_ylabel(r'Slope')
  axs[2][0].set_ylabel(r'Scatter')
  # axs[2][0].set_yscale('log') # turn off for now...
  axs[2][0].set_ylim(0,.5) # danergous hardcoding here...
  axs[2][1].set_xlabel(r'Mass ($M_{\rm vir} / M_{\odot}$)')
  axs[2][1].set_xscale('log')
  xlim = np.array(plt.xlim(10**min(mu),10**max(mu)))
  
  # go through the various KLLR plots
  for ii in range(len(Z_bins)-1):
    mask_ii = (Z_bins[ii] <= Z) * (Z < Z_bins[ii+1])
    label_ii = f'$z|[{Z_bins[ii]:0.2f},{Z_bins[ii+1]:0.2f})$'
    if printing:
      print("Processing",label_ii,"...")
    
    kw = .15 # [.1,.125,.15] # use in range (.05, .15]
    xrng = [min(mu),mu_max]
    K_all, K_mn, K_err = KLLR_bootstrap(mu[mask_ii],ell[mask_ii],
                                        xrng,nbins=100,kernel_width=kw)
    xr,yrm,icpt,sl,sig = K_all # KLLR 'truth', sampling all points
    xrm,yrmm,icptm,slm,sigm = K_mn # mean from bootstrap
    xre,yrme,icpte,sle,sige = K_err # st dev from bootstrap
    N = get_counts(mu[mask_ii],xr,kw)
    print('end N:',N[-1])
    max_safe_mu = min(max(xr[N>=max_safe_N]),mu_max)
    msm = mask_ii * (mu > max_safe_mu)
    mN = mask_N = N >= max_safe_N
    # for all galaxies (end tag = 'g')
    K_allg, K_mng, K_errg = KLLR_bootstrap(mu[mask_ii],ell_g[mask_ii],
                                           xrng,nbins=100,kernel_width=kw)
    xrg,yrmg,icptg,slg,sigg = K_allg
    xrgm,yrmgm,icptgm,slgm,siggm = K_mng # mean from bootstrap
    xrge,yrmge,icptge,slge,sigge = K_errg # st dev from bootstrap
    Ng = get_counts(mu[mask_ii],xrg,kw)
    max_safe_mug = max(xrg[Ng>=max_safe_N])
    msmg = mask_ii * (mu > max_safe_mug)
    mNg = mask_Ng = Ng >= max_safe_N
    print('max safe mus:',max_safe_mu,max_safe_mug)
    
    # plot mean relation + scatter
    axs[0][ii].set_title(label_ii)
    axs[0][ii].scatter(10**mu[msm],10**ell[msm],1,
                       alpha=.25,color=mpl_colors[3])
    axs[0][ii].scatter(10**mu[msmg],10**ell_g[msmg],1,
                       alpha=.25,color=mpl_colors[0])
    axs[0][ii].plot(10**xr[mN],10**yrm[mN],mpl_colors[3])
    axs[0][ii].fill_between(10**xr[mN],10**(yrm-sig)[mN],
                                         10**(yrm+sig)[mN],
                            color=mpl_colors[3],lw=0,alpha=.125)
    axs[0][ii].plot(10**xrg[mNg],10**yrmg[mNg],mpl_colors[0])
    axs[0][ii].fill_between(10**xrg[mNg],10**(yrmg-sigg)[mNg],
                                          10**(yrmg+sigg)[mNg],
                            color=mpl_colors[0],lw=0,alpha=.125)
    if add_expectations: 
      # add <lambda|mu> from Bleem+20
      M_vals = np.linspace(xlim[0],xlim[1])
      M_500c = cosmo.get_M_X(M_vals,Z=Z_bins[ii],X='500c')
      lo1,md1,hi1 = [lambdaM_Bleem(M_500c,Z=Z_bins[ii],idx=ii) for ii in [-1,0,+1]]
      M_500c = cosmo.get_M_X(M_vals,Z=Z_bins[ii+1],X='500c')
      lo2,md2,hi2 = [lambdaM_Bleem(M_500c,Z=Z_bins[ii+1],idx=ii) for ii in [-1,0,+1]]
      # mark extremes
      lo = np.minimum(lo1,lo2)
      lo_md = np.minimum(md1,md2)
      hi_md = np.maximum(md1,md2)
      hi = np.maximum(hi1,hi2)
      axs[0][ii].fill_between(M_vals,lo_md,hi_md,color=mpl_colors[8],lw=0,label='Bleem+20')
      axs[0][ii].fill_between(M_vals,lo,hi,alpha=.25,color=mpl_colors[8],lw=0)
    
    # plot evolution of slope
    axs[1][ii].plot(xlim,np.ones(2),'k')
    axs[1][ii].plot(10**xr[mN],sl[mN],mpl_colors[3])
    axs[1][ii].fill_between(10**xrm[mN],(slm-sle)[mN],(slm+sle)[mN],
                            alpha=.125,lw=0,color=mpl_colors[3])
    axs[1][ii].plot(10**xrg[mNg],slg[mNg],mpl_colors[0])
    axs[1][ii].fill_between(10**xrgm[mNg],(slgm-slge)[mNg],(slgm+slge)[mNg],
                            alpha=.125,lw=0,color=mpl_colors[0])
    
    if add_expectations:
      # add slope from Bleem+20
      lo,md,hi = [pars_B20['B'] + pm*pars_B20['B_err'] for pm in [-1,0,+1]]
      axs[1][ii].plot(xlim,md*ones,mpl_colors[8])
      axs[1][ii].fill_between(xlim,lo*ones,hi*ones,color=mpl_colors[8],alpha=.125,lw=0)
    
    # plot evolution of scatter
    axs[2][ii].plot(10**xr[mN],sig[mN],mpl_colors[3])
    axs[2][ii].fill_between(10**xrm[mN],(sigm-sige)[mN],(sigm+sige)[mN],
                            alpha=.125,lw=0,color=mpl_colors[3])
    axs[2][ii].plot(10**xrg[mNg],sigg[mNg],mpl_colors[0])
    axs[2][ii].fill_between(10**xrgm[mNg],(siggm-sigge)[mN],(siggm+sigge)[mN],
                            alpha=.125,lw=0,color=mpl_colors[0])
    
    # plot Poisson scatter
    axs[2][ii].plot(10**xr[mN],1/np.sqrt(10**yrm[mN]),
                    mpl_colors[3],ls=':')
    axs[2][ii].plot(10**xrg[mNg],10**(-yrmg[mNg]/2),
                    mpl_colors[0],ls=':')
    
    if add_expectations:
      # add scatter from Bleem+20
      lo,md,hi = [pars_B20['sig'] + pm*pars_B20['sig_err'] for pm in [-1,0,+1]]
      axs[2][ii].plot(xlim,md*ones,mpl_colors[8])
      axs[2][ii].fill_between(xlim,lo*ones,hi*ones,color=mpl_colors[8],alpha=.125,lw=0)
      # add Poisson scatter
      fish = (lo_md*hi_md)**(-1/4)
      axs[2][ii].plot(M_vals,fish,ls=':',color=mpl_colors[8])
  
  fig.tight_layout()
  fig.subplots_adjust(hspace=.05) # top=.95,bottom=0.091,left=0.075,right=.95,hspace=.30,wspace=.15)
  if saving:
    fname_out = 'HOD_KLLR.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()




# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# radial profile methods 

def get_NFW(mu,Z): # hardcoded, rather than cosmo_buzzard's softcoding
  " (mu,Z): returns NFW profile relative to critical density "
  Delta_c = cosmo.Delta_vir(Z) # virial overdensity <~ 178 rel. to crit
  Delta_v = Delta_c/cosmo.Omega_m(Z) # relative to mean matter
  c_vir = cosmo.get_c(10**mu,Z,'bhattacharya13') # concentration
  A = cosmo.A_NFW(c_vir) # concentration-dependent prefactor
  return Delta_v/(3 * A * x_midpts * (1./c_vir+x_midpts)**2)


def remove_zeros(a):
  """ 
  remove zeros from radial array by increasing bin size,
  weighting by volume bins (X)
  """
  for ii in range(len(a)-1): # can't average rightmost bin w/ rightwards
    jj = 1 # how many extra steps out to take
    while a[ii] <= 0: # still need to merge w/ larger bins
      vals = a[ii:ii+1+jj] * X[ii:ii+1+jj] # weight by volume bin
      a[ii:ii+1+jj] = np.mean(vals)/np.sum(X[ii:ii+1+jj])
      jj += 1
  return a


def get_delta(N,rvir,mu,Z,nix_nulls=False):
  """ returns overdensity in radial bins, given halo Rvir, mu, Z
  
  Parameters
  ----------
  N : array, size (N_halos,len(x_midpts))
    number counts for each halo for each radial bin
  rvir : array, size len(N_halos)
    virial radius of each halo in cMpc (convert out of 'per h' units!)
  mu : array, size len(N_halos)
    log10 mass of each halo
  Z : array, size len(N_halos)
    redshift of each halo
  nix_nulls (False) : bool, optional
    remove zeros (occur on far left side of the radial bins) from delta,
    averaging with their outer neighbors (effectively enlargening bins)
  
  Returns
  -------
  mean delta, delta err mean, low Poisson error, high Poisson error
  
  Notes
  -----
  Calculate mean overdensity & error of the mean relative to NFW, 
  then calculate an NFW profile from mean mu to get back to pure
  overdensity, not relative to NFW. 
  Returns this mean profile as well as its expected Poisson noise.
  """ 
  # calculate sample size
  N_halo = len(Z) if hasattr(Z,'__len__') else 1
  if N_halo > 1: # calculate mean mu
    diff = np.abs(mu-np.mean(mu))
    index_mean_mu = np.where(diff==min(diff))[0][-1]
  
  # calculate number counts expected in 'average' area of space
  rho = get_rho(Z)
  N_expected = np.outer(rvir**3 * rho,X)
  # calculate overdensity from number counts excess
  delta = N/N_expected - 1 # delta; unitless
  
  # calculate expected NFW profiles for each halo
  nfw = np.array([get_NFW(mu[ii],Z[ii]) for ii in range(N_halo)])
  N_nfw = (nfw + 1) * N_expected
  # calculate expected Poisson error on that profile
  nfw_sig = np.sqrt(N_nfw)/N_expected/np.sqrt(N_halo)
  
  # calculate relative profiles, find scatter and error of the mean
  rel = delta/nfw
  rel_sig = np.std(rel,axis=0)
  rel_sigx = rel_sig/np.sqrt(N_halo)
  # return to pure overdensity (not relative to NFW)
  delta_mean = np.mean(rel,axis=0) * nfw[index_mean_mu]
  delta_sigx = rel_sigx * nfw[index_mean_mu]
  
  if nix_nulls: # remove inner zeros via increased bin sizes
    delta_mean = remove_zeros(delta_mean)
  
  return delta_mean, delta_sigx, nfw[index_mean_mu], nfw_sig[index_mean_mu]


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# new radial plotters

def plot_delta_x_mu(f=fname_new,reds=False,mu_bins=[13.75,14,14.5,15.5],
                    Z_lim=[.10,.70],nix_nulls=False,cmap='copper',
                    printing=False,saving=True):
  with h5py.File(f,'r') as ds:
    mask_Z = (Z_lim[0] <= ds['Z'][:]) * (ds['Z'][:] < Z_lim[1])
    Z,mu,rvir = [ds[key][mask_Z] for key in ['Z','mu','rvir']]
    rvir /= cosmo.h
    radial = ds['radial'][()]
    N = radial[mask_Z,:,1 if reds else 0]
  # set up x-variable for ease of access
  x = x_midpts
  cw = cm.get_cmap(cmap)
  
  # iterate over mu bins
  for ii in range(len(mu_bins)-1):
    mask_ii = (mu_bins[ii] <= mu) * (mu < mu_bins[ii+1])
    label_ii = r'$\mu|[%0.2f,%0.2f)$' % (mu_bins[ii],mu_bins[ii+1])
    if printing:
      print('processing',label_ii)
    c_ii = cw(1-ii/(len(mu_bins)-2))
    dm,ds,nm,ns = get_delta(N[mask_ii],rvir[mask_ii],mu[mask_ii],
                              Z[mask_ii],nix_nulls=nix_nulls)
    # plot mean overdensity
    plt.plot(x,dm,color=c_ii,label=label_ii)
    # plot error of the mean for <delta>
    plt.fill_between(x,dm-ds,dm+ds,alpha=.125,lw=0,color=c_ii)
    # plot mean NFW profile
    plt.plot(x,nm,color=c_ii)
    # plot Poisson scatter expected in NFW profile
    plt.fill_between(x,nm-ns,nm+ns,alpha=.0625,lw=0,color=c_ii)
  
  # set up axes
  plt.xlabel(r'Cluster-centric distance $r/r_{\rm vir}$')
  plt.xscale('log')
  plt.xlim(min(x_bins),max(x_bins))
  plt.ylabel(r'Overdensity $\delta$')
  plt.yscale('log')
  # tidy up & display
  if len(mu_bins)<=5:
    plt.legend()
  else:
    norm = mpl.colors.Normalize(vmin=mu_bins[0],vmax=mu_bins[-1])
    sm = plt.cm.ScalarMappable(cmap=cmap+'_r', norm=norm)
    plt.colorbar(sm,ticks=mu_bins,label=label__mu_to_M,boundaries=mu_bins) 
    # aspect=40 doesn't make the colorbar thinner here... 
  plt.tight_layout()
  if saving:
    fname_out = 'delta_x_mu.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# display nice plot

d_mu = .25 # step in log mass
def plot_nice_radial(f=fname_new,reds=False,Z_lim=[.1,.7],
                     nix_nulls=False,printing=False,
                     cmap='copper',saving=True,
                     mu_bins=np.arange(13.75,15.5+d_mu,d_mu)):
  " make a nice double radial plot  "
  if printing:
    print("Scanning in file...")
  with h5py.File(f,'r') as ds:
    mask_Z = (Z_lim[0] <= ds['Z'][:]) * (ds['Z'][:] < Z_lim[1])
    Z,mu,rvir = [ds[key][mask_Z] for key in ['Z','mu','rvir']]
    rvir /= cosmo.h
    radial = ds['radial'][()]
    N = radial[mask_Z,:,1 if reds else 0]
  # set up x-variable for ease of access
  x = x_midpts
  cw = cm.get_cmap(cmap)
  
  # start plotting
  fig,axs = plt.subplots(2, 1, sharex=True, figsize=double_height)
  axs[0].set_ylabel(r'Galactic overdensity $\delta_{\rm gal}$')
  axs[0].set_ylim(.1,1e8)
  axs[1].set_ylabel(r'$\delta_{\rm gal} / \delta_{\rm NFW}$')
  axs[1].set_ylim(1e-2,1e2)
  axs[1].set_xlabel(r'Cluster-centric distance $r/r_{\rm vir}$')
  
  # iterate over mu bins
  for ii in range(len(mu_bins)-1):
    mask_ii = (mu_bins[ii] <= mu) * (mu < mu_bins[ii+1])
    label_ii = r'$\mu|[%0.2f,%0.2f)$' % (mu_bins[ii],mu_bins[ii+1])
    if printing:
      print('Processing',label_ii)
    c_ii = cw(1-ii/(len(mu_bins)-2))
    dm,ds,nm,ns = get_delta(N[mask_ii],rvir[mask_ii],mu[mask_ii],
                              Z[mask_ii],nix_nulls=nix_nulls)
    # set up data to be plotted
    yd = np.array([dm,dm-ds,dm+ds]) # mean, low, high
    yn = np.array([nm,nm-ns,nm+ns]) # mean, low, high
    for bot in range(2): # plot data
      div = nm if bot else 1
      # plot mean overdensity
      axs[bot].plot(x,yd[0]/div,color=c_ii,label=label_ii)
      # plot error of the mean for <delta>
      axs[bot].fill_between(x,yd[1]/div,yd[2]/div,alpha=.25,lw=0,color=c_ii)
      # plot mean NFW profile
      axs[bot].plot(x,yn[0]/div,color=c_ii)
      # plot Poisson scatter expected in NFW profile
      axs[bot].fill_between(x,yn[1]/div,yn[2]/div,alpha=.0625,lw=0,color=c_ii)
  
  for bot in range(2): # set up axes for both plots
    axs[bot].set_xlim(min(x_bins),max(x_bins))
    axs[bot].set_xscale('log')
    axs[bot].set_yscale('log')
  
  # set colorbar
  norm = mpl.colors.Normalize(vmin=mu_bins[0],vmax=mu_bins[-1])
  sm = plt.cm.ScalarMappable(cmap=cmap+'_r', norm=norm)
  fig.colorbar(sm, ticks=mu_bins, boundaries=mu_bins,
               label=r'$\mu$', ax=axs)
  
  if saving: # save figure
    fname_out = 'delta_x_mu_double.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  # do NOT use tight_layout here!
  plt.show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# create images 

def make_picture(f=fname_new,reds=False,nix_nulls=True,saving=True,
                 mu_range=[14.75,15],Z_range=[.1,.7],cmap='copper',
                 plot_radial=False,Npix=500,R=.45):
  """
  make picture of radial profile as cf NFW expectations
  Npix = side length of picture to generate; should be even preferably
  R = fraction of side length that the virial radius should be plotted
      should be less than 1/2 to show halo edge
  """
  with h5py.File(f,'r') as ds:
    # create masks & respective labels
    mask_mu = (mu_range[0] <= ds['mu'][:]) * (ds['mu'][:] < mu_range[1])
    mu_label = r'$\mu|[%0.2f,%0.2f)$' % (mu_range[0],mu_range[1])
    mask_Z = (Z_range[0] <= ds['Z'][:]) * (ds['Z'][:] < Z_range[1])
    Z_label = r'$Z|[%0.2g,%0.2g)$' % (Z_range[0],Z_range[1])
    mask = np.logical_and(mask_mu,mask_Z)
    # read in vetted data
    Ngal,Nred,Z,mu,rvir = [ds[key][mask] for key in ['Ngal','Nred','Z','mu','rvir']]
    rvir /= cosmo.h # get out of /h units, into cMpc
    radial = ds['radial'][:]
    radial = radial[mask]
    N = radial[:,:,1 if reds else 0]
  
  # grab overdensity data
  dm,ds,dnm,dns = get_delta(N,rvir,mu,Z,nix_nulls=nix_nulls)
  dl,dh = dm-ds,dm+ds
  dnl,dnh = dnm-dns,dnm+dns
  
  if plot_radial: # make radial plot
    plt.plot(x_midpts,dm,mpl_colors[0])
    plt.fill_between(x_midpts,dl,dh,color=mpl_colors[0],alpha=.125,lw=0)
    plt.plot(x_midpts,dnm,'k')
    plt.fill_between(x_midpts,dnl,dnh,color='k',alpha=.125,lw=0)
    # fix up axes
    plt.xlabel(r'Cluster-centric distance $r/r_{\rm vir}$')
    plt.xscale('log')
    plt.xlim(min(x_midpts),max(x_midpts))
    plt.ylabel(r'Relative overdensity $\delta/\delta_{\rm NFW}$')
    plt.yscale('log')
    # tidy up & display 
    plt.tight_layout()
    plt.show()
  
  # make picture
  points = np.zeros((Npix,Npix))
  x_pts = .5 + np.arange(Npix).astype(float) - Npix/2.
  x_pts /= Npix*R
  y_pts = .5 + np.arange(Npix).astype(float) - Npix/2.
  y_pts /= Npix*R
  x,delta = np.zeros((2,Npix,Npix))
  for ii in range(Npix):
    for jj in range(Npix):
      x[ii,jj] = np.sqrt(x_pts[ii]**2 + y_pts[jj]**2)
  
  if 0: # use np.interp (in log space)
    delta = np.exp(np.interp(np.log(x),np.log(x_midpts),np.log(dm)))
    delta_nfw = np.exp(np.interp(np.log(x),np.log(x_midpts),np.log(dnm)))
  else: # use scipy's spline interpolation (in log space)
    spl_delta = interpolate.splrep(np.log(x_midpts),np.log(dm))
    delta = np.exp(interpolate.splev(np.log(x), spl_delta))
    spl_delta_nfw = interpolate.splrep(np.log(x_midpts),np.log(dnm))
    delta_nfw = np.exp(interpolate.splev(np.log(x), spl_delta_nfw))
  
  
  print('\nmin x: %0.6f' % np.min(x))
  if 0: # check radial variable set up correctly
    plt.figure(figsize=(7,6.5))
    plt.imshow(x,extent=np.array([-1,1,-1,1])/R/2.)
    t = np.linspace(0,2*np.pi,100)
    plt.plot(np.cos(t),np.sin(t),'r',lw=1,dashes=[14,10])
    plt.xlabel(r'$x/r_{\rm vir}$')
    plt.ylabel(r'$y/r_{\rm vir}$')
    plt.tight_layout()
    plt.show()
  
  # plot comparison 
  t = np.linspace(0,2*np.pi,100)
  if 0: print('dels:',delta,'\n',delta_nfw)
  dels = np.log10([delta,delta_nfw])
  vmn,vmx = np.min(dels),np.max(dels)
  
  from mpl_toolkits.axes_grid1 import AxesGrid
  fig = plt.figure(figsize=(10,4.75))
  grid = AxesGrid(fig, 111,  # similar to subplot(142)
                  nrows_ncols=(1, 3),
                  axes_pad=0.0,
                  share_all=True,
                  label_mode="L",
                  cbar_location="right",
                  cbar_mode="single")
  
  for ii in range(2):
    im = grid[ii].imshow(dels[ii],extent=np.array([-1,1,-1,1])/R/2.,
                    vmin=vmn,vmax=vmx,cmap=cmap)
    grid[ii].set_xlim(grid[ii].get_xlim())
    grid[ii].set_ylim(grid[ii].get_ylim())
    grid[ii].plot(np.cos(t),np.sin(t),'r',lw=1) #,dashes=[14,10])
    grid[ii].plot(np.cos(t)/10,np.sin(t)/10,'r',lw=.5)
    # grid[ii].plot(np.cos(t)/100,np.sin(t)/100,'blue',lw=.1)
    if not ii: # left plot
      grid[ii].set_title(mu_label) # + " & " + Z_label)
      grid[ii].set_ylabel(r'$y/r_{\rm vir}$')
    else: # right plot
      grid[ii].set_title('NFW expectations')
    grid[ii].set_xlabel(r'$x/r_{\rm vir}$')
  
  rel = np.log10(dels[0]/dels[1])
  mx = max(np.max(-rel),np.max(rel))
  grid[2].imshow(rel,extent=np.array([-1,1,-1,1])/R/2.,
                 vmin=mx,vmax=mx,cmap=cmap)
  
  grid.cbar_axes[0].colorbar(im)
  cax = grid.cbar_axes[0]
  axis = cax.axis[cax.orientation]
  axis.label.set_text(r'$\log_{10} \delta$')
  plt.tight_layout()
  if saving:
    fname_out = 'twodeeplot.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()




# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

d_mu = .550001
def plot_fR_x_mu(f=fname_new,Z_lim=[.1,.7],printing=False,cmap='copper',
                 saving=True,mu_bins=np.arange(13.75,15.4+d_mu,d_mu)):
  """
  plot red fraction varying w/ cluster-centric distance, binned by mass
  """
  if printing:
    print("Scanning in file...")
  with h5py.File(f,'r') as ds:
    mask_Z = (Z_lim[0] <= ds['Z'][:]) * (ds['Z'][:] < Z_lim[1])
    Z,mu,rvir = [ds[key][mask_Z] for key in ['Z','mu','rvir']]
    rvir /= cosmo.h
    radial = ds['radial'][()]
    mask_N = ds['Ngal'][:] > 0 # demand there be at least one galaxy
    radial_gal = radial[mask_Z*mask_N,:,0]
    radial_red = radial[mask_Z*mask_N,:,1]
    fR = radial_red/radial_gal
  # set up x-variable for ease of access
  x = x_midpts
  cw = cm.get_cmap(cmap)
  
  # iterate over mu bins
  for ii in range(len(mu_bins)-1):
    mask_ii = (mu_bins[ii] <= mu) * (mu < mu_bins[ii+1])
    label_ii = label__M_bins % (mu_bins[ii],mu_bins[ii+1])
    if printing:
      print('Processing',label_ii)
    c_ii = cw(1-ii/(len(mu_bins)-2))
    N_halo = np.sum(radial_gal[mask_ii]>0,axis=0)
    # calculate mean, stdev, err mean
    fR_mean = np.nanmean(fR[mask_ii],axis=0)
    fR_std = np.nanstd(fR[mask_ii],axis=0)
    fR_err = fR_std / np.sqrt(N_halo)
    plt.scatter(x,fR_mean,10,color=c_ii,label=label_ii)
    if 1: # set full scatter for low count areas
      fR_mean[N_halo<10] = .5
      fR_err[N_halo<10] = .5
    plt.fill_between(x,fR_mean-1*fR_err,fR_mean+1*fR_err,
                     alpha=.25,lw=0,color=c_ii)
  
  # tidy up and display
  plt.ylabel('Red Fraction')
  ylim = plt.ylim()
  plt.ylim(max(ylim[0],0),1) # keep lower limit at at least zero
  plt.xlabel(r'Cluster-centric distance $r/r_{\rm vir}$')
  plt.xlim(min(x),10)
  plt.xscale('log')
  
  if len(mu_bins)>2:
    plt.legend(loc='lower right')
  plt.tight_layout()
  if saving: # save figure
    fname_out = 'fR_x_mu.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# previous radial plotter

def plot_rp(fname=fname_agg % 16,bool_red=False,cmap='copper_r',
            rho_shift=2,z_bins=np.linspace(.1,.7,5), 
            mu_bins=[13.75,14,14.5,15.5],save_output=False,offset=0):
  """
  Plot radial profiles
  
  fname=fname_agg % 16        # default filename to aggregated data
  bool_red=False              # plot red galaxies only
  cmap='copper_r'             # colormap
  rho_shift=2                 # vertical offset: 10**rho_shift
  z_bins=np.linspace(.1,.3,5) # redshift bins for plotting
  mu_bins=[13.5,14,14.5,16]   # mass bins for plotting
  save_output=False           # save as pdfs
  offset=0                    # remove this many edge points
  
  Reminder on how the radial dataset is defined: 
  dxi=.1; xi_bins = np.arange(-3,1,dxi); x_bins = 10**xi_bins
  radial_report = np.zeros((N_halos,len(x_bins),2))
  r/r_vir = x,xr = dist/r_vir[ii],dist_r/r_vir[ii]
  radial_report[ii,jj,0] = len(x[(x_bins[jj]<x)*(x<x_bins[jj+1])])
  radial[halo number, x position, galaxy count]
  """
  # grab values
  with h5py.File(fname,'r') as ds:
    Z = ds['Z'][()]
    Nred = ds['Nred'][()]
    Ngal = ds['Ngal'][()]
    r200 = ds['r200' if 'r200' in ds.keys() else 'rvir'][()]/cosmo.h # cMpc (not per h)
    if 'mu' in ds.keys():
      mu = ds['mu'][:]
      m200 = 10**mu
      radial = ds['radial'][:,:,1 if bool_red else 0]
    else:
      m200 = ds['m200'][()]/cosmo.h # to convert out of per h units
      mu = np.log10(m200)
      radial = ds['radial'][:,:-1,1 if bool_red else 0]
  
  volumes = np.outer(r200**3,X) # cMpc^3
  rho_gal = radial/volumes
  rho_max = max(np.sum(rho_gal,axis=0))
  max_shift = 10**((len(mu_bins)-2)*rho_shift)
  
  # set up Z bins and labels
  masks_Z = [np.logical_and(z_bins[ii]<=Z,Z<z_bins[ii+1]) \
             for ii in range(len(z_bins)-1)]
  labels_Z = [f'$z|[{z_bins[ii]:0.2f},{z_bins[ii+1]:0.2f})$' \
              for ii in range(len(z_bins)-1)]
  
  # set up mu bins and labels
  mu_bins = np.array(mu_bins)
  labels_mu = [f'$\mu|[{mu_bins[jj]:0.2f},{mu_bins[jj+1]:0.2f})$' \
               for jj in range(len(mu_bins)-1)]
  masks_mu = [np.logical_and(mu_bins[jj]<=mu,mu<mu_bins[jj+1]) \
              for jj in range(len(mu_bins)-1)]
  cw = cm.get_cmap(cmap)
  if len(mu_bins)>2: # more than one bin
    colors = cw((mu_bins[:-1]-mu_bins[0])/(mu_bins[-2]-mu_bins[0]))
  else: # single color for single bin
    colors = [cw[0]]
  
  plt.rcParams['font.size'] = 12
  for ii in range(len(z_bins)-1): # for each z-bin,
    z_mask,z_label = masks_Z[ii],labels_Z[ii]
    z_ii = np.mean(Z[z_mask])
    rho_bar = get_rho(z_ii) # galaxy count per cMpc^3
    # start a plot
    for fignum in [1,2]:
      plt.figure(fignum,figsize=(10,6))
      plt.subplot(221+ii) # TODO: softcode to allow different z-bins
                          # see plot_PDF for how I got Nrows/Ncols
      if 1:
        plt.title(z_label)
      else: # TODO: get this right! 
        plt.figtext(0.25,0.5,z_label)
      plt.xscale('log')
      plt.xlim(x_bins[0+offset],x_bins[-(1+offset)])
      plt.yscale('log')
    
    plt.figure(1)
    plt.ylim(1,10**np.ceil(np.log10(rho_max)+.25)*max_shift) # round up
    plt.figure(2)
    plt.ylim(1e-2,1e2) # this seems safe, as most are w/i factor of 10
    
    for jj in np.flip(range(len(mu_bins)-1)): # plot each mu bin
      mu_mask,mu_label = masks_mu[jj],labels_mu[jj]
      mu_jj = np.mean(mu[mu_mask])
      shift_jj = 10**(rho_shift*jj)
      mask = np.logical_and(z_mask,mu_mask)
      
      N_ij = np.sum(radial[mask],axis=0)
      rho_gal_ij = N_ij/np.sum(volumes[mask],axis=0)
      rho_err = np.zeros(len(N_ij)) # set up zero error as default
      rho_err = np.divide(rho_gal_ij/rho_bar,np.sqrt(N_ij),
                          rho_err,where=N_ij>0)
      rho_mn,rho_pl = [(rho_gal_ij/rho_bar - 1 + mp*rho_err) \
                       for mp in [-1,+1]]
      
      # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
      plt.figure(1)
      plt.scatter(x_midpts,(rho_gal_ij/rho_bar-1)*shift_jj,2,
                  label=mu_label,color=colors[jj])
      plt.fill_between(x_midpts[N_ij>1],rho_mn[N_ij>1]*shift_jj,
                                        rho_pl[N_ij>1]*shift_jj,
                       alpha=.25,color=colors[jj],linewidth=0)
      c = cosmo.get_c(10**mu_jj,z_ii)
      # TODO: calculate delta for virial case Delta_vir
      nfw = cosmo.NFW(x_midpts,200,c_Delta=c,r_Delta=1)/cosmo.Omega_m(z_ii)
      plt.plot(x_midpts,nfw*shift_jj,color=colors[jj])
      
      # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
      plt.figure(2)
      plt.scatter(x_midpts,(rho_gal_ij/rho_bar-1)/nfw,2,
                  label=mu_label,color=colors[jj])
      plt.fill_between(x_midpts[N_ij>1],(rho_mn/nfw)[N_ij>1],
                                        (rho_pl/nfw)[N_ij>1],
                       alpha=.25,color=colors[jj],linewidth=0)
      plt.plot(x_midpts,x_midpts/x_midpts,'k')
    
    if ii==1: # only generate one legend
      for fignum in [1,2]:
        plt.figure(fignum)
        plt.legend(loc='upper right') # title='Mass range'
  
  fig = plt.figure(1)
  fig.text(.5,.015,r'$r/r_{\rm vir}$')
  fig.text(.015,.5,r'$\rho/\bar{\rho}$',rotation='vertical')
  plt.tight_layout()
  fig.subplots_adjust(left=.085,bottom=.085)
  if save_output is not False:
    fname1 = 'rp1.pdf'
    plt.savefig(fname1,format='pdf',dpi=1000)
    print(f'saved {fname1}')
  
  fig = plt.figure(2)
  fig.text(.5,.015,r'$r/r_{\rm vir}$')
  fig.text(.015,.5,r'$\rho/\rho_{\rm NFW}$',rotation='vertical')
  plt.tight_layout()
  fig.subplots_adjust(left=.085,bottom=.085)
  if save_output is not False:
    fname2 = 'rp2.pdf'
    plt.savefig(fname2,format='pdf',dpi=1000)
    print(f'saved {fname2}')
  
  # top=.944,bottom=.075,left=.075,right=.985,hspace=.253,wspace=.111
  plt.show()
  plt.rcParams['font.size'] = 12 # reset sizes
  


def fR_radial(fname=fname_agg % 16,cmap='copper_r',
              mu_bins=[13.75,14,14.5,15.5],saving=False):
  """
  Plot f_R vs radius
  fname=fname_agg % 16
  cmap='copper_r'
  mu_bins=[13.5,14,14.5,16]
  saving=False
  """
  # grab values
  with h5py.File(fname,'r') as ds:
    Z = ds['Z'][()]
    Nred = ds['Nred'][()]
    Ngal = ds['Ngal'][()]
    r200 = ds['r200' if 'r200' in ds.keys() else 'rvir'][()]/cosmo.h # cMpc (not per h)
    if 'mu' in ds.keys():
      mu = ds['mu'][:]
      m200 = 10**mu
      radial_all = ds['radial'][:,:,0] # Ngal; include last radial idx
      radial_red = ds['radial'][:,:,1] # Nred; "
    else:
      m200 = ds['m200'][()]/cosmo.h # to convert out of per h units
      mu = np.log10(m200)
      radial_all = ds['radial'][:,:-1,0] # Ngal; skip last radial idx
      radial_red = ds['radial'][:,:-1,1] # Nred; "
  
  # plot each mu bin
  n_ii = len(mu_bins) - 1
  mx = len(mu_bins) - 2
  cw = cm.get_cmap(cmap) # set up plot colors
  
  for ii in range(n_ii):
    mask_ii = (mu_bins[ii] <= mu) * (mu < mu_bins[ii+1])
    mu_label = f'$\mu|[{mu_bins[ii]:0.3g},{mu_bins[ii+1]:0.3g})$'
    Nred = np.sum(radial_red[mask_ii],axis=0)
    Ngal = np.sum(radial_all[mask_ii],axis=0)
    # don't plot empty x coordinates
    mask = Ngal > 1 
    Nred = Nred[mask]
    Ngal = Ngal[mask]
    x_vals = x_midpts[mask]
    radial_fR_stacked = Nred/Ngal # same dimension as x_midpts
    col = cw(1-ii/mx)
    plt.scatter(x_vals,radial_fR_stacked,10,color=col,label=mu_label)
    
    # handle error
    sm1 = (Nred-np.sqrt(Nred))/(Ngal+np.sqrt(Ngal))
    sp1 = (Nred+np.sqrt(Nred))/(Ngal-np.sqrt(Ngal))
    sm1,sp1 = np.clip([sm1,sp1],0,1)
    plt.fill_between(x_vals,sm1,sp1,alpha=.25,linewidth=0,color=col)
    
  plt.xscale('log')
  plt.xlabel(r'$r/r_{\rm vir}$')
  plt.ylabel(r'$f_R$')
  plt.xlim(1e-3,10)
  ylim = plt.ylim()
  plt.ylim(max(ylim[0],0),1)
  if len(mu_bins)>2:
    plt.legend()
  plt.tight_layout()
  if saving is not False:
    plt.savefig('fR_radial.pdf',format='pdf',dpi=1000)
  plt.show()

########################################################################
# TNV comparison plots

bins_TNV = {
  "Z_bins":[[.3,.35],[.45,.5],[.6,.65]],
  "lambda_bins":[[30,45],[45,60]],
}

vals_TNV = { # vals = [mean, scatter, error of the mean]
  "fR":[[[.8075,.1,.025], # fR[Z_bin,lambda_bin,vals]
         [.8306,.1,.025]],
        [[.8504,.1,.025],
         [.8603,.1,.025]],
        [[.6326,.1,.025],
         [.6634,.1,.025]]],
  "fR_global":[[.4736,.4374],[.4260,.4539],[.3948,.4211]]
}

"""
# zbin, richness bin, red/fraction est., field reference
0 0 0.8075327249735125 0.47356605679649794
0 1 0.8305923863605039 0.4373757949125596
1 0 0.8504364476771304 0.425990472870919
1 1 0.8602917432020917 0.45392635201068254
2 0 0.632600436768205 0.3947945560596241
2 1 0.663411053175356 0.4210898077862765
"""

if 1: # turn into np arrays
  fname_TNV_radial = 'TNV_radial.txt'
  df = pd.read_csv(fname_TNV_radial)
  TNV_R = 10**df['R [arcmin]'].values
  Z0L1, Z1L1, Z2L1, Z0L0, Z1L0, Z2L0 = [df[key].values \
    for key in ['Z0L1', 'Z1L1', 'Z2L1', 'Z0L0', 'Z1L0', 'Z2L0']]
  TNV_v = np.array([[Z0L0,Z0L1],[Z1L0,Z1L1],[Z2L0,Z2L1]])
else: # use r/r200 from TNV
  fname_TNV_radial = 'TNV_radial_relative.txt' # DNE
  pass


def plot_fR_Z_TNV(fname=fname_new,bins=bins_TNV,vals=vals_TNV,
                  cmap='autumn',saving=False):
  with h5py.File(fname,'r') as ds:
    Z,mu,Ngal,Nred = [ds[key][:] for key in ['Z','mu','Ngal','Nred']]
    fR = Nred/Ngal
  # set up colormap
  cw = cm.get_cmap(cmap)
  # plot in bins
  xlim = plt.xlim(np.min(bins['Z_bins']),np.max(bins['Z_bins']))
  xlim = plt.xlim(.1,.7)
  L = len(bins['lambda_bins'])
  for jj in range(L):
    l_low,l_high = bins['lambda_bins'][jj]
    mask_jj = (l_low <= Nred) * (Nred < l_high) # select lambda range
    label_jj = r'$N_{\rm red}|[%g,%g)$' % (l_low,l_high)
    c_jj = cw([.75,.25][jj]) # grab color from cmap
    # run kllr
    kw = .0375
    lm = kllr_model(kernel_width=kw)
    xr,yrm,icpt,sl,sig = lm.fit(Z[mask_jj],fR[mask_jj],nbins=100)
    sqt = np.sqrt(get_counts(Z,xr,kw))
    # plot results
    plt.plot(xr,yrm,label=label_jj,color=c_jj)
    plt.fill_between(xr,yrm-sig,yrm+sig,alpha=.125,linewidth=0,color=c_jj) # scatter
    plt.fill_between(xr,yrm-10*sig/sqt,yrm+10*sig/sqt,alpha=.25,linewidth=0,color=c_jj)
    # plot TNV results
    for ii in range(len(bins['Z_bins'])):
      Z_low,Z_high = bins['Z_bins'][ii]
      mask_ii = (Z_low <= Z) * (Z < Z_high) # select in redshift range
      label_ii = r'$z|[%0.2f,%0.2f)$' % (Z_low,Z_high)
      # read in data from vals
      mean,sig,err = vals['fR'][ii][jj]
      # add scatter point for each measurement
      Z_mid = (Z_low + Z_high)/2.
      hdZ = (Z_high - Z_low)/2. # half of dZ
      shift = (jj - (L - 1)/2.) * (hdZ/L/2)
      xerr = np.array([hdZ + shift, hdZ - shift])
      xerr = xerr[..., np.newaxis] # make 1d for errorbar
      plt.errorbar(Z_mid + shift,mean,sig,xerr,fmt='o',capsize=0,color=c_jj)
      plt.errorbar(Z_mid + shift,mean,err,xerr,fmt='o',capsize=5,color=c_jj)
  # tidy up & display
  plt.xlabel('Redshift')
  plt.xlim(.25,.70)
  plt.ylabel('Red fraction')
  plt.legend(loc='upper right')
  plt.tight_layout()
  if saving:
    fname_out = 'fR_Z_TNV.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


def plot_fR_lambda_TNV(fname=fname_new,bins=bins_TNV,vals=vals_TNV,
                       Nred_min=15,saving=False):
  # grab values
  with h5py.File(fname,'r') as ds:
    mask = ds['Nred'][:] > Nred_min/10
    Z,mu,Ngal,Nred = [ds[key][mask] for key in ['Z','mu','Ngal','Nred']]
    ell = np.log(Nred)
    fR = Nred/Ngal
  # set up colormap
  colors = ["red","green","blue"]
  cmap = LSC.from_list("redshift", colors)
  cw = cm.get_cmap(cmap)
  # plot in bins
  L = len(bins['Z_bins'])
  for ii in range(L):
    Z_low,Z_high = bins['Z_bins'][ii]
    mask_ii = (Z_low <= Z) * (Z < Z_high) # select in redshift range
    label_ii = r'$z|[%0.2f,%0.2f)$' % (Z_low,Z_high)
    c_ii = cw(1-ii/(L-1))
    # kllr analysis
    kw = .20
    lm = kllr_model(kernel_width=kw)
    xr,avg,icpt,sl,std = lm.fit(ell[mask_ii],fR[mask_ii],
                                [np.log(Nred_min/10),5.3],nbins=100)
    l_mids = np.exp(xr)
    N = get_counts(ell[mask_ii],xr,kw)
    err_mean = std/np.sqrt(N)
    mask = (N > 2) # beyond this, scatter can't be (normally) calculated
    plt.plot(l_mids[mask],avg[mask],color=c_ii,linewidth=1,label=label_ii)
    plt.fill_between(l_mids[mask],(avg-std)[mask],(avg+std)[mask],color=c_ii,lw=0,alpha=.125)
    for n_sig in range(1,2): # previously 4, to get +/- 3 sigma
      plt.fill_between(l_mids[mask],avg[mask]-n_sig*err_mean[mask],
                                    avg[mask]+n_sig*err_mean[mask],
                                    alpha=.125,color=c_ii,linewidth=0)
    # add TNV data
    for jj in range(len(bins['lambda_bins'])):
      l_low,l_high = bins['lambda_bins'][jj]
      ell_low,ell_high = np.log([l_low,l_high])
      # read in data from vals
      mean,sig,err = vals['fR'][ii][jj]
      # add scatter point for each measurement
      l_mid = np.sqrt(l_high*l_low)
      hdell = (ell_high-ell_low)/2. # half delta ell
      shift = (ii - (L - 1)/2.) * hdell/L/2
      l_shift = l_mid*np.exp(shift)
      xerr = np.array([l_shift-l_low, l_high-l_shift])
      xerr = xerr[..., np.newaxis] # make 1d for errorbar
      plt.errorbar(l_shift,mean,sig,xerr,fmt='o',capsize=0,color=c_ii)
      plt.errorbar(l_shift,mean,err,xerr,fmt='o',capsize=5,color=c_ii)
    pass
  pass
  # set up axes
  plt.xscale('log')
  plt.xlabel(r'Richness')
  plt.ylabel(r'$f_R$')
  xlim = np.array(plt.xlim(Nred_min,120)) # H09 uses (1,180)
  plt.ylim(.5,1)
  plt.legend(loc='upper right')
  plt.tight_layout()
  if saving:
    fname_out = 'fR_lambda_TNV.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


def plot_fR_x_TNV(fname=fname_new,bins=bins_TNV,saving=False,N_low=2):
  # grab values
  with h5py.File(fname,'r') as ds:
    Z = ds['Z'][()]
    Nred = ds['Nred'][()]
    Ngal = ds['Ngal'][()]
    Rvir = ds['r200' if 'r200' in ds.keys() else 'rvir'][()]/cosmo.h # cMpc (not per h)
    if 'mu' in ds.keys():
      mu = ds['mu'][:]
      m200 = 10**mu
      radial_gal = ds['radial'][:,:,0] # Ngal; include last radial idx
      radial_red = ds['radial'][:,:,1] # Nred; "
    else:
      m200 = ds['m200'][()]/cosmo.h # to convert out of per h units
      mu = np.log10(m200)
      radial_gal = ds['radial'][:,:-1,0] # Ngal; skip last radial idx
      radial_red = ds['radial'][:,:-1,1] # Nred; "
    fR = radial_red/radial_gal
  # set up colormap
  colors = ["red","green","blue"]
  cmap = LSC.from_list("redshift", colors)
  cw = cm.get_cmap(cmap)
  # plot in bins
  fig,axs = plt.subplots(1,2,sharex=True,sharey=True,figsize=double_width/1.25)
  # subplot
  L = len(bins['Z_bins'])
  for ii in range(L):
    Z_low,Z_high = bins['Z_bins'][ii]
    mask_ii = (Z_low <= Z) * (Z < Z_high) # select in redshift range
    label_ii = r'$z|[%0.2f,%0.2f)$' % (Z_low,Z_high)
    c_ii = cw(1-ii/(L-1))
    for jj in range(len(bins['lambda_bins'])):
      l_low,l_high = bins['lambda_bins'][jj]
      mask_jj = (l_low <= Nred) * (Nred < l_high) # select lambda range
      label_jj = r'$\lambda|[%0.2f,%0.2f)$' % (l_low,l_high)
      axs[jj].set_title(label_jj)
      # set up joint mask & color
      mask = mask_ii * mask_jj # total mask
      Rvir_mid = np.median(Rvir[mask])
      Z_mid = np.median(Z[mask])
      if 0: # check how good median Rvir is as a fit
        # there ~10% scatter in these approximations,
        # with a spread of ~.285 in Rvir (ranging from 0.196 to 0.381)
        plt.clf()
        plt.title('median Z: %0.3g' % Z_mid)
        plt.hist(Rvir[mask],log=True)
        print("spread: %0.3g" % np.std(Rvir[mask]))
        ylim = plt.ylim(plt.ylim())
        plt.plot(Rvir_mid*np.ones(2),ylim,'k')
        plt.show()
      x_new = cosmo.x_to_R(Z_mid,x_midpts,Rvir_mid)
      # plot something
      N_halo = np.sum(radial_gal[mask]>0,axis=0)
      # calculate mean, stdev, err mean
      fR_mean = np.nanmean(fR[mask],axis=0)
      fR_std = np.nanstd(fR[mask],axis=0)
      fR_err = fR_std / np.sqrt(N_halo)
      axs[jj].scatter(x_new,fR_mean,10,color=c_ii,label=label_ii)
      # set full scatter for low count areas
      fR_mean[N_halo < N_low] = .5
      fR_err[N_halo < N_low] = .5
      axs[jj].fill_between(x_new,fR_mean-1*fR_err,fR_mean+1*fR_err,
                       alpha=.25,lw=0,color=c_ii)
      # add TNV data
      axs[jj].plot(TNV_R,TNV_v[ii,jj],color=c_ii)
  # tidy up and display
  for jj in range(len(bins['lambda_bins'])): # label all x-axes
    axs[jj].set_xlabel(r'Cluster-centric distance R (arcmin)')
  plt.xlim(1e-2,1e+2)
  plt.xscale('log')
  axs[0].set_ylabel('Red Fraction')
  ylim = plt.ylim()
  plt.ylim(0,1)
  plt.legend(loc='lower right')
  plt.tight_layout()
  if saving:
    fname_out = 'fR_x_TNV.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


########################################################################
def needed_indices(NSIDE=16):
  if NSIDE==8:
    hpix = hpix_8
  elif NSIDE==16:
    hpix = hpix_16
  else:
    print("WARNING: Converting hpix...")
    hpix = convert_index(16384,NSIDE,hpix_16384)
  
  indices = np.array(sorted(set(hpix)))
  fnames = np.array([fname % (NSIDE,unique_identifier(NSIDE,index)) \
                     for index in indices])
  fmasks = np.array([isfile(f) for f in fnames])
  fnames = fnames[~fmasks] # non-existant files needing made
  Nfiles = len(fnames) # how many more to do
  
  f_list = np.arange(rank,Nfiles,size)
  fnames = fnames[f_list]
  indices = indices[~fmasks][f_list]
  return fnames,indices

########################################################################
if rank==0: 
  elapsed = time()-start_time # booting time in seconds
  if elapsed>60: # for the longer boot times
    print(f"Booting time: {elapsed/60.:0.3g} min")
  elif elapsed>2:
    print(f"Booting time: {elapsed:0.3g} s")
  # else print nothing

########################################################################

if __name__=='__main__':
  # TODO: this is bad, since running voxel at the moment scans in the full ds! :O
  print("rework intro, so you don't scan in the entire dataset!")
  print(f"[{rank}/{size}] ~fin")
  raise SystemExit
