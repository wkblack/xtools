#!/bin/python
########################################################################
# stack up plots, grouped by mass bins

from matplotlib import pyplot as plt
import numpy as np
import galaxy_id as gid

if 0: # use short list
  hl = gid.HID_list
else: # use full list
  import pandas as pd
  df_haloes = pd.read_csv('massive_subset.csv')
  hl = np.array(df_haloes.HID1,dtype=int)

DISPLAY_FREQUENCY = 10
N_BINS = 10
N_DECADES = 3
MAX_LENGTH = N_BINS*N_DECADES
dNdV_tot = np.zeros(MAX_LENGTH) # to sum results
mass_tot = np.zeros(DISPLAY_FREQUENCY) # to sum results
finals_unset=True

for i,hid in enumerate(hl):
  print "HID:",hid
  hdl = gid.halo_data_list(hid)
  df = gid.members_in_R20(hid,hdl)
  if 0:
    dv = gid.vet(df,hdl) # vet by m_*(z) and R200
  else: 
    dv = df
  X,dNdV,dX = gid.radial_profile(dv,hdl,plotting=False,
                                 steps_per_decade=N_BINS)
  
  if finals_unset and len(dX)==MAX_LENGTH:
    X_final,dX_final = X,dX
    finals_unset = False
    print 'finals set!'
  
  if len(dNdV)>MAX_LENGTH: # it's too long
    print "Warning! Should probably increase MAX_LENGTH."
    dNdV_tot += dNdV[-MAX_LENGTH:]
  else: # it's too short
    dNdV_tot += np.insert(dNdV,0,np.zeros(MAX_LENGTH-len(dNdV)))
  
  mass_tot[i%DISPLAY_FREQUENCY]=hdl['M200']
  
  if (i+1)%DISPLAY_FREQUENCY==0: # every 15 haloes
    # display the data and reset dNdV_tot
    plt.bar(X_final[:-1],dNdV_tot/MAX_LENGTH,width=dX_final,align='edge') # bottom=min(.1,min(dNdV))
    plt.title("Mass range & average: [%g,%g] & %g" \
           % (min(mass_tot),max(mass_tot),sum(mass_tot)/len(mass_tot)))
    plt.xscale('log'); plt.yscale('log');
    plt.xlabel(r'Fractional Radius $r/r_{200}$'); plt.ylabel(r'Average $N/{r_{200}}^3$');
    plt.show()
    dNdV_tot*=0
    
# plt.scatter(range(MAX_LENGTH),dNdV_tot); plt.show()

# plt.bar(X_final[:-1],dNdV_tot,width=dX_final,align='edge') # bottom=min(.1,min(dNdV))
# plt.show()
