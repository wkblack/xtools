#!/bin/python
import h5py

# print off a directory of contents for a given hdf5 file

def unpack(f,level=0):
  try:
    for name in f.keys():
      print '\t'*level,name
      unpack(f[name],level+1)
  except AttributeError:
    pass

if __name__=='__main__':
  Chinchilla = '/project/projectdirs/des/jderose/Chinchilla/Herd/Chinchilla-3/'
  version = 'v1.9.8/sampleselection/Y3a/mastercat/Buzzard-3_v1.9.8_Y3a_mastercat.h5'
  f = h5py.File(Chinchilla+version)
  unpack(f)
