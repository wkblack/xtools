#!/bin/python

import galaxy_id as gid

from cosmology import project_to_xy_screen as project
from cosmology import rotate_to_north_pole as to_north
from cosmology import to_degrees
from cosmology import degrees_between as db
# from cosmology import rad_btw_sky as rbs
from cosmology import RAD2DEG
from cosmology import halo_points
from cosmology import aardvark
h=aardvark["H0"]/100.

from numpy import linspace,pi,ones,sin
from matplotlib import pyplot as plt
from matplotlib.patches import Circle


# HID = gid.test_HID2
HID = 7111109
df = gid.members(HID)

if 0: # make space plot
  gid.space_plot(df,halo=True)
  print 'fin: space plot'

z,ra,dec,r200,Rs = gid.halo_data(HID)
theta200 = to_degrees(r200/h,z)
print 'theta200=%g' % theta200

ra_circle,dec_circle = halo_points((ra,dec),theta200)
ra_i, dec_i = df.TRA.values, df.TDEC.values

phi_i, theta_i = to_north((ra_i,dec_i), (ra,dec))
L=100 # number of points for circle
phi_h, theta_h = linspace(0,2*pi,100), theta200*ones(100)

rh=df.RHALO.values
zh=df.Z.values
r=rh/(1+zh); x=r/r200
r2=rh/(1+z); x2=r2/r200
x3=rh/r200
th=db((ra_i,dec_i),(ra,dec)); y=th/theta200
# th2=rbs((ra_i,dec_i),(ra,dec))*RAD2DEG; y2=th2/theta200

X,Y = x,y

if 0: 
  plt.scatter(rh,x2-x)
  plt.xlabel(r'RHALO')
  plt.ylabel(r'rh/(1+z)/r200-rh/(1+df.Z)/r200')
  plt.show() 
if 0: 
  plt.scatter(rh/r200,y,label='rh')
  plt.scatter(rh/r200/(1+z),y,label='rh/(1+z)')
  plt.scatter(rh/r200/(1+zh),y,label='rh/(1+df.Z)')
  plt.scatter(rh/r200*(1+z),y,label='rh*(1+z)')
  plt.scatter(rh/r200*(1+zh),y,label='rh*(1+df.Z)')
  plt.scatter(rh/r200*.7,y,label='rh*.7')
  plt.scatter(rh/r200/.7,y,label='rh/.7')
  plt.scatter(rh/r200/(1+z)*.7,y,label='rh/(1+z)*.7')
  plt.scatter(rh/r200/(1+z)/.7,y,label='rh/(1+z)/.7')
  if 0: 
    for i in [-3,-2,-1,0,1,2,3]: 
      for j in [-3,-2,-1,0,1,2,3]: 
        plt.plot([0,1],[0,1],'k')
        plt.scatter(rh/r200*(1+z)**i*.7**j,y,label='rh*(1+z)^%i*.7^%i'%(i,j))
        plt.xlabel(r'$r/r_{200}$'); plt.ylabel(r'$\theta/\theta_{200}$'); 
        plt.legend()
        plt.show()
  
  plt.plot([0,1],[0,1],'k')
  plt.xlabel(r'$r/r_{200}$'); plt.ylabel(r'$\theta/\theta_{200}$'); 
  plt.legend()
  plt.show()
if 0: 
  plt.scatter(rh/r200,y,label='rh/r200')
  plt.scatter(rh/r200/(1+z),y,label='rh/r200/(1+z)')
  plt.scatter(rh/r200/(1+zh),y,label='rh/r200/(1+zh)')
  plt.scatter(rh/r200*(1+z),y,label='rh/r200*(1+z)')
  plt.scatter(rh/r200*(1+zh),y,label='rh/r200*(1+zh)')
  plt.xlabel(r'$RHALO/r_{200}$')
  plt.ylabel(r'$\theta/\theta_{200}$')
  plt.legend()
  plt.show()
if 1: 
  plt.plot([0,1],[0,0],'k') # all should be below this line!
  plt.scatter(Y,Y-X,label='z from individual galaxies')
  plt.scatter(Y,Y-x2,label='z from halo')
  
  if 0: # plot other powers
    plt.scatter(y,y-x3,label='0,0'); # 5% error
    plt.scatter(y,y-x3*(1+zh)**-1*.7**0,label='-1,0')
    plt.scatter(y,y-x3*(1+zh)**3*.7**2,label='3,2')
    plt.scatter(y,y-x3*(1+zh)**-1*.7**-1,label='-1,-1')
    plt.scatter(y,y-x3*(1+zh)**2*.7**1,label='2,1')
    plt.scatter(y,y-x3*(1+zh)**-2*.7**-2,label='-2,-2')
    plt.scatter(y,y-x3*(1+zh)**1*.7**0,label='1,0')
    # plt.scatter(x,y-x); plt.scatter(x,y-x); # 37% error
    # plt.scatter(x2,y-x2); plt.scatter(x2,y-x2); # 36% error
  
  plt.legend()
  plt.xlabel(r'$\theta/\theta_{200}$')
  plt.ylabel(r'$\theta/\theta_{200}-r/r_{200}$'); 
  plt.show()

raise SystemExit


if 0: # plot in polar
  plt.polar(phi_i,sin(theta_i),'k.')
  plt.polar(phi_h,sin(theta_h),'r-')
  plt.show()





X,Y = project((ra_i,dec_i),(ra,dec))


def plot_points(X,Y,theta200=-1,center=(0,0)): 
  # plot X & Y w/ equal axes
  plt.scatter(X,Y,c=db((X,Y),center),marker='.')
  plt.xlabel(r'X'); plt.ylabel(r'Y')
  plt.xlim(min(X),max(X)); plt.ylim(min(Y),max(Y)); 
  ax = plt.gca()
  ax.set_aspect('equal', 'box')
  if theta200>0: 
    ax.add_artist(Circle(center,theta200,fill=False,color='r'))
  plt.colorbar()
  plt.tight_layout()
  plt.show()

plot_points(ra_i,dec_i,theta200,center=(ra,dec))
plot_points(X,Y,theta200)

# gid.space_plot(df,halo=True)
