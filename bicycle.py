#!/sw/lsa/centos7/python-anaconda2/created-20170424/bin/python

########################################################################
# the new version of theWheel.py
# inputs: 
#  1) observed lambda -- from cluster catalog 
#  2) halo true mass  -- from halo truth cat. (MVIR)
# these quantities are matched through Arya's catalog 
########################################################################

########################################################################
# preliminary test for input with {lambda,z,mass}: 
from os.path import isfile
import config as cfg

# input_filename = 'RM_training_fixed.csv'
input_filename = 'RM_training_corrected.csv'
input_file = cfg.xtools + input_filename
assert(isfile(input_file))

# input with M200 for M1 -> M4
m200_dir = cfg.xtools
m200_filename = 'halo_M200.csv'
m200_file = m200_dir + m200_filename
assert(isfile(m200_file))

# input with Lx/Tx/Fx datacube for interpolation
cube_path = 'parameters/Models/L_to_Flux/CRb05-2_overdl2.fits'
cube_file = cfg.xtra + cube_path
assert(isfile(cube_file)) 

########################################################################
########################################################################
# Structure: 
# 1. Read input file for {lambda,z,mass} 
# 2. convert mass into {Lx,Tx}
# 3. convert {Lx,Tx,z} into Fx
# 4. save as csv
########################################################################
########################################################################

########################################################################
# imports 

from time import time
from sys import stdout
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
print "Importing libraries...\r",
stdout.flush(); start = time() 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

import numpy as np
import numpy.random as npr # needed for scatter in scaling relations 
from astropy.io import fits # to import data

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
end = time() 
print "Importing libraries took %g seconds" % (end-start) 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

########################################################################
# define cosmology 

# sorry it's hardcoded to Aa at the moment! 
from cosmology import aardvark
h = aardvark["H0"]/100. 

from cosmology import E # evolutionary constant

m200_m500_factor = 0.714 # conversion factor from Arya (loose)

########################################################################
if __name__=='__main__': 
  # 1. Read input file for {lambda,z,mvir} and for {m1 -> m4} in M200
  
  print "Reading in matching dataset...\r",
  stdout.flush(); start = time() 
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # item list: CLUSTERID,M_HALOID1,M_HALOID2,M_HALOID3,M_HALOID4,
  #            M_HALOIDN,STR1,STR2,STR3,STR4,
  #            STRO,STR_EXP,LAMBDA,Z,CENTRAL_FLAG,
  #            CENTRAL_BCG_FLAG,RA,DEC,R_LAMBDA,LAMBDA_ERR,
  #            M_1,M_2
  fields = (1,2,6,7,12,13, 
            14,15,16,17,19,20,21) 
  # (HID1,HID2,STR1,STR2,LAMBDA,Z, 
  #  CENTRAL_FLAG,CENTRAL_BCG_FLAG,RA,DEC,LAMBDA_ERR,M1,M2)
  data = np.loadtxt(input_file,delimiter=',',skiprows=1,usecols=fields)
  fields = ["HID1","HID2","STR1","STR2","LAMBDA","z",
            "CENTRAL_FLAG","CENTRAL_BCG_FLAG","RA","DEC",'LAMBDA_ERR',"M1","M2"]
  # NOTE: mass is virial, units: 1e-14 Msol/h, so we must convert mass: 
  # data[:,6:8] *= 1e14/h # Msol
  # but it'll be overwritten anyway, so it's commented out for now.
  
  [HID1,HID2,STR1,STR2,lam,z,CENTRAL_FLAG,CENTRAL_BCG_FLAG,RA,DEC,
   lam_err,mvir1,mvir2] = [data[:,i] for i in range(len(fields))]
  
  CEN_mask = np.logical_and(CENTRAL_FLAG,CENTRAL_BCG_FLAG) # central mask
  if 0: print "\033[K" + "sum mask/~mask :",sum(CEN_mask),"/",sum(~CEN_mask)
  
  # mask data to be in-bounds of our investigation:
  z_mask = np.logical_and(0.1<=z,z<=0.3); lam_mask = 20<=lam
  dat_mask = np.logical_and(z_mask,lam_mask)
  
  # grab M200 for H1 & H2 (Msol/h)
  m200_lst = np.loadtxt(m200_file,delimiter=',',skiprows=1,usecols=(4,5))/h # Msol
  m500_lst = m200_lst * m200_m500_factor 
  data[:,11:13] = m500_lst # overwrite data
  [M1,M2]=[m500_lst[:,i] for i in range(2)]
  mass = M1
  
  if 0: # TESTING
    # plot up MVIR vs LAMBDA
    from matplotlib import pyplot as plt
    if 0: # use virial mass
      y=mvir1 # *1e14
    elif 1: # use M200
      y=mass/m200_m500_factor
    else: # use M500
      y=mass
    
    print 'min//max lam : %g // %g' % (min(lam),max(lam))
    
    # create new mask
    nzm = narrow_z_mask = np.logical_and(0.18<=z,z<=0.22)
    # CEN_mask = np.logical_and(CEN_mask,narrow_z_mask)
    
    xx,yy = np.log(lam), np.log(y)
    p = np.polyfit(xx[CEN_mask],yy[CEN_mask],1)
    
    if 0: # plot inverse as well
      plt.scatter(lam[~CEN_mask],y[~CEN_mask],
                  marker='.',alpha=.3,edgecolors='g',facecolors='none')
    if 1: # plot masked data
      plt.scatter(lam[CEN_mask],y[CEN_mask],
                  marker='.',alpha=.3,edgecolors='y',facecolors='none')
    
    
    plt.plot(lam[CEN_mask],np.exp(p[1])*lam[CEN_mask]**p[0],c='b')
    plt.xlabel(r'Richness $\lambda$')
    plt.xscale('log') 
    plt.ylabel(r'$M_{200}$ (M$_\odot/h$)')
    plt.xlim(15,200)
    if 1: 
      plt.ylim(5.5e12,2e15) # Arya's plot
    else: 
      plt.ylim(2e12,1e15)
    plt.yscale('log') 
    plt.show() 
    raise SystemExit
  
  alter_mass = False
  if alter_mass: # change mass value to reflect strengths
    normalize_mass = False
    if normalize_mass: # normalized to unity if M1 is only halo
      mass = (STR1*M1+STR2*M2)/(STR1+STR2)
    else: # un-normalized
      mass = STR1*M1+STR2*M2
      # closer to the Chandra+XMM data, 
      # but that's not the way to combine them. 
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  end = time() 
  print "Reading in matching dataset took %g seconds" % (end-start) 


########################################################################
# 2. convert mass into {Lx,Tx}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# calculate Lx

LxS_MantzEtAl2016 = { # Lx scaling parameters
  "M_p":6e14, # Msol # M500
  "z_p":0.35,
  "a":1.087e45,
  "M_slope":1.26, # ln(erg/s) # M500 slope
  "E_slope":1.20, # ln(erg/s)
  "sig":0.24,     # Lx scatter 
  # "sig":0.000001,     # Lx scatter (FIXME)
  "url":"http://adsabs.harvard.edu/abs/2016MNRAS.456.4020M" # Table 3
}

LxS_RykoffEtAl2008 = { # Lx scaling parameters # seems less accurate than Mantz
  "M_p":1e14/h*m200_m500_factor, # Msol # M500
  "z_p":.2,         # dummy value
  "a":12.6e42/h**2, # plus scatter
  "M_slope":1.65,   # plus scatter
  "E_slope":0,      # ignored in this model
  "sig":np.sqrt(1.35**2+1.6**2), # approximate, ignores M_slope scatter
  "url":"https://academic.oup.com/mnrasl/article/387/1/L28/981484" # abstract
}

def Lx_calc(mass,z,LxS=LxS_MantzEtAl2016,cosmology=aardvark):
  E_vals = E(z,cosmology)
  Ep_val = E(LxS["z_p"],cosmology)  
  Mp_val = LxS["M_p"] # * cosmology["H0"] # no h conversion here. 
  # LxS are the scaling parameters between mass and luminosity
  lnLx = np.log(LxS['a']) + LxS['M_slope'] * np.log(mass/Mp_val) \
           + LxS['E_slope'] * np.log(E_vals/Ep_val) \
           + npr.normal(0.0, LxS['sig'], len(z))
  #        - 44*np.log(10) # this normalization was used, but it's ugly.
  return np.exp(lnLx) # ergs/s # would need /10^44 to normalize

if __name__=='__main__': 
  print "Calculating Lx...\r",
  stdout.flush(); start = time() 
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if 1: 
    Lx = Lx_calc(mass,z) # units: erg/s
  else: 
    Lx = Lx_calc(mass,z,LxS=LxS_RykoffEtAl2008) # units: erg/s
  Lx = Lx_calc(mass,z) # units: erg/s
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  end = time() 
  print "Calculating Lx took %g seconds" % (end-start) 



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# calculate Tx

TxS_MantzEtAl2016 = { # Tx scaling parameters
  "M_p":6e14,
  "z_p":0.35,
  "a":8.84, # = exp(2.18) 
  "M_slope":0.66, # kT/keV
  "E_slope":0.61, # kT/keV
  "sig":0.13, # scatter
  # "sig":0.000001, # scatter --> FIXME
  "url":"http://adsabs.harvard.edu/abs/2016MNRAS.456.4020M" # table 3
}

def Tx_calc(mass,z,TxS=TxS_MantzEtAl2016,cosmology=aardvark): 
  Mp_val = TxS["M_p"] # * cosmology["H0"] # no h conversion here. 
  Ep_val = E(TxS["z_p"],cosmology)  
  E_vals = E(z,cosmology)
  
  lnTx = np.log(TxS['a']) + TxS['M_slope']*np.log(mass/Mp_val) \
              + TxS['E_slope'] * np.log(E_vals/Ep_val) \
              + npr.normal(0.0,TxS['sig'], len(z))
  return np.exp(lnTx) # keV

WMAP9 = { # as quoted in Farahi 2018
  "H0" : 70,
  "WL" : 0.72,
  "Wm" : 0.28,
  "Wr" : 0,
  "Wk" : 0,
  "wDE": -1.0,
  "url": "https://www.aanda.org/articles/aa/abs/2018/12/aa31321-17/aa31321-17.html",
}

def Tx_calc_Farahi(m200,z,cosmology=WMAP9):
  # NOTE: m200 is mass enclosed within overdensity of 200; 
  #       returns {T_300kpc}/keV
  # Farahi scaling parameters: 
  url = "https://www.aanda.org/articles/aa/pdf/2018/12/aa31321-17.pdf"
  pi_T_mean = 0.45
  pi_T_sigma = 0.24
  alpha_T_mean = 1.89
  alpha_T_sigma = 0.15
  beta_T_mean = -1.29
  beta_T_sigma = 1.14
  M_p = 1e14 # M200 in Msol
  z_p = 0.25
  E_p = E(z_p,cosmology)
  # derived parameters 
  pi_T = npr.normal(pi_T_mean,pi_T_sigma,len(z)) # get a random sample
  alpha_T = npr.normal(alpha_T_mean,alpha_T_sigma,len(z)) # ditto
  beta_T = npr.normal(beta_T_mean,beta_T_sigma,len(z)) # ditto
  assert(np.all(alpha_T>0)) # it's 12.5 sigma away, but still possible
  a = 2.2*np.exp(-pi_T/alpha_T)
  M_slope = 1/alpha_T
  E_slope = -beta_T/alpha_T
  E_vals = E(z,cosmology)
  # full relation: 
  lnTx = np.log(a) + M_slope*np.log(E_vals*m200/M_p) \
                   + E_slope*np.log(E_vals/E_p) 
  return np.exp(lnTx) 

if __name__=='__main__': 
  print "Calculating Tx...\r",
  stdout.flush(); start = time() 
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  Tx = Tx_calc(mass,z) # technically, this is kT
  # Tx = Tx_calc_Farahi(mass,z)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  end = time() 
  print "Calculating Tx took %g seconds" % (end-start) 


########################################################################
# 3. convert {Lx,Tx,z} into Fx

keV_in_erg = 624150647.99632

def Fx_calc(Lx,Tx,z,cubepath=cube_file):
  # read in data cube
  cube = fits.open(cubepath)[1].data
  ZCUBE = cube[0]['Z']
  lg10TCUBE = np.log10(cube[0]['T'])
  L_CUBE = cube[0]['CR_PERL_TIMESD2']
  del cube # clear up some space
  
  iZ = np.array(float(len(ZCUBE) - 1) * (z - ZCUBE[0]) \
               / (ZCUBE[-1] - ZCUBE[0])).astype(int) # list of z-indices
  
  lg10Tx = np.log10(Tx)
  ilgT = np.array(float(len(lg10TCUBE) - 1) * (lg10Tx - lg10TCUBE[0]) \
         / (lg10TCUBE[-1] - lg10TCUBE[0])).astype(int) # list of lg10Tx-indices
  # iNH=0
  
  # set up corners of cube to be interpolated
  Z2 = ZCUBE[iZ + 1]
  Z1 = ZCUBE[iZ]
  
  T2 = lg10TCUBE[ilgT + 1]
  T1 = lg10TCUBE[ilgT]
  
  L11 = [L_CUBE[iZ[index]][ilgT[index]] for index in range(len(iZ))]
  L12 = [L_CUBE[iZ[index]+1][ilgT[index]] for index in range(len(iZ))]
  L21 = [L_CUBE[iZ[index]][ilgT[index]+1] for index in range(len(iZ))]
  L22 = [L_CUBE[iZ[index]+1][ilgT[index]+1] for index in range(len(iZ))]
  
  # count rates per Lx
  CRPERLx = (L11 * (Z2 - z) * (T2 - lg10Tx) 
           + L21 * (z - Z1) * (T2 - lg10Tx) 
           + L12 * (Z2 - z) * (lg10Tx - T1) 
           + L22 * (z - Z1) * (lg10Tx - T1)) / ((Z2 - Z1) * (T2 - T1))
  
  # flux is count rates per luminosity times luminosity
  # units: ergs/s/cm^2
  return CRPERLx * Lx / keV_in_erg * 10**-44 # needed for unit adjustment

if __name__=='__main__': 
  print "Calculating Fx...\r",
  stdout.flush(); start = time() 
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  Fx = Fx_calc(Lx,Tx,z)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  end = time() 
  print "Calculating Fx took %g seconds" % (end-start) 


########################################################################
if __name__=='__main__': 
  # 4. save as csv
  
  print "Saving data...\r",
  stdout.flush(); start = time() 
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  
  newfields = ["Lx","Tx","Fx"] 
  newdata = np.transpose([Lx,Tx,Fx])
  
  # add new fields to data
  outfields = np.concatenate((fields,newfields)) 
  outheader = ",".join(outfields) 
  outdata = np.concatenate((data,newdata),axis=1)
  
  outname = "output_bicycle.csv"
  np.savetxt(outname,outdata,delimiter=',',header=outheader,fmt='%s',comments='') 
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  end = time() 
  print "Saving %s took %g seconds" % (outname,(end-start))
