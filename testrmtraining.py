#!/bin/python
import config as cfg
matching_path = cfg.xtools + 'RM_training.csv'
matching_path = cfg.xtools + 'testrm.csv'

fname = matching_path
with open(fname) as fp:  
  num_elements=-1
  while True: 
    line=fp.readline()
    new_num_elements=len(line.split(","))
    if new_num_elements != num_elements: 
      print "num elements:",new_num_elements
      print "line:",line
      num_elements=new_num_elements
    #
    if not line:
      break

# so all lines had 22 elements besides the last. 
