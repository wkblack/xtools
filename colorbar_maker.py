#!/bin/python
# taken from HYRY's answer on: 
# https://stackoverflow.com/questions/16595138/standalone-colorbar-matplotlib

import pylab as pl
import numpy as np

a = np.array([[.1,.3]])
pl.figure(figsize=(9, 1.5))
img = pl.imshow(a, cmap="coolwarm")
pl.gca().set_visible(False)
cax = pl.axes([0.1, 0.4, 0.8, 0.3]) # left, bottom, width, height
pl.colorbar(cax=cax,orientation='horizontal',label=r'Redshift $z$')
pl.show() 
# pl.savefig("colorbar.pdf")
