#!/sw/lsa/centos7/python-anaconda2/created-20170424/bin/python
# creates subspace of Aardvark sky
# reads in {RA,DEC,[extent]} or {Halo ID}

from glob import glob
from astropy.io import fits as pyfits
import numpy as np
from sys import stdout

from cosmology import to_degrees

import warnings # just to suppress the one
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning) 

import config as cfg

########################################################################
# function definitions 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def makeSubSky(ra,dec,extent=1,folder=cfg.Aardvark['halo_dir'],
               fname=cfg.Aardvark['halo_fname']): 
  # print "making sub-sky patch at RA=%g, DEC=%g..." % (ra,dec)
  # values to grab: 
  RA=[]; DEC=[]; HALOID=[]; R200=[]; N19 = []
  M200 = []; Z=[]; RAOLD=[]; DECOLD=[]
  M500 = []; R500 = []
  
  # read in matching files
  files = glob(folder+fname)
  for i,curr_file in enumerate(files): 
    print "\033[K" + "Processing %i/%i: %s\r" % (i+1,len(files),curr_file),
    stdout.flush() 
    try: 
      h = pyfits.open(curr_file)[1].data[::]
    except IOError: 
      print "Error reading %s --- Skipping." % curr_file
      continue 
    # file read in okay.
    mask = h['M200'] > 1e12; # vet out light masses (was 1e13)
    if 1: print '\nmask:',mask
    if mask.all(False): continue # empty subset; no action needed. 
    h = h[mask] # cut out small halos (was e13)
    
    # mask by RA & DEC
    ra_mask = np.logical_or(np.abs(h['RA']-360-ra)<extent/2.,
                            np.abs(h['RA']-ra)<extent/2.)
    dec_mask = np.abs(h['DEC']-dec) < extent/2.
    mask = np.logical_and(ra_mask,dec_mask) 
    
    if mask.all(False): continue # empty subset; no action needed. 
    h = h[mask] # cut out halos outside the range
    
    # append on items
    N19+=list(h['N19'][:])
    HALOID+=list(h['HALOID'][:]); Z+=list(h['Z'][:])
    RA+=list(h['RA'][:]); DEC+=list(h['DEC'][:])
    M200+=list(h['M200'][:]); R200+=list(h['R200'][:]/(h['Z']+1.))
    M500+=list(h['M200'][:]*0.714)
    R500+=list(h['R200'][:]/(h['Z']+1.)*0.658)
  
  # clear screen and print victory message
  print "\033[K" + "All files processed." 
  
  if len(RA)<1: 
    print "ERROR: No halos contained in that patch of sky!"
    return 1
  
  # rotate RA & DEC to origin
  RAOLD=RA; DECOLD=DEC
  RA = np.array(RA)
  RA=(RA-ra)%360 # shift origin and make values in [0,360)
  RA[RA>180]-=360 # correct values to center around zero
  DEC=np.array(DEC)-dec # shift desired dec to origin
  
  # create new fits file with the vetted data
  col1 = pyfits.Column(name='HALOID', format='J',array=np.array(HALOID))
  col2 = pyfits.Column(name='RA',     format='E',array=np.array(RA))
  col3 = pyfits.Column(name='DEC',    format='E',array=np.array(DEC))
  col4 = pyfits.Column(name='M200',   format='E',array=np.array(M200))
  col5 = pyfits.Column(name='R200',   format='E',array=np.array(R200))
  col6 = pyfits.Column(name='M500',   format='E',array=np.array(M500))
  col7 = pyfits.Column(name='R500',   format='E',array=np.array(R500))
  col8 = pyfits.Column(name='Z',      format='E',array=np.array(Z))
  col9 = pyfits.Column(name='RA_OLD', format='E',array=np.array(RAOLD))
  col10= pyfits.Column(name='DEC_OLD',format='E',array=np.array(DECOLD))
  col11= pyfits.Column(name='N19',format='E',array=np.array(N19))
  cols = pyfits.ColDefs([col1, col2, col3, col4, col5, col6, \
                         col7, col8, col9, col10, col11])
  
  tbhduHalos = pyfits.BinTableHDU.from_columns(cols)
  PrimaryHDUList = np.arange(1); hdu = pyfits.PrimaryHDU(PrimaryHDUList)
  thdulist = pyfits.HDUList([hdu,tbhduHalos])
  outfile = 'sightseeing_RA%g_DEC%g.fit' % (ra,dec)
  thdulist.writeto(outfile) 
  return 0



########################################################################
if __name__ == '__main__':
  from sys import argv
  if len(argv)<2: 
    print "Syntax: >>> RA DEC [width]"
    print "    or: >>> HID"
    raise SystemExit
  
  # defaults
  choice=-1
  
  if len(argv)==2: # use HID
    HID = int(argv[1])
    # scan bicycle output from
    fname = cfg.xtools + 'output_bicycle.csv'
    dat = np.loadtxt(fname,delimiter=',',skiprows=1)
    HID1 = dat[:,0]; idx_mask = HID1==int(HID)
    lam = dat[idx_mask,4]; z = dat[idx_mask,5]
    ra = dat[idx_mask,8]; dec = dat[idx_mask,9]
    
    if len(lam)>1: # check for duplicity
      print "This HID features in multiple clusters;"
      print "Select a cluster to process:"
      for i in range(len(lam)): 
        print "\ti=%i : lambda=%0.2g, z=%0.2g, (RA,DEC)=(%g,%g)" \
             % (i,lam[i],z[i],ra[i],dec[i])
      choice=int(raw_input("Choice: i="))
      lam = lam[choice]; z = z[choice]
      ra = ra[choice]; dec = dec[choice]
      print 
    else: 
      print "Using HID %i" % HID # % (ra[0],dec[0])
    
  else: # use RA+DEC
    HID = -1
    ra = float(argv[1])
    dec = float(argv[2])
  
  from cosmology import theta_R_lambda, to_degrees
  theta_R_lambda = theta_R_lambda(lam,z) # 0.086697
  
  theta_500kpc = to_degrees(.5,z) # 500 kpc in degrees
  
  if len(argv)>3: # read in width of the sky patch
    extent = float(argv[3])
  else: 
    extent = max(3*theta_R_lambda,.6) # .6 is always larger here, but this is a safeguard.
    # see if I can use 3*theta_500
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  print "Creating subsky at RA=%g, DEC=%g" % (ra,dec)
  if makeSubSky(ra,dec,extent):
    print "System failure!"
    raise SystemExit 
  else: 
    print "Subsky created." # at RA:%g, DEC:%g" % (ra,dec)
  
  fname = 'sightseeing_RA%g_DEC%g.fit' % (ra,dec) 
  
  # move file to XTRA input folder
  from os import system as run
  inptDir = cfg.xtra + 'Catalog/Input_File/'
  otptDir = cfg.xtra + 'Catalog/Maps/Surface_Brightness/'
  run("mv %s %s" % (fname,inptDir))
  
  print "Run:" 
  print "cd %s" % cfg.xtra
  print "python main.py 1 %s" % fname
  print "python main.py 2 %s %i %i" % (fname,HID,choice)
  print "display %s%s_[0.5-2.0].pdf &"%(otptDir,fname.split('.fit')[0])
  print "cd -"
  print
  
  raise SystemExit
