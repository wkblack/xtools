#!/global/common/cori_cle6/software/python/2.7-anaconda-5.2/bin/python
# W.K.Black 2019.08
# swap red galaxies of massive halos with blue galaxies (from low-mass)

print "in swapper.py"

########################################################################
# fields that need swapping between datasets
########################################################################

swap_fields=['PX','PY','PZ','VX','VY','VZ','RA','DEC','TRA','TDEC',
             'CENTRAL','HALOID','Z']

def swap_galaxies(galaxy_list,GID1,GID2):
  # swap locational information beteween two galaxies
  g1,g2 = np.where(galaxy_list.ID==GID1)[0][0], \
          np.where(galaxy_list.ID==GID2)[0][0]
  for field in swap_fields:
    galaxy_list[g1][field],galaxy_list[g2][field] \
    = galaxy_list[g2][field],galaxy_list[g1][field]
  return

########################################################################
# imports for purification
########################################################################

import cosmology as cosmo
import galaxy_id as gid
import numpy as np
import HK02 # Hu & Kravtsov 2002
import os

from galaxy_id import vet_by_m_star,ES0_mask
from r_conversion import r_Delta
from astropy.table import Table
from astropy.io import fits
from sys import stdout

def purify_set(halo_list,galaxy_list,fR_target=1.0,
               out_dir='./',out_file='tmp.fits', # default save location
               printing=False,status_update=True,
               DELTA_Z=.05,Z_MIN=.1,Z_MAX=.3,
               params=cosmo.buzzard):
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # check for saving access and prepare outfile
  if not os.access(out_dir, os.W_OK):
    print "ERROR: no writing access for %s!" % out_dir
    raise SystemExit
  # else, writing access available
  fname_out = out_dir + out_file
  if os.path.isfile(fname_out):
    print "file '%s' already exists!" % fname_out
    return
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # initialize halo parameters
  if printing or status_update: print "Reading in halos"
  df_halo = Table.read(halo_list, format='fits').to_pandas()
  df_halo = df_halo[(Z_MIN<df_halo.Z)&(df_halo.Z<Z_MAX)]
  try:
    df_halo = df_halo.sort_values(by='M200C',ascending=False)
  except KeyError: # in case it doesn't have M200C
    df_halo = df_halo.sort_values(by='M200',ascending=False)
  HID_list = df_halo.HALOID.values
  z_list = df_halo.Z.values
  z_min,z_max = min(z_list),max(z_list)
  z_curr = z_min
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # read in all galaxies and vet down to usable ones
  if printing or status_update: print "Reading in galaxies"
  galaxies = fits.open(galaxy_list)[1].data 
  if printing: print "galaxy count before vetting:\t",len(galaxies)
  galaxies = galaxies[(Z_MIN<galaxies['Z'])&(galaxies['Z']<Z_MAX)] 
  if printing: print "galaxy count after z vet:\t",len(galaxies)
  galaxies = vet_by_m_star(galaxies,fits=True) # vet by 0.2 L*
  if printing: print "galaxy count after m* vet:\t",len(galaxies)
  if status_update: print "Reading and vetting complete."
  
  mask_avail = np.ones(len(galaxies)) # start will all galaxies available
  
  if 0: # DEBUG: plot up histograms
    print "Creating histograms..."
    from matplotlib import pyplot as plt
    
    if 1: # plot vs redshift
      plt.hist(z_list,bins=32)
      plt.title('all halos vs redshift')
      plt.show()
      
      plt.hist(galaxies.Z,bins=32)
      plt.title('observable galaxies vs redshift')
      plt.show()
    elif 0: # plot vs mass
      plt.hist(np.log10(df_halo.M200C),log=True,bins=32)
      plt.title(r'halo mass $\log_{10} (M_{200C})$')
      plt.show()
    
    raise SystemExit
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  while z_curr < z_max: # for each redshift bin
    if status_update or printing:
      print "Analyzing redshift bin (%0.4g,%0.4g)" \
             % (z_curr,z_curr+DELTA_Z)
    
    avg_z = z_curr + DELTA_Z/2.
    mask = np.logical_and(z_curr<z_list,z_list<z_curr+DELTA_Z)
    sum_mask = sum(mask)
    top,bot = 0,sum_mask
    HID_bot = -1 # to grab field galaxies first
    
    # hold onto all galaxies in the current redshift bin
    z_mask = np.array((z_curr<galaxies['Z']) \
                    & (galaxies['Z']<z_curr+DELTA_Z))
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    while top < bot: # for the top half of halos
      if 0 and top>100: # DEBUGGING: cut short
        z_curr=z_max
        bot=top
      
      # get data of the top HALOID
      HID_top = HID_list[mask][top]
      hdl = gid.halo_data_list(HID_top,halo_data=df_halo)
      if hdl is None or len(hdl)==0:
        print "ERROR: hdl empty!"
        raise SystemExit
      Dv = HK02.Dc(hdl['Z'],params) # virial overdensity for halo
      r200 = HK02.rh(hdl['RS'],200,Dv,hdl['RVIR'])/1000. # physical Mpc
      
      # select galaxies of the top HID
      subset = np.logical_and(z_mask,mask_avail)
      dist = gid.true_dist(galaxies[subset],
                           hdl)/(1+hdl['Z']) # physical
      in_halo = dist<r200
      gals_ceil = galaxies[subset][in_halo]
      
      if len(gals_ceil)<1: # we can skip this halo.
        if printing: print "empty halo!"
        top+=1 # move onto next most massive halo
        if status_update:
          print "top//bot//total = %g//%g//%g\r" % (top+1,bot+1,sum_mask),
          stdout.flush()
        continue
      
      # calculate Nred,Ngal,N_needed
      mask_ES0_ceil = ES0_mask(gals_ceil,hdl,fits=True)
      Ngal,Nred = len(mask_ES0_ceil),sum(mask_ES0_ceil)
      N_needed = np.ceil(Ngal*fR_target-Nred)
      
      if printing: 
        print "top//bot//total = %g//%g//%g" % (top+1,bot+1,sum_mask)
        print "Ngal-Nred=N_needed: %i-%i=%i" % (Ngal,Nred,N_needed)
      elif status_update:
        print "top//bot//total = %g//%g//%g\r" % (top+1,bot+1,sum_mask),
        stdout.flush()
      
      # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
      # while the most massive halo still wants gals
      while N_needed > 0: 
        # grab galaxies from the bottom HID 
        mask_hid_floor = np.logical_and(z_mask[subset],
                                    galaxies[subset]['HALOID']==HID_bot)
        gals_floor = galaxies[subset][mask_hid_floor]
        # calculate Nred,Ngal,N_needed
        mask_ES0_floor = ES0_mask(gals_floor,hdl,fits=True)
        Nred_floor = sum(mask_ES0_floor)
        
        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
        # swap as many galaxies as possible:
        # up to N_needed of the Nred_floor galaxies 
        
        # have blue galaxies:
        blue_id_list = gals_ceil[~mask_ES0_ceil].ID
        # have red galaxies:
        red_id_list = gals_floor[mask_ES0_floor].ID
        
        ii=0 # counts number of galaxies swapped
        while Nred_floor>0 and N_needed>0:
          if printing: 
            print "poor halo getting mugged has %i RS gals" % Nred_floor
          # grab galaxy IDs # TODO? start with the bluest of the red?
          GID1,GID2=blue_id_list[ii],red_id_list[ii]
          swap_galaxies(galaxies,GID1,GID2)
          # these three are all tied:
          ii+=1; Nred_floor-=1; N_needed-=1;
        
        if Nred_floor<1: # the victim is fully broke -> move on
          if printing: print "poor fellow is broke"
          bot-=1 # move onto the next poorest hao
          HID_bot = HID_list[mask][bot]
          if printing: 
            print "top//bot//total = %g//%g//%g" % (top+1,bot+1,sum_mask)
          elif status_update:
            print "top//bot//total = %g//%g//%g\r" % (top+1,bot+1,sum_mask),
            stdout.flush()
      
      # end while N_needed >0 loop
      # the mugger is satisfied -> move on
      top+=1 # move onto next most massive halo
      
      # now that we've finished processing this halo,
      # nix all galaxies of this halo into the mask!
      mask_avail[subset][in_halo]=0
    
    if printing: 
      print "Completed redshift bin (%g,%g)" % (z_curr,z_curr+DELTA_Z)
    z_curr+=DELTA_Z # move onto the next redshift bin
  
  # save output to catalogue
  print "Saving..."
  if 0: # corrupted?
    # galaxies.tofile(fname_out)
    galaxies.tofile(fname_out,sep='\n') # messy, but technically works. :) 
  elif 0: # doesn't work
    hdu = fits.PrimaryHDU(galaxies)
    hdu.writeto(fname_out)
  elif 0: # doesn't work
    galaxies.write(fname_out,format='fits')
  else:
    # TODO: just tear it apart and save by hand, if needs be...
    # maybe if I grab the original fits hdulist and save *that*? :)
    hdulist = fits.open(galaxy_list) # [1].data 
    hdulist[1].data = galaxies
    
    hdulist.writeto(fname_out)
  print "Saved '%s'" % fname_out
  return

########################################################################
# convert from one NSIDE index to another 
########################################################################

from healpy import pix2ang,ang2pix
NSIDE_gals,NSIDE_halos=8,2

def convert_index(NSIDE_in,NSIDE_out,index,
                  nest_in=False,nest_out=False):
  # see healpy/rotator.py +208 for details
  theta,phi = pix2ang(NSIDE_in,index,nest=nest_in)
  return ang2pix(NSIDE_out,theta,phi,nest=nest_out)

########################################################################
if __name__=='__main__':
  print "in main()"
  # Read in:
  # 1) filenames of galaxy & halo catalogues
  # 2) goal red fraction f_R to reach for the most massive halos
  
  f_R=1.0 # as default
  
  if 0: # use Aardvark simulation 
    from config import Aardvark as sim
    from cosmology import aardvark as sim_cosmology
  else: # use Buzzard simulation 
    from config import Buzzard as sim
    from cosmology import buzzard as sim_cosmology
  
  from sys import argv
  if len(argv)==1:
    print "Input: " + ">>> GALAXY_FILE_INDEX [f_R=1.0]"
    raise SystemExit
  else:
    if len(argv)>1:
      galaxy_index=int(argv[1])
      if galaxy_index < 0:
        print "Using hardcoded value for galaxy index"
        galaxy_index = 277
        # galaxy_index = 0
    if len(argv)>2:
      f_R=float(argv[2])
      try:
        assert(f_R>=0)
        assert(f_R<=1)
      except AssertionError:
        print "ERROR: Red fraction must be in interval [0,1]!"
        raise SystemExit
  
  # pick out the corresponding halo file
  halo_index = convert_index(NSIDE_gals,NSIDE_halos,galaxy_index,
                             nest_in=sim["glxy_nest"],
                             nest_out=sim["halo_nest"])
  
  glxy_fname = sim["glxy_dir"] + sim["glxy_ftemp"] % galaxy_index
  halo_fname = sim["halo_dir"] + sim["halo_ftemp"] % halo_index
  
  # print inputs being used
  print "Using halo file",halo_fname
  print "and galaxy file",glxy_fname
  print "with f_R=%g\n" % (f_R)
  
  # feed into redifier:
  from config import skymaps
  purify_set(halo_fname,glxy_fname,fR_target=f_R,
             DELTA_Z=.025,printing=False,
             out_dir=skymaps,out_file='test_%i.fits' % galaxy_index,
             params=sim_cosmology)
  
  print "~fin"
