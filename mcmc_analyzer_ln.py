#!/bin/python
# combine mcmc individual outputs

from re import findall
from glob import glob

in_fname = 'mcmc_result_lnfxfloor-%i.txt'
out_fname = 'mcmc_result_combined.dat'
f_out = open(out_fname,"w+")
f_out.write("# floor, alpha, alpha_std, beta, beta_std, sigma, sigma_std\n")

numstr = "[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?"

for i in range(11,23):
  with open(in_fname % i) as f_in: 
    [floor]=findall(numstr,f_in.next()) 
    [alpha,alpha_std]=findall(numstr,f_in.next())
    [beta,beta_std]=findall(numstr,f_in.next())
    [sigma,sigma_std]=findall(numstr,f_in.next())
    dat = [floor,alpha,alpha_std,beta,beta_std,sigma,sigma_std]
    # f_out.write(["%s, " % d for d in dat]+"\n") 
    f_out.write(", ".join(dat)+'\n')

f_out.close() 
