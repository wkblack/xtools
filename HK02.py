# rewrite of r_conversion... maybe something different will happen? 
# DOI:10.1086/345846 URL:iopscience.iop.org/article/10.1086/345846
########################################################################

import numpy as np

a_params=[0,.5116,-.4283,-3.13e-3,-3.52e-5]

def p(f_val,a=a_params):
  return a[2] + a[3]*np.log(f_val) + a[4]*(np.log(f_val))**2

def x_f(f_val,a=a_params): 
  return (a[1]*f_val**(2*p(f_val,a))+.75**2)**(-.5) + 2*f_val

def f_x(x_val):
  return x_val**3 * (np.log(1+x_val**(-1)) - (1+x_val)**(-1))

def rh(rs,Dh,Dv,rv):
  return rs/x_f(1.0*Dh/Dv*f_x(rs/rv))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

if 1:
  W0,WR,WL = .286,0,1-.286 # Buzzard parameters
else:
  W0,WR,WL = .23,0,.77 # Aardvark parameters

def E_sq(z): 
  return W0*(1+z)**3 + WR*(1+z)**2 + WL

def Wm(z):
  return W0*(1+z)**3 / E_sq(z)

from cosmology import Wm as cosmo_Wm
def x_Dv(z,params=None):
  if params==None:
    return Wm(z)-1
  else:
    return cosmo_Wm(z,params)-1

def Dc(z,params=None): # Delta_vir
  """
  calculate virial Delta at a given redshift for a given cosmology
  i.e.: overdensity of virialized region as cf critical density
  using original form from Bryan and Norman 1997
  DOI:10.1086/305262 URL:arxiv.org/abs/astro-ph/9710107
  """
  return 18*np.pi**2 + 82*x_Dv(z,params) - 39*x_Dv(z,params)**2
  # NOTE: ranges from 110 to 125 over z=[.1,.3]
  #       ranges from 100 to 170 over z=[0.,2.]
  #       So it's always larger than R200 in our case
