#!/bin/python
# read in datasets, stack up colors

########################################################################
# imports 

from matplotlib import pyplot as plt
import galaxy_id as gid
import pandas as pd
import numpy as np

########################################################################
# import most massive haloes
f = 'massive_subset.csv'
dfh = pd.read_csv(f) # halo dataset
# HID1,HID2,STR1,STR2,LAMBDA,z,CENTRAL_FLAG,CENTRAL_BCG_FLAG,RA,DEC,LAMBDA_ERR,M1,M2,Lx,Tx,Fx
hid_list = np.array(dfh.HID1.values,dtype=int)
z_list = np.array(dfh.z.values)

from matplotlib import cm
colormap = 'coolwarm'
cw = cm.get_cmap(colormap)
mini,maxi=min(z_list),max(z_list)
colors = cw((z_list-mini)/(maxi-mini))

########################################################################
# iterate through each halo

MAX = 10000 # maximum count
for ii,HID in enumerate(hid_list[:MAX]):
  print "Processing halo %i/%i" % (ii+1,min(MAX,len(hid_list)))
  hdl = gid.halo_data_list(HID)
  df = gid.members_in_R20(HID,hdl)
  dv = gid.vet(df,hdl)
  del(df)
  
  g,r,i,z,Y = gid.get_grizY(dv)
  if 0: 
    col = gid.RGB_from_gri(dv)
    plt.scatter(i,g-r,5,c=col,edgecolors='k',linewidths=.5)
  else:
    plt.scatter(i,g-r,15,c=colors[ii],linewidths=0,alpha=.25)
  
  del(dv)

plt.xlabel(r'$i$'); plt.ylabel(r'$g-r$')
plt.show()


print "~fin"
