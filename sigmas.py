import numpy as np
from scipy.special import erf,erfinv

one_sigma = sixty_eight = erf(1/np.sqrt(2))
two_sigma = ninety_five = erf(2/np.sqrt(2))
three_sig = ninety_nine = erf(3/np.sqrt(2))

def sigval(N_sig):
  N_sig = np.array(N_sig)
  report = erf(N_sig/np.sqrt(2))
  mask_plus = N_sig < 1
  mask_minus = N_sig < -1
  if hasattr(N_sig,'__len__'):
    report[mask_plus] = .5 + (one_sigma-.5)*N_sig[mask_plus]
    report[mask_minus] = 1 - erf(-N_sig[mask_minus]/np.sqrt(2))
  elif mask_minus:
    report = 1 - erf(-N_sig/np.sqrt(2))
  elif mask_plus:
    report = .5 + (one_sigma-.5)*N_sig
  return report


def signum(q_sig): 
  """ 
  input sigma quantile (e.g. .68) and return # of sigma away (e.g. 1)
  """ 
  report = np.sqrt(2)*erfinv(q_sig)
  mask_plus = q_sig < one_sigma
  mask_minus = q_sig < 1 - one_sigma
  if hasattr(q_sig,'__len__'):
    report[mask_plus] = (q_sig[mask_plus]-.5)/(one_sigma-.5)
    report[mask_minus] = -np.sqrt(2) * erfinv(1-q_sig[mask_minus])
  elif mask_minus:
    report = -np.sqrt(2) * erfinv(1-q_sig)
  elif mask_plus:
    report = (q_sig-.5)/(one_sigma-.5)
  return report


if __name__=='__main__':
  from matplotlib import pyplot as plt
  inpt = one_sigma + (np.random.random(100)-.5)/5.
  inpt = np.random.random(10**4)
  out1 = signum(inpt)
  out2 = sigval(out1)
  err = out2-inpt
  plt.scatter(inpt,err,1)
  plt.ylim(min(err),max(err))
  plt.show()
