########################################################################
# m*(z) function fitting
########################################################################
import numpy as np

RykoffEtAl2014 = {
  "p1":[22.44,3.36,.273,-0.0618,-0.0227], # z<=.5 coefficients
  "p2":[22.94,3.08,-11.22,-27.11,-18.02], # z>.5 coefficients
  "lims":[.05,.5,.7], # lowest,transition,highest bounds for piecewise
  "doi":"10.1088/0004-637X/785/2/104", # equation 9
  "url":"https://iopscience.iop.org/article/10.1088/0004-637X/785/2/104/pdf"
}


def _m_star_val(z,low=True,params=RykoffEtAl2014):
  " evaluate m* value for either high or low case "
  p = params["p1"] if low else params["p2"]
  lnz = np.log(z)
  return p[0]+p[1]*lnz+p[2]*lnz**2+p[3]*lnz**3+p[4]*lnz**4


Z_extrap_warn = "WARNING: Extrapolating redshift! [m_star_model]"

def m_star(z,params=RykoffEtAl2014,warnings=True):
  " evaluate m* for full redshift range (low + high ranges) "
  z = np.array(z)
  lims = params['lims']
  low = (z <= lims[1])
  if hasattr(z,'__len__') and len(z)>1:
    report = np.zeros(np.shape(z))
    report[low] = _m_star_val(z[low],True,params)
    report[~low] = _m_star_val(z[~low],False,params)
    if warnings and np.any(z<lims[0]) or np.any(z>lims[2]): 
      print(Z_extrap_warn)
    return report
  else: # only one entry
    if low:
      if warnings and z<lims[0]: print(Z_extrap_warn)
      return _m_star_val(z,True,params)
    else:
      if warnings and z>lims[2]: print(Z_extrap_warn)
      return _m_star_val(z,False,params)


def L_star_shift(cutoff=0.2):
  " from definition of magnitude "
  return -2.5*np.log10(cutoff)
  # where 0.4 -> 0.9949
  #   and 0.2 -> 1.7474


def m_star_cutoff(z,params=RykoffEtAl2014,warnings=True,cutoff=0.4):
  " return m_i cutoff, based on some shift from L*, e.g. L > 0.2 L_* "
  return m_star(z,params,warnings) + L_star_shift(cutoff)


########################################################################
# legacy / unused things

RykoffEtAl2012 = {
  "p1":[12.27,62.36,-289.79,729.69,-709.42],
  "lims":[.05,.35],
  "doi":"10.1088/0004-637X/746/2/178",
  "url":"https://arxiv.org/abs/1104.2089"
}

def old_m_star_val(z,params=RykoffEtAl2012):
  return np.poly1d(np.flip(params['p1']))(z)


WKB202101 = { # broader fit, lower accuracy
  "p1":[21.00,2.222,1.954], # offset, log slope, linear slope
  "lims":[0,2.35], # beyond this, error > .5 mag
  "note":"This has delta <~ .2 for z|[.05,2.2)",
  "author":"William Kevin Black, Jan 2021"
}

def wkb_m_star(z):
  return 21.30858611-0.31089378 + 2.2218146 * np.log(z) + 1.95441435 * z

def wkb_m_star_val(z,params=WKB202101,warnings=True):
  z = np.array(z)
  if warnings and np.any(z>params['lims'][-1]):
    print(Z_extrap_warn)
  return params['p1'][0] + params['p1'][1]*np.log(z) + params['p1'][2]*z


H17_fit = {
  "ln_slope":2.3440992,
  "p1":[-8.98095121,47.76700857,-104.4070308,117.06548976,
        -70.05793056,22.40154193,-3.28320484,0.11919918+21.57836241],
  "lims":[.04,1.2], # beyond this, it's extrapolating
  "url":"academic.oup.com/mnras/article/467/4/4015/2939812",
  "doi":"10.1093/mnras/stx175",
  "note":"Green line of Figure 3, m_i fitting"
  # certainly differs from R14, by nearly whole mag near z=.7
}

def H17_m_star_val(z,params=H17_fit,warnings=True):
  z = np.array(z)
  if warnings and (np.any(z<params['lims'][0]) 
                or np.any(z>params['lims'][-1])):
    print(Z_extrap_warn)
  return params['ln_slope']*np.log(z) + np.poly1d(params['p1'])(z)



