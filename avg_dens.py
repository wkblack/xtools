from astropy.io import fits
import galaxy_id as gid
import config as cfg
import pandas as pd
import numpy as np

# redshift bins
Delta_z = .025
z_min,z_max = .1,.3
z_vals = np.arange(z_min,z_max,Delta_z)
avg_gal_count = np.zeros((len(z_vals),2))

# read in the data
# sim = cfg.Aardvark
sim = cfg.Buzzard
gf = sim['galaxies'] # [0:2]

for jj,fname in enumerate(gf):
  print "Analyzing galaxy file %i/%i." % (jj+1,len(gf))
  data=fits.open(fname)[1].data
  
  # calculate count in bins
  for i,z in enumerate(z_vals):
    dv = data[(data.Z>z)*(data.Z<z+Delta_z)]
    # vet by 0.2L_* in i-band:
    dv = gid.vet_by_m_star(dv,fits=True)
    avg_gal_count[i,0] += len(dv)
    
    # get ES0 mask to select red galaxies
    z_avg = z + .5*Delta_z
    mask_red = gid.ES0_mask(dv,{"Z":z_avg},fits=True)
    avg_gal_count[i,1] += sum(mask_red)
    
    # calculate volume?
    pass

# TODO: divide by total buzzard sky area (in deg^2)
# from pixel_analysis import buzzard_sky
# TODO: divide by volume of each region? 
# check that the entire sky was read in, in making this calculation. 
# avg_gal_count /= float(buzzard_sky) # galaxies per sq deg

fname = 'rho_Dz.csv'
if 1:
  # save as pandas df
  df = pd.DataFrame({'z':z_vals,'N_gal':avg_gal_count[:,0],
                                'N_red':avg_gal_count[:,1]})
  df.to_csv(fname,index=False)
print "saved %s" % fname

for i in range(len(z_vals)): 
  fR_i = 1.0 * avg_gal_count[i,1] / avg_gal_count[i,0]
  print "z=%#.3g : N_gal=%g/sq deg, N_red=%g/sq deg \t f_R=%g" % (z_vals[i],avg_gal_count[i,0],
                                          avg_gal_count[i,1],fR_i)

# these could be taken as # per solid angle (of a galaxy file), 
# which can then be used per pix to calculate \rho/\bar\rho
