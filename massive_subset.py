#!/bin/python


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# create a subset of the most massive N clusters of output_bicycle.py 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

import pandas as pd
outname = 'massive_subset.csv'

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
from numpy import logical_and as np_and

def make_subset(N=45): # this includes a high-S1, low-lambda, high-Tx system
  # read in the dataset
  df = pd.read_csv('output_bicycle.csv')
  # exclude badly centered haloes 
  df = df[np_and(df.CENTRAL_FLAG,df.CENTRAL_BCG_FLAG)]
  # grab the top N haloes
  df = df.sort_values(by=['M1'],ascending=False)[:N]
  df.set_index('HID1') # doesn't change anything, I don't think...
  # save
  df.to_csv(outname,index=False)
  print "Saved %s" % outname
  return df

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def get_galaxy_subsets(df=pd.read_csv(outname),i=0,imax=1e4,overwriting=False): 
  # create (if not already) galaxy files for the haloes in this sample
  from galaxy_id import members
  from galaxy_id import members_from_sightline as mfs
  from galaxy_id import members_in_R20 as mir
  from numpy import arange
  
  MAX = min(imax,len(df))
  if i>MAX: 
    print "ERROR: Starting value beyond maximum!"
    return -1
  
  for ii in arange(i,MAX): 
    hid=int(df.HID1.values[ii])
    print "Processing Halo ID %i (%i/%i)" % (hid,ii+1,MAX)
    # add in checks for an AttributeError, 
    # to check whether df has PX / HALOPX
    # something like: try: df = ...; df.PX; except AttributeError: then overwrite
    if 0: 
      # members(hid,overwrite=overwriting) # subset of the R20 group!
      mfs(hid,overwrite=overwriting)
    else:
      mir(hid,overwrite=overwriting)
  
  print "Finished processing galaxy subsets." 
  return 0

if __name__=='__main__': 
  df = make_subset(500)
  # get_galaxy_subsets(df,i=14,overwriting=True)
  get_galaxy_subsets(df) # ,i=1,overwriting=True)
  print '~fin'
