#!/bin/python
# input HID
# output Ngal, Ngal_r<R200, and Ngal_r<R200^red (shifted to plot red sequence) 
import pandas as pd

# read in massive_subset.csv
df_m = pd.read_csv('massive_subset.csv')
# ,HID1,HID2,STR1,STR2,LAMBDA,z,CENTRAL_FLAG,CENTRAL_BCG_FLAG,RA,DEC,LAMBDA_ERR,M1,M2,Lx,Tx,Fx

import galaxy_id as gid

for hid in df_m.HID1.values: 
  print '#'*70
  print "Processing Halo ID %i" % hid
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # get values for halo from massive_subset.csv
  ss = df_m[df_m['HID1']==hid] # make subset of input data
  ra,dec,z = ss.RA.values[0], ss.DEC.values[0], ss.z.values[0]
  r200 = gid.read_R200(hid)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # create subsets of the data for that halo 
  
  df = gid.members(hid)
  print 'initial galaxy count: %i' % len(df)
  
  df = gid.vet_by_r(df) # overwrite, since we don't care about total set
  print 'galaxy count after r vet: %i' % len(df)
  
  df = gid.vet_by_radius(df,r200,ra,dec,z)
  print 'galaxy count after R200 vet: %i' % len(df) 
  
  # gid.joint_plot(df)
  
  wkb_params = {
    "slope":-.075,
    "intercept":-.035
  }
  gid.joint_plot(df,gmr_shift=True,redshift=z,params=wkb_params)
  
  # df_r200_red = gid.vet_by_gmr(df,.5)
  # Ngal_r200_red = len(df_r200_red)
  
  
  
  
  
  
  
  # raise SystemExit

