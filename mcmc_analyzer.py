#!/bin/python
########################################################################
# combine mcmc individual outputs
########################################################################

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# imports

# grab numbers from the file
from re import findall
numstr = "[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?"

# grab all files matching in_fname
from glob import glob

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# create output file  

out_fname = 'mcmc_result_combined.dat'
f_out = open(out_fname,"w+")
f_out.write("# floor, alpha, alpha_std, beta, beta_std, sigma, sigma_std\n")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# plot each file

in_fname = 'mcmc_result_log10fxfloor*.txt'
files = glob(in_fname) 
for f in files: 
  with open(f) as f_in: 
    [base,floor]=findall(numstr,f_in.next()) 
    [alpha,alpha_std]=findall(numstr,f_in.next())
    [beta,beta_std]=findall(numstr,f_in.next())
    [sigma,sigma_std]=findall(numstr,f_in.next())
    dat = [floor,alpha,alpha_std,beta,beta_std,sigma,sigma_std]
    # f_out.write(["%s, " % d for d in dat]+"\n") 
    f_out.write(", ".join(dat)+'\n')

f_out.close() 
print "%s created" % out_fname
