#!/bin/python
########################################################################

from astropy.io import fits # to import data
from astropy.table import Table
import numpy as np
import numpy.random as npr
import config as cfg

########################################################################
# load in subsky file to strip

fdir = cfg.skymaps + 'halos/'
fname = 'ily_775.fit'

print "loading..."
hdulist = fits.open(fdir+fname)
namelist = hdulist[1].columns.names
hdulist1data = hdulist[1].data
data = np.array(hdulist1data[0:1])

# print namelist

########################################################################
# Fill file with data
########################################################################
# NOTE: only *500 inputs are read by XTRA

#			# names: 
data[0][0] = 1    	# HALOID
data[0][1] = 0    	# RA
data[0][2] = 0    	# DEC
if 0: 
  data[0][5] *= 10	# M500
  data[0][6] *= 10	# R500
  data[0][7] = 0.01	# Z
  outname="Virgo"
elif 0: 
  data[0][5] = 1e13	# M500
  data[0][6] = 1.40	# R500
  data[0][7] = 0.3	# Z
  outname="single2"
elif 1: 
  data[0][5] = 2e14	# M500
  data[0][6] = 0.7	# R500
  data[0][7] = 0.3	# Z
  outname="single3"

data[0][8] = 0.0	# RA_OLD
data[0][9] = 0.0	# DEC_OLD
#               	# N19

########################################################################
t = Table(data,names=namelist)
t.write('%s.fit' % outname,format='fits') 
print 'written!'

