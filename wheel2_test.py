#!/bin/python
########################################################################
# testing whether Arya's file has the right formats... 
########################################################################

########################################################################
# imports 

from time import time
from sys import stdout #import flush # to overwrite lines

print "Importing libraries...\r",
stdout.flush(); start = time() 

from os.path import isfile
from astropy.io import fits # to import data
import numpy as np
from astropy.io import fits as pyfits
from glob import glob

end = time() 
print "Importing libraries took %g seconds" % (end-start) 
########################################################################

########################################################################
# files
import config as cfg

from config import Aardvark as sim
halo_path=sim['halo_dir']+sim['halo_fname']

matchingdir=cfg.xtools
matchingfname='RM_training_fixed.csv'
matching_path=matchingdir+matchingfname

assert isfile(matching_path),"Error with matching path"

########################################################################
# load in the matching dir, look at the first element's halo (print M1)

fields = (0,1,12,13,16,17,20,21) # (C_ID,H_ID,lam,z,ra,dec,M1,M2)
matching_data = np.loadtxt(matching_path,delimiter=',',
                           skiprows=1,usecols=fields)

line=matching_data[1] # grab the first line
hid=line[1]
lam=line[2]
m1=line[6]
print "HID:",hid
print "lam:",lam
print "M1:",m1

########################################################################
# find that halo in the catalog, print its mass

# FIXME: THIS IS THE SLOWEST WAY IN THE WORLD TO DO IT
fields=['HALOID', 'M200', 'R200', 'M200B', 'R200B', 'M500', 'R500', 'MVIR', 'RVIR', 'M2500', 'R2500', 'VRMS', 'RS', 'JX', 'JY', 'JZ', 'SPIN', 'HALOPX', 'HALOPY', 'HALOPZ', 'HALOVX', 'HALOVY', 'HALOVZ', 'LUMTOT', 'LUM20', 'LBCG', 'RA', 'DEC', 'Z', 'NGALS', 'N18', 'N19', 'N20', 'N21', 'N22', 'EDGE', 'HOST_HALOID', 'XOFF', 'VOFF', 'LAMBDA', 'B_TO_A', 'C_TO_A', 'AX', 'AY', 'AZ', 'VIRIAL_RATIO']
desired_fields = ["ID","NAME","RA",'DEC','Z_LAMBDA','Z_LAMBDA_ERR', \
          'LAMBDA','LAMBDA_ERR','S','Z_SPEC','OBJID']
# certainly need: LAMBDA, Z_LAMBDA    (n_data=len(z))
for fname in glob(halo_path):
  try: 
    h = pyfits.open(fname)[1].data[::]
  except IOError: 
    print "Error reading %s" % iFname
    print "Skipping..."
    continue 
  # it's open
  mask = h['HALOID']==hid # does that need to be converted? 
  h = h[mask]
  if len(h)>0: 
    print fields[7],':',h[0][7]

########################################################################
# see if they're comparable consistently! 
