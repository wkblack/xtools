#!/bin/python
# Goal: create m_*(z) based off of minimum i-band magnitude m_i
# looking at the RM galaxy catalogues

########################################################################
# imports 

import numpy as np
import pandas as pd
from astropy.table import Table
from config import Aardvark as sim
rm_clusters = sim['rm_dir']+sim['rm_clusters']
rm_galaxies = sim['rm_dir']+sim['rm_members']

from matplotlib import rcParams
rcParams['lines.markersize'] = 2
from matplotlib import pyplot as plt

from galaxy_id import m_star
from galaxy_id import RykoffEtAl2012 as R12

########################################################################
# load datasets

# ['MEM_MATCH_ID', 'RA', 'DEC', 'MODEL_MAG', 'MODEL_MAGERR', 'IMAG', 'IMAG_ERR', 'ZRED', 'ZRED_E', 'ZRED_CHISQ', 
#  'BCG_SPEC_Z', 'Z_SPEC_INIT', 'Z_INIT', 'Z', 'LAMBDA_CHISQ', 'LAMBDA_CHISQ_E', 'LAMBDA_ZRED', 'LAMBDA_ZRED_E', 'R_LAMBDA', 'SCALEVAL', 
#  'MASKFRAC', 'C_LAMBDA', 'C_LAMBDA_ERR', 'MAG_LAMBDA_ERR', 'CHISQ', 'Z_LAMBDA', 'Z_LAMBDA_E', 'Z_LAMBDA_NITER', 'EBV_MEAN', 'LNLAMLIKE', 
#  'LNBCGLIKE', 'LNLIKE', 'PZBINS', 'PZ', 'NCROSS', 'RMASK', 'RA_ORIG', 'DEC_ORIG', 'W', 'DLAMBDA_DZ', 
#  'DLAMBDA_DZ2', 'DLAMBDAVAR_DZ', 'DLAMBDAVAR_DZ2', 'LAMBDA_CHISQ_C', 'LAMBDA_CHISQ_CE', 'NCENT', 'NCENT_GOOD', 'RA_CENT', 'DEC_CENT', 'ID_CENT', 
#  'LAMBDA_CHISQ_CENT', 'ZLAMBDA_CENT', 'P_BCG', 'P_CEN', 'Q_CEN', 'P_FG', 'Q_MISS', 'P_SAT', 'P_C', 'BCG_ILUM', 
#  'ILUM', 'Z_LAMBDA_RAW', 'Z_LAMBDA_E_RAW', 'LIM_EXPTIME', 'LIM_LIMMAG', 'LIM_LIMMAG_HARD']
cols=['MEM_MATCH_ID','IMAG','IMAG_ERR','Z','Z_LAMBDA']
df_c = Table.read(rm_clusters, format='fits')[cols].to_pandas()
mask = np.logical_and(.1<df_c.Z_LAMBDA.values,df_c.Z_LAMBDA.values<.3)
dv_c = df_c[mask]
del mask

# ['MEM_MATCH_ID', 'Z', 'RA', 'DEC', 'R', 'P', 'PFREE', 'THETA_I', 'THETA_R', 'MODEL_MAG', 
#  'MODEL_MAGERR', 'IMAG', 'IMAG_ERR', 'ZRED', 'ZRED_E', 'ZRED2', 'ZRED2_E', 'CHISQ', 'ZSPEC', 'ID']
cols=['MEM_MATCH_ID','Z','IMAG','IMAG_ERR','ID','ZRED','ZRED2'] # zred seems much more varied...
df_g = Table.read(rm_galaxies, format='fits')[cols].to_pandas()
t = Table.read(rm_galaxies,format='fits')['MODEL_MAG']

# grab colors
cols=['TMAG_u','TMAG_g','TMAG_r','TMAG_i','TMAG_z'] #,'TMAG_Y']
df_g_mag = pd.DataFrame(data=t.data.byteswap().newbyteorder(),columns=cols)

# mask by redshifts
mask = np.logical_and(.1<df_g.Z.values,df_g.Z.values<.3)
dv_g = df_g[mask]; dv_g_mag = df_g_mag[mask];
del mask

if 0: # DEBUG
  # compare IMAG to MODEL_MAG_I
  print "plotting..."
  diff = dv_g.IMAG-dv_g_mag.TMAG_i
  plt.scatter(dv_g.Z,diff)
  plt.ylim(min(diff),max(diff))
  plt.show()
  print "fin~"
  raise SystemExit

########################################################################
# iteratively find minimums

MAX_ITER=3000 # TODO: expand to all

id_list = dv_c.MEM_MATCH_ID.values[:MAX_ITER]
z_list = dv_c.Z_LAMBDA.values[:MAX_ITER]
L=len(z_list)
min_i,max_i = np.zeros(L),np.zeros(L)
min_z,max_z = np.zeros(L),np.zeros(L)

for ii,ID in enumerate(id_list): # for each cluster (ID)
  if ii>MAX_ITER: 
    print "breaking out of for loop early"
    break # TODO: rm this
  
  # isolate member galaxies
  mask = dv_g.MEM_MATCH_ID==ID
  dv_ii = dv_g[mask]
  dv_zz = dv_g_mag[mask]
  
  # find the minimum m_i and m_z
  # min_i[ii],max_i[ii] = min(dv_ii.IMAG),max(dv_ii.IMAG)
  min_i[ii],max_i[ii] = min(dv_zz.TMAG_i),max(dv_zz.TMAG_i)
  min_z[ii],max_z[ii] = min(dv_zz.TMAG_z),max(dv_zz.TMAG_z)
  
  del dv_ii,dv_zz

if 1: # plot up data
  if 1: 
    plt.scatter(z_list,min_i,label=r'$\min(m_i)$',linewidth=0,alpha=.75)
    plt.scatter(z_list,max_i,label=r'$\max(m_i)$',linewidth=0,alpha=.75)
    plt.ylabel(r'$m_i$')
  else: 
    plt.scatter(z_list,min_z,label=r'$\min(m_z)$',linewidth=0,alpha=.75)
    plt.scatter(z_list,max_z,label=r'$\max(m_z)$',linewidth=0,alpha=.75)
    plt.ylabel(r'$m_z$')
  plt.xlabel(r'Redshift $z$')
  
  zz = np.linspace(.1,.3,100)
  ll = r'$m_*(z)+1.75 \rightarrow L_{{\rm cut}} = 0.2 L_*$'
  plt.plot(zz,m_star(zz,R12)+1.75,'k',label=ll,linewidth=2)
  
  plt.legend()
  plt.show()
  
  if 0: # compare
    err = (m_star(z_list,R12)+1.75-max_i)/m_star(z_list,R12)
    plt.plot([.1,.3],[0,0],'k')
    plt.scatter(z_list,err)
    plt.xlabel(r'Redshift $z$')
    plt.ylabel(r'relative difference')
    plt.show()

# save data?
print "~fin"
