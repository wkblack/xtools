########################################################################
# comparison of mass--richness scatter between the two methods
from kllr import kllr_model
from glob import glob
import h5py
import numpy as np

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# plotting imports 
from matplotlib import pyplot as plt
mpl_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
pm = np.array([+1,-1])
mp = -pm
golden_ratio = (1+np.sqrt(5))/2.
golden_aspect = 5*np.array([golden_ratio,1])
plt.rcParams['figure.figsize'] = golden_aspect
plt.rcParams['font.size'] = 15

lbl_mu = r'$\mu$'
lbl_M = r'Mass ($M_{\rm vir} / {\rm M}_{\odot}$)'
lbl_ell_diff = r'$\ell_{\rm GM}-\ell_{\rm RD}$'
lbl_x = r'$r/r_{\rm vir}$'
lbl_f = [r'$f_{\rm GM}$',r'$f_{\rm RD}$']
labels = ['GMM+','RD']

lbl_sig = r'\sigma_{ \ell_{\rm %s} | \mu  }'
lbl_sigrel = r'$%s \, / \, %s$' % ((lbl_sig%labels[1]),(lbl_sig%labels[0]))


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# sigmas for quantile analysis
from scipy.special import erf
sv3 = [erf(ii/np.sqrt(2)) for ii in range(1,1+3)]
one_sigma = sixty_eight = sv3[0]
two_sigma = ninety_five = sv3[1]
three_sig = ninety_nine = sv3[2]
# combine sigma values: [-2,-1,0,+1,+2]
sv = sigma_vals = [1-two_sigma,1-one_sigma,.5,one_sigma,two_sigma]

########################################################################
# import
fname_RD = 'out_voxel_0000000_to_0160000.h5'
fname_GMMp = sorted(glob('gmm_voxel_0000000_to_*.h5'))[0]

f_RD,f_GM = [h5py.File(fname,'r') for fname in [fname_RD,fname_GMMp]]
# ['Ngal', 'Nred', 'Z', 'mu', 'radial', 'rvir', 'vals']
N_max = min(f_RD.attrs['N'],f_GM.attrs['N'])
vars_RD = [f_RD[key][:N_max] for key in f_RD.keys()]
vars_GM = [f_GM[key][:N_max] for key in f_GM.keys()]

Z_RD,Z_GM = [vrs[2] for vrs in [vars_RD,vars_GM]]
assert(np.all(Z_GM==Z_RD))
Z = np.copy(Z_RD)

Z_range = [.05,.75]
Z_range = [.1,.7]

mask_Z = (Z_range[0] < Z) * (Z < Z_range[1])
Z = Z[mask_Z]

fR_RD,fR_GM = [vrs[1][mask_Z]/vrs[0][mask_Z] for vrs in [vars_RD,vars_GM]]
l_RD,l_GM = [np.log10(vrs[1][mask_Z]) for vrs in [vars_RD,vars_GM]]

mu_RD,mu_GM = [vrs[3][mask_Z] for vrs in [vars_RD,vars_GM]]
assert(np.all(mu_GM==mu_RD))
mu = np.copy(mu_RD)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# radial imports

from voxel import x_midpts # for plotting
rad_G_RD,rad_G_GM = [vrs[4][mask_Z,:,0] for vrs in [vars_RD,vars_GM]]
rad_R_RD,rad_R_GM = [vrs[4][mask_Z,:,1] for vrs in [vars_RD,vars_GM]]

N_gal_RD,N_gal_GM = np.sum(rad_G_RD,axis=0),np.sum(rad_G_GM,axis=0)
N_red_RD,N_red_GM = np.sum(rad_R_RD,axis=0),np.sum(rad_R_GM,axis=0)

import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)

fR_radial_RD,fR_radial_GM = N_red_RD/N_gal_RD, N_red_GM/N_gal_GM
fR_radial_err_RD,fR_radial_err_GM = fR_radial_RD/np.sqrt(N_gal_RD), fR_radial_GM/np.sqrt(N_gal_GM)

fR_radial_RD_min = fR_radial_RD * (1-1/np.sqrt(N_red_RD)) / (1+1/np.sqrt(N_gal_RD))
fR_radial_RD_min = np.clip(fR_radial_RD_min,0,1)
fR_radial_RD_max = fR_radial_RD * (1+1/np.sqrt(N_red_RD)) / (1-1/np.sqrt(N_gal_RD))
fR_radial_RD_max = np.clip(fR_radial_RD_max,0,1)

fR_radial_GM_min = fR_radial_GM * (1-1/np.sqrt(N_red_GM)) / (1+1/np.sqrt(N_gal_GM))
fR_radial_GM_min = np.clip(fR_radial_GM_min,0,1)
fR_radial_GM_max = fR_radial_GM * (1+1/np.sqrt(N_red_GM)) / (1-1/np.sqrt(N_gal_GM))
fR_radial_GM_max = np.clip(fR_radial_GM_max,0,1)

warnings.filterwarnings("default", category=RuntimeWarning)


########################################################################
# begin plotting

def plot_l_mu(kw=.1,nb=32):
  " plot scatter in mass--richness relation "
  lm = kllr_model(kernel_type='gaussian', kernel_width = kw)
  
  for mu,l,lbl in zip([mu_GM,mu_RD],[l_GM,l_RD],['GMM+','RD']):
    xrange, yrange_mean, intercept, slope, scatter = lm.fit(mu, l, nbins=nb)
    # plt.subplot(221)
    # plt.title('scatter')
    plt.plot(xrange,scatter,label=lbl)
    # plt.subplot(222)
    # plt.title('ymean')
    # plt.plot(xrange,yrange_mean,label=lbl)
    # plt.subplot(223)
    # plt.title('icept')
    # plt.plot(xrange,intercept,label=lbl)
    # plt.subplot(224)
    # plt.title('slope')
    # plt.plot(xrange,slope,label=lbl)
  
  plt.xlabel(lbl_mu)
  plt.ylabel(r'$\sigma_{ \ell | \mu  }$')
  plt.legend()
  plt.tight_layout()
  plt.show()


def plot_sigrel_mu(kw=.1,nb=64,saving=True):
  " plot relative scatters in mass--richness relation "
  lm = kllr_model(kernel_type='gaussian', kernel_width = kw)
  xr, _, _, _, sig_GM = lm.fit(mu, l_GM, nbins=nb)
  xr, _, _, _, sig_RD = lm.fit(mu, l_RD, nbins=nb)
  
  plt.plot(xr,np.ones(np.shape(xr)))
  plt.plot(xr,sig_RD/sig_GM)
  plt.xlabel(lbl_mu)
  plt.xlim(min(xr),max(xr))
  plt.ylabel(lbl_sigrel)
  plt.tight_layout()
  if saving:
    fname_out = 'cf__sigrel_mu.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


def plot_sigrel_mu2(kw=.1,saving=True,printing=False):
  " plot relative scatters in mass--richness relation wth bootstrap err"
  K_all, K_mn, K_err = KLLR_bootstrap(mu,l_GM,nbins=100,kernel_width=kw,printing=printing)
  xr,_,_,_,sig_GM = K_all # KLLR 'truth', sampling all points
  _,_,_,_,sigm_GM = K_mn # mean from bootstrap
  _,_,_,_,sige_GM = K_err # st dev from bootstrap
  lo_GM,hi_GM = sigm_GM - sige_GM, sigm_GM + sige_GM
  
  K_all, K_mn, K_err = KLLR_bootstrap(mu,l_RD,nbins=100,kernel_width=kw,printing=printing)
  xr,_,_,_,sig_RD = K_all # KLLR 'truth', sampling all points
  _,_,_,_,sigm_RD = K_mn # mean from bootstrap
  _,_,_,_,sige_RD = K_err # st dev from bootstrap
  lo_RD,hi_RD = sigm_RD - sige_RD, sigm_RD + sige_RD
  
  N = get_counts(mu,xr,kw) # pseudocount of points
  
  plt.plot(10**xr,np.ones(np.shape(xr)),color=mpl_colors[0])
  plt.fill_between(10**xr,lo_GM/sig_GM,hi_GM/sig_GM,
                   alpha=.125,lw=0,color=mpl_colors[0])
  plt.plot(10**xr,sig_RD/sig_GM,color=mpl_colors[1])
  plt.fill_between(10**xr,lo_RD/sig_GM,hi_RD/sig_GM,
                   alpha=.125,lw=0,color=mpl_colors[1])
  # set up axes
  plt.xlabel(lbl_M)
  plt.xlim(10**min(xr),10**max(xr))
  plt.xscale('log')
  plt.ylabel(lbl_sigrel)
  plt.tight_layout()
  if saving:
    fname_out = 'cf__sigrel_mu2.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()



def plot_diff_fR():
  """ 
  plot histogram of differences in red fraction definition 
  along with a plot to discern any mass trend
  """
  nbp = plt.hist(l_GM-l_RD,100,log=True)
  plt.xlabel(lbl_ell_diff)
  plt.ylabel(r'Count')
  plt.tight_layout()
  plt.show()
  
  # see if there's a mass trend
  plt.scatter(mu_GM[l_GM>1],10**(l_GM-l_RD)[l_GM>1],2,alpha=64/256,linewidth=0)
  plt.xlabel(lbl_mu)
  plt.ylabel(r'$\lambda_{\rm GM}/\lambda_{\rm RD}$')
  plt.yscale('log')
  plt.tight_layout()
  plt.show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def plot_fR_muZ():
  " plot red fraction over mass and redshift "
  for xvar,xlbl in zip([mu,Z],[lbl_mu,'Redshift']):
    lm = kllr_model(kernel_type = 'gaussian', kernel_width = 0.005)
    for f_R,lbl in zip([fR_GM,fR_RD],[r'$f_{\rm GM}$',r'$f_{\rm RD}$']):
      xrange, yrange_mean, intercept, slope, scatter = lm.fit(xvar, f_R, nbins=32)
      plt.plot(xrange,yrange_mean,label=lbl)
      plt.fill_between(xrange,yrange_mean-scatter,yrange_mean+scatter,alpha=.125,linewidth=0)
    
    plt.xlabel(xlbl)
    plt.ylabel(r'$f_R$')
    plt.tight_layout()
    plt.legend()
    plt.show()


def plot_fR_Z(mu_lim=15):
  """
  plot red fraction as a function of redshift
  look at only mu>mu_lim to cf Butcher--Oemler Effect
  """ 
  lm = kllr_model(kernel_type = 'gaussian', kernel_width = 0.05)
  for f_R,lbl in zip([fR_GM,fR_RD],[r'$f_{\rm GM}$',r'$f_{\rm RD}$']):
    xrange, yrange_mean, intercept, slope, scatter = lm.fit(Z[mu>mu_lim], f_R[mu>mu_lim], nbins=32)
    plt.plot(xrange,yrange_mean,label=lbl)
    plt.fill_between(xrange,yrange_mean-scatter,yrange_mean+scatter,alpha=.125,linewidth=0)
  
  plt.title(r'$\mu > %0.3g$' % mu_lim)
  plt.xlabel('Redshift')
  plt.ylabel(r'$f_R$')
  plt.tight_layout()
  plt.legend()
  plt.show()


def plot_fR_Z_segmented(Z_split=.54, nb=32, saving=True):
  # set up bin divisions
  nb_L = np.floor(nb * (Z_split-Z_range[0])/(Z_range[1]-Z_range[0])).astype(int)
  nb_R = nb - nb_L
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # plot up GMM+, segmented at Z_split
  lm = kllr_model(kernel_type = 'gaussian', kernel_width = 0.05)
  # left side
  xrange, yrange_mean, intercept, slope, scatter = lm.fit(\
      Z[Z<=Z_split], fR_GM[Z<=Z_split], nbins=nb_L)
  plt.plot(xrange,yrange_mean,label=labels[0],color=mpl_colors[0])
  plt.fill_between(xrange,yrange_mean-scatter,yrange_mean+scatter,
                   alpha=.125,linewidth=0,color=mpl_colors[0])
  # right side
  xrange, yrange_mean, intercept, slope, scatter = lm.fit(\
      Z[Z>Z_split], fR_GM[Z>Z_split], nbins=nb_R)
  plt.plot(xrange,yrange_mean,color=mpl_colors[0])
  plt.fill_between(xrange,yrange_mean-scatter,yrange_mean+scatter,
                   alpha=.125,linewidth=0,color=mpl_colors[0])
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # plot up Red Dragon, unsegmented
  xrange,yrange_mean,intercept,slope,scatter = lm.fit(Z,fR_RD,nbins=32)
  plt.plot(xrange,yrange_mean,label=labels[1],color=mpl_colors[1])
  plt.fill_between(xrange,yrange_mean-scatter,yrange_mean+scatter,
                   alpha=.125,linewidth=0,color=mpl_colors[1])
  
  plt.xlabel('Redshift')
  plt.xlim(Z_range)
  plt.ylabel(r'$f_R$')
  plt.tight_layout()
  plt.legend()
  if saving:
    fname_out = 'cf__fR_Z.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()

from voxel import get_counts

def plot_fR_mu(saving=True):
  " plot red fraction over mass "
  kw = 0.05
  lm = kllr_model(kernel_type = 'gaussian', kernel_width = kw)
  for ii,f_R,lbl in zip([0,1],[fR_GM,fR_RD],labels):
    xr, yr, icpt, slope, sig = lm.fit(mu, f_R, nbins=100)
    N = get_counts(mu,xr,kw)
    mN = mask_N = (N >= 10) # trust stats
    plt.plot(xr[mN],yr[mN],label=lbl,color=mpl_colors[ii])
    plt.fill_between(xr[mN],(yr-sig)[mN],(yr+sig)[mN],
                     alpha=.125,linewidth=0,color=mpl_colors[ii])
    mm = mask_mu = (mu >= max(xr[mN]))
    plt.scatter(mu[mm],f_R[mm],10,color=mpl_colors[ii])
  
  plt.xlabel(lbl_mu)
  plt.xlim(min(xr),max(xr))
  plt.ylabel(r'$f_R$')
  plt.tight_layout()
  plt.legend()
  if saving:
    fname_out = 'cf__fR_mu.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


from voxel import KLLR_bootstrap

def plot_fR_mu2(saving=True):
  " plot red fraction over mass "
  kw = 0.05
  
  for ii,f_R,lbl in zip([0,1],[fR_GM,fR_RD],labels):
    mx = mu[mu>xrng[0]]
    f_x = f_R[mu>xrng[0]]
    # run bootstrap KLLR
    K_all,K_mn,K_err = KLLR_bootstrap(mx,f_x,nbins=100,kernel_width=kw)
    xr,yrm,icpt,sl,sig = K_all # KLLR 'truth', sampling all points
    xrm,yrmm,icptm,slm,sigm = K_mn # mean from bootstrap
    xre,yrme,icpte,sle,sige = K_err # st dev from bootstrap
    N = get_counts(mx,xr,kw) # pseudocount of points
    
    # plot results
    mN = mask_N = (N >= 2) # valid scatter
    plt.plot(xr[mN],yrm[mN],label=lbl,color=mpl_colors[ii])
    lo,hi = yrmm - yrme, yrmm + yrme
    plt.fill_between(xr[mN],lo[mN],hi[mN],
                     alpha=.125,linewidth=0,color=mpl_colors[ii])
    lo,hi = yrmm - sigm, yrmm + sigm
    plt.fill_between(xr[mN],lo[mN],hi[mN],
                     alpha=.125,linewidth=0,color=mpl_colors[ii])
    mm = mask_mu = (mx >= max(xr[mN]))
    if sum(mask_mu) > 0:
      plt.scatter(mx[mm],f_x[mm],10,color=mpl_colors[ii])
  
  plt.xlabel(lbl_mu)
  plt.xlim(min(xr),max(xr))
  plt.ylabel(r'$f_R$')
  plt.tight_layout()
  plt.legend()
  if saving:
    fname_out = 'cf__fR_mu2.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


def plot_fR_x(n_sig=5,even_spread=False,saving=True):
  for f_R,sig_fR,fR_min,fR_max,lbl in zip(
        [fR_radial_GM,fR_radial_RD],
        [fR_radial_err_GM,fR_radial_err_RD],
        [fR_radial_GM_min,fR_radial_RD_min],
        [fR_radial_GM_max,fR_radial_RD_max],
        labels):
    plt.plot(x_midpts,f_R,label=lbl)
    if even_spread: # use even spread, from stdev
      plt.fill_between(x_midpts,f_R-n_sig*sig_fR,f_R+n_sig*sig_fR,
                       alpha=.125,linewidth=0)
    else: # use uneven spread +/- from Poisson noise more directly
      plt.fill_between(x_midpts,fR_min,fR_max,alpha=.125,lw=0)
  
  plt.xlabel(lbl_x)
  plt.xlim(1e-3,10)
  plt.xscale('log')
  plt.ylabel(r'$f_R$')
  plt.ylim(0,1)
  plt.tight_layout()
  plt.legend()
  if saving:
    fname_out = 'cf__fR_x.pdf'
    plt.savefig(fname_out,format='pdf',dpi=1000)
    print(f"Saved {fname_out}")
  plt.show()


if __name__=='__main__':
  print('-- in main --')
  # plot_fR_muZ() # two at once
  # plot_fR_Z(15) # implement mass limit
  # plot_l_mu() # plot sigma_{ell|mu} for both
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # saving: 
  # plot_sigrel_mu()
  plot_sigrel_mu2(printing=True)
  # plot_fR_Z_segmented()
  # plot_fR_mu()
  # plot_fR_mu2()
  # plot_fR_x()
  print('~fin')
