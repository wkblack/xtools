# quick file to check out which halo indices the galaxy files would match up to... 

########################################################################
# grab all indices for galaxy files
import numpy as np

# choose which simulation to use
if 1:
  print "Using Buzzard"
  from config import Buzzard as sim
else:
  print "Using Aardvark"
  from config import Aardvark as sim

from glob import glob
from os.path import splitext
from swapper import convert_index, NSIDE_gals, NSIDE_halos
from os.path import isfile

########################################################################
# grab all galaxy files
########################################################################
galaxy_names = sim["glxy_dir"]+sim["glxy_fname"]
galaxy_files = sorted(glob(galaxy_names))
assert(len(galaxy_files)==204) # check it's the right number

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# turn those filenames into healpix indices
gal_indices = [int(splitext(fname)[0].split('.')[-1]) 
               for fname in galaxy_files]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# convert to halo indices
matching_halo_indices = convert_index(NSIDE_gals,NSIDE_halos,gal_indices,
                             nest_in=sim["glxy_nest"],
                             nest_out=sim["halo_nest"])

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# check whether the files exist
template = sim["halo_dir"]+sim["halo_ftemp"]
halo_names = [template % i for i in matching_halo_indices]
if 0: # print out desired halo files
  print "halo files we're looing for:"
  for ff in set(halo_names):
    print ff
  print "\n"
exists = [isfile(fname) for fname in halo_names]
print "Halo files exist for %i/%i galaxies." % (sum(exists),len(exists))

########################################################################
# grab all halo files
########################################################################
halo_names = sim["halo_dir"]+sim["halo_fname"]
halo_files = sorted(glob(halo_names))
assert(len(halo_files)==15) # check it's the right number

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# turn those filenames into healpix indices
halo_indices = [int(splitext(fname)[0].split('.')[-1]) 
               for fname in halo_files]

########################################################################
# plot up the difference

from matplotlib import pyplot as plt
if 1:
  plt.subplot(122)
  plt.scatter(gal_indices,matching_halo_indices,c=exists)
  plt.colorbar()
  plt.xlabel('galaxy indices')
  plt.ylabel('halo indices')
  
  plt.subplot(131)
  nbins=max(matching_halo_indices)-min(matching_halo_indices)+1
  plt.hist(matching_halo_indices,bins=nbins*3,orientation='horizontal')
  
  plt.show()
else:
  import healpy as hp
  halo_tot = np.arange(12*NSIDE_halos**2)
  halo_mask = np.array([idx in halo_indices for idx in halo_tot])
  hp.mollview(halo_mask,nest=sim["halo_nest"])
  plt.title('existing halo files')
  plt.show() 
  
  gal_tot = np.arange(12*NSIDE_gals**2)
  gal_mask = np.array([idx in gal_indices for idx in gal_tot])
  hp.mollview(gal_mask,nest=sim["glxy_nest"])
  plt.title('existing galaxy files')
  plt.show() 
