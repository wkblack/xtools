#!/sw/lsa/centos7/python-anaconda2/created-20170424/bin/python

########################################################################
# file: galaxy_id.py 
# author: W.K.Black
# creation date: June 2018

# Description: 
# uses halo ID (HID) to grab a set of galaxies from the galaxy decks
# includes fxns to look at galaxy colors: plot g-r vs r, plot on the sky

# TODO: 
# count of galaxies, also by color
########################################################################

# files imported through config file:
import config as cfg

from os.path import isfile # to validate

# details: http://www.slac.stanford.edu/~risa/des_bcc/bcc_v1.0_tags.html
# ['ID', 'INDEX', 'ECATID', 'COEFFS', 'TMAG', 'OMAG', 'FLUX', 'IVAR', 'OMAGERR', 'AMAG', 
#  'RA', 'DEC', 'Z', 'HALOID', 'RHALO', 'M200', 'NGALS', 'R200', 'CENTRAL', 'TRA', 
#  'TDEC', 'EPSILON', 'GAMMA1', 'GAMMA2', 'KAPPA', 'MU', 'LMAG', 'MAG_U', 'SIZE', 'ARBORZ', 
#  'ARBORZ_ERR', 'ANNZ', 'ANNZ_ERR', 'PHOTOZ_GAUSSIAN', 'PX', 'PY', 'PZ', 'VX', 'VY', 'VZ', 
#  'PSTAR', 'PQSO', 'TE', 'TSIZE']

########################################################################
# global imports 
########################################################################

from time import time
from sys import stdout #import flush # to overwrite lines
from astropy.table import Table
from astropy.io import fits
from glob import glob
import pandas as pd
import numpy as np

from matplotlib import pyplot as plt

import cosmology as cosmo

from r_conversion import r_Delta

########################################################################
# test values:
########################################################################

test_HID = 12997483
ra,dec = 16.8374,1.20001

test_HID2 = 14069242 # the richest halo

test_HID3 = 11807174
r200_HID3 = 0.211587

test_HID4 = 39234917
r200_HID4 = 0.52453661

lowlam1 = 3752539
lowlam2 = 12460028

HID_list = [11922429,13128015,7111109,14069242,11521337,11655342]
# from most massive haloes

lowlam_list = [6300629,3752539,4422655,7112155,6839502,\
               12190930,12995851,8314236,12459543]


########################################################################
# read in R200 from halo files for a given HID
########################################################################

def read_R200(HID,sim=cfg.Aardvark): # a bit deprecated---it's part of halo_data
  # read in R200 from halo truth catalogs for a given HID
  # (would be a fine time to return other things, like {RA,DEC})
  for f in sim['halos']:
    dat = fits.open(f)[1].data
    if HID not in dat['HALOID']: continue
    return dat[dat['HALOID']==HID]['R200'][0]
  # else: 
  return -1

########################################################################
# get halo data in general 

def halo_data(HID): 
  assert(type(HID) in {int,np.int64,np.integer})
  # read in halo ID, spew out info on that halo {z,RA,DEC,R200}
  for f in sim['halos']:
    try:
      dat = fits.open(f)[1].data
      dat = dat[dat['HALOID']==HID][0]
      return dat['Z'],dat['RA'],dat['DEC'],dat['R200'],dat['Rs']
      # NOTE: Rs needed for rescaling R200 to R_Delta (for any Delta)
    except IndexError:
      continue
  print "ERROR: Halo ID %i not contained in halo datasets!" % HID
  raise SystemExit

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def halo_data_list(HID,sim=cfg.Aardvark,halo_data=None): 
  assert(type(HID) in {int,np.int64,np.integer})
  # read in halo ID, spew out an hdl on that halo
  if halo_data is None:
    assert(len(sim['halos'])>0)
    for f in sim['halos']:
      try:
        dat = fits.open(f)[1].data
        return dat[dat['HALOID']==HID][0]
      except IndexError:
        continue
    print "ERROR: Halo ID %i not contained in halo datasets!" % HID
    raise SystemExit
  else:
    # make decisions based on datatype of halo_data
    if type(halo_data)==fits.fitsrec.FITS_rec:
      hdl = {}
      if 'CENTRAL' in halo_data.names: # so galaxies can be input
        mask = np.logical_and(halo_data.HALOID==HID,
                              halo_data.CENTRAL==1)
      else:
        mask = halo_data.HALOID==HID
      if sum(mask)==0: # no matches
        return None
      for name in halo_data.names:
        hdl[name]=halo_data[mask][name][0]
    elif type(halo_data)==np.ndarray:
      hdl = {}
      if 'CENTRAL' in halo_data.dtype.names:
        mask = np.logical_and(halo_data['HALOID']==HID,
                              halo_data['CENTRAL']==1)
      else:
        mask = halo_data['HALOID']==HID
      if sum(mask)==0: # no matches
        return None
      for name in halo_data.dtype.names:
        hdl[name]=halo_data[mask][name][0]
    elif type(halo_data)==pd.core.frame.DataFrame:
      hdl = halo_data[halo_data.HALOID.values==HID]
      if len(hdl)==0: return None
      hdl = hdl.to_dict('records')[0]
    # make sure PX and HALOPX are both extant
    for coord in {'X','Y','Z'}:
      hdl['HALOP%s' % coord] = hdl['P%s' % coord]
    return hdl

# Halo parameters in hdl: 
# 'HALOID','M200','R200','M200B','R200B','M500','R500','MVIR','RVIR','M2500',
# 'R2500','VRMS','RS','JX','JY','JZ','SPIN','HALOPX','HALOPY', 'HALOPZ',
# 'HALOVX','HALOVY','HALOVZ','LUMTOT','LUM20','LBCG','RA','DEC','Z','NGALS',
# 'N18','N19','N20','N21','N22','EDGE','HOST_HALOID','XOFF','VOFF','LAMBDA',
# 'B_TO_A','C_TO_A','AX','AY','AZ','VIRIAL_RATIO'

########################################################################
# parameters for red sequence
########################################################################

HaoEtAl2010 = { 
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # slope of [g-r vs i] relation vs redshift z
  # redshift evolution given as slopes and intercepts 
  # for red sequence {zero point, width, and slope}
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  
  # zero point
  "m_zp":3.049, "m_zp_sigma":.011,
  "b_zp":0.623, "b_zp_sigma":.002,
  # width of g-r ridgeline
  "m_w":.136, "m_w_sigma":.010,
  "b_w":.037, "b_w_sigma":.002,
  # g-r vs i slope
  "m_gmr":-.075, "m_gmr_sigma":.005,
  "b_gmr":-.003, "b_gmr_sigma":.001,
  # lower and upper redshift limits for interpolation
  "z_limits":[.1,.3], # used for extrapolation warnings
  'i_max_A':np.exp(3.1638),
  'i_max_k':0.1428,
  "url":"https://arxiv.org/pdf/0907.4383.pdf" # figs 7 and 9
}

def limi(z,params=HaoEtAl2010):
  # from github.com/jgbrainstorm/pyjh/blob/master/pyhao/cmvlimi.py
  # the i_max value Jiangang used (private correspondance)
  return params['i_max_A']*z**params['i_max_k']
  # TODO: add availablity to use m*(z)+0.4 from DES

def gmr_line(i,z,params=HaoEtAl2010):
  # returns approximate red sequence ridgeline and width
  # check if outside redshift range or if too faint:
  if np.any(z<params['z_limits'][0]) \
  or np.any(z>params['z_limits'][1]):
    print "Warning: Extrapolating redshift! [galaxy_id.gmr_line]"
  return np.outer((params["m_gmr"]*z+params["b_gmr"]),(i-limi(z,params))) \
       + np.outer((params["m_zp"]*z+params["b_zp"]),np.ones(np.shape(i))), \
         np.array(params["m_w"]*z+params["b_w"])

def ES0_mask(df,hdl,params=HaoEtAl2010,fits=False):
  zh = hdl['Z'] # it should be relative to the halo's redshift
  g,r,i,z,Y = get_grizY(df,fits)
  return ES0_mask_explicit(g,r,i,zh,params)
  # gmr,wth = gmr_line(i,zh,params)
  # mask_under = (g-r)>(gmr[0]-2*wth);
  # mask_above = (g-r)<(gmr[0]+2*wth);
  # return np.logical_and(mask_under,mask_above)

def ES0_mask_explicit(g,r,i,zh,params=HaoEtAl2010):
  gmr,wth = gmr_line(i,zh,params)
  mask_under = (g-r)>(gmr[0]-2*wth);
  mask_above = (g-r)<(gmr[0]+2*wth);
  return np.logical_and(mask_under,mask_above)

def select_ES0(df,hdl,params=HaoEtAl2010,fits=False):
  return df[ES0_mask(df,hdl,params,fits)]

########################################################################
# vet galaxies
########################################################################
# NOTE: moved from angular to real space! Doing an angular vet misses 
#       line-of-sight galaxies that are within theta200 but without r200

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def true_dist(df,hdl): # equivalent to RHALO if HID matches
  # returns distance between galaxy and host halo
  """ near identical to RHALO for HID matches (within ~1e-4)
      relative error is well under one percent (besides the bcg) """
  # df.P[XYZ], hdl['HALOP[XYZ]'], and df.RHALO are all in comoving Mpc/h
  try:
    return np.sqrt((df['PX']-hdl['HALOPX'])**2 \
                 + (df['PY']-hdl['HALOPY'])**2 \
                 + (df['PZ']-hdl['HALOPZ'])**2) # cMpc/h
  except KeyError: # in case hdl comes form df[CENTRAL==1]
    return np.sqrt((df['PX']-hdl['PX'].values)**2 \
                 + (df['PY']-hdl['PY'].values)**2 \
                 + (df['PZ']-hdl['PZ'].values)**2) # cMpc/h

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# vet galaxies without R200 for given sky coordinates

def vet_by_radius(df,radius=999): # RHALO=1000 if HALOID=-1
  # input vetting radius should be physical Mpc/h
  return df[(df.RHALO/(1+df.Z))<radius]

def vet_by_true_radius(df,hdl,radius): # units must be physical Mpc/h
  return df[(true_dist(df,hdl)/(1+hdl['Z']))<radius]

def vet_by_delta(df,hdl=None,delta=200): 
  if hdl != None: 
    r200,Rs = hdl['R200'],hdl['Rs']
  else: 
    z,ra,dec,r200,Rs = halo_data(get_HID(df))
  
  return vet_by_radius(df,radius=r_Delta(delta,Rs/1000.,r200))

h = cosmo.aardvark['H0']/100.

def cf_angular(df,hdl=None,power=-1): 
  if hdl==None:
    HID = get_HID(df)
    z,ra,dec,r200,Rs = halo_data(HID)
  else:
    HID = hdl['HALOID']
    z,ra,dec,r200 = hdl['Z'],hdl['RA'],hdl['DEC'],hdl['R200']
  
  # compare angular distances to physical distances
  # plot radius for each galaxy vs RHALO
  df = vet_by_radius(df,r200) # maybe hardcode the cut...
  if 1: # ERROR: this isn't the right way!! 
    theta = np.sqrt((df.RA-ra)**2+(df.DEC-dec)**2)
    r = cosmo.from_degrees(theta,z)*h # Mpc/h, physical
  else: # use 3d space!
    r = cosmo.dx_from_delta_theta(df.RA,df.DEC,df.Z,ra,dec,z)
  r_gal = df.RHALO*(1+z)**power # Mpc/h, physical
  plt.scatter(r,r_gal,c=df.Z) 
  MAX = max(max(r),max(r_gal))
  plt.plot([0,MAX],[0,MAX],c='k')
  plt.xlabel(r'$\Delta\Theta$ (Mpc/h, physical)')
  plt.ylabel(r'RHALO (Mpc/h, physical)')
  plt.show() 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# m* function from Rykoff &al. 2012, equation 12
# url=https://iopscience.iop.org/article/10.1088/0004-637X/746/2/178

RykoffEtAl2012 = {
  "p":[12.27,62.36,-289.79,729.69,-709.42], # coefficients
  "limits":[.05,.35], # lower and upper, respectively
  "scale":"linear",
  "doi":"10.1088/0004-637X/746/2/178",
  "url":"https://iopscience.iop.org/article/10.1088/0004-637X/746/2/178"
}

RykoffEtAl2014 = {
  "p":[22.44,3.36,.273,-0.0618,-0.0227], # coefficients
  "limits":[.05,.5], # lower and upper, respectively
  # NOTE: Technically, the method is piecewise, extending to .7
  #       but since this dataset only goes to .3, why bother? 
  "scale":"logarithmic",
  "doi":"10.1088/0004-637X/785/2/104",
  "url":"https://iopscience.iop.org/article/10.1088/0004-637X/785/2/104/pdf"
}
# NOTE: for z in (0.05,0.35), the relative error between these two
#       methods is <= 0.2%, so the two are essentially identical.
#       The 2014 method seems to extrapolate better, so it's default.

def L_star_shift(cutoff):
  # from definition of magnitude
  return -2.5*np.log10(cutoff)
  # where 0.4 -> 0.9949
  #   and 0.2 -> 1.7474

def m_star(z=0,params=RykoffEtAl2014): 
  if np.any(z<params['limits'][0]) or np.any(z>params['limits'][1]): 
    print "Warning: Extrapolating!"
  p=params["p"]
  if params["scale"]=='linear': 
    return p[0] + p[1]*z + p[2]*z**2 + p[3]*z**3 + p[4]*z**4
  elif params["scale"]=='logarithmic': 
    return p[0] + p[1]*np.log(z)    + p[2]*np.log(z)**2 \
                + p[3]*np.log(z)**3 + p[4]*np.log(z)**4
  else: 
    print "ERROR: invalid parameter set!"
    raise SystemExit

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# vet by m* limit (0.2 L_* in i-band)

def vet_by_m_star(df,hdl=None,fits=False,L_star_frac=0.2):
  if fits:
    # return df[df.TMAG[:,2] < (m_star(df.Z)+L_star_shift(L_star_frac))]
    return df[df['TMAG'][:,2] < (m_star(df['Z'])+L_star_shift(L_star_frac))]
  else:
    return df[df.TMAG_i < (m_star(df.Z)+L_star_shift(L_star_frac))]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# vet galaxies dimmer than m_r=r_limit 

def vet_by_r(df,r_limit=23): 
  return df[df.TMAG_r<r_limit]


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# vet galaxies by all of the above

def vet(df,hdl=None,delta=200): 
  if hdl != None: 
    r200,Rs = hdl['R200'],hdl['Rs']
  else: 
    z,ra,dec,r200,Rs = halo_data(get_HID(df))
  
  df = vet_by_true_radius(df,hdl,r_Delta(delta,Rs/1000.,r200))
  # df = vet_by_radius(df,r_Delta(delta,Rs/1000.,r200))
  df = vet_by_m_star(df,hdl)
  return df

########################################################################
# plot galaxies + shifted galaxies
########################################################################

def shift_gmr(i,gmr,redshift=0.2,params=HaoEtAl2010): 
  # Note: 0.2 is the apx center of Hao's sample
  rss = params['b_gmr'] + params['m_gmr']*redshift # red seq. slope
  return gmr-i*rss

def get_gmr(df,params=HaoEtAl2010,fits=False): 
  g,r,i,z,Y = get_grizY(df,fits)
  return shift_gmr(i,g-r,0.2,params)

def plot_shifted(df,hdl=None,params=HaoEtAl2010,fits=False):
  if hdl==None:
    HID = get_HID(df)
  else:
    HID = hdl['HALOID']
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # triptych: g-r v r, shifted g-r v r
  g,r,i,z,Y = get_grizY(df,fits)
  colors = RGB_from_gri(df)
  
  fig,(ax1,ax2,ax3) = plt.subplots(nrows=1, ncols=3)
  fig.suptitle("Halo ID: %i" % HID)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # g-r plot
  ax1.scatter(r,g-r,c=colors,edgecolors='k',linewidths=.5)
  ax1.set_xlabel(r'$i$'); ax1.set_ylabel(r'$g-r$')
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # histograms
  ax2.hist(g-r,50,orientation='horizontal') # np.arange(-2,+2,.1)
  ax2.set_xlabel(r'$N$'); # ax2.set_ylabel(r'$g-r$'); 
  
  gmr = shift_gmr(i,g-r,redshift=0.2,params=params)
  
  ax3.hist(gmr,50,orientation='horizontal') # np.arange(-2,+2,.1)
  ax3.set_xlabel(r'$N$'); # ax2.set_ylabel(r'$g-r$'); 
  ax3.set_title('Shifted') 
  
  if 0: # unify y-axes and strip second 
    ax_min,ax_max = min(0,min(g-r)),max(1.75,max(g-r))
    ax1.set_ylim(ax_min,ax_max); ax2.set_ylim(ax_min,ax_max)
    ax2.get_yaxis().set_visible(False)
  
  # plt.subplots_adjust(top=0.925,bottom=0.1,left=0.085,right=0.99,
                      # hspace=0.01,wspace=0.01)
  plt.show()


def quaternary(df,hdl=None,redshift=0.2,params=HaoEtAl2010,fits=False):
  if hdl==None:
    HID = get_HID(df)
  else:
    HID = hdl['HALOID']
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # quaternary: g-r v i and shifted g-r vs i and their histograms
  g,r,i,z,Y = get_grizY(df,fits)
  colors = RGB_from_gri(df)
  
  fig,((ax1,ax2),(ax3,ax4)) = plt.subplots(nrows=2, ncols=2)
  fig.suptitle("Halo ID: %i" % HID)
  
  gmr = shift_gmr(i,g-r,redshift,params)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # g-r plot
  ax1.scatter(i,g-r,c=colors,edgecolors='k',linewidths=.5)
  ax1.set_xlabel(r'$i$'); ax1.set_ylabel(r'$g-r$')
  
  ax3.scatter(i,gmr,c=colors,edgecolors='k',linewidths=.5)
  ax3.set_xlabel(r'$i$'); ax3.set_ylabel(r'$g-r$ shifted')
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # histograms
  ax2.hist(g-r,50,orientation='horizontal') # np.arange(-2,+2,.1)
  ax2.set_xlabel(r'$N$'); # ax2.set_ylabel(r'$g-r$'); 
  
  ax4.hist(gmr,50,orientation='horizontal') # np.arange(-2,+2,.1)
  ax4.set_xlabel(r'$N$'); # ax2.set_ylabel(r'$g-r$'); 
  # ax4.set_title('Shifted') 
  
  # plt.subplots_adjust(top=0.925,bottom=0.1,left=0.085,right=0.99,
  # plt.tight_layout()
  plt.show()

########################################################################
# grab galaxies for a given HID
########################################################################

cols_glxy = ['ID','RA','DEC','TRA','TDEC','Z','HALOID','RHALO',
             'CENTRAL','M200','R200','PX','PY','PZ','VX','VY','VZ']
# NOTE: RA and DEC observed (.'. lensed) while TRA and TDEC are true
# NOTE: RHALO is in comoving Mpc/h
cols_tmag = ['TMAG_g','TMAG_r','TMAG_i','TMAG_z','TMAG_Y']

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# merge two dictionaries together
def merge_dicts(d1,d2):
  dd = d1.copy()
  dd.update(d2)
  return dd

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# turn fits data into pandas dataframe, splitting TMAG into own columns

def fits_to_pandas(data):
  # create dictionary
  dy1=dict(zip(cols_glxy,\
           [data[col].byteswap().newbyteorder() \
           for col in cols_glxy]))
  dy2=dict(zip(cols_tmag,\
           [data['TMAG'][:,i].byteswap().newbyteorder() \
           for i in range(len(cols_tmag))]))
  d_merge = merge_dicts(dy1,dy2)
  
  # create dataframe
  return pd.DataFrame(d_merge)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# grab members matching HID
def members(HID,overwrite=False,sim=cfg.Aardvark): # encapsulated by members_in_R20
  # reads in HID
  # outputs galaxy list (pandas)
  
  # check if already exists
  fname = sim['snips'] + cfg.fname_galaxy_snips % HID
  if isfile(fname) and not overwrite: 
    return pd.read_csv(fname)
  
  # else, create the file from scratch
  
  # start with a blank DataFrame to be filled
  df_sum = pd.DataFrame(columns=cols_glxy+cols_tmag)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # iterate over each galaxy cluster list
  # if it's got the right HID, append to output list, 
  # including the splayed out grizY
  
  if len(sim['galaxies'])<1:
    print "ERROR: simulation has no galaxies!"
    raise SystemExit
  
  for ii,f in enumerate(sim['galaxies']): # read each file for processing
    if HID not in fits.open(f)[1].data['HALOID']: 
      print "\033[K" + "Skipping %i/%i\r" % (ii+1,len(sim['galaxies'])),
      stdout.flush()
      continue
    else: # halo is contained, and should be processed.
      print "Processing galaxy file %i/%i\r" % (ii+1,len(sim['galaxies'])),
      stdout.flush()
      pass
    
    df = (Table.read(f, format='fits')[cols_glxy]).to_pandas() 
    d2 = Table.read(f, format='fits')[['TMAG']]
    
    for i in range(len(cols_tmag)): 
      df[cols_tmag[i]] = np.array(d2['TMAG'][:,i])
    
    mask = df.HALOID.values==HID
    df_sum = df_sum.append(df[mask],ignore_index=True)
    
    print "\033[K" + "Galaxy file %i/%i contained %i galaxies." \
          % (ii+1,len(sim['galaxies']),sum(mask))
  
  # saves for future use (since it's a long process)
  df_sum.to_csv(fname,index=False)
  print "Saved %s" % fname
  
  # return compiled list of galaxy members for given halo
  return df_sum 


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def members_from_sightline(HID,hdl=None,overwrite=False,sim=cfg.Aardvark):
  # return solid angle = THETA200 (deg) on the sky, based on halo #HID
  
  # check if file already exists. If it does, return the dataset. 
  fname = sim['snips'] + cfg.fname_halo_sightline % HID
  if isfile(fname) and not overwrite: 
    return pd.read_csv(fname)
  
  # else the file doesn't exist, and we must look to the stars
  # to find galaxies alongthe sightline of the halo
  if hdl != None: 
    z,ra,dec,r200 = hdl['Z'],hdl['RA'],hdl['DEC'],hdl['R200']
  else: 
    z,ra,dec,r200,Rs = halo_data(HID)
  theta200 = cosmo.to_degrees(r200/h,z)
  theta200_ra = cosmo.ra_radius(dec,theta200)
  
  # start with a blank DataFrame to be filled
  df_sum = pd.DataFrame(columns=cols_glxy+cols_tmag)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # iterate over each galaxy cluster list
  # if it doesn't contain the halo's ra+dec range, skip it
  
  if ra>180: ra-=360
  for ii,f in enumerate(sim['galaxies']): # read each file for processing
    data = fits.open(f)[1].data
    # whittle down the data
    ra_i = data['RA']; ra_i[ra_i>180]-=360; dec_i = data['DEC']
    mask = np.logical_and(np.logical_and(dec-theta200<dec_i,
                                         dec_i<dec+theta200),
                          np.logical_and(ra-theta200_ra<ra_i,
                                         ra_i<ra+theta200_ra))
    del ra_i,dec_i
    if not np.any(mask):
      print "\033[K" + "Skipping %i/%i\r" % (ii+1,len(sim['galaxies'])),
      stdout.flush()
      continue
    else: # halo is contained, and should be processed.
      print "Processing galaxy file %i/%i\r" % (ii+1,len(sim['galaxies'])),
      stdout.flush()
      pass # keep calm and carry on!
  
    data = data[mask] # grabs relatively local patch
    ra_i = data['RA']; ra_i[ra_i>180]-=360; dec_i = data['DEC']
    # calculate angle theta btw target ra&dec and each galaxy's ra&dec
    Theta = cosmo.degrees_between((ra_i,dec_i),(ra,dec))
    data=data[Theta<theta200] # vet by theta200
    del Theta
    
    # create dictionary and dataframe for current data file
    dy1=dict(zip(cols_glxy,\
             [data[col].byteswap().newbyteorder() \
             for col in cols_glxy]))
    dy2=dict(zip(cols_tmag,\
             [data['TMAG'][:,i].byteswap().newbyteorder() \
             for i in range(len(cols_tmag))]))
    d_merge = merge_dicts(dy1,dy2)
    df = pd.DataFrame(d_merge)
    if len(df)<1:
      print "WARNING: Adding null dataframe!"
    
    # add that vet to the output
    df_sum = df_sum.append(df,ignore_index=True,sort=False)
    
    print "\033[K" + "Galaxy file %i/%i contained %i galaxies." \
          % (ii+1,len(sim['galaxies']),sum(mask))
  
  # saves for future use (since it's a long process)
  df_sum.to_csv(fname,index=False)
  print "Saved %s" % fname
  
  # return total output
  return df_sum

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def members_in_R20(HID,hdl=None,overwrite=False,sim=cfg.Aardvark): 
  # return all members within R20 of a given halo 
  fname = sim['snips'] + cfg.fname_halo_R20 % HID
  if isfile(fname) and not overwrite:
    return pd.read_csv(fname)
  # else: create file
  if hdl == None: hdl = halo_data_list(HID)
  x,y,z = hdl['HALOPX'],hdl['HALOPY'],hdl['HALOPZ']
  r20 = r_Delta(delta=20,Rs=hdl['Rs']/1000.,R_ref=hdl['r200'])
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # start with a blank DataFrame to be filled
  df_sum = pd.DataFrame(columns=cols_glxy+cols_tmag)
  # iterate over each galaxy cluster list
  for ii,f in enumerate(sim['galaxies']): # read each file for processing
    data = fits.open(f)[1].data
    dist = np.sqrt((data['PX']-x)**2 \
                 + (data['PY']-y)**2 \
                 + (data['PZ']-z)**2)
    
    mask = dist<r20
    if not np.any(mask):
      print "\033[K" + "Skipping %i/%i\r" % (ii+1,len(sim['galaxies'])),
      stdout.flush()
      continue
    else: # halo is contained, and should be processed.
      print "Processing galaxy file %i/%i\r" % (ii+1,len(sim['galaxies'])),
      stdout.flush()
      pass # keep calm and carry on!
    data = data[mask]
    
    df = fits_to_pandas(data)
    
    # add that vet to the output
    if len(df)<1:
      print "WARNING: Adding null dataframe!"
    df_sum = df_sum.append(df,ignore_index=True,sort=False)
    
    print "\033[K" + "Galaxy file %i/%i contained %i galaxies." \
          % (ii+1,len(sim['galaxies']),sum(mask))
  
  # saves for future use (since it's a long process)
  df_sum.to_csv(fname,index=False)
  print "Saved %s" % fname
  
  # return total output
  return df_sum

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# grab galaxies in a certain (square) region on the sky

def grab_square((ra,dec),dec_extent=0.3,overwrite=False,sim=cfg.Aardvark): # all inputs in degrees
  # dec_extent is ~radius, so area = (2*d_e)^2
  fname = 'squareRA%gDEC%gR%g.csv' % (ra,dec,dec_extent) # R='radius'
  if isfile(fname) and not overwrite:
    return pd.read_csv(fname)
  # else: create file
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # start with a blank DataFrame to be filled
  df_sum = pd.DataFrame(columns=cols_glxy+cols_tmag)
  # iterate over each galaxy cluster list
  for ii,f in enumerate(sim['galaxies']): # read each file for processing
    data = fits.open(f)[1].data
    mask = cosmo.square_selection((data['RA'],data['DEC']),
                                  (ra,dec),dec_extent) 
    
    if not np.any(mask): # skip empty sets
      print "\033[K" + "Skipping %i/%i\r" % (ii+1,len(sim['galaxies'])),
      stdout.flush()
      continue
    else: 
      print "Processing galaxy file %i/%i\r" % (ii+1,len(sim['galaxies'])),
      stdout.flush()
      pass # keep calm and carry on!
    
    data = data[mask]
    df = fits_to_pandas(data)
    
    # add that vet to the output
    df_sum = df_sum.append(df,ignore_index=True,sort=False)
  
  # saves for future use (since it's a long process)
  df_sum.to_csv(fname,index=False)
  print "Saved %s" % fname
  
  return df_sum # totalled output

########################################################################
# coloring -- get colorschemes based on grizY
########################################################################

def get_grizY(df,fits=False): 
  if fits: #TODO: rewrite so type of file is automatically detected?
    return [df['TMAG'][:,i] for i in range(5)]
  else:
    return df.TMAG_g.values,df.TMAG_r.values,df.TMAG_i.values,\
                  df.TMAG_z.values,df.TMAG_Y.values

def RGB_from_gri(df,saturate=False,fits=False): 
  g,r,i,z,Y = get_grizY(df,fits)
  MIN,MAX = min(min(g),min(r),min(i)),max(max(g),max(r),max(i))
  MAXMIN,MINMAX = max(min(g),min(r),min(i)),min(max(g),max(r),max(i))
  if saturate: 
    # one example: MIN,MAXMIN,MINMAX,MAX = (17, 19, 26, 27)
    DIFF = MINMAX-MIN # clip at the lowest maximum; makes brighter
    new_g,new_r,new_i = (g-MIN)/DIFF,(r-MIN)/DIFF,(i-MIN)/DIFF
    new_g,new_r,new_i = np.clip([new_g,new_r,new_i],0,1)
  else: # most will be red
    DIFF = MAX-MIN
    new_g,new_r,new_i = (g-MIN)/DIFF,(r-MIN)/DIFF,(i-MIN)/DIFF
  return np.array([1-new_i,1-new_r,1-new_g]).T

def RGBA_from_gri(df,saturate=False,fits=False):
  g,r,i,z,Y = get_grizY(df,fits)
  colors = RGB_from_gri(df,saturate,fits)
  tpcy = np.reshape((max(i)-i)/(max(i)-min(i)),(len(i),1))
  cx = np.concatenate((colors,tpcy),axis=1)
  return cx

def RGB_from_gr(df,fits=False): 
  g,r,i,z,Y = get_grizY(df,fits)
  MIN,MAX = min(min(g),min(r)),max(max(g),max(r))
  DIFF = MAX-MIN
  new_r,new_g = (r-MIN)/DIFF,(g-MIN)/DIFF
  return np.array([1-new_r,1-new_g,1-new_g]).T

def RGB_from_gmr(df,fits=False): # TODO: use actual gmr slope
  g,r,i,z,Y = get_grizY(df,fits); gmr = g-r
  MIN,AVG,MAX = np.percentile(gmr,[25,50,75])
  # MIN,MAX = min(gmr),max(gmr); AVG = (MAX-MIN)/2 # np.average(gmr)
  R=np.where(gmr<AVG,0,(gmr-AVG)/(MAX-AVG))
  G=np.where(gmr<AVG,(gmr-MIN)/(AVG-MIN),1-(gmr-AVG)/(MAX-AVG))
  B=np.where(gmr<AVG,1-(gmr-MIN)/(AVG-MIN),0)
  S=g+i+r
  m=((max(S)-S)/(max(S)-min(S)))
  # m=((max(i)-i)/(max(i)-min(i)))
  return np.clip(np.array([m*R,m*G,m*B]).T,0,1)

########################################################################
# create plots for the galaxies 
########################################################################

def get_HID(df): 
  assert(np.all(df.HALOID.values[0]==df.HALOID))
  return df.HALOID.values[0]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# color--color plot: g-r vs r

def color_plot(df,hdl,draw_gmr_line=False,params=HaoEtAl2010,
               color=None,saturate=False,fits=False): 
  # create color--color plot for galaxy group
  g,r,i,z,Y = get_grizY(df,fits)
  if np.all(color==None): color = RGB_from_gri(df,saturate,fits)
  plt.scatter(i,g-r,c=color,edgecolors='k',linewidths=.5)
  plt.xlabel(r'$m_i$'); plt.ylabel(r'$g-r$')
  plt.title('HID=%i' % hdl['HALOID'])
  
  if draw_gmr_line: 
    # draw approximate red sequence line + boundaries
    MIN,MAX = min(i)-.25,max(i)+.125
    i_list = np.linspace(MIN,MAX,10)
    plt.xlim(MIN,MAX)
    
    gmr,wth = gmr_line(i_list,hdl['Z'],params)
    # plot it up
    plt.plot(i_list,gmr[0],alpha=.5,color='k')
    plt.fill_between(i_list,gmr[0]+wth,gmr[0]-wth,
                     alpha=.125,color='r')
    plt.fill_between(i_list,gmr[0]+2*wth,gmr[0]-2*wth,
                     alpha=.125,color='r')
  
  plt.tight_layout()
  plt.show()

def color_plot_i(df,hdl=None,fits=False,**kwargs): 
  # create color--color plot for galaxy group
  g,r,i,z,Y = get_grizY(df,fits)
  colors = RGB_from_gri(df)
  plt.scatter(i,g-r,c=colors,edgecolors='k',linewidths=.5,**kwargs)
  plt.xlabel(r'$m_i$'); plt.ylabel(r'$g-r$')
  if hdl != None: plt.title('HID=%i' % hdl['HALOID'])
  plt.tight_layout()
  plt.show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# plot percentage of galaxies contained vs radius

def galaxy_fraction(df,hdl,physical_units=False,core=False):
  r200=hdl['r200']
  Ngal = len(df)
  x = true_dist(df,hdl)
  if not core: x = x[x!=min(x)]
  if not physical_units: x /= r200
  plt.hist(np.log10(x),50,density=True,histtype='step',cumulative=True,log=True)
  plt.xlabel(r'$\log_{10}(r/r_{200})$')
  plt.ylabel(r'$N/N_{{\rm tot}}$')
  plt.show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# create a plot on the sky (RA vs DEC) for the galaxies 

def min_min(l1,l2): 
  return min(min(l1),min(l2))
def max_max(l1,l2): 
  return max(max(l1),max(l2))

def space_plot(df,hdl=None,halo=False,color='w',saturate=False,
               fits=False,s=7,**kwargs):
  # create sky coordinates plot for galaxy group
  # pixels on black
  if fits:
    ra,dec = df.RA,df.DEC
  else:
    ra,dec = df.TRA.values,df.TDEC.values # true RA and DEC
  fig, ax = plt.subplots(nrows=1, ncols=1)
  ax.set_facecolor('k')
  
  # determine coloring (default: white circles)
  if hasattr(color,"lower") and color.lower() in {'rgb','grizY'}:
    col = RGBA_from_gri(df,saturate,fits)
  else:
    col = color
  
  if hdl != None:
    HID,z,ra_cen,dec_cen,r200 = hdl['HALOID'],hdl['Z'],hdl['RA'],\
                                   hdl['DEC'],hdl['R200']
  elif halo:
    HID = get_HID(df)
    z,ra_cen,dec_cen,r200,Rs = halo_data(HID)
  else:
    HID = None
  
  if halo: 
    # plot halo center and bounds 
    # TODO? plot circle at R200 (and R500?)
    theta200 = cosmo.to_degrees(r200/h,z)
    ra_circ,dec_circ = cosmo.halo_points((ra_cen,dec_cen),theta200)
    ax.plot(ra_circ,dec_circ,'r')
    ax.set_xlim(min_min(ra,ra_circ),max_max(ra,ra_circ))
    ax.set_ylim(min_min(dec,dec_circ),max_max(dec,dec_circ))
    # Make the halo appear (mostly) circular. 
    # Should look alright for |DEC| < 85 or so, and Aa halo set <80 :)
    ax.set_aspect((max(ra_circ)-min(ra_circ))/(max(dec_circ)-min(dec_circ)))
  else: 
    ax.set_xlim(min(ra),max(ra))
    ax.set_ylim(min(dec),max(dec))
    # ax.set_aspect('equal')
  
  ax.scatter(ra,dec,marker='.',c=col,edgecolor='',s=s,cmap='coolwarm',**kwargs)
  ax.set_xlabel('RA'); ax.set_ylabel('DEC')
  if HID!=None: plt.title('HID=%i' % HID)
  
  plt.tight_layout()
  plt.show() 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# make a 3D space plot of the galaxies

def plot_3D(df,hdl=None,s=5,edgecolor=None,c='k',**kwargs):
  from mpl_toolkits.mplot3d import Axes3D
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')
  ax.scatter(df.PX,df.PY,df.PZ,s=s,c=c,edgecolor=edgecolor,**kwargs)
  for a in {'x','y','z'}:
    exec(("ax.set_%slabel(r'$p_%s$ (comoving Mpc/h)')" % (a,a)))
  if hdl!=None:
    hx,hy,hz = hdl['HALOPX'],hdl['HALOPY'],hdl['HALOPZ'] # comoving Mpc/h
    r200 = hdl['R200']*(1+hdl['Z']) # comoving Mpc/h
    for a in {'x','y','z'}:
      exec(("ax.set_%slim(h%s-r200,h%s+r200)" % (a,a,a)))
  ax.set_aspect('equal')
  plt.show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def angular_profile(df,hdl=None,log=True): 
  if hdl==None: hdl = halo_data_list(get_HID(df))
  ra,dec = hdl['ra'],hdl['dec']
  
  theta=cosmo.degrees_between((df.TRA,df.TDEC),(ra,dec))
  if log: 
    plt.hist(np.log10(theta),bins=40,log=True)
    plt.xlabel(r'$\log_{10}(\theta/\theta_{200})$')
  else: 
    plt.hist(theta,bins=40,log=True)
    plt.xlabel(r'$\theta/\theta_{200}$')
  plt.ylabel(r'$N$')
  plt.show()

########################################################################
# define NFW profile

ChildEtAl2018 = { # input mass + z, output c_{200,crit}
  "indiv_all":{"A":75.4,"d":-0.422,"m":-0.089},
  "indiv_rlx":{"A":68.4,"d":-0.347,"m":-0.083},
  "stack_NFW":{"A":57.6,"d":-0.376,"m":-0.078},
  "stack_Ein":{"A":122.,"d":-0.446,"m":-0.101}, # Einasto profile
  "url":"iopscience.iop.org/article/10.3847/1538-4357/aabf95/meta",
  "doi":"10.3847/1538-4357/aabf95"
  # see table 2 and eqn 19
}

def c200c(z,M,params=ChildEtAl2018['stack_NFW']):
  return params['A']*((1.+z)**params['d'])*(M**params['m'])
  # scatter of sigma_c = c200c/3

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def A_NFW(c):
  return np.log(1+c)-c/(1.+c)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def NFW(r,Delta,c_Delta=None,Rs=None,r_Delta=None,M_Delta=None,rho_c=None):
  """
  return NFW profile (relative to rho_crit) for given set of radii r
  must have either {r_Delta or M_Delta} and either {c_Delta or Rs} defined
  """
  
  # can use alternate inputs: [cD or Rs] and [rD or MD]
  if r_Delta==None:
    if M_Delta!=None:
      # convert M_Delta to r_Delta; requires critical density
      r_Delta = (3*M_Delta/(4*np.pi*rho_c*Delta))**(1/3.)
    else:
      print "ERROR: Must either define r_Delta or M_Delta! [gid.NFW]"
      return -1
  
  if c_Delta==None:
    if Rs!=None:
      # convert Rs to c_Delta
      c_Delta = r_Delta/Rs
    else:
      print "ERROR: Must either define c_Delta or Rs! [gid.NFW]"
      return -1
  
  return Delta/(3*A_NFW(c_Delta))/(r/r_Delta)/(1./c_Delta+r/r_Delta)**2

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def radial_profile(df,hdl=None,steps_per_decade=5,core=False,use_r200=True,
                   physical_units=False,plotting=True,bar_plot=False,
                   draw_NFW=False,params=cosmo.aardvark): 
  # creates a radial profile dN/dV for a halo, scaled by r200
  if hdl==None:
    hdl = df[df.CENTRAL==1]
    r200,z = hdl['R200'].values,hdl['Z'].values
  else:
    r200,z = hdl['R200'],hdl['Z']
  
  x=true_dist(df,hdl)/(1+z) # relative distance from halo center
  
  if use_r200: x/=r200 # make x unitless
  if not core: x=x[x!=min(x)]
  
  # set decimal rounded decades to create bins
  x_floor = 10**np.floor(np.log10(min(x)))
  x_ceilg = 10**np.ceil(np.log10(max(x)))
  n_decades = int(np.log10(x_ceilg)-np.log10(x_floor))
  steps = n_decades*steps_per_decade
  X = x_floor*10**(np.arange(steps+1,dtype=float)/steps_per_decade)
  
  # calculate dN/dV (count per bin)
  j=np.arange(steps)
  N = np.array([sum(np.logical_and((X[i]<x),(x<X[i+1]))) for i in j])
  V = np.pi*4/3*(X[j+1]**3-X[j]**3) # unitless ((Mpc/h per R200)^3)
  if physical_units and use_r200: V*= r200**3 # (physical Mpc/h)^3
  dNdV = N/V # count per (physical Mpc/h)^3
  dX = X[j+1]-X[j]
  if plotting: 
    fac=1.1 # plot data limits white space scale
    if bar_plot: 
      plt.bar(X[:-1],dNdV,width=dX,align='edge') # bottom=min(.1,min(dNdV))
    else: 
      xx = X[:-1]+.5*dX
      plt.scatter(xx,dNdV)
      plt.ylim(min(dNdV[dNdV>1e-9])/fac,fac*max(dNdV))
    if physical_units or not use_r200: 
      plt.ylabel(r'$dN/dV$ (Mpc/h)$^{-3}$')
    else:
      plt.ylabel(r'$dN/dV$ per ${R_{200}}^3$')
    if use_r200:
      plt.xlabel(r'$r/r_{200}$'); 
    else:
      plt.xlabel(r'$r$ (Mpc/h)'); 
    plt.xlim(min(X)/fac,max(X)*fac)
    plt.xscale('log'); plt.yscale('log')
    
    if draw_NFW:
      # add line for NFW
      c200_val = c200c(hdl['Z'],hdl['M200'])
      r_vals = np.exp(np.linspace(np.log(min(X)),np.log(max(X)),100))
      nfw = NFW(r_vals,200,c_Delta=c200_val,r_Delta=1)
      # h = params['H0']/100.
      # problem: don't have Msol average...
      # rho_c = cosmo.rho_crit(hdl['Z'],params) # Msol/Mpc^3
      plt.plot(r_vals,rho_c*nfw,'k')
      pass
    
    plt.tight_layout()
    plt.show()
  return (X,dNdV,dX) # so stacked analysis is easier
  # return (X,N,V,dX)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# color histogram

def hist(df,hdl=None,fits=False): 
  if hdl==None:
    HID = get_HID(df)
  else:
    HID = hdl['HALOID']
  
  g,r,i,z,Y = get_grizY(df,fits)
  plt.hist(g-r,bins=np.arange(-2,+2,.01))
  plt.xlabel(r'$g-r$'); plt.ylabel(r'$N$'); 
  plt.title('Halo ID: %i' % HID)
  plt.tight_layout()
  plt.show()
  
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# joint color plot + histogram

def joint_plot(df,hdl=None,gmr_shift=False,redshift=-1,params=HaoEtAl2010,fits=False): 
  g,r,i,z,Y = get_grizY(df,fits)
  colors = RGB_from_gri(df)
  
  fig,(ax1,ax2) = plt.subplots(nrows=1, ncols=2)
  if hdl==None:
    fig.suptitle("Halo ID: %i" % get_HID(df))
  else:
    fig.suptitle("Halo ID: %i" % hdl['HALOID'])
  
  if not gmr_shift: 
    gmr = g-r
  elif gmr_shift and redshift>0: 
    gmr = shift_gmr(i,g-r,redshift,params)
  elif hdl==None: 
    print "WARNING: Accuracy loss without redshift input!"
    gmr = shift_gmr(i,g-r,0.2,params)
  else:
    gmr = shift_gmr(i,g-r,hdl['Z'],params)
  
  ax1.scatter(i,gmr,c=colors,edgecolors='k',linewidths=.5)
  ax1.set_xlabel(r'$m_i$'); ax1.set_ylabel(r'$g-r$')
  
  ax2.hist(gmr,50,orientation='horizontal') # np.arange(-2,+2,.1)
  ax2.set_xlabel(r'$N$'); # ax2.set_ylabel(r'$g-r$'); 
  
  # unify y-axes and strip second 
  # ax_min,ax_max = min(0,min(gmr)),max(1.75,max(gmr))
  ax_min,ax_max = min(gmr),max(gmr)
  ax1.set_ylim(ax_min,ax_max); ax2.set_ylim(ax_min,ax_max)
  ax2.get_yaxis().set_visible(False)
  
  plt.subplots_adjust(top=0.925,bottom=0.1,left=0.085,right=0.99,
                      hspace=0.01,wspace=0.01)
  plt.show()

########################################################################
# main program
########################################################################

if __name__=='__main__': 
  print "running main program"
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # import galaxy members for a given HID
  
  from sys import argv
  if len(argv)>1: # scan in from terminal
    HID = int(argv[1])
  else: # hardcode
    HID = test_HID
    # HID = test_HID2
    # HID = lowlam1
    # HID = 11922594
    # HID = 12190656
    print "using hardcoded HID: %i" % HID
  
  gm = members(HID,overwrite=False)
  print "found %g members" % len(gm)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # vet by radius
  
  if 0: # don't have these values on-hand at the moment
    median_RA,median_DEC = np.median(gm.RA.values),np.median(gm.DEC.values)
    rad = .2 # degrees
    gm = vet_by_radius(gm,rad,median_RA,median_DEC)
  
  # make plots
  # I'll do this in individual files.
  joint_plot(gm)
  
  # save dataset
  # already done by the members() function; 
  # only necessary if vetting in some way. 
  
  print "~fin"
  raise SystemExit

# cool line: 
# if None not in {R200,RA,DEC}: # then all are defined! :)
