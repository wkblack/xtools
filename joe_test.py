# temporary file meant to analyze pixel_analysis output
# and plot up like one of Joe's plots

import numpy as np
import pandas as pd
import config as cfg
import cosmology as cosmo
import pixel_analysis as pa

from matplotlib import pyplot as plt
from astropy.table import Table

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def grab_metadata(fname):
  # grab metadata stored in the filename
  md={"fname":fname}
  nums=cfg.find_floats(fname)
  # store redshift range and unique identifier
  if len(nums)>1:
    assert(len(nums)==3) # otherwise, I need to update this
    md["Z_MIN"]=nums[0]
    md["Z_MAX"]=nums[1]
    md["u"]=int(nums[2])
  else:
    assert(len(nums)==1) # otherwise, I need to update this
    md["Z_MIN"]=.1
    md["Z_MAX"]=1.
    md["u"]=int(nums[0])
  # convert u into NSIDE and index
  md['NSIDE']=pa.u_to_NSIDE(md['u'])
  md['index']=pa.u_to_index(md['u'])
  return md

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# 

def read_in_file(fname):
  df = pd.read_csv(fname)
  md = grab_metadata(fname)
  return df,md

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# sky fraction calculator

from cosmology import sq_deg_in_sky as sds
from cosmology import sq_rad_in_sky as srs

def sky_fraction(NSIDE,degrees=True):
  # equivalent to: healpy.nside2pixarea(NSIDE,degrees=True)
  # returns solid angle for that NSIDE 
  if degrees:
    return sds/(12*NSIDE**2) # sq deg
  else: # radians
    return srs/(12*NSIDE**2) # sq rad

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# join together halo dataset and newly created output

sim = cfg.Buzzard
h = cosmo.buzzard['H0']/100.

def join(df,md):
  # join pixel_analysis output with existing halo data
  halo_index = pa.convert_index(md['NSIDE'],pa.NSIDE_halos,md['index'],
                                nest=sim['nest'])
  halo_fname = sim['halo_dir'] + sim['halo_ftemp'] % halo_index
  df_halo = Table.read(halo_fname, format='fits').to_pandas()
  return pd.merge(df_halo,df,on='HALOID') # the joint dataframe

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# join two halo datas together

def join_datasets(df,md,fname):
  df2,md2 = read_in_file(fname)
  if md['Z_MIN'] != md2['Z_MIN'] or md['Z_MAX'] != md2['Z_MAX']: 
    print "WARNING: z-ranges don't match, so volumes inconsistent!"
  # merge datasets
  df_out = pd.concat([df,df2])
  # update metadata
  md['index'] = np.append(md['index'],md2['index'])
  # TODO: update anything else?
  return df_out,md

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# make histogram-like output, counting input by bins

def histogrammer(data,vmin=None,vmax=None,log=False,steps=10,step=None):
  # take in array-like data and output a histogram-like output, with log input option
  if log:
    if np.any(data<=0): 
      print "WARNING: removing nonpositive datapoints for log"
      data = data[data>0]
  
  if vmin==None: vmin=min(data)
  if vmax==None: vmax=max(data)
  
  if log:
    data=np.log(data)
    vmin=np.log(vmin)
    vmax=np.log(vmax)
  
  if step!=None: # if user defined a step size
    # break up data into some number of steps of size step
    if log: step=np.log(step)
    steps=np.ceil((vmax-vmin)/step).astype(int)
    if steps<1:
      print "ERROR: bounds preclude steps!"
      raise SystemExit
  else:
    step=1.0*(vmax-vmin)/steps
  
  step+=1e-15
  
  bins = [vmin+i*step for i in range(steps+1)]
  counts = [len(data[np.logical_and(bins[i]<=data,data<bins[i+1])]) \
            for i in range(steps)]
  if log:
    return np.exp(bins),np.array(counts)
  else:
    return np.array(bins),np.array(counts)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def plot1(df,md): # n(z)[deg**-2] vs redshift
  if 0:
    N_steps = 63
  else: 
    N_steps = 10
  area = sky_fraction(md['NSIDE']) # TODO: multiply by len(md['index'])
  vals = [5,10,15,20]
  labels = [r'$N_{\rm red}>%g$' % val for val in vals]
  for i in range(len(vals)):
    bins,counts = histogrammer(df.Z[df.Nred>vals[i]],steps=N_steps)
    # TODO: add Z_MIN and Z_MAX to the histogrammer fxn
    avg_bins = (bins[:-1]+bins[1:])/2.
    # TODO: make bar graphs like Joe's?
    plt.plot(avg_bins,counts/area,label=labels[i],marker='o')
    plt.ylabel(r'$N$/[steradian]')
  
  plt.xlabel(r'Redshift $z$')
  plt.legend()
  plt.show()
  return

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def plot2(df,md): # 10**4 n(z)[h^3 Mpc^-3] vs richness
  Omega = sky_fraction(md['NSIDE'],degrees=False) # in square radians
  vol = cosmo.coVol(md['Z_MIN'],md['Z_MAX'],Omega,params=cosmo.buzzard)
  bins,counts = histogrammer(df.Nred,log=True,steps=10)
  avg_bins = (bins[:-1]+bins[1:])/2.
  vals = 1e4*counts/vol/h**3
  plt.scatter(avg_bins,vals)
  plt.xscale('log')
  plt.yscale('log')
  fac=1.1
  plt.xlim(avg_bins[0]/fac,avg_bins[-1]*fac)
  plt.ylim(min(vals)/fac,max(vals)*fac)
  plt.xlabel(r'$N_{\rm red}$')
  plt.ylabel(r'$10^4 n(N_{\rm red})/({\rm Mpc}/h)^3$')
  plt.show()
  return

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
if __name__=='__main__':
  fname = 'HNN_4096.csv'
  hid_list,Ngal,Nred = np.loadtxt("HNN_4097.csv",unpack=True,delimiter=',').T
  df = pd.DataFrame({"HALOID":hid_list, "Ngal":Ngal, "Nred":Nred})
  md = grab_metadata(fname)
  
  df = join(df,md)
  
  print "plot 1"
  plot1(df,md)
  
  print "plot 2"
  plot2(df,md)
  
  print 'fin'
