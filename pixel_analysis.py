#!/bin/python
# methods to analyze pixels of the Aardvark and Buzzard catalogues
# created by W.K.Black, Aug 2019
########################################################################

########################################################################
# imports
########################################################################
import cosmology as cosmo
import galaxy_id as gid
import config as cfg
import healpy as hp
import pandas as pd
import numpy as np
import linmix
import HK02

from matplotlib import pyplot as plt
from r_conversion import r_Delta
from astropy.io import fits

"""
NSIDE=2 has resolution 29.3162 degrees
NSIDE=4 has resolution 14.6581 degrees
NSIDE=8 has resolution 7.32904 degrees
NSIDE=16 has resolution 3.66452 degrees
NSIDE=32 has resolution 1.83226 degrees
NSIDE=64 has resolution 0.91613 degrees
NSIDE=128 has resolution 0.458065 degrees
NSIDE=256 has resolution 0.229032 degrees
"""
NSIDE_MAX=128
# At that resolution, you'd risk having a halo span more than 9 pixels.

########################################################################
# check dataset length, ensure nonzero size, throw error elsewise

def check_len(df):
  if len(df)<1:
    print "ERROR: Empty dataset!"
    raise SystemExit

########################################################################
# pixel methods
########################################################################
NSIDE_gals,NSIDE_halos=8,2 # resolution for DES simulation
buzzard_sky=10313 # square degrees -- quarter of sky
buzzard_sky_str=buzzard_sky/np.degrees(1.)**2 # in steradians (apx pi)

fullsky=4*np.pi*np.degrees(1)**2 # total sky in square degrees

# solid angles for various resolutions
def pix(NSIDE):
  return hp.nside2pixarea(NSIDE,degrees=True) 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# convert between NSIDE
# (see healpy/rotator.py +208 for details)
def convert_index(NSIDE_in,NSIDE_out,index,
                  nest_in=False,nest_out=False,nest=None):
  assert(np.all(np.array(index)>=0)) # else this will have an error.
  if nest==None:
    theta,phi = hp.pix2ang(NSIDE_in,index,nest=nest_in)
    return hp.ang2pix(NSIDE_out,theta,phi,nest=nest_out)
  else: # simpler method, keeping backwards compatability for now
    theta,phi = hp.pix2ang(NSIDE_in,index,nest=nest)
    return hp.ang2pix(NSIDE_out,theta,phi,nest=nest)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# check whether (RA,DEC) in pixel 
def in_pixel(NSIDE,index,(RA,DEC),nest=True):
  indices = hp.ang2pix(NSIDE,RA,DEC,lonlat=True,nest=nest)
  if hasattr(index,'__len__'):
    # check whether it's in any of the indices
    mask = indices==index[0]
    for i in range(1,len(index)):
      mask = np.logical_or(mask,indices==index[i])
    return mask # [ix in index for ix in indices]
  else:
    return index == indices
# Debbuged this; seems to be working. 

def in_pixel_fits(NSIDE,index,data,nest=True,tru=True):
  if tru: # for halos
    return data[in_pixel(NSIDE,index,(data['TRA'],data['TDEC']),nest=nest)]
  else: # for galaxies (use RA,DEC instead of TRA,TDEC)
    return data[in_pixel(NSIDE,index,(data['RA'],data['DEC']),nest=nest)]
# Very probably working, since the above works fine.


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# grab 3 nearest neighbors (ranked)
def nearest_neighbors(NSIDE,pix_i,nest=True): # true for Buzzard
  # input: NSIDE + pixel 
  # output: three closest neighboring pixels on sky
  # NOTE: if NSIDE is too big, the three nearest may not be enough!!
  vec_0 = hp.pix2vec(NSIDE,pix_i,nest=nest)
  all_neighbors = hp.get_all_neighbours(NSIDE,pix,nest=nest,lonlat=True)
  vec_i = hp.pix2vec(NSIDE,all_neighbors,nest=nest)
  angles = hp.rotator.angdist(vec_0,vec_i)
  return all_neighbors[np.argsort(angles)[:3]]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# grab HID from group of NSIDE+pix

def HID_from_pix(NSIDE,pix_i,sim=cfg.Buzzard,z_mask=(.1,1.),printing=False): # TODO? add M-mask
  # take that pixel and convert it up into halo
  halo_indices = convert_index(NSIDE,NSIDE_halos,pix_i,nest=sim['nest'])
                               # use same encoding as the galaxy files
  hid_list = []
  if hasattr(halo_indices,'__len__'):
    halo_indices = set(halo_indices)
    halo_fnames = [sim['halo_dir'] + sim['halo_ftemp'] % idx \
                                               for idx in halo_indices]
    # read in all halo files, create a final set of HID to return
    for hf in halo_fnames: 
      try:
        data = fits.open(hf)[1].data
      except IOError:
        # skipping; file doesn't exist
        continue
      mask = in_pixel(NSIDE,pix_i,(data['TRA'],data['TDEC']),nest=sim['nest'])
      if z_mask != None:
        z_mask_i = np.logical_and(z_mask[0]<data['Z'],data['Z']<z_mask[1])
        mask = np.logical_and(mask,z_mask_i)
        del z_mask_i
      hid_list = np.append(hid_list,data['HALOID'][mask])
      del data,mask
  else:
    halo_fname = sim['halo_dir'] + sim['halo_ftemp'] % halo_indices
    try:
      data = fits.open(halo_fname)[1].data
    except IOError:
      # skipping; file doesn't exist
      if printing: print "File %s DNE!" % halo_fname
      return hid_list
    mask = in_pixel(NSIDE,pix_i,(data['TRA'],data['TDEC']),nest=sim['nest'])
    if z_mask != None:
      z_mask_i = np.logical_and(z_mask[0]<data['Z'],data['Z']<z_mask[1])
      mask = np.logical_and(mask,z_mask_i)
      del z_mask_i
    hid_list = data['HALOID'][mask]
    del data,mask
  return hid_list

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# grab galaxy data

def grab_halo_glxy_data(NSIDE,pix_i,blush=0,sim=cfg.Buzzard,
                        printing=False):
  if blush==0:
    # use default buzzard galaxies
    folder = sim['glxy_dir']
    ftemp = sim['glxy_ftemp']
  else:
    # use the blush factor from input, e.g. f_R=1
    folder = sim['blush_dir']
    ftemp = cfg.fname_galaxy_blush % blush
  fname_gals = folder + ftemp
  # Now read in the galaxy file corresponding to (NSIDE,pix_i)
  # along with that pixel's neighbors, and vet by those nine.
  
  pix_near = hp.get_all_neighbours(NSIDE,pix_i,nest=sim['nest']) # 8 near
  pixels = set(pix_near); pixels.add(pix_i); 
  pixels = np.array(list(pixels)) # 9 here
  pixels = pixels[pixels!=-1] # nix nonexistant neighbors to nix hp err
  gal_ids = convert_index(NSIDE,NSIDE_gals,pixels,nest=sim['nest'])
  halo_ids = convert_index(NSIDE,NSIDE_halos,pixels,nest=sim['nest'])
  f_gals = np.array([sim['glxy_dir'] + sim['glxy_ftemp']  % idx \
                                               for idx in set(gal_ids)])
  f_halos = np.array([sim['halo_dir'] + sim['halo_ftemp'] % idx \
                                               for idx in set(halo_ids)])
  # select only existing files
  f_gals = f_gals[[isfile(f) for f in f_gals]]
  f_halos = f_halos[[isfile(f) for f in f_halos]]
  
  if len(f_gals)<1 or len(f_halos)<1:
    # raise OSError("No halo files exist!")
    print "WARNING: Files DNE! [pixel_analysis.py +%i]" % cfg.lineNo()
    return None,None
  
  # then read in those files into RAM, save as galaxies and halos
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # grab galaxy data
  data_gals = fits.open(f_gals[0])[1].data
  if printing: print "data_gals initially:",len(data_gals)
  # vet to only select gals within the pixels
  data_gals = in_pixel_fits(NSIDE,pixels,data_gals,nest=sim['nest'],tru=False)
  if printing: print "data_gals in pixels:",len(data_gals)
  i=1
  while i<len(f_gals):
    # import other galaxy files and vet by pix_i
    data_i = fits.open(f_gals[i])[1].data
    data_i = in_pixel_fits(NSIDE,pixels,data_i,nest=sim['nest'],tru=False)
    if printing:
      if len(data_i)<1: 
        print "Galaxy file not in neighboring pix."
      else:
        print "Adding galaxy file..."
    data_gals = np.concatenate((data_gals,data_i))
    i+=1
  
  if printing: print "data_gals compiled!"
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # grab halo data
  data_halos = fits.open(f_halos[0])[1].data
  if printing: print "data_halos initially:",len(data_halos)
  data_halos = in_pixel_fits(NSIDE,pixels,data_halos,nest=sim['nest'])
  if printing: print "data_halos finally:",len(data_halos)
  
  i=1
  while i<len(f_halos):
    # import other halo files and vet by pix_i
    data_i = fits.open(f_halos[i])[1].data
    data_i = in_pixel_fits(NSIDE,pixels,data_i,nest=sim['nest'])
    if printing:
      if len(data_i)<1: 
        print "empty!"
      else:
        print "full!"
    data_halos = np.concatenate((data_halos,data_i))
    i+=1
  if printing: print "data_halos compiled!"
  
  return data_halos,data_gals

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# grab halo data for a list of pix

def grab_halo_data(NSIDE,pix_i,sim=cfg.Buzzard,printing=False):
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # find halo files containing those pixels
  halo_ids = convert_index(NSIDE,NSIDE_halos,pix_i,nest=sim['nest'])
  if hasattr(halo_ids,'__len__'):
    f_halos = np.array([sim['halo_temp'] % idx for idx in set(halo_ids)])
    f_halos = f_halos[[isfile(f) for f in f_halos]]
    # select only existing files
    if len(f_halos)<1: # check whether any files exist
      print "WARNING: Files DNE! [pixel_analysis.py +%i]" % cfg.lineNo()
      return None
  else:
    f_halos = sim['halo_temp'] % halo_ids
    if not isfile(f_halos):
      print "WARNING: Files DNE! [pixel_analysis.py +%i]" % cfg.lineNo()
      return None
  # print "HALOIDS:",halo_ids
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # read in data
  if hasattr(halo_ids,'__len__'):
    # open first file to initialize return data
    data_halos = fits.open(f_halos[0])[1].data
    data_halos = in_pixel_fits(NSIDE,pix_i,data_halos,nest=sim['nest'])
    L=len(data_halos)
  
    for i in range(1,len(f_halos)):
      # import other halo files and vet by pix
      data_i = fits.open(f_halos[i])[1].data
      data_i = in_pixel_fits(NSIDE,pix_i,data_i,nest=sim['nest'])
      if printing:
        if len(data_i)<1: 
          print "empty!"
        else:
          print "full!"
      data_halos = np.concatenate((data_halos,data_i))
      if len(data_halos)>L:
        L=len(data_halos)
      else:
        print "WARNING: no additions!" # maybe a bug if true
  
    if printing: print "data_halos compiled!"
  else:
    data_halos = fits.open(f_halos)[1].data
    data_halos = in_pixel_fits(NSIDE,pix_i,data_halos,nest=sim['nest'])
  
  return data_halos

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def grab_halo_df(NSIDE,indexes,sim=cfg.Buzzard,printing=False):
  data = grab_halo_data(NSIDE,indexes,sim=sim,printing=printing)
  return pd.DataFrame({"HALOID":data['HALOID'].byteswap().newbyteorder(),
                        "M200C":data['M200C'].byteswap().newbyteorder(),
                     "Redshift":data['Z'].byteswap().newbyteorder()})

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# unique identifier for each index + resolution
def unique_identifier(index,NSIDE):
  # eqn 1 of https://healpix.sourceforge.io/pdf/intro.pdf
  return np.array(index)+4*np.array(NSIDE)**2

def u_to_NSIDE(u):
  # eqn 2 of https://healpix.sourceforge.io/pdf/intro.pdf
  return (2**np.floor(np.log2(np.array(u)/4.)/2.)).astype(int)

def u_to_index(u):
  # eqn 3 of https://healpix.sourceforge.io/pdf/intro.pdf
  return (np.array(u)-4*u_to_NSIDE(u)**2).astype(int)

# NOTE: healpix claims to have all these routines internally, 
#       and probably does, but I didn't see them in the documentation.

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# ? check whether list of index inputs is a file already / loaded in RAM

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# ? get kids -> returns all pixels of smaller order within parent

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# ...

########################################################################
# astrophysics methods 
########################################################################

##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ## ##  ##  ##  ##  ##  ##
##  Methods for HID/Nred/Ngal dataset creation ##  ##  ##  ##  ##  ##
##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ## ##  ##  ##  ##  ##  ##

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# calculate Nred/Ngal
from sys import stdout
from os.path import isfile

fname_HNN = 'HNN_z%gto%g_%i.csv'
fname_HNN_all = 'HNN_z%gto%g_%i.csv'

def HID_Ngal_Nred(NSIDE,pix,z_mask=(.1,.3),blush=0,
                  # n_mask=(.1,.3),
              sim=cfg.Buzzard,params=cosmo.buzzard,printing=True):
  assert(NSIDE>NSIDE_gals) # too coarse resolution
  assert(NSIDE<NSIDE_MAX)  # too fine resolution---could miss galaxies
  if printing:
    print "\n" + "#" * 72
    print "Analyzing pix%i:%i" % (NSIDE,pix)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # check if file already exists
  u = unique_identifier(pix,NSIDE)
  if z_mask != None:
    fname = sim['pixels_dir'] + fname_HNN % (z_mask[0],z_mask[1],u)
  else:
    fname = sim['pixels_dir'] + fname_HNN_all % u
  
  if isfile(fname):
    if printing: print "Reading in %s from file" % fname
    return pd.read_csv(fname)
    # return np.loadtxt(fname,delimiter=',',unpack=True).T
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # generate halo ID list to iterate over in central index
  if printing: 
    print "Grabbing HID list"
  
  hid_list = HID_from_pix(NSIDE,pix,sim,z_mask=z_mask)
  
  if len(hid_list)<1:
    print "No halos exist in pix%i:%i!" % (NSIDE,pix)
    return None # TODO: create empty file
  
  # grab halo and galaxy files
  if printing:
    print "Grabbing halo and galaxy datasets"
  halo_data, galaxy_data = grab_halo_glxy_data(NSIDE,pix,sim=cfg.Buzzard,
                                            blush=blush,printing=printing)
  
  if z_mask: # vet by redshift
    halo_z_mask = np.logical_and(z_mask[0]<halo_data['Z'],
                                           halo_data['Z']<z_mask[1])
    galaxy_z_mask = np.logical_and(z_mask[0]<galaxy_data['Z'],
                                             galaxy_data['Z']<z_mask[1])
    halo_data = halo_data[halo_z_mask]
    galaxy_data = galaxy_data[galaxy_z_mask]
    # TODO: also vet hid_list by redshift!
  
  if True: # vet by m_star to ignore galaxies RM would ignore
    galaxy_data = gid.vet_by_m_star(galaxy_data,fits=True) 
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # make blank outputs for Ns
  Nred,Ngal = np.zeros((2,len(hid_list)))
  
  for i,HID in enumerate(hid_list):
    if printing: 
      print "Analyzing halo %i/%i\r" % (i+1,len(hid_list)),
      stdout.flush()
    hdl = gid.halo_data_list(HID,halo_data=halo_data) 
    if hdl==None:
      if printing: print "\nHID %i has no hdl" % HID
      continue
    # grab halos within r200
    dist = gid.true_dist(galaxy_data,hdl)/(1+hdl['Z']) # physical Mpc
    Dv = HK02.Dc(hdl['Z'],params) # virial overdensity for halo
    r200 = HK02.rh(hdl['RS'],200,Dv,hdl['RVIR'])/1000. # physical Mpc
    # TODO? save Dv and r200 along with the others?
    mask_R200 = dist<r200
    # create ES0 mask
    mask_ES0 = gid.ES0_mask(galaxy_data[mask_R200],hdl,fits=True)
    Nred[i] = sum(mask_ES0)
    Ngal[i] = len(mask_ES0)
  
  # save as csv!
  df = pd.DataFrame({"HALOID":hid_list.byteswap().newbyteorder(),
                     "Ngal":Ngal,"Nred":Nred})
  df.to_csv(fname,index=False)
  return df 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# grab many HNN from a sweep of indices
def HNN_df_from_sweep(NSIDE,pixels,z_mask=(.1,.3),blush=0,
                      sim=cfg.Buzzard,params=cosmo.buzzard,printing=True):
  if hasattr(pixels,'__len__'):
    return pd.concat([HID_Ngal_Nred(NSIDE,pix,z_mask=z_mask,blush=blush,
                      sim=sim,params=params,printing=printing)
                       for pix in pixels])
  else:
    return HID_Ngal_Nred(NSIDE,pixels,z_mask=z_mask,blush=blush,sim=sim,
                         params=params,printing=printing)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# plot PDF of red fraction

def df_for_HNN(NSIDE,pix,z_mask=(.1,.3),blush=0,sim=cfg.Buzzard,
                    params=cosmo.buzzard,printing=False):
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # merge halo and galaxy files
  
  if printing: 
    print "Grabbing galaxy and halo files."
  df_gals = HNN_df_from_sweep(NSIDE,pix,z_mask=z_mask,blush=blush,
                              sim=sim,params=params,printing=printing)
  df_halos = grab_halo_df(NSIDE,pix,sim=sim,printing=printing)
  if printing: 
    print "Grabbed galaxy and halo files."
  if 0: # random plots
    plt.scatter(df.mu,df.Nred,1,c=df.Redshift,cmap='coolwarm'); 
    plt.colorbar(); 
    plt.yscale('log'); 
    plt.show()
    print "Merging galaxy and halo data"
  df = pd.merge(df_gals,df_halos,on='HALOID')
  if len(df) < len(df_gals): 
    print "len(df_gals)=%g\nlen(df)=%g" % (len(df_gals),len(df))
  check_len(df)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # calculated columns
  
  mu = np.log10(df['M200C']); df['mu'] = mu
  fR = 1.0 * df.Nred/df.Ngal; df['fR'] = fR
  return df

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

'''
def plot_fR(NSIDE,pix,z_mask=(.1,.3),mu_bins=[13.5,14,14.5,15,16],stacking=True,
            blush=0,sim=cfg.Buzzard,params=cosmo.buzzard,printing=False):
  df = df_for_HNN(NSIDE,pix,z_mask=z_mask,blush=blush,sim=sim,
                       params=params,printing=printing)
'''
def plot_fR(df,stacking=False,mu_bins=[13.5,14,14.5,15,16],
            legend=True,yrange=None):
  mu_masks = [np.logical_and(df.mu>mu_bins[i],df.mu<mu_bins[i+1]) \
              for i in range(len(mu_bins)-1)]
  
  if 0: # plot ~ like Hansen+2009
    plt.scatter(df.Nred,df.fR,5,c=df.Redshift,cmap='coolwarm',alpha=.75); 
    plt.colorbar(); plt.show()
  
  for i in range(8): # eight bins
    Dz = .025 # (.3-.1)/8.
    zmin,zmax = .1 + i*.025, .1 + (i+1)*.025
    zmask = np.logical_and(zmin<df.Redshift,df.Redshift<zmax)
    plt.subplot(2,4,i+1)
    plt.title(r'z=(%g,%g)' % (zmin,zmax))
    plt.xlabel(r'red fraction $f_R$')
    plt.yscale('log')
    plt.xlim(0,1)
    
    if sum(zmask)<1: continue # nothing to plot here
    
    if stacking:
      plt.ylabel(r'count')
      dset = [df.fR[np.logical_and(zmask,mu_mask)] for mu_mask in mu_masks]
      labels = [r'$\mu|(%g,%g)$' % (mu_bins[ii],mu_bins[ii+1]) \
                for ii in range(len(mu_bins)-1)]
      histdata = plt.hist(dset,alpha=.5,label=labels,stacked=True)
    else:
      plt.ylabel(r'relative counts')
      for ii,mu_mask in enumerate(mu_masks):
        histdata = plt.hist(df.fR[np.logical_and(zmask,mu_mask)],
                            density=True,alpha=.5,bins=np.arange(0,1.1,.1),
                            label=r'$\mu|(%g,%g)$' \
                                  % (mu_bins[ii],mu_bins[ii+1]))
        if yrange!=None:
          plt.ylim(yrange)
    
    if legend:
      plt.legend()
    # histdata = plt.hist(df.fR[zmask],density=True)
  
  # plt.suptitle(r'')
  plt.tight_layout()
  plt.gcf().subplots_adjust(top=.96,bottom=0.07,left=0.05,right=.95,
                            hspace=.375,wspace=.3)
  plt.show()
  # return histdata





# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def plot_HNN(df,z_mask=(.1,.3),dZ=.025,mu_min=13.5,N_mu_bins=3,dmu=.5):
  # plot up mass / redshift evolution of fR
  # call with input of df = df_for_HNN(NSIDE,pix)
  # alternate flags: # pa.plot_HNN(df,N_mu_bins=6,dmu=.25)
  dv = df[(z_mask[0]<df.Redshift.values) * \
          (df.Redshift.values<z_mask[1]) * \
          (df.M200C.values>0) * (df.fR.values>=0)]
  reds = np.arange(z_mask[0]+dZ/2,z_mask[1]+dZ/2,dZ)
  for jj in range(N_mu_bins):
    mu_jj = mu_min + dmu*jj
    dv = dv[dv.mu.values > mu_jj]
    means,sig = np.zeros((2,len(reds)))
    for ii in range(len(reds)):
      zmn,zmx = z_mask[0]+dZ*ii,z_mask[0]+dZ*(ii+1)
      dv_ii = dv[(zmn<dv.Redshift.values)*(dv.Redshift.values<zmx)]
      if len(dv_ii)>0:
        means[ii] = np.mean(dv_ii.fR.values)
        sig[ii] = np.std(dv_ii.fR.values)
      else:
        means[ii] = None
    
    sigmean = sig/np.sqrt(max(len(dv),1))
    
    mask = np.isfinite(means)
    plt.errorbar(reds[mask],means[mask],sig[mask],capsize=2,fmt='.-',
                 label=r'$\mu>%g$' % mu_jj)
    plt.fill_between(reds[mask],means[mask]-sigmean[mask],
                     means[mask]+sigmean[mask],alpha=.25)
  
  # consider adding next line for background points:
  # plt.scatter(dv.Redshift.values,dv.fR.values,1,c=dv.mu.values,cmap='bone_r');
  plt.legend()
  plt.xlim(z_mask)
  plt.xlabel(r'Redshift $z$')
  plt.ylabel(r'Red fraction $f_R$')
  plt.tight_layout()
  plt.show()




# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def plot_HNN2(df,z_mask=(.1,.3),dZ=.06666667,backdrop=False,sigs=10,
              mu_min=13.5,N_mu_bins=6,dmu=.25,dx=.015):
  # plot up mass / redshift evolution of fR
  # call with input of df = df_for_HNN(NSIDE,pix)
  # alternate flags: dZ=.1, .066667
  # dx is the horizontal shift in mu in plotting
  
  assert(dx*2<dZ)
  
  dv = df[(z_mask[0]<df.Redshift.values) * \
          (df.Redshift.values<z_mask[1]) * \
          (df.M200C.values>0)]
  dv = dv[dv.fR.values>=0]
  
  reds = np.arange(z_mask[0]+dZ/2.,z_mask[1]+dZ/2.,dZ)
  
  # TODO: move from mu to mass! 
  # M200C is in Msol/h (see Buzzard paper, table 4)
  
  mu_max = mu_min + dmu * N_mu_bins
  mus = np.arange(mu_min+dmu/2.,mu_max+dmu/2.,dmu)
  assert(len(mus)==N_mu_bins)
  
  if backdrop:
    # plot backdrop
    plt.scatter(dv.mu.values,dv.fR.values,1,alpha=.125,
                c=dv.Redshift.values,cmap='coolwarm')
  
  # add averages for each mu + redshift bin
  for ii in range(len(reds)): # for each redshift bin
    zmn,zmx = z_mask[0]+dZ*ii,z_mask[0]+dZ*(ii+1)
    dv_ii = dv[(zmn<=dv.Redshift.values)*(dv.Redshift.values<zmx)]
    
    means,sig = np.zeros((2,N_mu_bins))
    for jj in range(N_mu_bins): # calculate for a given mass bin
      mu_jj = mu_min + dmu*jj
      dv_jj = dv_ii[(mu_jj<dv_ii.mu.values) * \
                          (dv_ii.mu.values<mu_jj+dmu)]
      if len(dv_jj)>0:
        means[jj] = np.mean(dv_jj.fR.values)
        sig[jj] = np.std(dv_jj.fR.values)
      else:
        means[jj] = None
    
    sigmean = sig/np.sqrt(max(len(dv_ii),1))
  
    mask = np.isfinite(means)
    plt.errorbar(mus[mask]+ii*dx,means[mask],sig[mask],capsize=2,fmt='.-',
                 label=r'$z|[%.2g,%.2g)$' % (zmn,zmx))
    plt.fill_between(mus[mask]+ii*dx,means[mask]-sigs*sigmean[mask],
                     means[mask]+sigs*sigmean[mask],alpha=.25)
  
  plt.legend()
  plt.xlim(mu_min,mu_max+dx*(len(reds)-1))
  plt.xlabel(r'lg mass $\mu$')
  plt.ylabel(r'Red fraction $f_R$')
  plt.tight_layout()
  plt.show()
  return

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 




# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# plot HOD

mu_label = r'$\mu\equiv\log_{10}\left(\frac{M_{200,c}}{M_\odot}\right)$'
fR_label = r'$f_R \equiv \frac{N_{\rm red}}{N_{\rm gal}}$'

def plot_HOD(df,plot_all=True):
  # TODO: plot in z-bins!
  #       see plot_fR for zmin,zmax binning
  
  MAX = max(df.Nred)
  plt.scatter(df.mu,df.Nred,1,c='r',label=r'$N_{\rm red}$')
  if plot_all:
    MAX = max(MAX,max(df.Ngal))
    plt.scatter(df.mu,df.Ngal,1,c='g',label=r'$N_{\rm gal}$')
  plt.ylim(.9,MAX*1.5)
  plt.yscale('log')
  plt.xlabel(mu_label)
  plt.ylabel(r'Galaxy count')
  plt.legend()
  
  plt.tight_layout()
  plt.show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def interp(z,N=100):
  return np.linspace(min(z),max(z),N)

def HOD_linmix(df,z_bins=[.1,.16667,.23333,.3],printing=False,scatter=False):
  # call with input of df = df_for_HNN(NSIDE,pix)
  for N in [df.Ngal.values,df.Nred.values]:
    if printing: print "top loop"
    for ii in range(len(z_bins)-1):
      if printing: print "inner loop %g" % ii
      mask = np.logical_and((z_bins[ii]<df.Redshift.values) * \
                            (df.Redshift.values<z_bins[ii+1]),N>0)
      x,y = df.mu.values[mask],np.log10(N[mask])
      # linmix
      lm = linmix.LinMix(x,y)
      if printing: print "Running linmix"
      lm.run_mcmc(silent=True)
      if printing: print "Completed linmix"
      alpha = lm.chain['alpha'].mean()
      beta = lm.chain['beta'].mean()
      sigsqr = lm.chain['sigsqr'].mean()
      sigma = np.sqrt(sigsqr)
      
      if printing:
        print("alpha mean and stddev:  {}, {}".format(
              lm.chain['alpha'].mean(),  lm.chain['alpha'].std()))
        print("beta mean and stddev:   {}, {}".format(
              lm.chain['beta'].mean(),   lm.chain['beta'].std()))
        print("sigsqr mean and stddev: {}, {}".format(
              lm.chain['sigsqr'].mean(), lm.chain['sigsqr'].std()))
      
      xs = interp(x) # spread out over input x values
      plt.plot(xs,alpha+xs*beta,label='z|[%0.2g,%0.2g)' \
               % (z_bins[ii],z_bins[ii+1]))
      
      if scatter: # show spread of inputs
        for i in range(0, len(lm.chain), len(lm.chain)/100): # plot fits 
          ys = lm.chain[i]['alpha'] + xs * lm.chain[i]['beta']
          plt.plot(xs, ys, color='b', alpha=0.02)
      
      plt.scatter(x,y,1,alpha=.25)
  
    plt.legend()
    plt.xlabel(r'log mass $\mu$')
    plt.ylabel(r'$\log_{10}$ galaxy count $N$')
    plt.tight_layout()
    plt.show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def plot_Ngal_fR(df,mu_min=None,mu_max=None,seaborn=False):
  if seaborn:
    import seaborn as sns
    sns.set(style="dark")
    sns.set(color_codes=True)
    if mu_min!=None:
      g=sns.jointplot(x='mu',y='fR',data=df[df.mu>mu_min],ylim=(0,1),kind='reg')
    else:
      g=sns.jointplot(x='mu',y='fR',data=df[df.mu>14.25],ylim=(0,1),kind='reg')
    g.set_axis_labels(mu_label,fR_label); 
    plt.show()
  else:
    plt.subplot(211)
    plt.scatter(df.mu,df.Nred,1,label=r'$N_{\rm red}$',c='r')
    plt.scatter(df.mu,df.Ngal,1,label=r'$N_{\rm gal}$',c='g')
    plt.ylabel(r'Galaxy count')
    plt.ylim(.9,1.1*max(df.Ngal))
    plt.gca().axes.xaxis.set_ticklabels([])
    plt.xlim(mu_min,mu_max)
    plt.legend()
    plt.yscale('log')
    
    plt.subplot(212)
    plt.scatter(df.mu,df.fR,5,alpha=.5,c=df.Redshift>.25,cmap='coolwarm')
    plt.ylabel(fR_label)
    plt.xlim(mu_min,mu_max)
    plt.xlabel(mu_label)
    
    plt.tight_layout()
    plt.show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# definitions from Hansen+2009
# formulas 13--15

def g(z):
  return 0.206-0.371*z

def h(z):
  if np.any(z<3.6/25.8):
    print "ERROR: invalid redshift input"
  return -3.6+25.8*z

def f_R(N200,z):
  # scipy.special.erf
  return g(z)*np.erf(np.log10(N200)-np.log10(h(z)))+0.69

def plot_H09_11(df,lines=True):
  N200 = np.linspace(3,100,1000)
  
  if lines:
    plt.plot(N200,f_R(N200,.20),label="z=%g" % .20)
    plt.plot(N200,f_R(N200,.28),label="z=%g" % .28)
    plt.legend()
  
  plt.scatter(df.Nred,df.fR,5,alpha=.5,c=df.Redshift>.25,cmap='coolwarm')
  plt.colorbar(label=r'$z>.25$')
  plt.xlabel(r'$N_{\rm red}$')
  plt.ylabel(fR_label)
  plt.xscale('log')
  plt.show()


##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ## ##  ##  ##  ##  ##  ##
##  Methods for stacked density profile creation                   ##
##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ## ##  ##  ##  ##  ##  ##

########################################################################
# static variables for method

# mass bins should range from >10^13 Msol/h up to <2*10^15 Msol/h (max)
# so an upper bound of 16 is plenty safe (low: ~1.43e13 Msol)
dmu=.1
mu_bins = np.arange(13,16,dmu)
# xi=log10(r/r200) gets sketchy <-2 and loses relevance >~ 0
# I grab all within R20, since it's the wider dataset
dxi=.1
xi_bins = np.arange(-3,1,dxi)
x_bins = 10.**xi_bins

# volume element per shell
j = np.arange(len(x_bins)-1)
X = 4./3*np.pi*(x_bins[j+1]**3-x_bins[j]**3)

# selecting [.1,.3] allows for red sequence selection from Hao+09
dz=.025
z_bins = np.arange(.1,.3+dz,dz)

density_report_template = np.zeros((len(z_bins),len(mu_bins),len(x_bins),6))

fname_RP = 'RP_z%gto%g_%i.npy'
fname_RP_all = 'RP_%i.npy'

########################################################################

def radial_profile(NSIDE,pix,z_mask=(.1,.3),m_star_mask=True,overwrite=False,
                   sim=cfg.Buzzard,params=cosmo.buzzard,printing=True):
  assert(NSIDE>NSIDE_gals) # too coarse resolution
  assert(NSIDE<NSIDE_MAX)  # too fine resolution---could miss galaxies
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # check if file already exists
  u = unique_identifier(pix,NSIDE)
  if z_mask != None:
    fname = sim['pixels_dir'] + fname_RP % (z_mask[0],z_mask[1],u)
  else:
    fname = sim['pixels_dir'] + fname_RP_all % u
  
  if isfile(fname) and not overwrite:
    if printing: print "Reading in %s from file" % fname
    return np.load(fname)
  elif printing:
    print "Creating file %s" % fname
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # generate halo ID list to iterate over in central index
  if printing: print "Grabbing HID list"
  hid_list = HID_from_pix(NSIDE,pix,sim,z_mask=z_mask)
  if len(hid_list)<1:
    print "No halos exist in pix%i:%i!" % (NSIDE,pix)
    return np.copy(density_report_template)
  
  # grab halo and galaxy files
  if printing:
    print "Grabbing halo and galaxy datasets"
  halo_data, galaxy_data = grab_halo_glxy_data(NSIDE,pix,sim=cfg.Buzzard,
                                               printing=printing)
  
  if z_mask: # vet by redshift
    halo_z_mask = np.logical_and(z_mask[0]<halo_data['Z'],
                                           halo_data['Z']<z_mask[1])
    galaxy_z_mask = np.logical_and(z_mask[0]<galaxy_data['Z'],
                                             galaxy_data['Z']<z_mask[1])
    halo_data = halo_data[halo_z_mask]
    galaxy_data = galaxy_data[galaxy_z_mask]
  
  if m_star_mask: # vet by m_star to ignore galaxies RM would ignore
    galaxy_data = gid.vet_by_m_star(galaxy_data,fits=True) 
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # save radial profiles in the density_report_template
  report = np.copy(density_report_template)
  N_skipped = 0
  N_subhalos_skipped = 0
  N_undermassive_skipped = 0
  
  for i,HID in enumerate(hid_list):
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # select galaxies of halo HID
    if printing: 
      print "Analyzing halo %i/%i\r" % (i+1,len(hid_list)),
      stdout.flush()
    hdl = gid.halo_data_list(HID,halo_data=halo_data) 
    
    # check it's a valid halo to process
    if hdl==None: # ensure hdl exists
      if printing: print "\nHID %i has no hdl" % HID
      continue
    elif hdl['HOST_HALOID']!=-1: # check it's not a subhalo
      # if printing: print "Skipping subhalo (HID %i/%i)" % (i+1,len(hid_list))
      N_subhalos_skipped += 1
      continue 
    else: # check it's not undermassive
      mu = np.log10(hdl['M200C'])
      if mu<13: 
        N_undermassive_skipped += 1
        continue # minimum to trust: 10^13 Msol/h ~ 1.43e13 Msol
      else: 
        pass # begin processing 
    
    # grab halos within r200
    dist = gid.true_dist(galaxy_data,hdl) # cMpc/h
    Dv = HK02.Dc(hdl['Z'],params) # virial overdensity for halo
    # print "Delta_v = %g" % Dv # gives ~120 ... Error? 
    # linear theory predicts 18pi^2 ~ 178 ... Gus remembers 98?
    r200 = HK02.rh(hdl['RS'],200,Dv,hdl['RVIR'])/1000. # cMpc/h
    r20 = HK02.rh(hdl['RS'],20,Dv,hdl['RVIR'])/1000.   # cMpc/h
    # print "mu,r200,r20,c:",mu,r200,r20,(1.0*hdl['RVIR']/hdl['RS'])
    mask_R20 = dist<r20*10 # to get outside of the R200 edge
    
    # check that there still are galaxies here.
    if sum(mask_R20)<1: 
      N_skipped += 1
      if printing and mu>14.0:
        print "\nSkipping empty halo of mu=%g" % mu
      continue
    
    # create ES0 mask
    mask_ES0 = gid.ES0_mask(galaxy_data[mask_R20],hdl,fits=True)
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # calculate number densities
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    
    # fractional distance from center
    x = np.array(dist[mask_R20]/r200)
    
    # deselect BCG:
    mask_ES0 = np.array(mask_ES0[x!=min(x)])
    x = x[x!=min(x)]
    
    # grab index for report
    ii = np.logical_and(z_bins<hdl['Z'],hdl['Z']<z_bins+dz)
    jj = np.logical_and(mu_bins<mu,mu<mu_bins+dmu)
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    """ Save format: 
    report[i] = var_i
         0(3) : N_halo
         1(4) : N_gal
         2(5) : Volume
    the parentheticals correspond to the same but for red
    """
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # count number of halos
    # if this halo has any galaxies, 
    # add to numbers of halos contributing to this ii&jj bin
    if len(mask_ES0)>0: # check there are any galaxies still # redundant; checked above in mask_R20
      report[ii,jj,:,0] += 1 # total halo count
    if sum(mask_ES0)>0: # check there are red galaxies still
      report[ii,jj,:,3] += 1 # red only
    
    # TODO: vectorize
    for kk in range(len(x_bins)-1):
      Nkk = sum(np.logical_and(x_bins[kk]<x,x<x_bins[kk+1]))
      Nkk_red = sum(np.logical_and.reduce((x_bins[kk]<x,x<x_bins[kk+1],mask_ES0)))
      Vkk = X[kk]*r200**3 # (cMpc/h)**3
      # TODO: add in Nkk & Nkk_red saving
      report[ii,jj,kk,1] += float(Nkk)
      report[ii,jj,kk,2] += Vkk
      
      report[ii,jj,kk,4] += float(Nkk_red)
      report[ii,jj,kk,5] += Vkk
  
  N_tot = N_undermassive_skipped + N_subhalos_skipped + N_skipped
  if printing and N_tot>0:
    print "Skipped %i undermassive halos" % N_undermassive_skipped 
    print "Skipped %i empty halos" % N_skipped
    print "Skipped %i sub-halos" % N_subhalos_skipped
    print "Total: %i/%i skipped" % (N_tot,len(hid_list))
  
  if printing: print "Saving..."," "*20
  np.save(fname,report)
  if printing: print "Done."
  return report

def RP_ds_from_sweep(NSIDE,pixels,z_mask=(.1,.3),m_star_mask=True,overwrite=False, 
                      sim=cfg.Buzzard,params=cosmo.buzzard,printing=False):
  if hasattr(pixels,'__len__') and len(pixels)>1: # many elements
    if printing:
      print "\n" + "#" * 72
      print "Analyzing pix%i %i/%i." % (NSIDE,1,len(pixels))
    ds_sum = radial_profile(NSIDE,pixels[0],z_mask=z_mask,
                            m_star_mask=m_star_mask,overwrite=overwrite,
                            sim=sim,params=params,printing=printing)
    for ii in range(1,len(pixels)):
      if printing: 
        print "\n" + "#" * 72
        print "Analyzing pix%i %i/%i." % (NSIDE,ii+1,len(pixels))
      ds_sum += radial_profile(NSIDE,pixels[ii],z_mask=z_mask,
                               m_star_mask=m_star_mask,overwrite=overwrite,
                               sim=sim,params=params,printing=printing)
  else: # only one element
    if hasattr(pixels,'__len__'): pixels=pixels[0] # select only element
    if printing:
      print "\n" + "#" * 72
      print "Analyzing single pix%i." % NSIDE
    ds_sum = radial_profile(NSIDE,pixels,z_mask=z_mask,
                            m_star_mask=m_star_mask,overwrite=overwrite,
                            sim=sim,params=params,printing=printing)
  return ds_sum


def sum_by_step(df,step,axis=0,log=False): # FIXME: add by-axis capabilities
  ii = np.arange(0,len(df)-step+1,step)
  out = np.zeros(len(ii))
  for jj in range(step):
    if log:
      out += np.log10(df[ii+jj])
    else: 
      out += df[ii+jj]
  out /= step
  if log: # what we plot on the abscissa
    out = 10.**out
  return out


def index_mask(length,stepsize):
  """
  create e.g.: [ [1,1,0,0,0,0],
                 [0,0,1,1,0,0],
                 [0,0,0,0,1,1] ]
  to give ranged masks
  """
  report = np.zeros((length/stepsize,length))
  for i in range(length/stepsize):
    report[i,i*stepsize:(i+1)*stepsize] = True
  return np.array(report).astype(bool)

rho_bar_fname = 'rho_Dz.csv'
def rho_bar(sim=cfg.Buzzard,overwrite=False):
  fname = sim['pixels_dir'] + rho_bar_fname
  if isfile(fname) and not overwrite:
    return pd.read_csv(fname)
  else: 
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # create and return pd df
    
    # redshift bins
    Delta_z = .025
    z_min,z_max = .1,.3
    z_vals = np.arange(z_min,z_max,Delta_z)
    gal_count = np.zeros((len(z_vals),2))
    
    # read in the data
    gf = sim['galaxies'] # [0:2]
    
    for jj,fname_i in enumerate(gf):
      print "Analyzing galaxy file %i/%i." % (jj+1,len(gf))
      data=fits.open(fname_i)[1].data
      
      # calculate count in bins
      for i,z in enumerate(z_vals):
        dv = data[(data.Z>z)*(data.Z<z+Delta_z)]
        # vet by 0.2L_* in i-band:
        dv = gid.vet_by_m_star(dv,fits=True)
        gal_count[i,0] += len(dv)
        
        # get ES0 mask to select red galaxies
        z_avg = z + .5*Delta_z
        mask_red = gid.ES0_mask(dv,{"Z":z_avg},fits=True)
        gal_count[i,1] += sum(mask_red)
    
    df = pd.DataFrame({'z':z_vals,'N_gal':avg_gal_count[:,0],
                                  'N_red':avg_gal_count[:,1]})
    df.to_csv(fname,index=False)
    print "saved %s" % fname
    return df

def plot_rp(ds,per_rho=True,mu_step=5,x_step=2,lines=True,red=False,
            plot_NFW=True,rho_lims=(.25,5e9),x_lims=(1e-3,4),yshift=0,
            per_NFW=False,
            legend=True,sim=cfg.Buzzard,params=cosmo.buzzard): # orig. yshift=.25
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # define compressed bins
  new_x_bins = sum_by_step(x_bins,x_step,log=True) # for plotting
  mu_idxs = index_mask(len(mu_bins),mu_step) # for indexing
  x_idxs = index_mask(len(x_bins),x_step)    # for indexing
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # plot by z_bins: 
  
  # read in rho bar (avg galaxy density)
  df_rho_bar = rho_bar(sim) # z,Ngal,Nred
  vol_i = np.array([cosmo.coVol(z_bins[ii],z_bins[ii+1],buzzard_sky_str,
                    params=params,per_h=True) # (cMpc/h)^3
                    for ii in range(len(z_bins)-1)])
  rho_bar_dz = (df_rho_bar.N_red if red \
                else df_rho_bar.N_gal).values / vol_i # num/(cMpc/h)^3
  
  # TODO: set up subplots with common x & y axes: 
  # fig,ax = plt.subplots(nrows=2,ncols=4,sharex=True,sharey=True,figsize=(2,4))
  # fig.text(0.5, 0.04, 'common X', ha='center')
  # fig.text(0.04, 0.5, 'common Y', va='center', rotation='vertical')
  
  for i in range(len(z_bins)-1):
    plt.subplot(2,4,i+1)
    
    for j in range(len(mu_idxs)):
      N_halo = np.sum(ds[i,mu_idxs[j],0,3 if red else 0])
      N_gal = np.array([np.sum(ds[i,mu_idxs[j],:,4 if red else 1]\
                [:,x_idxs[k]]) for k in range(len(x_idxs))])
      # volume for z-bin i, summed over mu_bins[mu_idxs[j]], for each x
      volume = np.array([np.sum(ds[i,mu_idxs[j],:,5 if red else 2]\
                [:,x_idxs[k]]) for k in range(len(x_idxs))])
      # skip if empty:
      if N_halo < 1: continue
      # calculate the average number density of each bin for a single halo
      rho_j = 1.0*(N_gal/volume)
      if per_rho: # plot per (Mpc/h)^3
        rho_j /= rho_bar_dz[i]
      
      rho_j_err = np.zeros(len(N_gal)) # prevent nan w/ null default
      rho_j_err = np.divide(rho_j,np.sqrt(N_gal),rho_j_err,where=N_gal>0)
      
      mu_min,mu_max = min(mu_bins[mu_idxs[j]]), max(mu_bins[mu_idxs[j]])+dmu
      mu_label = r'$\mu|(%g,%g)$' % (mu_min,mu_max)
      mu_shift = 10.**(yshift*(mu_min-13))
      
      if plot_NFW:
        z_avg = 0.5*(z_bins[i]+z_bins[i+1])
        Om = cosmo.Wm(z_avg)
        mu_avg = 0.5*(mu_min+mu_max)
        c200_val = gid.c200c(z_avg,10.**mu_avg)
        
      if per_NFW: # plot dividing out NFW for relative amplitudes
        nfw = gid.NFW(new_x_bins,200,c_Delta=c200_val,r_Delta=1)/Om
        plt.errorbar(new_x_bins,rho_j/nfw*mu_shift,rho_j_err/nfw*mu_shift,
                     label=mu_label,capsize=2,fmt='.')
        plt.fill_between(new_x_bins,(rho_j-rho_j_err)/nfw*mu_shift,
                                    (rho_j+rho_j_err)/nfw*mu_shift,alpha=.25)
        plt.plot([1e-5,1e3],np.ones(2)*mu_shift,'k') # NFW baseline
      else: # plot normally
        r_vals = np.exp(np.linspace(np.log(x_lims[0]),
                                    np.log(x_lims[1]),100))
        nfw = gid.NFW(r_vals,200,c_Delta=c200_val,r_Delta=1)/Om
        plt.plot(r_vals,nfw*mu_shift,'k')
        plt.errorbar(new_x_bins,rho_j*mu_shift,rho_j_err*mu_shift,
                     label=mu_label,capsize=2,fmt='.')
        plt.fill_between(new_x_bins,(rho_j-rho_j_err)*mu_shift,
                                    (rho_j+rho_j_err)*mu_shift,alpha=.25)
    
    if legend: plt.legend(loc='lower left')
    plt.xlabel(r'$r/r_{200,c}$')
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # craft y label
    # TODO: move this and x label to sides! 
    if red:
      dens = r'\rho_{\rm red}'
    else:
      dens = r'\rho_{\rm gal}'
    
    if per_NFW: # per NFW profile
      units = r'\rho_{\rm NFW}' # implicit Omega_m
    elif per_rho: # per mean galaxy count
      units = r'\bar\rho'
    else: # (per volume)
      units = r'{\rm Mpc^3}'
      # units = r'r_{200,c}^3'
    
    if yshift==0:
      shift = r''
    else:
      shift = r'*10^{%g(\mu_i-\mu_{\rm min})}' % yshift
    
    yl = r'$%s/%s %s$' % (dens,units,shift)
    plt.ylabel(yl)
    plt.xscale('log'); plt.yscale('log')
    plt.xlim(x_lims)
    plt.ylim(rho_lims)
    plt.title(r'z|(%g,%g)' % (z_bins[i],z_bins[i+1]))
  
  plt.tight_layout()
  plt.gcf().subplots_adjust(top=.954,bottom=.086,left=.053,right=.988,
                            hspace=0.297,wspace=0.289)
  plt.show()



##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ## ##  ##  ##  ##  ##  ##
##  Methods for Color--Magnitude dataset creation                  ##
##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ## ##  ##  ##  ##  ##  ##

fname_CM = 'CM_z%gto%g_%i.csv'
fname_CM_all = 'CM_%i.csv'

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# grab CM data

def CM_data(NSIDE,pix,z_mask=False,blush=0,
            sim=cfg.Buzzard,params=cosmo.buzzard,printing=True):
  assert(NSIDE>NSIDE_gals) # too coarse resolution
  assert(NSIDE<NSIDE_MAX)  # too fine resolution---could miss galaxies
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # check if file already exists
  
  u = unique_identifier(pix,NSIDE)
  if z_mask != None:
    fname = sim['pixels_dir'] + fname_CM % (z_mask[0],z_mask[1],u)
  else:
    fname = sim['pixels_dir'] + fname_CM_all % u
  
  if isfile(fname):
    if printing: print "Reading in %s from file" % fname
    return pd.read_csv(fname) # TODO: make hdf5 files!
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # load the data and create a CM diagram
  
  halo_data, galaxy_data = grab_halo_glxy_data(NSIDE,pix,sim=cfg.Buzzard,
                                            blush=blush,printing=printing)
  
  if z_mask != None: # vet by redshift
    halo_z_mask = np.logical_and(z_mask[0]<halo_data['Z'],
                                           halo_data['Z']<z_mask[1])
    galaxy_z_mask = np.logical_and(z_mask[0]<galaxy_data['Z'],
                                             galaxy_data['Z']<z_mask[1])
    halo_data = halo_data[halo_z_mask]
    galaxy_data = galaxy_data[galaxy_z_mask]
    # TODO: also vet hid_list by redshift!
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # save g,r,i and other necessary data ... maybe HID?
  
  g,r,i,z,Y = gid.get_grizY(galaxy_data,fits=True)
  
  df = pd.DataFrame({"HALOID":galaxy_data['HALOID'].byteswap().newbyteorder(),
                     "TMAG_g":g.byteswap().newbyteorder(), 
                     "TMAG_r":r.byteswap().newbyteorder(),
                     "TMAG_i":i.byteswap().newbyteorder(),
                     "TMAG_z":z.byteswap().newbyteorder(),
                     "TMAG_Y":Y.byteswap().newbyteorder(),
                     "Z":galaxy_data['Z'].byteswap().newbyteorder()})
  df.to_csv(fname,index=False)
  return df

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# compile CM data across many pix 

def CM_df_from_sweep(NSIDE,indexes,z_mask=(.1,.3),blush=0,
                     sim=cfg.Buzzard,params=cosmo.buzzard,
                     printing=False):
  if hasattr(indexes,'__len__'):
    return pd.concat([CM_data(NSIDE,idx,z_mask=z_mask,blush=blush,
                              sim=sim,params=params,printing=printing)
                        for idx in indexes])
  else:
    return CM_data(NSIDE,indexes,z_mask,blush=blush,
                   sim=sim,params=params,printing=printing)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# create CM plot

def df_for_CM_plot(NSIDE,indexes,z_mask=(.1,.3),mu_limit=None,
                   field=False,m_star_vet=True,
                   sim=cfg.Buzzard,printing=False):
  if printing: 
    print "Grabbing data for NSIDE=%g, idx:" % NSIDE,indexes
  df_gals = CM_df_from_sweep(NSIDE,indexes,z_mask=z_mask)
  df_halos = grab_halo_df(NSIDE,indexes,sim=sim,printing=printing)
  if printing: 
    print "Merging galaxy and halo data"
  df = pd.merge(df_gals,df_halos,on='HALOID')
  check_len(df)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # vet down 
  
  if field:
    print "Using only field galaxies!"
    df = df[df['HALOID']<1]
    check_len(df)
  
  if m_star_vet:
    print "Vetting by 0.2L*"
    df = gid.vet_by_m_star(df) # ignore galaxies RM would ignore
    check_len(df)
  
  if z_mask != None:
    print "Vetting by redshift range z|(%g,%g)" % z_mask
    df = df[np.logical_and(z_mask[0]<df['Z'],df['Z']<z_mask[1])]
    check_len(df)
  
  df['mu'] = np.log10(df['M200C'])
  if mu_limit != None:
    print "Vetting by mass range mu|(%g,%g)" % mu_limit
    df = df[np.logical_and(mu_limit[0]<df.mu,df.mu<mu_limit[1])]
    check_len(df)
  
  return df

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# create CM data

# m_i bins
dmi = .0625
mi_bins = np.arange(14.5,21+dmi,dmi)

# (g-r) bins
dgmr = .0375
gmr_bins = np.arange(-.3,2.1+dgmr,dgmr)

# output filename
fname_CM_ds = "CM_%i.npy"
CM_report_template = np.zeros((len(z_bins),len(mu_bins),
                               len(mi_bins),len(gmr_bins)))

def CM_ds(NSIDE,indexes,z_mask=(.1,.3),mu_limit=None,field=False,
          m_star_vet=True,sim=cfg.Buzzard,printing=False,overwrite=False):
  assert(NSIDE>NSIDE_gals) # too coarse resolution
  assert(NSIDE<NSIDE_MAX)  # too fine resolution---could miss galaxies
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # check if file already exists
  
  u = unique_identifier(indexes,NSIDE)
  # FIXME: do for each file
  fname = sim['pixels_dir'] + fname_CM_ds % u
  if isfile(fname) and not overwrite:
    return np.load(fname)
  # else: create file
  ds = CM_report_template
  
  df = df_for_CM_plot(NSIDE,indexes,z_mask=z_mask,mu_limit=mu_limit,
            field=field,m_star_vet=m_star_vet,sim=sim,printing=printing)
  
  # create masks
  z_masks = [np.logical_and(df.Z>z_bins[ii],df.Z<z_bins[ii]+dz) \
             for ii in range(len(z_bins))]
  mu_masks = [np.logical_and(df.mu>mu_bins[jj],df.mu<mu_bins[jj]+dmu) \
              for jj in range(len(mu_bins))]
  m_i = df.TMAG_i.values
  mi_masks = [np.logical_and(m_i>mi_bins[kk],m_i<mi_bins[kk]+dmi) \
              for kk in range(len(mi_bins))]
  gmr = df.TMAG_g.values-df.TMAG_r.values
  gmr_masks = [np.logical_and(gmr>gmr_bins[ll],gmr<gmr_bins[ll]+dgmr) \
               for ll in range(len(gmr_bins))]
  
  for ii in range(len(z_bins)):
    for jj in range(len(mu_bins)):
      for kk in range(len(mi_bins)):
        for ll in range(len(gmr_bins)):
          if 1:
            mask_tot = np.logical_and(
                       np.logical_and(z_masks[ii],mu_masks[jj]),\
                       np.logical_and(mi_masks[kk],gmr_masks[ll]))
            ds[ii,jj,kk,ll] += np.sum(mask_tot)
          else:
            ds[ii,jj,kk,ll] += np.sum(z_masks[ii] * mu_masks[jj] \
                                 * mi_masks[kk] * gmr_masks[ll])
  
  if 0: # plot
    [gmr_min,gmr_max,mi_min,mi_max]=[min(gmr_bins),max(gmr_bins),
                                     min(mi_bins),max(mi_bins)]
    ds_sum = np.sum(ds,axis=(0,1))
    plt.imshow(np.transpose(ds_sum)[::-1,:],cmap='bone',
               extent=[mi_min,mi_max,gmr_min,gmr_max]); 
    plt.colorbar(); plt.show()
  
  np.save(fname,ds)
  return ds


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def CM_plot(df,z_mask=(.1,.3),mu_limit=None,m_star_limit=True):
  # vet data by input values
  erst_z_mask = np.ones(len(df))
  erst_mu_mask = np.ones(len(df))
  
  if z_mask!=None:
    erst_z_mask = np.logical_and(z_mask[0]<df.Z,df.Z<z_mask[1])
  if mu_limit!=None:
    erst_mu_mask = np.logical_and(mu_limit[0]<df.mu,df.mu<mu_limit[1])
  
  df = df[np.logical_and(erst_z_mask,erst_mu_mask)]
  
  if m_star_limit!=None: # 0.2 L* limit
    df = gid.vet_by_m_star(df)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # plot remaining galaxies in redshift bins
  
  i_list = np.linspace(min(df.TMAG_i)-.2,max(df.TMAG_i)+.2,10)
  gmr = df.TMAG_g-df.TMAG_r
  
  for i in range(8): # eight redshift bins
    Dz = .025 # (.3-.1)/8.
    zmin,zmax = .1 + i*.025, .1 + (i+1)*.025
    zmask = np.logical_and(zmin<df.Z,df.Z<zmax)
    plt.subplot(2,4,i+1)
    plt.title(r'z=(%g,%g)' % (zmin,zmax))
    plt.xlabel(r'i'); plt.ylabel(r'g-r');
    plt.ylim(min(gmr)-.05,max(gmr)+.05)
    if 1: # draw in red sequence for zmin and zmax
      gmr_min,wth_min = gid.gmr_line(i_list,zmin)
      plt.fill_between(i_list,gmr_min[0]+2*wth_min,gmr_min[0]-2*wth_min,
                         alpha=.125,color='b')
      gmr_max,wth_max = gid.gmr_line(i_list,zmax)
      plt.fill_between(i_list,gmr_max[0]+2*wth_max,gmr_max[0]-2*wth_max,
                         alpha=.125,color='r')
      plt.xlim(min(i_list),max(i_list))
        
    if sum(zmask)<1: continue # no points to plot
    
    plt.scatter(df.TMAG_i[zmask],gmr[zmask],10,c=df.Z[zmask],
                cmap='coolwarm',alpha=.5)
    plt.colorbar()
    plt.xlim(min(df.TMAG_i[zmask])-.1,max(df.TMAG_i[zmask])+.1)
  
  if mu_limit != None:
    plt.suptitle(r'$%g < \mu < %g$' % mu_limit)
  plt.tight_layout()
  plt.gcf().subplots_adjust(top=.95,bottom=.075,left=.05,right=.95,
                            hspace=.25,wspace=.325)
  plt.show()
  return

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# maybe add method for upsizing data, i.e.: taking lower NSIDE value
# datasets and merging them into higher? Not high priority though,
# since it already works for the smaller pixels

########################################################################
# main method
########################################################################

if __name__=='__main__':
  if 1: # utd 18 Feb 2020
    print "plotting (relative) radial profiles"
    NSIDE=16; points = list(range(512)) + list(range(512,3072))
  elif 0: # plot most up-to-date radial profiles (7 Feb 2020)
    print "plotting radial profiles"
    NSIDE=32; points = list(range(806)) + list(range(1000,1127))
  if 0: # test plot_rp
    print "testing plot_rp..."
    NSIDE=32; points=range(2**7)
  skyarea = len(points)*pix(NSIDE)
  print "Using sky patch size %g deg^2 = %g%% of the buzzard sky" \
         % (skyarea,100.*skyarea/buzzard_sky)
  ds = RP_ds_from_sweep(NSIDE,points)
  if 0: # default settings
    plot_rp(ds)
  else: # advanced settings
    plot_rp(ds,rho_lims=(1e-2,1e17),yshift=4,x_step=2,legend=False)
    plot_rp(ds,yshift=0,per_NFW=True,x_step=2,legend=False,
             rho_lims=(1e-2,1e+2),x_lims=(2e-3,2e0))
  print '~fin'
  raise SystemExit
