########################################################################
# A fit for the Buzzard red sequence
########################################################################
import numpy as np
from m_star_model import m_star_cutoff

HaoEtAl2010 = { 
  # slope of [g-r vs i] relation vs redshift z
  "m_bR":3.049, "m_bR_s":.011,
  "b_bR":0.623, "b_zp_s":.002,
  # width of g-r ridgeline
  "m_sR":.136,  "m_sR_s":.010,
  "b_sR":.037,  "b_sR_s":.002,
  # g-r vs i slope
  "m_aR":-.075, "m_aR_s":.005,
  "b_aR":-.003, "b_aR_s":.001,
  # lower and upper redshift limits for interpolation
  "Z_range":[.1,.3], # used for extrapolation warnings
  'i_max_A':np.exp(3.1638), 'i_max_k':0.1428, # for m_{i,max}
  "url":"https://arxiv.org/pdf/0907.4383.pdf" # figs 7 and 9
}

gmr_fit = {
  # g-r vs i slope
  "m_aR":-0.03477, # pm .004180
  "b_aR":-0.01036, # pm .0008813
  # evolution of the zero point
  "m_bR":2.949,    # pm 0.03006
  "b_bR":0.6060,   # pm 0.006275
  # width of g-r ridgeline
  "m_lgsR":1.330,    # pm 0.02142
  "b_lgsR":-1.445,   # pm 0.004688
  "Z_range":[.05,.375], # limits for interpolation goodness of fit
  "url":"https://www.overleaf.com/read/kkqybvpqzwxk"
}

def gmr_line(Z,m_i,params=gmr_fit,warnings=True):
  if (np.any(Z<params['Z_range'][0]) \
  or np.any(Z>params['Z_range'][1])) \
  and warnings:
    print("Warning: Extrapolating redshift! [ES0_model.gmr_line]")
  # return line (for each m_i input) and scatter
  return (params['m_aR']*Z+params['b_aR']) * (m_i - m_star_cutoff(Z)) \
       + (params['m_bR']*Z+params['b_bR']), \
         10.**(params['m_lgsR']*Z+params['b_lgsR']) # scatter

# (r-i) red sequence model (quick and dirty)
x1,y1 = .38, .575
x2,y2 = .64, 1.075
rmi_m = (y2-y1)/(x2-x1)
rmi_b = y2-rmi_m*x2

rmi_fit = {
  # r-i vs i slope: take there to be no slope of the RS (false)
  "m_aR":0, "b_aR":0,
  # evolution of the zero point
  "m_bR":rmi_m, "b_bR":rmi_b,
  # width of g-r ridgeline
  "m_lgsR":0, "b_lgsR":-1,
  "Z_range":[.32,.70], # limits for interpolation goodness of fit
}

def rmi_line(Z,m_i,params=rmi_fit,warnings=True):
  if (np.any(Z<params['Z_range'][0]) \
  or np.any(Z>params['Z_range'][1])) \
  and warnings:
    print("Warning: Extrapolating redshift! [ES0_model.rmi_line]")
  # return line (for each m_i input) and scatter
  return (params['m_aR']*Z+params['b_aR']) * (m_i - m_star_cutoff(Z)) \
       + (params['m_bR']*Z+params['b_bR']), \
         10.**(params['m_lgsR']*Z+params['b_lgsR']) # scatter


def ES0_mask(Z,m_g,m_r,m_i,params=gmr_fit,warnings=True):
  gmr,wth = gmr_line(Z,m_i,params,warnings)
  mask_under = (m_g - m_r) > gmr - 2*wth
  mask_above = (m_g - m_r) < gmr + 2*wth
  return np.logical_and(mask_under,mask_above)


def ES0_mask2(Z,m_g,m_r,m_i,warnings=True):
  mask_Z = Z < .32
  if hasattr(Z,'__len__'):
    # handle low redshift
    gmr,wth = gmr_line(Z[mask_Z],m_i[mask_Z],gmr_fit,warnings)
    mask_under_gmr = (m_g[mask_Z] - m_r[mask_Z]) > gmr - 2*wth
    mask_above_gmr = (m_g[mask_Z] - m_r[mask_Z]) < gmr + 2*wth
    mask_gmr = np.copy(mask_Z)
    mask_gmr[mask_gmr] = np.logical_and(mask_under_gmr,mask_above_gmr)
    
    # handle high redshift
    rmi,wth = rmi_line(Z[~mask_Z],m_i[~mask_Z],rmi_fit,warnings)
    mask_under_rmi = (m_r[~mask_Z] - m_i[~mask_Z]) > rmi - 2*wth
    mask_above_rmi = (m_r[~mask_Z] - m_i[~mask_Z]) < rmi + 2*wth
    mask_rmi = np.copy(~mask_Z)
    mask_rmi[mask_rmi] = np.logical_and(mask_under_rmi,mask_above_rmi)
    
    return np.logical_or(mask_gmr,mask_rmi)
  
  else: # process seperately, for the single redshift
    if mask_Z: # low redshift; use gmr relation
      gmr,wth = gmr_line(Z,m_i,gmr_fit,warnings)
      mask_under_gmr = (m_g - m_r) > gmr - 2*wth
      mask_above_gmr = (m_g - m_r) < gmr + 2*wth
      return np.logical_and(mask_under_gmr,mask_above_gmr)
    else: # high redshift; use rmi relation
      rmi,wth = rmi_line(Z,m_i,rmi_fit,warnings)
      mask_under_rmi = (m_r - m_i) > rmi - 2*wth
      mask_above_rmi = (m_r - m_i) < rmi + 2*wth
      return np.logical_and(mask_under_rmi,mask_above_rmi)




