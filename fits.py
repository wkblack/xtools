#!/sw/lsa/centos7/python-anaconda2/created-20170424/bin/python
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Fits analysis program 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

print "Fits Analysis Program"
from time import time
from sys import stdout

print "Importing libraries...\r",
stdout.flush(); start = time() 

from astropy.io.fits import open as fitsopen
from os.path import isfile, join
from os import listdir
from sys import argv

end = time() 
print "Importing libraries took %g seconds" % (end-start) 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
if len(argv)>1: 
  files = argv[1:]
else: 
  # look at all files in current directory 
  fdir = '.' 
  files = [ f for f in listdir(fdir) if isfile(join(fdir,f)) ]
  print "files: %s\n" % files

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
for f in files: 
  try: 
    hdulist = fitsopen(f) 
  except IOError: 
    print "Error: couldn't read in %s" % f
    continue
  
  print
  hdulist.info() 
  print
  
  # keys don't seem to be too helpful... 
  # headers only show in interactive script
  for i in range(len(hdulist)): 
    print "list %i keys: %s" % (i,hdulist[i].header.keys())
  print
  
  # print "Column info:"
  # hdulist[1].columns.info()
  
  print "Column names: \n", hdulist[1].columns.names
  
  print "Sample data:" 
  h = hdulist[1].data[0]
  for i in range(min(15,len(h))): 
    print h[i],
  print "\n"
  
  hdulist.close() 
  print

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# print "Finished processing"
