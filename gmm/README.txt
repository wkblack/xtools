Core codes: 
  get_cm_ds.py  
   * used to get datasets for healpy galaxy pix8
  
  gmm_base.py  
   * the main code for the likelihood
   * includes analysis tools for plots
  
  run_gmm.py
   * the main code to run the MCMC
   * produces "errors" files, containing median +/- 1 sigma values

Supporting codes: 
  m_star_model.py  
    * contains 0.2L* method from Rykoff+14
  
  create_sky_gif.py  
    * creates gifs as linked to in paper 

  plot_cm.py  
    * used to plot ds; not used in paper
  
  global_vars.py  
    * stores a few global variables 
