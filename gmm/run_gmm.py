# test gmm with generated data 
import numpy as np

import gmm_base
import get_cm_ds as gcd
import global_vars # import summed pixels filename &al. vars

theta0 = gmm_base.theta_fake 

if 0: # generate fake data
  m_i,gmr = gmm_base.generate_data(N=1000)
  ds = gcd.data_to_ds(.2,m_i,gmr,printing=False)
  # FIXME: get actual redshift value!
elif 0: # use actual dataset, reading in individual files
  from global_vars import fname_pix_temp # pixel template name
  # processed pix8 galaxy file
  ds = np.load(fname_pix_temp % 0)
  for kk in range(1,192):
    ds += np.load(fname_pix_temp % kk)
else: # use pre-compiled dataset
  ds = np.load(global_vars.fname_total)

from matplotlib import pyplot as plt
from get_cm_ds import dz,z_bins,dmi,mi_bins,dgmr,gmr_bins

########################################################################
import emcee
from gmm_base import ln_prob
from gmm_base import plot_chain_progression as alice

from scipy.special import erf
one_sigma = sixty_eight = erf(1/np.sqrt(2))
two_sigma = ninety_five = erf(2/np.sqrt(2))

import sys
if len(sys.argv)>1: # read in from command line
  jj = int(sys.argv[1])
else: # hardcode
  jj = 7 # z bin
  print "Using hardcoded pix index"

if 1: # fix offset guesses based on min & max: 
  z = z_bins[jj]+dz/2.
  print "redshift %g" % z
  min_N = 10.**(min(z,.3)*2/.3+2)
  large_gmr = np.where(np.sum(ds[jj],axis=0)>min_N)
  min_gmr,max_gmr = gmr_bins[[np.min(large_gmr),np.max(large_gmr)]]
  print "min & max gmr w/ N>%i:" % min_N,min_gmr,max_gmr
  offset_guess = (max_gmr+min_gmr)/2.
  spread_guess = (max_gmr-min_gmr)/2.
  print 'guess:',offset_guess
  theta0 = np.array(theta0)
  theta0[2] = offset_guess
  theta0[4] = offset_guess

print 'Total galaxy count in ds[%i]: %i' % (jj,np.sum(ds[jj]))

nwalkers, ndim = 32,len(theta0)

walkers = theta0 * (1 + 1e-2*(np.random.randn(nwalkers,ndim)-.5))
sampler = emcee.EnsembleSampler(nwalkers,ndim,ln_prob,
                                args=(ds[jj],z_bins[jj]+dz/2.))
nsteps = 5000
sampler.run_mcmc(walkers,nsteps,progress=True)

# plot chain progression: 
samples = sampler.get_chain()
if 0: 
  theta_fit = alice(samples,gmm_base.labels,theta0) # plot input guess
else: 
  theta_fit = alice(samples,gmm_base.labels,None) # no red lines

theta_fit = [np.quantile(samples[-500:,:,ll],.5) for ll in range(ndim)]

# plot CM diagram
import plot_cm
plot_cm.cm_from_ds(ds,i=jj,theta=theta_fit)

# display corner plot
flat_samples = sampler.get_chain(discard=1250, flat=True) #thin=15, flat=True)
if 0: gmm_base.plot_corner(flat_samples)

print 
errs = gmm_base.errors(flat_samples)
from global_vars import fname_err_temp
np.save(fname_err_temp % jj,errs)

