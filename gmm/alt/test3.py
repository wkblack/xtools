# gaussian mixture model testing
from matplotlib import pyplot as plt
import corner
import numpy as np
import numpy.random as npr

from m_star_model import m_star_cutoff as mi_max

########################################################################
# describe likelihood function
########################################################################

def normal(theta,x): 
  mean,scatter = theta
  return (2*np.pi*scatter**2)**-.5 * np.exp(-(x-mean)**2/(2*scatter**2))

def line(theta,m_i,z):
  a,b,s = theta
  return (m_i-mi_max(z)) * a + b

def varying_normal(theta,m_i,gmr,z):
  a,b,s = theta
  # plt.plot(gmr,normal((line(theta,m_i,z),s),gmr)); plt.show() # test
  return normal((line(theta,m_i,z),s),gmr)

def likelihood(theta,m_i,gmr,z): 
  # unpack parameters from theta
  fR,ar,br,lgsr,ab,bb,lgsb = theta # lg = log_10
  # return likelihood
  return fR*varying_normal((ar,br,10.**lgsr),m_i,gmr,z) \
       + (1-fR)*varying_normal((ab,bb,10.**lgsb),m_i,gmr,z)

def ln_likelihood(theta,m_i,gmr,z):
  probs = likelihood(theta,m_i,gmr,z)
  if np.any(probs<=0):
    return -np.inf
  else:
    return np.sum(np.log(probs))

def ln_prior(theta):
  fR,ar,br,lgsr,ab,bb,lgsb = theta
  """
  if (fR<=1)*(fR>.5) \
        *(np.abs(ar)<.1)*(np.abs(ab)<.1) \
        *(bb>0)*(bb<br)*(br<3) \
        *(np.abs(br)<3)*(np.abs(bb)<3) \
        *(np.abs(lgsr)<4)*(np.abs(lgsb)<4)*(lgsb>lgsr):
  """
  if (fR<=1) * (fR>.5) \
   * (np.abs(ar)<1) * (np.abs(ab)<1) \
   * (np.abs(br)<4) * (np.abs(bb)<4) \
   * (np.abs(lgsr)<4) * (np.abs(lgsb)<4) * (lgsb>lgsr):
    return 0
  else: 
    return -np.inf

def log_probability(theta,m_i,gmr,z):
  return ln_prior(theta) + ln_likelihood(theta,m_i,gmr,z)

########################################################################
# generate fake test data (for real: use actual halo(s))
########################################################################

if 1: # fake 'true parameters'
  slope_red = -.03
  slope_blue = -.01
  offset_red = 1.2 # from max_i
  offset_blue = .5 # "
  scatter_red = .05
  scatter_blue = .25
  red_fraction = .8
  redshift = .2
  # ignore redshift evolution in this model; 
  # do delta z and delta mu bins
  
  theta_fake = (red_fraction,
                slope_red,offset_red,np.log10(scatter_red),
                slope_blue,offset_blue,np.log10(scatter_blue))

if 1: # generate data
  N = 200
  min_i,max_i = 16,mi_max(redshift)
  m_i = max_i - np.abs(npr.normal(0,(max_i-min_i)*.325,N))
  red_selection = npr.random(N)<red_fraction
  gmr = np.empty(N)
  line_red = (m_i[red_selection]-max_i) * slope_red + offset_red 
  gmr[red_selection] = npr.normal(line_red,scatter_red,sum(red_selection))
  line_blue = (m_i[~red_selection]-max_i) * slope_blue + offset_blue
  gmr[~red_selection] = npr.normal(line_blue,scatter_blue,sum(~red_selection))
  # TODO: use likelihood to do this
  if 0: 
    plt.scatter(m_i[red_selection],gmr[red_selection],c='r')
    plt.scatter(m_i[~red_selection],gmr[~red_selection],c='b')
    plt.xlabel(r'$m_i$')
    plt.ylabel(r'$g-r$')
    plt.tight_layout()
    plt.show()
  data = (m_i,gmr,redshift)

########################################################################
# run emcee
########################################################################
import emcee

# FIXME: below here was just copied from another code:
nwalkers, ndim = 32, 7
walkers = np.array(theta_fake)*(1 + 1e-1 * np.random.randn(nwalkers,ndim))

sampler = emcee.EnsembleSampler(nwalkers, ndim, log_probability, args=data)
nsteps = 1000
sampler.run_mcmc(walkers, nsteps, progress=True);

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# analyze mcmc chains
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

samples = sampler.get_chain()
labels = ['$f_R$', '$a_R$', '$b_R$', '$\log_{10} \sigma_R$', 
                   '$a_B$', '$b_B$', '$\log_{10} \sigma_B$']
# tau = sampler.get_autocorr_time()
# print "tau:",tau

if 1: # plot chain progression
  fig, axes = plt.subplots(ndim, figsize=(10, 7), sharex=True)
  
  for i in range(ndim):
      ax = axes[i]
      ax.plot(samples[:, :, i], "k", alpha=0.3)
      ax.set_xlim(0, len(samples))
      ax.set_ylabel(labels[i])
      ax.yaxis.set_label_coords(-0.1, 0.5)
      # plot actual
      ax.plot([0,nsteps],[theta_fake[i],theta_fake[i]],'r')
      # plot end median
      end_N = 200
      med = np.median(samples[-end_N:, :, i])
      x = np.ones((end_N,nwalkers))*med
      ax.plot([nsteps-1-end_N,nsteps-1],[med,med],'b')
  
  axes[-1].set_xlabel("step number")
  
  # plt.suptitle(r'Autocorrelation time: $\tau=%g$' % tau[0])
  plt.show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# analyze data

flat_samples = sampler.get_chain(discard=250, thin=15, flat=True)
if 0: # forge corner plot
  fig = corner.corner(flat_samples, labels=labels, truths=theta_fake);
  plt.tight_layout()
  plt.show()

if 1: # plot up fit
  medians = [np.percentile(flat_samples[:,i],50) for i in range(ndim)]
  fR_med,aR_med,bR_med,lgsR_med,aB_med,bB_med,lgsB_med = medians
  # plot points
  plt.scatter(m_i[red_selection],gmr[red_selection],c='r')
  plt.scatter(m_i[~red_selection],gmr[~red_selection],c='b')
  # plot fit
  mi_fits = np.linspace(min(m_i)-1,max_i+.1,10)
  blue_line = (mi_fits-max_i) * aB_med + bB_med
  plt.fill_between(mi_fits,blue_line - 10.**lgsB_med,
                           blue_line + 10.**lgsB_med,color='b',alpha=.125)
  plt.fill_between(mi_fits,blue_line - 2*10.**lgsB_med,
                           blue_line + 2*10.**lgsB_med,color='b',alpha=.125)
  red_line = (mi_fits-max_i) * aR_med + bR_med
  plt.fill_between(mi_fits,red_line - 2*10.**lgsR_med,
                           red_line + 2*10.**lgsR_med,color='r',alpha=.125)
  plt.fill_between(mi_fits,red_line - 10.**lgsR_med,
                           red_line + 10.**lgsR_med,color='r',alpha=.125)
  # fix plot
  plt.xlabel(r'$m_i$')
  plt.ylabel(r'$g-r$')
  plt.tight_layout()
  plt.show()
  

