# gaussian mixture model testing
from matplotlib import pyplot as plt
import numpy as np
import numpy.random as npr

########################################################################
# m*(z) function from Rykoff+14
########################################################################

RykoffEtAl2014 = {
  "p1":[22.44,3.36,.273,-0.0618,-0.0227], # z<=.5 coefficients
  "p2":[22.94,3.08,-11.22,-27.11,-18.02], # z>.5 coefficients
  "lims":[.05,.5,.7], # lowest,transition,highest bounds for piecewise
  "doi":"10.1088/0004-637X/785/2/104",
  "url":"https://iopscience.iop.org/article/10.1088/0004-637X/785/2/104/pdf"
}

def m_star_val(z,low=True,params=RykoffEtAl2014):
  # evaluate m* value for either high or low case
  p = params["p1"] if low else params["p2"]
  lnz = np.log(z)
  return p[0]+p[1]*lnz+p[2]*lnz**2+p[3]*lnz**3+p[4]*lnz**4

def m_star(z,params=RykoffEtAl2014,warnings=True):
  lims=params['lims']
  extrap_warn="WARNING: Extrapolating! [test2.m_star()]"
  low=z<=lims[1]
  if hasattr(z,'__len__') and len(z)>1:
    report = np.zeros(np.shape(z))
    report[low] = m_star_val(z[low],True,params)
    report[~low] = m_star_val(z[~low],False,params)
    if warnings and np.any(z<lims[0]) or np.any(z>lims[2]): 
      print extrap_warn
    return report
  else: # only one entry
    if low:
      if warnings and z<lims[0]: print extrap_warn
      return m_star_val(z,True,params)
    else:
      if warnings and z>lims[2]: print extrap_warn
      return m_star_val(z,False,params)

def L_star_shift(cutoff=0.2):
  # from definition of magnitude
  return -2.5*np.log10(cutoff)
  # where 0.4 -> 0.9949
  #   and 0.2 -> 1.7474


########################################################################
# describe likelihood function
########################################################################

def normal(theta,x): 
  mean,scatter = theta
  return (2*np.pi*scatter**2)**-.5 * np.exp(-(x-mean)**2/(2*scatter**2))

def m_i_max(z):
  return m_star(z) + L_star_shift()

def line(theta,m_i,z):
  a,b,s = theta
  return (m_i-m_i_max(z)) * a + b

def varying_normal(theta,m_i,gmr,z):
  a,b,s = theta
  # plt.plot(gmr,normal((line(theta,m_i,z),s),gmr)); plt.show() # test
  return normal((line(theta,m_i,z),s),gmr)

def likelihood(theta,m_i,gmr,z): 
  # unpack parameters from theta
  fR,R,B = theta
  # return likelihood
  return fR*varying_normal(R,m_i,gmr,z) + (1-fR)*varying_normal(B,m_i,gmr,z)

def prior(theta):
  fR,R,B = theta
  ar,br,sr = R
  ab,bb,sb = B
  return (fR<=1)*(fR>.25)*(sr>0)*(sb>0)

def log_probability(theta,m_i,gmr,z):
  if prior(theta)==0:
    return -np.inf()
  else:
    return np.log(likelihood(theta,m_i,gmr,z))

########################################################################
# generate fake test data (for real: use actual halo(s))
########################################################################

if 1: # fake 'true parameters'
  slope_red = -.03
  slope_blue = -.01
  offset_red = 1.2 # from max_i
  offset_blue = .5 # "
  scatter_red = .05
  scatter_blue = .25
  red_fraction = .8
  redshift = .2
  # ignore redshift evolution in this model; 
  # do delta z and delta mu bins
  
  R_fake = (slope_red,offset_red,scatter_red)
  B_fake = (slope_blue,offset_blue,scatter_blue)
  theta_fake = (red_fraction,R_fake,B_fake)

if 1: # generate data
  N = 200
  min_i,max_i = 16,22
  m_i = max_i - np.abs(npr.normal(0,(max_i-min_i)*.325,N))
  red_selection = npr.random(N)<red_fraction
  gmr = np.empty(N)
  line_red = (m_i[red_selection]-max_i) * slope_red + offset_red 
  gmr[red_selection] = npr.normal(line_red,scatter_red,sum(red_selection))
  line_blue = (m_i[~red_selection]-max_i) * slope_blue + offset_blue
  gmr[~red_selection] = npr.normal(line_blue,scatter_blue,sum(~red_selection))
  # TODO: use likelihood to do this
  if 0: 
    plt.scatter(m_i[red_selection],gmr[red_selection],c='r')
    plt.scatter(m_i[~red_selection],gmr[~red_selection],c='b')
    plt.show()
  data = (m_i,gmr,redshift)

########################################################################
# run emcee
########################################################################
import emcee

# FIXME: below here was just copied from another code:
nwalkers, ndim = 32, 3
print theta_fake
print "dimensional error---rewrite theta!"
raise SystemExit
walkers = np.array(theta_fake)*(1 + 1e-3 * np.random.randn(nwalkers,ndim))

sampler = emcee.EnsembleSampler(nwalkers, ndim, log_probability, args=data)
sampler.run_mcmc(walkers, 5000, progress=True);
