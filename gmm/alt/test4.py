# gaussian mixture model testing
# try simplifying
from matplotlib import pyplot as plt
import corner
import numpy as np
import numpy.random as npr

########################################################################
# m*(z) function from Rykoff+14
########################################################################

RykoffEtAl2014 = {
  "p1":[22.44,3.36,.273,-0.0618,-0.0227], # z<=.5 coefficients
  "p2":[22.94,3.08,-11.22,-27.11,-18.02], # z>.5 coefficients
  "lims":[.05,.5,.7], # lowest,transition,highest bounds for piecewise
  "doi":"10.1088/0004-637X/785/2/104",
  "url":"https://iopscience.iop.org/article/10.1088/0004-637X/785/2/104/pdf"
}

def m_star_val(z,low=True,params=RykoffEtAl2014):
  # evaluate m* value for either high or low case
  p = params["p1"] if low else params["p2"]
  lnz = np.log(z)
  return p[0]+p[1]*lnz+p[2]*lnz**2+p[3]*lnz**3+p[4]*lnz**4

def m_star(z,params=RykoffEtAl2014,warnings=True):
  lims=params['lims']
  extrap_warn="WARNING: Extrapolating! [test2.m_star()]"
  low=z<=lims[1]
  if hasattr(z,'__len__') and len(z)>1:
    report = np.zeros(np.shape(z))
    report[low] = m_star_val(z[low],True,params)
    report[~low] = m_star_val(z[~low],False,params)
    if warnings and np.any(z<lims[0]) or np.any(z>lims[2]): 
      print extrap_warn
    return report
  else: # only one entry
    if low:
      if warnings and z<lims[0]: print extrap_warn
      return m_star_val(z,True,params)
    else:
      if warnings and z>lims[2]: print extrap_warn
      return m_star_val(z,False,params)

def L_star_shift(cutoff=0.2):
  # from definition of magnitude
  return -2.5*np.log10(cutoff)
  # where 0.4 -> 0.9949
  #   and 0.2 -> 1.7474


########################################################################
# describe likelihood function
########################################################################

def normal(theta,x): 
  mean,scatter = theta
  return (2*np.pi*scatter**2)**-.5 * np.exp(-(x-mean)**2/(2.*scatter**2))

def ln_normal(theta,x):
  mean,scatter = theta
  return -.5*np.log(2*np.pi*scatter**2) - (x-mean)**2/(2.*scatter**2)

def m_i_max(z):
  return m_star(z) + L_star_shift()

def line(theta,m_i,z):
  a,b,s = theta
  return (m_i-m_i_max(z)) * a + b

def varying_normal(theta,m_i,gmr,z):
  a,b,s = theta
  # plt.plot(gmr,normal((line(theta,m_i,z),s),gmr)); plt.show() # test
  return normal((line(theta,m_i,z),s),gmr)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

def likelihood(theta,m_i,gmr,z): 
  # unpack parameters from theta
  fR,br,lgsr,bb,lgsb = theta # lg = log_10
  # return likelihood
  return fR*normal((br,10.**lgsr),gmr) \
       + (1-fR)*normal((bb,10.**lgsb),gmr)
  # TODO: hidden z-variable to determine to which gaussian m_i + gmr should draw!

"""
def ln_likelihood(theta,m_i,gmr,z):
  # unpack parameters from theta
  fR,br,lgsr,bb,lgsb = theta # lg = log_10
  # return likelihood
  return fR*ln_normal((br,10.**lgsr),gmr) \
       + (1-fR)*ln_normal((bb,10.**lgsb),gmr)
"""

def prior(theta):
  fR,br,lgsr,bb,lgsb = theta
  return (fR<=1)*(fR>.5) \
        *(bb>0)*(bb<br)*(br<3) \
        *(np.abs(br)<3)*(np.abs(bb)<3) \
        *(np.abs(lgsr)<4)*(np.abs(lgsb)<4)*(lgsb>lgsr)

"""
def log_probability(theta,m_i,gmr,z):
  if prior(theta)==0:
    return -np.inf*np.ones(np.shape(gmr))
  return np.log(likelihood(theta,m_i,gmr,z))
"""

def log_probability(theta,m_i,gmr,z):
  if prior(theta)==0:
    return -np.inf
  else:
    return np.sum(likelihood(theta,m_i,gmr,z))

########################################################################
# generate fake test data (for real: use actual halo(s))
########################################################################

if 1: # fake 'true parameters'
  slope_red = -.03
  slope_blue = -.01
  offset_red = 1.2 # from max_i
  offset_blue = .5 # "
  scatter_red = .05
  scatter_blue = .25
  red_fraction = .8
  redshift = .2
  # ignore redshift evolution in this model; 
  # do delta z and delta mu bins
  
  theta_fake = (red_fraction,
                offset_red,np.log10(scatter_red),
                offset_blue,np.log10(scatter_blue))

if 1: # generate data
  N = 100
  min_i,max_i = 16,22
  m_i = max_i - np.abs(npr.normal(0,(max_i-min_i)*.325,N))
  red_selection = npr.random(N)<red_fraction
  gmr = np.empty(N)
  line_red = (m_i[red_selection]-max_i) * slope_red + offset_red 
  gmr[red_selection] = npr.normal(line_red,scatter_red,sum(red_selection))
  line_blue = (m_i[~red_selection]-max_i) * slope_blue + offset_blue
  gmr[~red_selection] = npr.normal(line_blue,scatter_blue,sum(~red_selection))
  # TODO: use likelihood to do this
  if 0: 
    plt.scatter(m_i[red_selection],gmr[red_selection],c='r')
    plt.scatter(m_i[~red_selection],gmr[~red_selection],c='b')
    plt.show()
    raise SystemExit
  data = (m_i,gmr,redshift)

########################################################################
# run emcee
########################################################################
import emcee

nwalkers, ndim = 32, 5
walkers = np.array(theta_fake)*(1 + 1e-1 * np.random.randn(nwalkers,ndim))

sampler = emcee.EnsembleSampler(nwalkers, ndim, log_probability, args=data)
nsteps = 5000
sampler.run_mcmc(walkers, nsteps, progress=True);

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# analyze mcmc chains
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# tau = sampler.get_autocorr_time()

samples = sampler.get_chain()
labels = ['$f_R$', '$b_R$', '$\log_{10} \sigma_R$', 
                   '$b_B$', '$\log_{10} \sigma_B$']

if 1: # plot chain progression
  fig, axes = plt.subplots(ndim, figsize=(10, 7), sharex=True)
  
  for i in range(ndim):
      ax = axes[i]
      ax.plot(samples[:, :, i], "k", alpha=0.3)
      ax.set_xlim(0, len(samples))
      ax.set_ylabel(labels[i])
      ax.yaxis.set_label_coords(-0.1, 0.5)
      # plot actual
      ax.plot([0,nsteps],[theta_fake[i],theta_fake[i]],'r')
      # plot end median
      med = np.median(samples[-100:, :, i])
      x = np.ones((100,32))*med
      ax.plot([nsteps-101,nsteps-1],[med,med],'b')
  
  axes[-1].set_xlabel("step number")
  
  # plt.suptitle(r'Autocorrelation time: $\tau=%g$' % tau)
  plt.show()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# analyze data

if 1: # forge corner plot
  flat_samples = sampler.get_chain(discard=100, thin=15, flat=True)
  fig = corner.corner(
      flat_samples, labels=labels, truths=theta_fake
  );
  plt.tight_layout()
  plt.show()
