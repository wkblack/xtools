import numpy as np
from matplotlib import pyplot as plt
from get_cm_ds import dz,z_bins,dmi,mi_bins,dgmr,gmr_bins

ii = 1 # processed pix8 galaxy file
ds = np.load('CM_ds_%i.npy' % ii)

########################################################################
import emcee
import gmm_base
from gmm_base import ln_prob,labels
from gmm_base import plot_chain_progression as alice


theta0 = np.array([.7,.05,1.1,-1,0,.6,2])
nwalkers, ndim = 32,7

jj=1 # redshift bin
walkers = theta0 * (1+1e-1*(np.random.randn(nwalkers,ndim)-.5))
sampler = emcee.EnsembleSampler(nwalkers,ndim,ln_prob,
                                args=(ds[jj],z_bins[jj]+dz/2.))
nsteps = 1000
sampler.run_mcmc(walkers,nsteps,progress=True)

samples = sampler.get_chain()
if 1: # show chain progression
  alice(samples,gmm_base.labels,theta0)

flat_samples = sampler.get_chain(discard=250, thin=15, flat=True)
if 0: # forge corner plot
  fig = corner.corner(flat_samples, labels=labels, truths=theta_fake);
  plt.tight_layout()
  plt.show()
