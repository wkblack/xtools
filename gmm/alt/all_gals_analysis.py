import numpy as np
from matplotlib import pyplot as plt

z=[.1,.1,.2,.2,.3,.3]

########################################################################

aR=[-0.017834,-0.0175966,-0.0258033,-0.025825,-0.0379012,-0.0369188]
aRplus=[0.000882518,0.00383311,0.000534784,0.000551018,0.0202758,0.000881193]
aRmin=[0.000660434,0.0120632,0.000663988,0.00056749,0.0292029,0.000667078]
aRbars=[aRmin,aRplus]

plt.errorbar(z,aR,aRbars,capsize=5,fmt='.',ls="",c='k')
plt.plot([0,1],[-.003,-.003-0.075],color='r') # Hao's result
plt.xlim(.05,.35)
plt.xlabel(r'Redshift $z$')
plt.ylabel(r'Ridgeline slope $a_R$')
plt.tight_layout()
plt.show()

########################################################################

bR=[0.861802,0.862061,1.12728,1.12743,1.40133,1.40096]
bRplus=[0.00107543,0.00472804,0.000862256,0.000705055,0.286222,0.000908882]
bRmin=[0.00089734,2.93633,0.000688612,0.000707562,0.0292575,0.00156369]
bRbars=[bRmin,bRplus]

plt.errorbar(z,bR,bRbars,capsize=5,fmt='.',ls="",c='k')
plt.plot([0,1],[.623,.623+3.049],color='r') # Hao's result
plt.xlim(.05,.35)
plt.xlabel(r'Redshift $z$')
plt.ylabel(r'Zero point $b_R$')
plt.tight_layout()
plt.show()

########################################################################

sR=[-1.31054,-1.3126,-1.18353,-1.18406,-1.07582,-1.07437]
sRplus=[0.00490217,2.36965,0.00236915,0.00248512,0.0157644,0.00171462]
sRmin=[0.00495322,0.0250971,0.00289892,0.00251424,0.504727,0.0117351]
sRbars=[sRmin,sRplus]

plt.errorbar(z,sR,sRbars,capsize=5,fmt='.',ls="",c='k')
plt.plot([0,1],[.037,.037+.136],color='r') # Hao's result
plt.xlim(.05,.35)
plt.xlabel(r'Redshift $z$')
plt.ylabel(r'Scatter $s_R$')
plt.tight_layout()
plt.show()
