# gaussian mixture model testing
from matplotlib import pyplot as plt
import numpy as np
import numpy.random as npr

########################################################################
# generate fake test data (for real: use actual halo(s))
########################################################################

# fake 'true' parameters
slope_red = -.03
slope_blue = -.01
offset_red = 1.2 # from max_i
offset_blue = .5 # "
scatter_red = .05
scatter_blue = .25
red_fraction = .8

R_fake = (slope_red,0,offset_red,0,scatter_red,0)
B_fake = (slope_blue,0,offset_blue,0,scatter_blue,0)
theta_fake = red_fraction,R_fake,B_fake
             
# no redshift evolution

min_i,max_i = 16,22

# generate data
N = 200
# m_i = npr.random(N)*(max_i-min_i)+min_i
m_i = max_i - np.abs(npr.normal(0,(max_i-min_i)*.325,N))

red_selection = npr.random(N)<red_fraction
gmr = np.empty(np.shape(m_i))
line_red = (m_i[red_selection]-max_i) * slope_red + offset_red 
gmr[red_selection] = npr.normal(line_red,scatter_red,sum(red_selection))
line_blue = (m_i[~red_selection]-max_i) * slope_blue + offset_blue
gmr[~red_selection] = npr.normal(line_blue,scatter_blue,sum(~red_selection))


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def plot_random_input():
  # plot data
  plt.scatter(m_i[red_selection],gmr[red_selection],c='r')
  plt.scatter(m_i[~red_selection],gmr[~red_selection],c='b')
  mm = np.array([min_i,max_i])
  if 1: # plot input
    plt.fill_between(mm,offset_red+slope_red*(mm-max_i) - scatter_red,
                        offset_red+slope_red*(mm-max_i) + scatter_red,
                        color='r',alpha=.125)
    plt.fill_between(mm,offset_red+slope_red*(mm-max_i) - 2*scatter_red,
                        offset_red+slope_red*(mm-max_i) + 2*scatter_red,
                        color='r',alpha=.125)
    plt.fill_between(mm,offset_blue+slope_blue*(mm-max_i) - scatter_blue,
                        offset_blue+slope_blue*(mm-max_i) + scatter_blue,
                        color='b',alpha=.125)
    plt.fill_between(mm,offset_blue+slope_blue*(mm-max_i) - 2*scatter_blue,
                        offset_blue+slope_blue*(mm-max_i) + 2*scatter_blue,
                        color='b',alpha=.125)
  plt.xlabel(r'$m_i$')
  plt.ylabel(r'$g-r$')
  plt.xlim(min_i,max_i)
  plt.tight_layout()
  plt.show()

########################################################################
# craft probability function

def normal(theta,x): 
  mean,scatter = theta
  return (2*np.pi*scatter**2)**-.5 * np.exp(-(x-mean)**2/(2*scatter**2))

def m_i_max(z):
  # 0.2L* cut
  return 22 # FIXME: grab from : import galaxy_id as gid; 

def line(theta,m_i,z):
  a0,a1,b0,b1,s0,s1 = theta
  a = a0+a1*z
  b = b0+b1*z
  return (m_i-m_i_max(z)) * a + b

def varying_normal(theta,m_i,gmr,z):
  a0,a1,b0,b1,s0,s1 = theta
  s = s0+s1*z
  # plt.plot(gmr,normal((line(theta,m_i,z),s),gmr)); plt.show() # test
  return normal((line(theta,m_i,z),s),gmr)

def likelihood(theta,m_i,gmr,z): 
  # unpack parameters from theta
  fR,R,B = theta
  # return likelihood
  return fR*varying_normal(R,m_i,gmr,z) + (1-fR)*varying_normal(B,m_i,gmr,z)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# test likelihood

m_i_vals = np.linspace(16,22,1000)
gmr_vals = np.linspace(-.2,1.8,1000)
m_i_v,gmr_v = np.meshgrid(m_i_vals,gmr_vals,sparse=True)

if 1: 
  Llhood = likelihood(theta_fake,m_i_v,gmr_v,0)
  h = plt.contourf(m_i_vals,gmr_vals,np.log10(Llhood))
  plt.xlabel(r'$m_i$')
  plt.ylabel(r'$g-r$')
  plt.xlim(min_i,max_i)
  plt.tight_layout()
  plt.colorbar()
  plt.show()

if 0:
  # plot along constant m_i
  Llhood = likelihood(theta_fake,18,gmr_vals,0)
  # linear plot
  plt.subplot(211)
  plt.plot(gmr_vals,Llhood)
  plt.xlabel(r'$g-r$')
  plt.ylabel(r'$\mathcal{L}$')
  # log plot
  plt.subplot(212)
  plt.plot(gmr_vals,Llhood)
  plt.xlabel(r'$g-r$')
  plt.ylabel(r'$\mathcal{L}$')
  plt.yscale('log')
  # display
  plt.tight_layout()
  plt.show()

########################################################################
# test emcee!


