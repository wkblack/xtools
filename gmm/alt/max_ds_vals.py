import numpy as np
from matplotlib import pyplot as plt
import global_vars

ds_tot = np.load(global_vars.fname_total)

def min_max_gmr(ds=ds_tot,index=0): 
  # return min and max gmr for a given ds redshift slice (index)
  dz = ds[index]
  return 0,2.5
