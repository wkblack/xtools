from matplotlib import pyplot as plt
import corner
import numpy as np
import numpy.random as npr

def normal(theta,x):
  mu,sig = theta
  return np.exp(-.5*(x-mu)**2/sig**2)/np.sqrt(2*np.pi*sig**2)

if 0: # test normal
  xx = np.linspace(-5,5,100)
  theta = (1,2)
  yy = normal(theta,xx)
  plt.plot(xx,yy)
  plt.show()

# generate some data
theta_true = (.75,3,2,5,.1)
fR,muR,sR,muB,sB = theta_true

N=500
gmr = np.linspace(0,10,N)
seed = npr.random(N)
data = (seed<=fR) * npr.normal(muR,sR,N)\
     + (seed>fR) * npr.normal(muB,sB,N)

if 0: # plot
  plt.hist(data,bins=20)
  plt.show()

def ln_likelihood(theta,data,dummy):
  fR,muR,lgsR,muB,lgsB = theta
  probs = (fR)*normal((muR,10.**lgsR),data) \
        + (1-fR)*normal((muB,10.**lgsB),data)
  if np.any(probs<=0):
    return -np.inf
  else:
    # print "out:", np.sum(np.log(probs))
    return np.sum(np.log(probs))

def ln_prior(theta):
  fR,muR,lgsR,muB,lgsB = theta
  if fR<=1 and fR>0 and np.abs(muR)<20 and np.abs(muB)<20 \
      and np.abs(lgsR)<5 and np.abs(lgsB)<5:
    return 0
  else:
    return -np.inf

def ln_prob(theta,data,dummy):
  # print "out2:", ln_prior(theta) + ln_likelihood(theta,data,dummy)
  return ln_prior(theta) + ln_likelihood(theta,data,dummy)

########################################################################
# run emcee
########################################################################
import emcee

nwalkers = 16
ndim = 5
theta_start = np.array((fR,muR,np.log10(sR),\
                           muB,np.log10(sB)))
walkers = theta_start*(1+npr.randn(nwalkers,ndim))
sampler = emcee.EnsembleSampler(nwalkers,ndim,ln_prob,args=(data,0))
nsteps = 5000
sampler.run_mcmc(walkers,nsteps,progress=True)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# analyze MCMC
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

samples = sampler.get_chain()
labels = ['$f_R$', '$b_R$', '$\log_{10} \sigma_R$', 
                   '$b_B$', '$\log_{10} \sigma_B$']

if 1: # plot chain progression
  fig, axes = plt.subplots(ndim, figsize=(10, 7), sharex=True)
  
  for i in range(ndim):
      ax = axes[i]
      ax.plot(samples[:, :, i], "k", alpha=0.3)
      ax.set_xlim(0, len(samples))
      ax.set_ylabel(labels[i])
      ax.yaxis.set_label_coords(-0.1, 0.5)
      # plot actual
      ax.plot([0,nsteps],[theta_start[i],theta_start[i]],'r')
      # plot end median
      med = np.median(samples[-100:, :, i])
      x = np.ones((100,nwalkers))*med
      ax.plot([nsteps-101,nsteps-1],[med,med],'b')
  
  axes[-1].set_xlabel("step number")
  
  # plt.suptitle(r'Autocorrelation time: $\tau=%g$' % tau)
  plt.show()
