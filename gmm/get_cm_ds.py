import numpy as np
import sys
xtools = '/global/homes/w/wkblack/xproject/xtools'
sys.path.append(xtools)
from config import Buzzard as sim
from astropy.io import fits
gf = sim['galaxies']

from m_star_model import m_star_cutoff as mi_max # good from .05 to .7

def vet_data(data,printing=True):
  # MaxBCG constraints: 
  #  * velocity dispersion > 400 km/s
  #  - >10 RS galaxies?
  #  - brighter than 0.4L*? 
  if printing: print "data len before vetting:",len(data)
  data = data[np.logical_and(data.Z>.05,data.Z<.7)]
  if printing: print "data len after z|(.05,.7) vet:",len(data)
  data = data[data.TMAG[:,2]<mi_max(data.Z)]
  if printing: print "data len after 0.2L* vet:",len(data)
  data = data[data.M200>10.**13]
  if printing: print "data len after mass vet:",len(data)
  return data

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# turn into report

# bin up by redshift
# dz = .05
dz = .01
# z_bins = np.arange(.1,.7+dz,dz)
z_bins = np.arange(.05,.7+dz,dz)

# m_i bins
dmi = .0625
# mi_bins = np.arange(14.5,21+dmi,dmi)
mi_bins = np.arange(13.5,23.1+dmi,dmi)

# (g-r) bins
# dgmr = .0375
dgmr = .04 # not sure that's better...
# gmr_bins = np.arange(-.3,2.1+dgmr,dgmr)
gmr_bins = np.arange(-.25,2.5+dgmr,dgmr)

# output template
from global_vars import fname_pix_temp
CM_report_template = np.zeros((len(z_bins),len(mi_bins),len(gmr_bins)))



def data_to_ds(Z,m_i,gmr,printing=True):
  """
  input: data vector of equal lengths: {m_i, g-r, Z}
  output: ds[Z,m_i,gmr] (as counts)
  """
  if not hasattr(Z,'__len__'): # correct for single redshift bin (many zeros!)
    Z = np.ones(len(m_i))*Z
  
  L = len(Z)
  assert len(m_i)==L
  assert len(gmr)==L
  
  report = np.copy(CM_report_template)
  
  z_subset = np.zeros((len(z_bins)-1,L))
  for i in range(len(z_bins)-1):
    z_subset[i] = np.logical_and(z_bins[i]<=Z,Z<z_bins[i+1])
    # print 'z bin sum:',np.sum(z_subset[i])
  
  mi_subset = np.zeros((len(mi_bins)-1,L))
  for j in range(len(mi_bins)-1):
    mi_subset[j] = np.logical_and(mi_bins[j]<=m_i,m_i<mi_bins[j+1])
    # print 'mi bin sum:',sum(mi_subset[j])
  
  gmr_subset = np.zeros((len(gmr_bins)-1,L))
  for k in range(len(gmr_bins)-1):
    gmr_subset[k] = np.logical_and(gmr_bins[k]<=gmr,gmr<gmr_bins[k+1])
    # print 'gmr bin sum:',sum(gmr_subset[k])
  
  for i in range(len(z_bins)-1):
    if printing:
      print "in z-bin [%g,%g)" % (z_bins[i],z_bins[i]+dz)
    for j in range(len(mi_bins)-1):
      for k in range(len(gmr_bins)-1):
        report[i,j,k] += len(Z[(z_subset[i]*mi_subset[j]\
                                   *gmr_subset[k]).astype(bool)])
  return report




def fits_to_ds(data,printing=True):
  report = np.copy(CM_report_template)
  mi = data.TMAG[:,2]
  gmr = data.TMAG[:,0] - data.TMAG[:,1]
  if printing: 
    print "gmr range:",min(gmr),max(gmr)
  Z = data.Z
  return data_to_ds(Z,mi,gmr,printing)

import sys

if __name__=='__main__':
  max_pix8 = 192 # total number of pix8 in quarter of sky
  if len(sys.argv)==2: # read in single value
    ivals = range(int(sys.argv[1]),max_pix8)
  elif len(sys.argv)==3:
    i_min,i_max = int(sys.argv[1]),int(sys.argv[2])
    print "scanning values ii|[%i,%i)" % (i_min,i_max)
    ivals = range(i_min,i_max)
  else:
    print "using hardcoded values (len=%i)" % len(sys.argv)
    i_min = 31
    i_max = max_pix8
    ivals = range(i_min,i_max)
    print "scanning values ii|[%i,%i)" % (i_min,i_max)
  
  for ii in ivals:
    print "Processing ii=%i..." % ii
    fname_CM_ds = fname_pix_temp % ii
    data = fits.open(gf[ii])[1].data
    data = vet_data(data)
    report = fits_to_ds(data,False)
    print "Saving..."
    np.save(fname_CM_ds,report)
  
  print 'fin'
  raise SystemExit
  
