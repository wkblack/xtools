# gaussian mixture model testing
from matplotlib import pyplot as plt
import corner
import numpy as np
import numpy.random as npr

from m_star_model import m_star_cutoff as mi_max

########################################################################
# describe likelihood function
########################################################################

labels = ['$f_R$', '$a_R$', '$b_R$', '$\log_{10} \sigma_R$', 
                            '$b_B$', '$\log_{10} \sigma_B$']

def normal(theta,x): 
  mean,scatter = theta
  return (2*np.pi*scatter**2)**-.5 * np.exp(-(x-mean)**2/(2*scatter**2))

def line(theta,m_i,z):
  a,b,s = theta
  return (m_i-mi_max(z)) * a + b

def varying_normal(theta,m_i,gmr,z):
  a,b,s = theta
  # plt.plot(gmr,normal((line(theta,m_i,z),s),gmr)); plt.show() # test
  return normal((line(theta,m_i,z),s),gmr)

def likelihood(theta,m_i,gmr,z): 
  # unpack parameters from theta
  fR,ar,br,lgsr,bb,lgsb = theta # lg = log_10
  # return likelihood
  return fR*varying_normal((ar,br,10.**lgsr),m_i,gmr,z) \
       + (1-fR)*varying_normal((0,bb,10.**lgsb),m_i,gmr,z)

def ln_likelihood(theta,m_i,gmr,z,multiplicity=1):
  probs = likelihood(theta,m_i,gmr,z)
  if np.any(probs<=0):
    return -np.inf
  else:
    if 0:
      plt.figure(figsize=(10,5))
      plt.subplot(121)
      plt.imshow(multiplicity)
      plt.subplot(122)
      plt.imshow(np.log(probs))
      plt.tight_layout()
      plt.show()
    return np.sum(multiplicity*np.log(probs))

def ln_prior(theta,min_gmr=0,max_gmr=2.5): # TODO: add gmr prior?
  fR,ar,br,lgsr,bb,lgsb = theta
  if (fR<=1) * (fR>.05) * (np.abs(ar)<.5) \
    * (bb>min_gmr) * (bb>.4) * (br>bb) * (br<max_gmr) * (br<2.25)\
    * (lgsr<0) * (lgsr>-2) * (lgsb<0) * (lgsb>-2):
    return 0
  else: 
    return -np.inf

def log_probability(theta,m_i,gmr,z):
  return ln_prior(theta) + ln_likelihood(theta,m_i,gmr,z)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# likelihood using ds
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
from get_cm_ds import dz,z_bins,dmi,mi_bins,dgmr,gmr_bins

mi_ds = np.array([[mi_bins[i] for j in range(len(gmr_bins))]
                              for i in range(len(mi_bins))])

gmr_ds = np.array([[gmr_bins[j] for j in range(len(gmr_bins))]
                                for i in range(len(mi_bins))])

def ln_likelihood_ds(theta,ds,z):
  return ln_likelihood(theta,mi_ds,gmr_ds,z,ds)

def ln_prob(theta,ds,z):
  min_N = 10.**(min(z,.3)*2/.3+2)
  # print "min N: %g" % min_N
  large_gmr = np.where(np.sum(ds,axis=0)>min_N)
  min_gmr,max_gmr = gmr_bins[[np.min(large_gmr),np.max(large_gmr)]]
  # print "min & max gmr:",min_gmr,max_gmr
  return ln_prior(theta,min_gmr,max_gmr) + ln_likelihood_ds(theta,ds,z)

########################################################################
# plotting for mcmc

def plot_chain_progression(samples,labels,theta=None,end_N=200):
  nsteps,nwalkers,ndim = np.shape(samples)
  fig, axes = plt.subplots(ndim, figsize=(10, 7), sharex=True)
  medians = np.zeros(ndim)
  
  for i in range(ndim):
      ax = axes[i]
      ax.plot(samples[:, :, i], "k", alpha=0.3)
      ax.set_xlim(0,nsteps)
      ax.set_ylabel(labels[i])
      ax.yaxis.set_label_coords(-0.1, 0.5)
      if np.all(theta!=None): # plot actual
        ax.plot([0,nsteps],[theta[i],theta[i]],'r')
      if end_N>0: # plot end median for last end_N points
        med = np.median(samples[-end_N:, :, i])
        x = np.ones((end_N,nwalkers))*med
        ax.plot([nsteps-1-end_N,nsteps-1],[med,med],'b')
        medians[i] = med
  
  axes[-1].set_xlabel("step number")
  
  plt.show()
  return medians

def plot_corner(flat_samples,labels=labels,theta=None,size=(10,7)):
  fig = corner.corner(flat_samples, labels=labels, truths=theta,
                      quantiles = [.16,.50,.84], show_titles=True,
                      verbose=True);
  # TODO: figure out how to use label_kwargs={'labelpad':50}, 
  fig = plt.gcf()
  fig.set_size_inches(10,7)
  plt.tight_layout()
  fig.subplots_adjust(hspace=.25,wspace=.5,right=.961)
  plt.show()
  
########################################################################
from scipy.special import erf

one_sigma = sixty_eight = erf(1/np.sqrt(2))
two_sigma = ninety_five = erf(2/np.sqrt(2))

def errors(flat_samples,printing=True):
  ndim = len(flat_samples[0,:])
  chains = [flat_samples[:,ii] for ii in range(ndim)]
  report = np.zeros((ndim,3))
  
  for kk,chain in enumerate(chains):
    bot,median,top = np.quantile(chain,.5+np.array([-1,0,+1])*one_sigma/2)
    sm1,sp1 = median-bot,top-median
    if printing: 
      print "%s = %g^{+%g}_{-%g}$" % (labels[kk][:-1],median,sp1,sm1)
      if sm1/median > .1 or sp1/median > .1:
        print "WARNING: large errors!"
    report[kk,:] = median,sm1,sp1
    
  return report

########################################################################
# plot up median values and errors, read from files

from global_vars import fname_err_temp,fname_err_glob
from glob import glob
import re

all_labels = [r'red fraction $f_R$',r'Slope $a_R$',r'Offset $b_R$',
              r'Scatter $\log_{10} \sigma_R$',r'Offset $b_B$', 
              r'Scatter $\log_{10} \sigma_B$'] # y-axis

red_labels = [r'Slope $a_R$',r'Offset $b_R$',
              r'Scatter $\log_{10} \sigma_R$'] # y-axis
H09 = [[-.003,-0.075], [.623,3.049], [.037,.136]]
H09_sig = [[.001,.005],[.002,.011],[.002,.010]]

def plot_values(file_nums=None):
  if file_nums==None:
    out_files = sorted(glob(fname_err_glob))
    file_nums = [int(re.findall(r'\d+',fname)[0]) for fname in out_files]
    # print "file nums:",file_nums
  
  z_v = np.array([.1,.3]) # H09 range
  Z_vals = np.zeros(len(file_nums))
  R = np.zeros(len(file_nums))
  Rplus = np.zeros(len(file_nums))
  Rmin = np.zeros(len(file_nums))
  
  for ii in range(1,4): # for each red parameter
    # plot H09 results
    plt.xlabel(r'Redshift $z$')
    plt.ylabel(all_labels[ii])
    # plt.plot([0,1],H09[ii-1],color='r')
    b,a = H09[ii-1]
    b_sig,a_sig = H09_sig[ii-1]
    
    if ii==3: # log it
      plt.plot(z_v,np.log10(b+a*z_v),'r')
      plt.fill_between(z_v,np.log10((b-b_sig)+(a-a_sig)*z_v),
                           np.log10((b+b_sig)+(a+a_sig)*z_v),
                       alpha=.25,linewidth=0,color='r')
    else:
      plt.plot(z_v,b+a*z_v,'r')
      plt.fill_between(z_v,((b-b_sig)+(a-a_sig)*z_v),
                           ((b+b_sig)+(a+a_sig)*z_v),
                       alpha=.25,linewidth=0,color='r')
    
    for jj,fnum in enumerate(file_nums): # plot point from each file
      Z = z_bins[fnum]
      dsv = np.load(fname_err_temp % fnum)
      med,sm1,sp1 = dsv[ii]
      Z_vals[jj] = Z; R[jj] = med; Rmin[jj] = sm1; Rplus[jj] = sp1
      
    plt.xlim(0,max(Z_vals)+.05)
    plt.errorbar(Z_vals,R,[Rmin,Rplus],capsize=2,fmt='.',ls="",c='k')
    plt.tight_layout()
    plt.show()
  
  pass


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

blue_labels = [r'red fraction $f_R$',r'Offset $b_B$',
               r'Scatter $\log_{10} \sigma_B$'] # vertical-axis

def plot_values_blue():
  out_files = sorted(glob(fname_err_glob))
  file_nums = [int(re.findall(r'\d+',fname)[0]) for fname in out_files]
  
  z_v = np.array([.1,.3]) # H09 range
  Z_vals = np.zeros(len(file_nums))
  R = np.zeros(len(file_nums))
  Rplus = np.zeros(len(file_nums))
  Rmin = np.zeros(len(file_nums))
  
  for ii in [0,4,5]: # for each red parameter
    plt.xlabel(r'Redshift $z$')
    plt.ylabel(all_labels[ii])
    for jj,fnum in enumerate(file_nums): # plot point from each file
      Z = z_bins[fnum]
      dsv = np.load(fname_err_temp % fnum)
      med,sm1,sp1 = dsv[ii]
      Z_vals[jj] = Z; R[jj] = med; Rmin[jj] = sm1; Rplus[jj] = sp1
      
    plt.xlim(0,max(Z_vals)+.05)
    plt.errorbar(Z_vals,R,[Rmin,Rplus],capsize=2,fmt='.',ls="",c='k')
    plt.tight_layout()
    plt.show()



########################################################################
# generate data

# some temporary parameters
slope_red = -.03
slope_blue = -.01
offset_red = 1.2 # from max_i
offset_blue = .5 # "
scatter_red = .05
scatter_blue = .25
red_fraction = .6
redshift = .2
# ignore redshift evolution in this model; 
# do delta z and delta mu bins

theta_fake = (red_fraction,
              slope_red,offset_red,np.log10(scatter_red),
                         offset_blue,np.log10(scatter_blue))
  

def generate_data(theta=theta_fake,redshift=redshift,N=200):
  # TODO: use likelihood to do this
  fR,ar,br,lgsr,bb,lgsb = theta
  min_i,max_i = 16,mi_max(redshift)
  m_i = max_i - np.abs(npr.normal(0,(max_i-min_i)*.325,N))
  red_selection = npr.random(N)<fR
  gmr = np.empty(N)
  line_red = (m_i[red_selection]-max_i) * ar + br 
  gmr[red_selection] = npr.normal(line_red,10.**lgsr,sum(red_selection))
  line_blue = (m_i[~red_selection]-max_i) * 0 + bb
  gmr[~red_selection] = npr.normal(line_blue,10.**lgsb,sum(~red_selection))
  return m_i,gmr


########################################################################
# main method, testing emcee

if __name__=='__main__':
  ######################################################################
  # generate fake test data (for real: use actual halo(s))
  ######################################################################
  
  m_i,gmr = generate_data(theta_fake,redshift,N=200)
  data = (m_i,gmr,redshift)
  
  if 0: 
    plt.scatter(m_i[red_selection],gmr[red_selection],c='r')
    plt.scatter(m_i[~red_selection],gmr[~red_selection],c='b')
    plt.xlabel(r'$m_i$')
    plt.ylabel(r'$g-r$')
    plt.tight_layout()
    plt.show()
  
  ######################################################################
  # run emcee
  ######################################################################
  import emcee
  
  # FIXME: below here was just copied from another code:
  nwalkers, ndim = 32, 7
  walkers = np.array(theta_fake)*(1 + 1e-1 * np.random.randn(nwalkers,ndim))
  
  sampler = emcee.EnsembleSampler(nwalkers, ndim, log_probability, args=data)
  nsteps = 1000
  sampler.run_mcmc(walkers, nsteps, progress=True);
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # analyze mcmc chains
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  
  samples = sampler.get_chain()
  # tau = sampler.get_autocorr_time()
  # print "tau:",tau

  plot_chain_progression(samples,labels,theta_fake,end_N=200)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # analyze data
  
  flat_samples = sampler.get_chain(discard=250, thin=15, flat=True)
  if 0: # forge corner plot
    fig = corner.corner(flat_samples, labels=labels, truths=theta_fake);
    plt.tight_layout()
    plt.show()
  
  if 1: # plot up fit
    medians = [np.percentile(flat_samples[:,i],50) for i in range(ndim)]
    fR_med,aR_med,bR_med,lgsR_med,aB_med,bB_med,lgsB_med = medians
    # plot points
    plt.scatter(m_i[red_selection],gmr[red_selection],c='r')
    plt.scatter(m_i[~red_selection],gmr[~red_selection],c='b')
    # plot fit
    mi_fits = np.linspace(min(m_i)-1,max_i+.1,10)
    blue_line = (mi_fits-max_i) * aB_med + bB_med
    plt.fill_between(mi_fits,blue_line - 10.**lgsB_med,
                             blue_line + 10.**lgsB_med,color='b',alpha=.125)
    plt.fill_between(mi_fits,blue_line - 2*10.**lgsB_med,
                             blue_line + 2*10.**lgsB_med,color='b',alpha=.125)
    red_line = (mi_fits-max_i) * aR_med + bR_med
    plt.fill_between(mi_fits,red_line - 2*10.**lgsR_med,
                             red_line + 2*10.**lgsR_med,color='r',alpha=.125)
    plt.fill_between(mi_fits,red_line - 10.**lgsR_med,
                             red_line + 10.**lgsR_med,color='r',alpha=.125)
    # fix plot
    plt.xlabel(r'$m_i$')
    plt.ylabel(r'$g-r$')
    plt.tight_layout()
    plt.show()
