import sys
xtools = '/global/homes/w/wkblack/xproject/xtools'
sys.path.append(xtools)
import numpy as np

from m_star_model import m_star_cutoff
from matplotlib import pyplot as plt
from astropy.io import fits
from config import Buzzard

from matplotlib import cm
cw = cm.get_cmap('coolwarm')

def read_gal_file(index,sim=Buzzard):
  gf = sim['galaxies']
  return fits.open(gf[index])[1].data

def plot_CM(mi,gmr,c='k'):
  plt.scatter(mi,gmr,1,c=c)
  plt.xlabel(r'$m_i$')
  plt.ylabel(r'$g-r$')
  # plt.tight_layout()

def dz_gif(data,Dz,start_z=.05,end_z=.7,keep=True,trim=False):
  if trim: # remove start and end if null
    min_z,max_z = min(data.Z),max(data.Z)
    start_z,end_z = max(start_z,min_z),min(end_z,max_z)
  
  Z = data.Z
  mi = data.TMAG[:,2]
  gmr = data.TMAG[:,0] - data.TMAG[:,1]
  # z_curr = start_z
  z_vals = np.arange(start_z,end_z,Dz)
  colors = cw((z_vals-start_z)/(end_z-start_z))
  plt.ion() # turn interactive mode on
  for ii,z_curr in enumerate(z_vals):
  # while z_curr+Dz<end_z: 
    if keep:
      plt.title(r'$z|[%g,%g)$' % (start_z,z_curr+Dz))
    else:
      plt.title(r'$z|[%g,%g)$' % (z_curr,z_curr+Dz))
    mi_cutoff = m_star_cutoff(z_curr+Dz/2.)
    mask = (Z>=z_curr) * (Z<z_curr+Dz) * (mi<mi_cutoff)
    plot_CM(mi[mask],gmr[mask],colors[ii]) # mi<mi_cutoff)
    plt.xlim(14,23)
    plt.ylim(-.5,2.5)
    # z_curr += Dz
    if 1:
      print 'saving file no %i' % ii
      plt.savefig("out_%05i.png" % ii)
    else: 
      plt.draw()
      plt.pause(.0001)
    if not keep:
      plt.clf()

if __name__=='__main__':
  data = read_gal_file(0)
  dz_gif(data,.01,keep=False)
