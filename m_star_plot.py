from matplotlib import pyplot as plt
import numpy as np
import h5py

########################################################################
# read in files

pars_tags = ['phi1','phi2','alpha1','alpha2','m*'] # unpack pars
if 1: # read in from last generated dataset
  fname = 'm_fit_pars.h5'
  with h5py.File(fname,'r') as ds:
    Z_bins,Z_mids = [ds[tag][:] for tag in ['Z_bins','Z_mids']]
    phi1,phi2,alpha1,alpha2,m_star = [ds[tag][:] for tag in pars_tags]
  dZ = Z_bins[1:] - Z_bins[:-1]
else: # read in from segmented list
  from glob import glob
  fnames = glob('m_fit_pars*.h5')
  # fnames = ['m_fit_pars_lowZ.h5','m_fit_pars_midZ.h5','m_fit_pars_topZ.h5']
  Z_bins,Z_mids,phi1,phi2,alpha1,alpha2,m_star,dZ = np.empty((8,0))
  for ii in range(len(fnames)):
    with h5py.File(fnames[ii],'r') as ds:
      Z_bii,Z_mii = [ds[tag][:] for tag in ['Z_bins','Z_mids']]
      Z_bins = np.append(Z_bins,Z_bii)
      Z_mids = np.append(Z_mids,Z_mii)
      p1,p2,a1,a2,ms = [ds[tag][:] for tag in pars_tags]
      phi1 = np.append(phi1,p1)
      phi2 = np.append(phi2,p2)
      alpha1 = np.append(alpha1,a1)
      alpha2 = np.append(alpha2,a2)
      m_star = np.append(m_star,ms)
      dZ = np.append(dZ,Z_bii[1:] - Z_bii[:-1])


########################################################################
# function calls

def fit__m_star(loglog=False,display=True,fit_order=7):
  # initial semilog / log-log fitting
  x,y = Z_mids[m_star>0],m_star[m_star>0]
  lx,ly = np.log([x,y])
  Z1 = np.polyfit(lx,ly if loglog else y,1,
                  w=np.ones(np.shape(x)) if loglog else 1/(1+x))
  Y1 = np.poly1d(Z1)(lx)
  if loglog:
    Y1 = np.exp(Y1)
  R1 = y - Y1 # remainder form first fit
  
  if display:
    x_vals = np.linspace(min(Z_mids),max(Z_mids),1000)
    l_xv = np.log(x_vals)
    fig,axs = plt.subplots(2,1,gridspec_kw={'height_ratios': [1.62, 1]},sharex=True)
    # plot fit created
    axs[0].scatter(x,y,10)
    axs[0].set_ylim(axs[0].get_ylim())
    y_fit = np.poly1d(Z1)(l_xv)
    if loglog:
      y_fit = np.exp(y_fit)
    axs[0].plot(x_vals,y_fit)
    axs[0].set_ylabel(r'$m_*$')
    # plot remainders
    axs[1].scatter(x,R1)
    axs[1].plot(x_vals,0*x_vals,'k')
    axs[1].set_ylabel(r'remainder')
    axs[1].set_xlim(x_vals[0],x_vals[-1])
    axs[1].set_xlabel(r'Redshift')
    plt.tight_layout()
    plt.show()

  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # correction of remainders
  Z2 = np.polyfit(x,R1,fit_order)
  R2 = R1 - np.poly1d(Z2)(x)
  
  if display:
    fig,axs = plt.subplots(2,1,gridspec_kw={'height_ratios': [1.62, 1]},sharex=True)
    # plot fit created
    axs[0].scatter(x,R1,10)
    axs[0].set_ylim(axs[0].get_ylim())
    y_fit = np.poly1d(Z2)(x_vals)
    axs[0].plot(x_vals,y_fit)
    axs[0].set_ylabel(r'$m_*$ - fit 1')
    # plot remainders
    axs[1].scatter(x,R2)
    axs[1].plot(x_vals,0*x_vals,'k')
    axs[1].set_ylabel(r'remainder')
    axs[1].set_xlim(x_vals[0],x_vals[-1])
    axs[1].set_xlabel(r'Redshift')
    plt.tight_layout()
    plt.show()
  
  pct = 100*(1-np.sum(R2**2)/np.sum(R1**2))
  print(f"Improvement ~ {pct:0.3g}%")
  
  return Z1,Z2


def plot_phi():
  plt.scatter(Z_mids,phi1/dZ,10,label=r'$\phi_1/\Delta z$')
  plt.scatter(Z_mids,phi2/dZ,10,label=r'$\phi_2/\Delta z$')
  plt.xlabel('Redshift')
  plt.xlim(1e-3,2.35)
  plt.xscale('log')
  plt.yscale('log')
  plt.ylim(.1,2*np.max([phi1/dZ,phi2/dZ]))
  plt.legend()
  plt.tight_layout()
  plt.show()


def plot_alpha():
  plt.scatter(Z_mids,alpha1,10,label=r'$\alpha_1$')
  plt.scatter(Z_mids,alpha2,10,label=r'$\alpha_2$')
  plt.xlabel('Redshift')
  plt.legend()
  plt.tight_layout()
  plt.show()


if 0:
  plt.scatter(Z_mids,m_star,10)
  plt.xlabel('Redshift')
  plt.ylabel(r'$m_*$')
  plt.tight_layout()
  plt.show()
elif __name__=='__main__':
  plot_phi()
  plot_alpha()
  Z1,Z2 = fit__m_star(loglog=False,display=True)
  print(Z1,Z2)
else:
  print("Loaded!") 
