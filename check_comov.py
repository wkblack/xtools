import config as cfg
from astropy.io import fits

hf = cfg.Buzzard['halos']
print "reading in halo file"
hdul = fits.open(hf[0])
data = hdul[1].data
print "data retrieved. fields:" , data.names

dv = data[(data.M200C>10.**13.5)*(data.M200C<10.**13.505)]
print "data vet 1 length:", len(dv)
# dvv = dv[dv.M200C<10.**13.5025]
dvv = data[(data.M200C>10.**14.5)*(data.M200C<10.**14.6)]
print "data vet 2 length:", len(dvv)
dvvv = data[(data.M200C>10.**13.0)*(data.M200C<10.**13.005)]
print "data vet 3 length:", len(dvvv)

from matplotlib import pyplot as plt
print "plotting..."

yplot="RVIR"
# yplot="RS"
plt.scatter(dv.Z_COS,dv[yplot],marker='.',label='dv: (13.5,13.505)')
plt.scatter(dvv.Z_COS,dvv[yplot],marker='.',label='dvv: (14.5,14.6)')
plt.scatter(dvvv.Z_COS,dvvv[yplot],marker='.',label='dvvv: (13,13.005)')
import numpy as np
zz = np.linspace(0,2.5,100)
# plt.plot(zz,50./(1+zz),'r',label='1/(1+z)')
for val in [700,1600,480]:
# for val in [80,300,65]:
  plt.plot(zz,val*(1+zz),label='(1+z)')
plt.legend()
plt.yscale('log')
plt.xlabel('Z')
plt.ylabel(yplot)
plt.tight_layout()
plt.show()

"""
import seaborn as sns
sns.jointplot(dvvv["Z_COS"],np.log10(dvvv["RS"]),kind='reg')
plt.plot(zz,np.log10(50*(1+zz)))
plt.show()
"""
