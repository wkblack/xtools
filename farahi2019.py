#!/bin/python
# plotter for the three datasets: Chandra (1), XMM (2), and Aardvark (3)
# I should probably give it a more logical name... xD
# but I got the data from Arya after looking at his 2019 paper, 
# so there's a _bit_ of logic here. :) 

########################################################################
# file declarations 
from os.path import isfile

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# dirs: 
import config as cfg
filedir = cfg.xtools + 'Farahi2019DES/'

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# files
filename1='chandra_rm_DESY1_ss2.csv' 
assert(isfile(filedir+filename1))
filename2='xmm_rm_DESY1_ss2.csv' 
assert(isfile(filedir+filename2))
filename3 = 'output_bicycle.csv'
assert(isfile(cfg.xtools+filename3))
# rm_fname = 'RM_training_fixed.csv' # unneeded, since S1,S2 in bicycle
# assert(isfile(cfg.xtools+rm_fname))



########################################################################
import numpy as np
# 1) Chandra
data1=np.loadtxt(filedir + filename1,delimiter=',',skiprows=1)
[tx1_err,lx1_err,lx1_bol_err,z1,lambda1,
 tx1,lx1,lx1_bol,lambda1_err,lambda1_true,
 lambda1_true_err,serendipity1] = [data1[:,i] for i in range(12)]
mask1 = np.logical_and(tx1>0,lx1>0)

# 2) XMM
data2=np.loadtxt(filedir + filename2,delimiter=',',skiprows=1)
[tx2,tx2_err,lx2_bol,lx2_bol_m,lx2_bol_p,
 z2,lambda2,lambda2_err,lambda2_true,lambda2_true_err,
 lim_exptime2] = [data2[:,i] for i in range(11)]

# 3) Aardvark
data3=np.loadtxt(cfg.xtools + filename3,delimiter=',',skiprows=1)
[HID1,HID2,STR1,STR2,lambda3,z3,CENTRAL_FLAG,CENTRAL_BCG_FLAG,RA,DEC,
 lambda3_err,m1_3,m2_3,lx3,tx3,fx3] = [data3[:,i] for i in range(16)]
CEN = np.logical_and(CENTRAL_FLAG,CENTRAL_BCG_FLAG)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# scale XMM temperature to align with Chandra's

correct_XMM_Tx = True
plot_XMM_Tx_correction = False

FarahiEtAl2019 = { # parameters
  'a':1.0133,
  'b':0.1008,
  'url':'http://arxiv.org/abs/1903.08042'
}

def align_XMM_Tx_to_Chandra((Tx,err_tx),params=FarahiEtAl2019): 
  # equation (1) of Farahi &al 2019
  # shifts Tx in XMM sample to the Tx of the Chandra sample
  # return 10**(params['a']*np.log10(Tx)+params['b'])
  tx_shift = 10**params['b']*Tx**params['a']
  err_tx *= tx_shift*params['a']/Tx
  return (tx_shift,err_tx*tx_shift*params['a']/Tx)

if correct_XMM_Tx: # correct XMM temperatures
  if plot_XMM_Tx_correction:
    from matplotlib import pyplot as plt
    plt.scatter(tx2,tx2_err)
  tx2,tx2_err = align_XMM_Tx_to_Chandra((tx2,tx2_err))
  # tx2_err = align_XMM_Tx_to_Chandra(tx2_err)
  if plot_XMM_Tx_correction: 
    plt.scatter(tx2,tx2_err)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$T_X$')
    plt.ylabel(r'error in/$T_X$')
    plt.show()
    raise SystemExit

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# check data ranges

if 0: # print diagnostics and exit
  print "redshit max//min: %g//%g" % (min(z3),max(z3))
  print "lambda max//min: %g//%g" % (min(lambda3),max(lambda3))
  raise SystemExit
else: # check limits
  assert(min(lambda3)>20) 
  assert(min(z3)>.1 and max(z3)<.3) 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set redshift limits

zmask1 = np.logical_and(.1<z1[mask1],z1[mask1]<.3)
zmask2 = np.logical_and(.1<z2,z2<.3)
zmask3 = np.logical_and(.1<z3,z3<.3)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set strength limits

if 1: 
  limit=0.8
  mask3_str = (STR1-STR2)>=limit
  mask3_str_label=r'Aardvark $(S_1-S_2>%g)$' % limit
elif 0: 
  limit=0.1
  mask3_str = (STR1-STR2)<limit
  mask3_str_label=r'Aardvark $(S_1-S_2<%g)$' % limit
else: 
  limit=1
  mask3_str = (STR1-STR2)<limit # all
  mask3_str_label=r'Aardvark'

########################################################################
# derived variables 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# signal to noise ratio
# RSD = relative std dev = coef. of variation c_v = std.dev./mean
# SNR = signal to noise ratio = RSD^-2

# Chandra
rsd_lx1=lx1_err/lx1 # relative std.dev. (RSD) / coeff of variation (c_v)
snr_lx1=rsd_lx1**-2 # signal to noise ratio (SNR) 
rsd_tx1=tx1_err/tx1 # relative std.dev. (RSD) / coeff of variation (c_v)
snr_tx1=rsd_tx1**-2 # signal to noise ratio (SNR) 

# XMM (only bolometric Lx included) 
lx2_bol_err = (lx2_bol_p-lx2_bol_m)/2.
rsd_lx2_bol=lx2_bol_err/lx2_bol # relative std.dev. (RSD) / coeff of variation (c_v)
snr_lx2_bol=rsd_lx2_bol**-2 # signal to noise ratio (SNR) 
rsd_tx2=tx2_err/tx2 # relative std.dev. (RSD) / coeff of variation (c_v)
snr_tx2=rsd_tx2**-2 # signal to noise ratio (SNR) 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# flux
from bicycle import Fx_calc
fx1 = Fx_calc(lx1[mask1],tx1[mask1],z1[mask1]).astype(float)
fx2 = Fx_calc(lx2_bol*10**44,tx2,z2).astype(float)

keV_in_erg = 624150647.99632 # conversion factor for french datacube

########################################################################
from matplotlib import pyplot as plt
print 'Completed loading'

from cosmology import r # cosmology dependent; default Aardvark
from cosmology import to_degrees
from cosmology import theta_R_lambda

def find_nearest(array, value, count):
  array = np.asarray(array)
  return np.argsort(np.abs(array-value))[:count]


########################################################################
# plotting functions


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def plot_fx_vs_lambda(color_by='dataset',mask_redshift=False,factor=1):
  plt.title(r'Flux vs Richness')
  
  if mask_redshift: 
    # zmin,zmax = .2,.3
    zmin,zmax = .15,.3
    print "Plotting subset: z|(%g,%g)" % (zmin,zmax)
  else: 
    zmin,zmax = 0,.7
    print "Plotting z|(%g,%g)" % (zmin,zmax)
  zmask1 = np.logical_and(zmin<z1[mask1],z1[mask1]<zmax)
  zmask2 = np.logical_and(zmin<z2,z2<zmax)
  zmask3 = np.logical_and(zmin<z3,z3<zmax)
  if sum(zmask1)*sum(zmask2)*sum(zmask3)<1: 
    print "WARNING: null mask!"
  
  from cosmology import E
  # use powers of the evolution factor to scale redshift evolution
  # unscale aardvark?
  if factor=='redshift': 
    E_slope_Lx,E_slope_Tx = 1.20,0.61
    power = E_slope_Lx+E_slope_Tx
    if mask_redshift:
      factor1 = E(z1[mask1][zmask1])**power
      factor2 = E(z2[zmask2])**power
      factor3 = E(z3[zmask3])**power
    else:
      factor1 = E(z1[mask1])**power
      factor2 = E(z2)**power
      factor3 = E(z3)**power
  else: 
    factor1,factor2,factor3 = factor,factor,factor
  
  
  if color_by=='dataset':
    plt.scatter(lambda3[zmask3],fx3[zmask3]/factor3,marker='.',label=r'Aardvark',c='#16161d')
    plt.scatter(lambda2[zmask2],fx2[zmask2]/factor2,label=r'XMM',c='red')
    plt.scatter(lambda1[mask1][zmask1],fx1[zmask1]/factor1,label=r'Chandra',c='purple')
    plt.legend(loc='lower right') 
  elif color_by=='redshift':
    mapc = 'coolwarm'
    plt.scatter(lambda3[zmask3],fx3[zmask3]/factor3,c=z3[zmask3],cmap=mapc,marker='.')
    plt.clim(zmin,zmax)
    plt.scatter(lambda2[zmask2],fx2[zmask2]/factor2,c=z2[zmask2],cmap=mapc,edgecolor='k')
    plt.clim(zmin,zmax)
    plt.scatter(lambda1[mask1][zmask1],fx1[zmask1]/factor1,c=z1[mask1][zmask1],cmap=mapc,edgecolor='k')
    plt.clim(zmin,zmax)
    plt.colorbar(label=r'Redshift $z$') 
  elif color_by=='temperature':
    mapc = 'coolwarm'
    txmin,txmax=-1,1
    plt.scatter(lambda3[zmask3],fx3[zmask3]/factor3,
                c=np.log10(tx3[zmask3]),cmap=mapc,marker='.')
    plt.clim(txmin,txmax)
    plt.scatter(lambda2[zmask2],fx2[zmask2]/factor2,
                c=np.log10(tx2[zmask2]),cmap=mapc,edgecolor='k')
    plt.clim(txmin,txmax)
    plt.scatter(lambda1[mask1][zmask1],fx1[zmask1]/factor1,
                c=np.log10(tx1[mask1][zmask1]),cmap=mapc,edgecolor='k')
    plt.clim(txmin,txmax)
    plt.colorbar(label=r'$\log_{10}(T_X/{\rm keV})$') 
  else: 
    print "ERROR: invalid coloring key (%s)" % color_by
    raise SystemExit
  
  if 0: # print some ranges: 
    print 'min fx C/X/A :',min(fx1),min(fx2),min(fx3)
    print 'max fx C/X/A :',max(fx1),max(fx2),max(fx3)
    print 'min z C/X/A :',min(z1),min(z2),min(z3)
    print 'max z C/X/A :',max(z1),max(z2),max(z3)
  
  plt.xscale('log') 
  plt.xlabel(r'Richness $\lambda$')
  
  plt.yscale('log') 
  # plt.ylim(1e-7/factor,1e-2/factor) 
  if factor!='redshift': 
    plt.ylabel(r'Flux $f_X$ (ergs/s/cm$^2$)')
    plt.ylim(6e-16/factor,6e-12/factor) 
  else: 
    plt.ylabel(r'$f_X/E(z)^{%g}$ (ergs/s/cm$^2$)' % power)
    plt.ylim(2e-17,2e-12) 
  
  plt.tight_layout()
  plt.show() 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def plot_LxTx_vs_z(): # plot lx & tx v z
  plt.scatter(z1[mask1],lx1[mask1],c='blue',label=r'Chandra Lx') # around 2e44
  plt.scatter(z1[mask1],lx1_bol[mask1],c='yellow',label=r'Chandra Lx$_{\rm bol}$') # around 8e44
  plt.scatter(z2,lx2_bol*10**44,c='green',label=r'XMM Lx$_{\rm bol}$') # around 1e44 
  plt.yscale('log') 
  plt.ylim(3e42,3e46)
  plt.xlabel(r'redshift $z$')
  plt.ylabel(r'luminosity $L_X$')
  plt.legend()
  plt.show() 
  
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def plot_SNR(vs): 
  if vs in {'redshift','z'}: # plot SNR vs redshift
    plt.title(r'Signal-to-Noise Ratio vs Redshift') 
    # axes: 
    x1 = z1[mask1]
    x2 = z2
    plt.xlabel(r'redshift $z$')
    out_fname = 'snr_z.png'
  elif vs in {'flux','fx','Fx'}:
    plt.title(r'Signal-to-Noise Ratio vs Flux')
    x1 = fx1
    x2 = fx2
    plt.xlabel(r'flux $f_x$')
    plt.xscale('log')
    out_fname = 'snr_fx.png'
  else: 
    print "Unrecognized input. Exiting..."
    return
  
  plt.ylabel(r'SNR')
  plt.ylim(1e0,1e5)
  plt.yscale('log') 
  plt.grid(axis='y')
  
  # data and markers: 
  plt.scatter(x1,snr_lx1[mask1],s=72,c='yellow',label=r'$L_X$',
              alpha=.5,edgecolors=None)
  plt.scatter(x1,snr_lx1[mask1],s=18,c='black',marker=r'$c$')
  plt.scatter(x1,snr_tx1[mask1],s=72,c='red',label=r'$T_X$',
              alpha=.5,edgecolors=None)
  plt.scatter(x1,snr_tx1[mask1],s=18,c='black',marker=r'$c$')
  
  plt.scatter(x2,snr_lx2_bol,s=72,c='yellow',
              alpha=.5,edgecolors=None)
  plt.scatter(x2,snr_lx2_bol,s=18,c='black',marker=r'$x$')
  plt.scatter(x2,snr_tx2,s=72,c='red',
              alpha=.5,edgecolors=None)
  plt.scatter(x2,snr_tx2,s=18,c='black',marker=r'$x$')
  
  plt.legend(loc='lower left') 
  plt.savefig(out_fname)
  plt.show() 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def plot_SNR_texp(): # plot vs exposure time
  x2 = lim_exptime2
  
  # Lx
  plt.scatter(x2,snr_lx2_bol,s=72,c='yellow',label=r'$L_X$',
              alpha=.5,edgecolors=None)
  plt.scatter(x2,snr_lx2_bol,s=18,c='black',marker=r'$x$')
  
  # Tx
  plt.scatter(x2,snr_tx2,s=72,c='red',label=r'$T_X$',
              alpha=.5,edgecolors=None)
  plt.scatter(x2,snr_tx2,s=18,c='black',marker=r'$x$')
  
  plt.xlabel(r'Exposure Time $t_{\rm exp}$')
  plt.xscale('log') 
  plt.xlim(8e1,1e3)
  
  plt.ylabel(r'Signal-to-Noise Ratio (SNR)')
  plt.yscale('log') 
  plt.ylim(6e-1,2e5)
  
  plt.legend() 
  plt.tight_layout()
  plt.show() 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def plot_SNR_serendipity(): # plot vs serendipity
  plt.title('Chandra Serendipity')
  seren = serendipity1[mask1].astype(int)   
  
  if 0: # vs flux
    x1 = fx1
    plt.xlabel(r'Flux $f_X$ ($10^{44}$ ergs/s/cm$^2$)')
    plt.xscale('log') 
    plt.xlim(1.5e-5,1.5e-3)
  else: # vs redshift
    x1 = z1[mask1]
    plt.xlabel(r'Redshift $z$')
  
  # Lx 
  plt.scatter(x1,snr_lx1[mask1],s=72,c='yellow',label=r'$L_X$',
              alpha=.5,edgecolors=None)
  plt.scatter(x1[seren],snr_lx1[seren],s=18,c='black',marker=r'$s$')
  
  # Tx
  plt.scatter(x1,snr_tx1[mask1],s=72,c='red',label=r'$T_X$',
              alpha=.5,edgecolors=None)
  plt.scatter(x1[seren],snr_tx1[seren],s=18,c='black',marker=r'$s$')
  
  plt.ylabel(r'Signal-to-Noise Ratio (SNR)')
  plt.yscale('log') 
  plt.ylim(1e0,1e5)
  
  plt.legend() 
  plt.tight_layout()
  plt.show() 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def plot_RSD(): # relative standard deviation
  plt.title(r'RSD for $T_X$ (orange) and $L_X$ (yellow) in Chandra')
  plt.scatter(z1[mask1],rsd_lx1[mask1],c='yellow')
  plt.scatter(z1[mask1],rsd_tx1[mask1],c='orange')
  plt.xlabel(r'redshift $z$')
  plt.ylabel(r'RSD')
  plt.yscale('log') 
  plt.ylim(5e-3,5e-1)
  plt.show() 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def plot_Tx_vs_lambda(cen=True,label_IDs=False): # plot Tx vs richness
  # plt.scatter(lambda3[~CEN],tx3[~CEN],marker='.',c='r')
  plt.errorbar(lambda1[mask1],tx1[mask1],
               xerr=lambda1_err[mask1],yerr=tx1_err[mask1],fmt='o',c='r',alpha=.5)
  plt.errorbar(lambda2,tx2,xerr=lambda2_err,yerr=tx2_err,fmt='o',c='b',alpha=.5)
  
  if cen: 
    x = lambda3[CEN]
    y = tx3[CEN]
    colors = STR1[CEN]
    ids = HID1[CEN]
  else: 
    x = lambda3
    y = tx3
    colors = STR1
    ids = HID1
  
  p3 = plt.scatter(x,y,marker='.',c=colors)
  plt.xscale('log')
  plt.yscale('log')
  plt.xlabel(r'Richness $\lambda$')
  plt.ylabel(r'Temperature $T_X$ (keV)')
  plt.ylim(.1,20)
  plt.legend(['Aardvark','Chandra','XMM'],loc='lower right')
  plt.colorbar(p3,label=r'Strength $S_1$')
  
  if label_IDs: 
    for i,txt in enumerate(ids):
      plt.annotate(int(txt), (x[i], y[i])) 
  
  plt.tight_layout() 
  plt.show() 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def plot_lx_bol_XMM(): # plot lx_bol for XMM
  if 1: # plot error bars 
    plt.errorbar(z2,lx2_bol,np.array([lx2_bol-lx2_bol_m,
                                      lx2_bol_p-lx2_bol]),fmt='.')
  elif 0: # plot lower lims individually
    plt.scatter(z2,lx2_bol_m)
    plt.scatter(z2,lx2_bol)
    plt.scatter(z2,lx2_bol_p)
  else: # ? I don't understand uplims/lolims
    plt.errorbar(z2,lx2_bol,lolims=lx2_bol_m,uplims=lx2_bol_p,fmt='.')
  
  plt.xlabel('redshift') 
  plt.ylabel('luminosity (bolometric)') 
  plt.yscale('log') 
  plt.show() 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def str_test(): 
  if 1: 
    plt.scatter(STR1,STR2,c=z3)
    plt.colorbar(cmap='coolwarm')
    if 0: # log scale
      plt.xscale('log')
      plt.yscale('log')
      plt.ylim(2e-4,.5)
      plt.show()
      raise SystemExit
    
    plt.xlabel(r'$S_1$')
    plt.ylabel(r'$S_2$')
    plt.plot([0,.5],[0,.5],c='k')
    plt.plot([.5,1],[.5,0],c='k')
    plt.plot([.4,.8],[.4,0],c='gray',ls='--')
    plt.tight_layout() 
    plt.show() 
  
  if 0: # how many clusters are dominated by a single halo? 
    print "count of clusters with majority S1 :",sum(STR1>.5)
    print "percentage : %0.2g%%" % (sum(STR1>.5)*100.0/len(STR1))
    print "count of clusters with majority (S1+S2) :",sum(STR1+STR2>.5)
    print "percentage : %0.2g%%" % (sum(STR1+STR2>.5)*100.0/len(STR1))
  
  if 0: 
    plt.hist(STR1-STR2)
    plt.show() 
  
  if 1:
    plt.scatter(lambda3[mask3_str],fx3[mask3_str],marker='.',
                label=mask3_str_label,c='#16161d')
    plt.scatter(lambda2,fx2,label=r'XMM',c='red')
    plt.scatter(lambda1[mask1],fx1,label=r'Chandra',c='purple')
    
    plt.xscale('log') 
    plt.xlabel(r'Richness $\lambda$')
    plt.yscale('log') 
    plt.ylim(8e-16,8e-12)
    plt.ylabel(r'Flux $f_X$ (ergs/s/cm$^2$)')
    
    plt.legend() #loc='lower right') 
    plt.tight_layout() 
    plt.show() 



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def str_test_2(plot_s1s2=False,plot_fx=False,plot_tx=False,plot_lx=False,
               plot_M=False,binary=True,cen=True,fitlines=False): 
  print "Using S1 limit of %g" % limit
  if plot_s1s2: # plot triangle S1 vs S2 relation
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    if cen: 
      plt.scatter(STR1[CEN],STR2[CEN],c=z3[CEN],cmap='coolwarm')
    else: 
      plt.scatter(STR1,STR2,c=z3,cmap='coolwarm')
    plt.xlabel(r'$S_1$') 
    plt.ylabel(r'$S_2$') 
    # plot bounding lines
    plt.axes().set_aspect('equal')
    plt.plot([0,.5],[0,.5],c='k'); plt.ylim(0,.5)
    plt.plot([.5,1],[.5,0],c='k'); plt.xlim(0,1.)
    plt.plot([limit,(1+limit)/2],[0,(1-limit)/2],c='gray',ls='--')
    # plt.plot([0,(1-limit)/2.],[limit,(1+limit)/2],c='gray',ls='--')
    # add tight colorbar
    ax1=plt.gca() # get current axis to return to it later
    divider = make_axes_locatable(ax1)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    plt.colorbar(label=r'Redshift $z$',cax=cax) 
    plt.sca(ax1) # return to first axis
    # display 
    plt.tight_layout()
    plt.show() 
  
  if plot_fx: # plot the corresponding fluxes
    y1=fx1
    y2=fx2
    y3=fx3
    plt.ylim(7e-16,7e-12)
    plt.ylabel(r'Flux $f_X$ (ergs/s/cm$^2$)')
  elif plot_lx: # plot the corresponding luminosities
    y1=lx1[mask1]
    y2=lx2_bol*1e44
    y3=lx3
    # plt.ylim(7e38,1.5e47)
    plt.ylabel(r'Luminosity (ergs/s)')
  elif plot_tx: # plot the corresponding luminosities
    y1=tx1[mask1]
    y2=tx2
    y3=tx3
    # plt.ylim(2,19)
    plt.ylabel(r'Temperature (keV)')
  
  if plot_fx or plot_lx or plot_tx:
    if cen: 
      strmask3=np.logical_and(mask3_str,CEN)
      antimask=np.logical_and(~mask3_str,CEN)
    else: 
      strmask3=mask3_str
      antimask=~mask3_str
    
    if binary: 
      plt.scatter(lambda3[antimask],y3[antimask],marker='.',
                  label=mask3_str_label,c='#16161d')
      plt.scatter(lambda3[strmask3],y3[strmask3],marker='.',
                  label='Aardvark $S_1-S_2\geq0.8$',c='lime')
    else: 
      if cen: 
        plt.scatter(lambda3[CEN],y3[CEN],marker='.',
                    label='Aardvark',c=STR1[CEN])
      else: 
        plt.scatter(lambda3,y3,marker='.',label='Aardvark',c=STR1)
    
    zmask1 = np.logical_and(.1<z1[mask1],z1[mask1]<.3)
    zmask2 = np.logical_and(.1<z2,z2<.3)
    plt.scatter(lambda2[zmask2],y2[zmask2],label=r'XMM',c='purple',edgecolor='k')
    plt.scatter(lambda1[mask1][zmask1],y1[zmask1],label=r'Chandra',c='red',edgecolor='k')
    # plt.errorbar(lambda1[mask1],tx1[mask1],
                 # xerr=lambda1_err[mask1],yerr=tx1_err[mask1],fmt='o',c='r',alpha=.5)
    # plt.errorbar(lambda2,tx2,xerr=lambda2_err,yerr=tx2_err,fmt='o',c='b',alpha=.5)
    
    if fitlines: 
      # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
      # draw a polynomial fit for the Aardvark data
      lnx = np.log(lambda3); lny = np.log(y3); xs = np.sort(lambda3)
      p = np.polyfit(lnx,lny,1,w=STR1)
      plt.plot(xs,np.exp(p[1])*xs**p[0],c='lime',label="Aa")
      # and the XMM + Chandra input
      zmask = np.logical_and(.1<np.concatenate([z1[mask1],z2]),
                                np.concatenate([z1[mask1],z2])<.3)
      lnx_XC = np.log(np.concatenate([lambda1[mask1],lambda2]))
      xs_XC = np.sort(np.concatenate([lambda1[mask1],lambda2]))
      lny_XC = np.log(np.concatenate([y1,y2]))
      p2 = np.polyfit(lnx_XC[zmask],lny_XC[zmask],1) 
      plt.plot(xs_XC,np.exp(p2[1])*xs_XC**p2[0],c='gray',label='X+C')
      # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    
    plt.xscale('log') 
    plt.xlabel(r'Richness $\lambda$')
    plt.yscale('log') 
    # labels and lims were here... 
    
    plt.legend() #loc='lower right') 
    plt.tight_layout() 
    plt.show() 
  
  if plot_M: # plot the corresponding masses
    if binary: 
      plt.scatter(lambda3[~mask3_str],m1_3[~mask3_str],marker='.',
                  label='Aardvark $S_1-S_2<0.8$',c='#16161d',alpha=.5)
      plt.scatter(lambda3[mask3_str],m1_3[mask3_str],marker='.',
                  label='Aardvark $S_1-S_2\geq0.8$',c='lime',alpha=.5)
    else: 
      if cen: 
        plt.scatter(lambda3[CEN],m1_3[CEN],
                    marker='.',c=STR1[CEN]-STR2[CEN])
      else: 
        plt.scatter(lambda3,m1_3,marker='.',c=STR1-STR2)
    plt.xscale('log') 
    plt.xlabel(r'Richness $\lambda$')
    plt.yscale('log') 
    # plt.ylim(8e-16,8e-12)
    plt.ylabel(r'Mass $M_1$ (M$_\odot$)')
    
    if binary: 
      plt.legend() #loc='lower right') 
    else: 
      plt.colorbar(label=r'$S_1-S_2$')
    plt.tight_layout() 
    plt.show() 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def compare_thetas(): 
  x=to_degrees(.5,z3)
  y=theta_R_lambda(lambda3,z3)
  print "Circle theta min max :",min(min(x),min(y)),max(max(x),max(y))
  plt.scatter(2*x,y,c=z3,cmap='coolwarm')
  plt.xlabel(r'$2\theta_{500 {\rm kpc}}$')
  plt.ylabel(r'$\theta_{R_\lambda}$')
  plt.xlim(0,.2)
  plt.ylim(0,.2)
  plt.plot([0,.2],[0,.2],c='k')
  # plt.plot([0,.1],[0,.2],c='gray')
  plt.axes().set_aspect('equal')
  plt.colorbar(label=r'Redshift $z$')
  plt.tight_layout() 
  plt.show() 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def plot_M_vs_Lambda(label_IDs=False,cen=True): 
  if cen: 
    x=lambda3[CEN]; y=m1_3[CEN]; col=STR1[CEN]; hids=HID1[CEN]
  else: 
    x=lambda3; y=m1_3; col=STR1; hids=HID1
  
  plt.scatter(x,y,c=col)
  
  if label_IDs: 
    for i,txt in enumerate(hids):
      plt.annotate(int(txt), (x[i],y[i])) 
  
  plt.xscale('log'); plt.yscale('log') 
  plt.xlabel(r'Richness $\lambda$'); plt.ylabel(r'Mass $M_1$') 
  plt.show() 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def quickplot(): 
  # plt.scatter(lambda1[mask1],lx1_bol[mask1],c='r',label='Chandra')
  # plt.scatter(lambda2,lx2_bol*1e44,c='purple',label='XMM')
  plt.show() 
  return


########################################################################
if __name__=='__main__': 
  from sys import argv
  finding = len(argv)>1 and argv[1] in {'f','find','finding'}
  if finding: # search for a certain halo, given Tx, lambda, &al. 
    # using Aardvark data: [lambda3,z3,m1_3,m2_3,lx3,tx3,fx3]
    prompt = "\nLocate halo by: \n"\
           + " 0: richness lambda \n 1: redshift z \n 2: mass m1 \n"\
           + " 3: luminosity Lx \n 4: temperature Tx \n 5: flux Fx \n"\
           + " 6: S1-S2 \n 7: RA,DEC \n 8: random \n" \
           + " *: exit \n Choice: "
    while True: 
      try:
        choice = int(raw_input(prompt))
      except ValueError: 
        print "Exiting..."
        raise SystemExit
      
      if choice==0: searchlist=lambda3
      elif choice==1: searchlist=z3
      elif choice==2: searchlist=m1_3
      elif choice==3: searchlist=lx3
      elif choice==4: searchlist=tx3
      elif choice==5: searchlist=fx3
      elif choice==6: searchlist=STR1-STR2
      elif choice==7: searchlist=RA
      elif choice==8: searchlist=HID1
      else: raise SystemExit
      
      Nselect=20
      if choice < 7: # search for nearest values
        try: 
          value = float(raw_input(" Value: "))
        except ValueError: 
          print "Invalid input. Exiting." 
        idxs = find_nearest(searchlist,value,Nselect)
      elif choice==7: # search in a radius about point RA,DEC
        try: 
          raVal = float(raw_input(" RA Value: "))
          decVal = float(raw_input(" DEC Value: "))
        except ValueError: 
          print "Invalid input. Exiting." 
        # now search by difference in RA and DEC
        RA_mod = np.copy(RA); RA_mod[RA>180]-=360
        if raVal > 180: raVal -=360
        idxs = find_nearest(np.sqrt((RA_mod-raVal)**2+(DEC-decVal)**2),
                            0,Nselect)
      else: # select randomly 
        idxs = np.random.choice(len(searchlist),Nselect,replace=False)
      
      # print output
      print
      for idx in idxs: 
        print ("Halo %i\tby cluster @ [RA DEC z]=[%g %g %g] l=%.3g has "\
              + "m1=%.3g, Lx=%.3g, Tx=%.3g, Fx=%.3g, S1-S2=%g") \
              % (HID1[idx],RA[idx],DEC[idx],z3[idx],lambda3[idx],
               m1_3[idx],lx3[idx],tx3[idx],fx3[idx],STR1[idx]-STR2[idx])
      # "theta_R_lambda=%g" % theta_R_lambda(lambda3[idx],z3[idx])
  
  else: # not finding; just plotting 
    # plot_fx_vs_lambda(color_by='dataset',mask_redshift=True)
    # plot_fx_vs_lambda(color_by='temperature',mask_redshift=False) # ,factor='redshift')
    # plot_fx_vs_lambda(color_by='redshift',mask_redshift=True)
    # plot_LxTx_vs_z()
    # plot_SNR('fx') 
    # plot_SNR_texp()
    # plot_SNR_serendipity()
    # plot_RSD() 
    # plot_lx_bol_XMM()
    # str_test() 
    
    if 0: # plot set of Lx,Tx,Fx
      str_test_2(plot_lx=True,binary=False,cen=True) 
      str_test_2(plot_tx=True,binary=False,cen=True) 
      str_test_2(plot_fx=True,binary=False,cen=True) 
    
    if 0: 
      if 1: 
        plot_Tx_vs_lambda()
      else:
        plot_Tx_vs_lambda(label_IDs=True)
    
    # str_test_2(plot_s1s2=False,plot_fx=False,plot_M=True,binary=False,cen=True) 
    # compare_thetas()
    # plot_M_vs_Lambda(label_IDs=True)
    # quickplot()
    str_test_2(plot_s1s2=True)
    pass
    
  print "fin"
