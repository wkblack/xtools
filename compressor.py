#!/sw/lsa/centos7/python-anaconda2/created-20170424/bin/python
# galaxy compressor (fits) 
# to be used on files like /SkyCatalogues/Aa_observed_galaxy_catalogs/
#                     aardvark_v1.0/obs/Aardvark_v1.0c_des_rotated.1.fit

# reads in a fits file with galaxy (ra,dec)
# along with target ra and dec positions
# and spits out a compressed file


from astropy.io.fits import open as fitsopen
from sys import argv

if len(argv)<2: 
  print "ERROR: requires file input" 
  raise SystemExit
# else: continue 

files = argv[1:]

for f in files: 
  try: 
    hdulist = fitsopen(f) 
    print "Processing %s..." % f
  except IOError: 
    print "Error: couldn't read in %s" % f
    raise SystemExit
  
  data = hdulist[1].data
  
  import numpy as np
  if 1: # grab local points
    # set targets: # TODO: softcode this
    ra_target,dec_target,extent = 24.53,0.,.6
    mask = np.logical_and(np.abs(data['ra']-ra_target) < extent/2.,
                          np.abs(data['dec']-dec_target) < extent/2.)
    outfname = 'galaxy_RA%gDEC%g.csv' % (ra_target,dec_target)
    np.savetxt(outfname,np.c_[data['ra'][mask],data['dec'][mask]],
               delimiter=',',header='Galaxy RA,DEC')
  
  hdulist.close()

print '~fin'
