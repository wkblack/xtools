#!/bin/python
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# read in Ngal/Nred list along with M200 list, 
# then plot log N vs log M
# create the Ngal/Nred list with investigate_galaxy_colors.py 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

########################################################################
# imports 

import pandas as pd 
import numpy as np

from matplotlib import pyplot as plt
from numpy import log10

########################################################################
# read in Ngal,Nred
fname_N = 'ngal_nred.csv'
df_N = pd.read_csv(fname_N)
Ngal = df_N.Ngal.values
Nred = df_N.Nred.values
f_R = 1.0*Nred/Ngal
f_B = 1-f_R

########################################################################
# read in M200
# HID1,HID2,STR1,STR2,LAMBDA,z,CENTRAL_FLAG,CENTRAL_BCG_FLAG,RA,DEC,LAMBDA_ERR,M1,M2,Lx,Tx,Fx
fname_M = 'massive_subset.csv'
df_M = pd.read_csv(fname_M)
from cosmology import m200_m500_factor
M200 = df_M.M1.values/m200_m500_factor
lam = df_M.LAMBDA.values
Lx,Tx,Fx = df_M.Lx.values,df_M.Tx.values,df_M.Fx.values
label_Lx=r'$L_X/({\rm ergs/s})$'
label_log10Lx=r'$\log_{10} [L_X/({\rm ergs/s})]$'
# S1,S2 = df_M.STR1.values,df_M.STR2.values
z = df_M.z.values

print "median f_R:",np.median(f_R)
print "median f_R(z<.25):",np.median(f_R[z<.25])
print "median f_R(z>.25):",np.median(f_R[z>.25])

########################################################################
# update basic params
if 0: # for large pictures
  plt.rcParams.update({'font.size': 12,'axes.labelsize':24})

########################################################################
if 0: # plot f_R vs richness, colored by redshift
  plt.scatter(lam,f_R,c=z,cmap='coolwarm')
  ax = plt.gca()
  ax.set_xlabel(r'Richness $\lambda$')
  # plt.xlabel(r'Richness $\lambda$'); 
  plt.ylabel(r'Red fraction $f_R$')
  plt.xscale('log')
  plt.colorbar(label=r'Redshift $z$')
  plt.tight_layout()
  plt.show()
  print '~fin'; raise SystemExit

########################################################################
if 0: 
  # plot f_R vs richness vs redshift in 3D space
  from mpl_toolkits.mplot3d import Axes3D
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')
  ax.scatter(log10(lam),f_R,z,c=z)
  ax.set_xlabel(r'Richness $\log_{10}[\lambda]$')
  ax.set_ylabel(r'Red fraction $f_R$')
  ax.set_zlabel(r'Redshift $z$')
  plt.show()
  print '~fin'; raise SystemExit

########################################################################
if 0: 
  # plot f_R vs redshift
  plt.scatter(z,f_R,c=log10(lam))
  plt.xlabel(r'Redshift $z$'); plt.ylabel(r'Red fraction $f_R$')
  plt.colorbar(label=r'Richness $\lambda$')
  plt.tight_layout()
  plt.show()
  print '~fin'; raise SystemExit

########################################################################
# plot shift between Ngal and Nred
if 0: 
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set title and labels 
  plt.title(r'$N$ drift')
  if 0: # plot vs mass
    x1,x2 = M200,M200
    plt.xlabel(r'$M_{200}$')
  else: 
    x1,x2 = lam,lam
    plt.xlabel(r'Richness $\lambda$')
  y1,y2 = Ngal,Nred
  col=1.0*y2/y1 # color as fractional change
  plt.ylabel(r'Galaxy Count $N$')
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # calculate bounds
  MIN_X,MAX_X = min(min(x1),min(x2)),max(max(x1),max(x2))
  MIN_Y,MAX_Y = min(min(y1),min(y2)),max(max(y1),max(y2))
  MIN,MAX = min(MIN_X,MIN_Y),max(MAX_X,MAX_Y)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # plot Nred
  plt.scatter(lam,Nred,label=r'$N_{{\rm red}}$',c=col,cmap='plasma_r')
  plt.colorbar(label=r'$N_{{\rm red}}/N_{{\rm gal}}$')
  if 1:
    # plot arrows: Ngal -> Nred
    plt.quiver(x1,y1,x2-x1,y2-y1,color='gray',alpha=.5,
               angles='xy',scale_units='xy',scale=1)
    # plot Ngal
    plt.scatter(lam,Ngal,label=r'$N_{{\rm gal}}$',c='g')
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set scale and limits
  plt.xscale('log'); plt.yscale('log')
  sf = scale_factor = 1.1
  plt.xlim(MIN_X/sf,MAX_X*sf); plt.ylim(MIN_Y/sf,MAX_Y*sf)
  plt.plot([MIN,MAX],[MIN,MAX],'k') # plot equality line
  plt.fill_between([MIN,MAX],[MIN/2.,MAX/2.],[MIN*2,MAX*2],color='k',alpha=.05)
  
  # set axes equal:
  plt.gca().set_aspect('equal')
  
  plt.tight_layout() 
  plt.show()
  print "exiting..."
  raise SystemExit

########################################################################
# analyze slope and scatter

from time import time
from sys import stdout
import linmix

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# linmix inputs 

silent_mcmc=True
MAX_ITER=100000
# MAX_ITER=50

if MAX_ITER<100: print "Warning: MAX_ITER=%i" % MAX_ITER

if 1:
  # set up pivot values
  from numpy import median
  if 1: # use median values
    M_p,N_p,N_p2 = log10(median(M200)),log10(median(Ngal)),log10(median(Nred))
  elif 0: # use hardcoded values
    M_p,N_p,N_p2 = 15,200,200
  else: # use non-pivoted data
    M_p,N_p,N_p2 = 1,1,1
  print "Using pivot values Mp=%g, Np=%g, Np2=%g" % (M_p,N_p,N_p2)
  
  x,xsig = log10(M200)-M_p,(M200/M200)*1e-4
  y,ysig = log10(Ngal)-N_p,(Ngal/Ngal)*1e-4
  y2 = log10(Nred)-N_p2
else:
  # TODO: add fR vs 
  print "ERROR: Not set!"
  raise SystemExit

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
if 0: # plot input before (and without) running linmix
  print "Plotting"
  # plt.errorbar(x,y,xerr=xsig,yerr=ysig,ls=" ",c='k',alpha=.25)
  # plt.xscale('log'); plt.yscale('log')
  # plt.xlim(min(x),max(x)); plt.ylim(min(y),max(y))
  fig,(ax1,ax2) = plt.subplots(nrows=2, ncols=1)
  p1 = ax1.scatter(x,y,c=Tx,alpha=.75)
  ax1.set_ylabel(r'$\log_{10} N_{{\rm gal}}/N_{p1}$')
  ax2.scatter(x,y2,c=Tx,alpha=.75)
  ax2.set_ylabel(r'$\log_{10} N_{{\rm red}}/N_{p2}$')
  ax2.set_xlabel(r'$\log_{10} M_{200}/M_p$')
  
  fig.subplots_adjust(top=0.95, bottom=.1, left=.11, right=.85, hspace=.2, wspace=.2)
  cbar_ax = fig.add_axes([0.875, 0.1, 0.04, .85 ]) # [left, bottom, width, height] 
  fig.colorbar(p1, cax=cbar_ax, label=r'Temperature $T_X$ (keV)')
  
  plt.show()
  raise SystemExit

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# start linmixing

print "Mixing...\r",
start=time(); stdout.flush()
##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##
lm1 = linmix.LinMix(x,y,xsig,ysig,K=2)
lm1.run_mcmc(silent=silent_mcmc,maxiter=MAX_ITER)
##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##
end=time(); elapsed=end-start;
print "Mixing complete! Took %g seconds." % elapsed

# print results
print("alpha mean and stddev:  {}, {}".format(
      lm1.chain['alpha'].mean(),  lm1.chain['alpha'].std()))
print("beta mean and stddev:   {}, {}".format(
      lm1.chain['beta'].mean(),   lm1.chain['beta'].std()))
print("sigsqr mean and stddev: {}, {}".format(
      lm1.chain['sigsqr'].mean(), lm1.chain['sigsqr'].std()))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

print "Mixing...\r",
start=time(); stdout.flush()
##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##
lm2 = linmix.LinMix(x,y2,xsig,ysig,K=2)
lm2.run_mcmc(silent=silent_mcmc,maxiter=MAX_ITER)
##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##
end=time(); elapsed=end-start;
print "Mixing complete! Took %g seconds." % elapsed

# print results
print("alpha mean and stddev:  {}, {}".format(
      lm2.chain['alpha'].mean(),  lm2.chain['alpha'].std()))
print("beta mean and stddev:   {}, {}".format(
      lm2.chain['beta'].mean(),   lm2.chain['beta'].std()))
print("sigsqr mean and stddev: {}, {}".format(
      lm2.chain['sigsqr'].mean(), lm2.chain['sigsqr'].std()))

########################################################################
# plot results on top of plots
from numpy import linspace

if True: # plot results
  fig,(ax1,ax2) = plt.subplots(nrows=2, ncols=1)
  # fig,ax1 = plt.subplots(nrows=1, ncols=1)
  xs = linspace(min(x),max(x),20)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # fig1
  
  for i in range(0, len(lm1.chain), 25):
    ys = lm1.chain[i]['alpha'] + xs * lm1.chain[i]['beta']
    ax1.plot(xs, ys, color='b', alpha=0.02)
  alpha, beta = lm1.chain['alpha'].mean(), lm1.chain['beta'].mean()
  ys = alpha + xs * beta
  ax1.plot(xs, ys, color='k')
  # 
  p1 = ax1.scatter(x,y,c=Tx,alpha=.75,cmap='afmhot',edgecolors='k',
                   linewidths=.5)
  # ax1.errorbar(x,y,xerr=xsig,yerr=ysig,ls=" ",c='k',alpha=.25)
  # ax1.set_xlabel(r'$\log_{10} M_{200}$')
  ax1.set_ylabel(r'$\log_{10} N_{{\rm gal}}/N_{p1}$')
  ax1.set_xticks([])
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # fig2
  
  for i in range(0, len(lm2.chain), 25):
    ys = lm2.chain[i]['alpha'] + xs * lm2.chain[i]['beta']
    ax2.plot(xs, ys, color='b', alpha=0.02)
  alpha = lm2.chain['alpha'].mean()
  beta = lm2.chain['beta'].mean()
  ys = alpha + xs * beta
  ax2.plot(xs, ys, color='k')
  # 
  ax2.scatter(x,y2,c=Tx,alpha=.75,cmap='afmhot',edgecolors='k',linewidths=.5)
  # ax2.errorbar(x,y2,xerr=xsig,yerr=ysig,ls=" ",c='k',alpha=.25)
  ax2.set_xlabel(r'$\log_{10} M_{200}/M_p$')
  ax2.set_ylabel(r'$\log_{10} N_{{\rm red}}/N_{p2}$')
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up the colorbar
  
  # plt.subplots_adjust(top=.99,bottom=0.1,left=.11,right=.97,hspace=0,wspace=0)
  fig.subplots_adjust(top=0.95, bottom=.1, left=.11, right=.82, hspace=.2, wspace=.2)
  cbar_ax = fig.add_axes([0.87, 0.1, 0.05, .85 ]) # [left, bottom, width, height] 
  fig.colorbar(p1, cax=cbar_ax, label=r'$T_X$ (keV)')
  
  plt.show()

########################################################################
print "~fin"
