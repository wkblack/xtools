#!/global/common/cori/software/python/2.7-anaconda-5.2/bin/python
# read in low-richness + high-temperature systems and investigate 
# run massive_subset.py to create the list of most massive systems

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# imports and inputs

import galaxy_id as gid
from matplotlib import pyplot as plt
import numpy as np

low_HID = np.array([3752539,4422655,6300629,10719618,12190656,6839502,11922594])
high_HID = np.array([14069242,11655342,537544,7111109,7111233,13531712])

# scan in HID list from maximum weights
from pandas import read_csv
df = read_csv('massive_subset.csv')
# ,HID1,HID2,STR1,STR2,LAMBDA,z,CENTRAL_FLAG,CENTRAL_BCG_FLAG,RA,DEC,LAMBDA_ERR,M1,M2,Lx,Tx,Fx

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up HID list to be read in

MAX = 1000 # maximum element count
hid_list = df.HID1.values.astype(int)[:MAX]
z_list = df.z.values[:MAX]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up lists

ngal = np.zeros(len(hid_list))
nred = np.zeros(len(hid_list))
min_m = np.zeros(len(hid_list))
max_m = np.zeros(len(hid_list))

gmr_cut_list = np.zeros(len(hid_list))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# for ii,HID in enumerate(np.flip(hid_list,0)): # reverse order
for ii,HID in enumerate(hid_list): 
  print "Analyzing Halo ID %i (%i/%i)" % (HID,(ii+1),len(hid_list))
  hdl = gid.halo_data_list(HID)
  df = gid.members_in_R20(HID,hdl)
  print "Redshift z=%g" % hdl['z']
  
  dv = gid.vet(df,hdl) # vets by R200 and 0.2 L_*
  ngal[ii] = len(dv) # total galaxy count
  dr = gid.select_ES0(dv,hdl)
  nred[ii] = len(dr)
  
  if 0: # optical analysis only
    # gid.plot_3D(df,hdl,c=df.Z)
    gid.space_plot(df,hdl,halo=True)
    gid.radial_profile_isochoric(df,hdl)
    continue
  
  if 0: # color analysis only
    gid.color_plot(dv,hdl,True)
    continue
  
  # grab extremum values for a band
  # min_m[ii],max_m[ii] = min(dv.TMAG_z.values),max(dv.TMAG_z.values)
  min_m[ii],max_m[ii] = min(dv.TMAG_i.values),max(dv.TMAG_i.values)
  
  print "[Ngal, Nred] = [%i, %i]" % (ngal[ii],nred[ii])
  del(df,dv,dr)

zz = np.sort(z_list)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
if 0: # look at min/max of m_z // m_r
  plt.scatter(z_list,min_m,10,label=r'min',alpha=.75,linewidths=0)
  plt.scatter(z_list,max_m,10,label=r'max',alpha=.75,linewidths=0)
  plt.plot(zz,gid.m_star(zz),'gray',label=r'$m_*$')
  plt.plot(zz,gid.m_star(zz)+1.75,'k',label=r'$m_*+1.75$')
  plt.xlabel(r'Redshift $z$')
  # plt.ylabel(r'Magnitude $m_z$')
  plt.ylabel(r'Magnitude $m_i$')
  plt.legend()
  plt.show()
  raise SystemExit

from pandas import DataFrame
outname = 'ngal_nred.csv'
df_out = DataFrame({'Ngal':ngal,'Nred':nred})
df_out.to_csv(outname,index=False)

print '~fin' 
