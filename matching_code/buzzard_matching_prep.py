from membership_matching_algorithm import membership_matching, read_matched_clusters
from astropy.table import Table
from glob import glob

import numpy as np
import os, sys
from os.path import isfile

# xtools = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

if 1:
  xtools = '/global/u1/w/wkblack/xproject/xtools/'
  sys.path.append(xtools)
  from config import Buzzard as sim
else:
  xtools = '/home/wkblack/projects/xproject/xtools/'
  sys.path.append(xtools)
  from config import Aardvark as sim

########################################################################
# vet down unneccessary things (?)

def vet(catalogue):
  # maybe just return the necessary columns ... though I'd like a superset.
  # galaxies = GAMA_cluster_members[['HaloID', 'GroupID', 'Pmem']]
  return

def read_without_multicols(fname,idx=None):
  # read in fits filename
  # return pandas dataframe with multicols removed
  df = Table.read(fname,format='fits')
  keys = np.array(df.keys())
  MIN = min([np.size(df[key]) for key in keys])
  # mask=[np.size(df[key])==np.size(df['HALOID']) for key in df.keys()]
  mask = np.array([np.size(df[key])==MIN for key in keys])
  vals = list(keys[mask])
  if idx==None:
    return df[vals].to_pandas()
  else:
    return df[vals].to_pandas().set_index(idx)

########################################################################
# create merged catalogues, based off of the following catalouges:
# clusters, members, galaxies, halos
from pixel_analysis import in_pixel

fname_gals_template = 'gals/gals_of_%i.csv'
fname_gals_search = fname_gals_template.replace('%i','*')

def join_gals_to_members(sim):
  f_members = sim['members']
  df_members = read_without_multicols(f_members,idx='ID')
  print "read in members"
  
  # figure out which galaxy files need analyzing
  # mask = [in_pixel(8,i,(df_members.RA,df_members['DEC']),nest=sim['nest']) for i in ...
  
  df_joined = df_members
  print "member count: %i" % len(df_members)
  
  f_galaxies = sim['galaxies']
  
  for i,f_gal in enumerate(f_galaxies):
    idx = int(f_gal.split('.')[-2]) # TODO: softcode somehow!
    fname_out = fname_gals_template % idx
    if isfile(fname_out): 
      print "File '%s' already exists" % fname_out
      continue
    else: 
      print "Processing galaxy file %i/%i to create '%s'" \
             % (i+1,len(f_galaxies),fname_out)
      
    df_gal = read_without_multicols(f_gal,idx='ID')
    print "read in galaxies"
    
    # merge
    df_joined = df_members.join(df_gal, rsuffix='_gal',how='inner')
    df_joined.to_csv(fname_out,index=False)
    print "saved datasets for galaxy file %i (len=%i)" \
           % (idx,len(df_joined))
  
  print 'joined all %i galaxy files' % len(f_galaxies)
  
  return

def amalgamate(sim):
  if 0:
    for key in ['clusters','members']:
      print sim[key]
    for key in ['halos','galaxies']:
      print sim[key][:3],len(sim[key])
  
  # fname_out = 'test.csv'
  # df_joined.to_csv(fname_out,index=False)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # join all catalogues together 
  # (consider if I should do that literally, or just members + gals)
  
  # set Pmem=1 for each member: GAMA_members['Pmem'] = 1.0
  
  # GAMA_cluster_members = GAMA_clusters.join(GAMA_members.set_index('GroupID'), 
  #                                           on='GroupID', rsuffix='_c')

########################################################################
# use Arya's matching code (follow test.py)

if __name__=='__main__':
  join_gals_to_members(sim)
  print '~fin'
  raise SystemExit
    
  if 0:
    merged = amalgamate(sim)
    print '~fin'; raise SystemExit
    vetted = vet(merged)
    membership_matching(vetted, 'MEM_MATCH_ID', 'HALOID', 'Pmem')
    matched_clusters = read_matched_clusters()
    print '~fin'



