#!/bin/python
print "in square_analyze.py"

# import cosmology as c; from matplotlib import pyplot as plt; import numpy as np; 
import galaxy_id as gid; 
print "imported galaxy_id.py"

cluster_data = [{"RA":65.99,"DEC":-7.51,"Z":.29,"lambda":190,"HALOID":537544,"R_LAMBDA":0.63937}, # 65.993347 -7.51133
                {"RA":37.50,"DEC":-4.68,"Z":.26,"lambda":76.51,"HALOID":11922429,"R_LAMBDA":0.547946},
                {"RA":24.28,"DEC":-68.94,"Z":.27,"lambda":63.92,"HALOID":4155253,"R_LAMBDA":0.547946}] # 24.282009 -68.943893

if 1: 
  print "importing rm matching"
  # import matching
  import pandas as pd
  df_rm = pd.read_csv('RM_training.csv')

for cd in cluster_data:
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  print "Processing cluster associated with Halo ID %i." % cd["HALOID"]
  # check cluster details
  df_rm_i = df_rm[df_rm.M_HALOID1.values==cd['HALOID']]
  print df_rm_i
  print df_rm_i.LAMBDA
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # grab square
  df = gid.grab_square((cd["RA"],cd["DEC"]),.3)
  hdl = gid.halo_data_list(cd["HALOID"])
  dv = gid.vet_by_m_star(df,cd)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # grab colors
  col_df = gid.RGB_from_gri(df,hdl)
  col_dv = gid.RGB_from_gri(dv,hdl)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # hack hdl
  if 1: 
    hdl['r200']=cd["R_LAMBDA"]
    print 'diff:',cd["RA"]-hdl['ra'],cd["DEC"]-hdl['dec']
    hdl['ra']=cd["RA"]
    hdl['dec']=cd["DEC"]
    hdl['Z']=cd["Z"]
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # plot it up
  # gid.space_plot(df,hdl,halo=True)
  # gid.space_plot(df,hdl,halo=True,color=col_df)
  gid.space_plot(dv,hdl,halo=True,s=15,color=col_dv)
  # gid.space_plot(dv,hdl,halo=True,color=col_dv)

print '~fin'

