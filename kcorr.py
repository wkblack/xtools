#!/bin/python
# testing out k-correction from Nord &al 2008
# https://academic.oup.com/mnrasl/article/383/1/L10/1168518
# converts rest frame to observed soft-band ([0.5--2.0] keV)
import numpy as np
from numpy import log

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# definitions 

# first factor
def K1(kT):
  return -0.209 + log(kT) * (1.18-0.39*log(kT))

# second factor
def K2(kT):
  return -0.098 + log(kT) * (-0.092 + 0.085*log(kT))

# combined factor
def kcorr(z,kT,silent=False): 
  if not silent and np.any(z>2): print "Warning: accuracy loss expected with z>2"
  return 1 + z*(K1(kT) + z*K2(kT))

# correct luminosity Lx from rest frame to soft-band
def korrect(Lrest,z,kT,silent=False): 
  return Lrest * kcorr(z,kT,silent)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

if __name__=='__main__': 
  import matplotlib
  import matplotlib.pyplot as plt
  cmap = matplotlib.cm.get_cmap('viridis')
  # plot kcorr for z=0,5, w/ vert at z=2 to show cutoff
  
  kTmin,kTmax=.1,10
  kT=np.linspace(kTmin,kTmax,200)
  plt.axhline(0,c='k')
  plt.axvline(1,c='r')
  if 0: # plot K1, K2
    plt.plot(kT,K1(kT),label=r'$K_1$')
    plt.plot(kT,K2(kT),label=r'$K_2$')
  elif 1: # plot K(z,kT)
    for z in {0,.1,.2,.3,.4,.5}: 
      plt.plot(kT,kcorr(z,kT),c=cmap(z),label=r'$z=%g$'%z)
  plt.xlabel(r'kT')
  plt.legend() 
  plt.show()
  
  zmin,zmax = .1,.3
  # zmin,zmax = 0,3
  N_z=200
  z = np.linspace(zmin,zmax,N_z)
  kT = np.ones(N_z)
  mn,mx = -1,1 # in the paper, kT~[1,10].
  N = 5
  for p in np.linspace(mn,mx,N): 
    plt.plot(z,kcorr(z,kT*10**p,silent=True),
             label='%g keV' % 10**p,
             c=cmap((p-mn)/(mx-mn)))
  if zmax>=2: 
    plt.axvline(2,c='r')
  if N<7: 
    plt.legend()
  plt.xlabel(r'Redshift $z$') 
  plt.ylabel(r'$K(z,{\rm k}T_X)$')
  plt.show() 

'''
The result: low kT values have negative K corrections,
which would yield negative luminosities, so something's fishy here.

'''

