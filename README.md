# Workflow notes:                                              duration:
------------------------------------------------------------------------
1. `bicycle.py` (formerly `theWheel.py`)                          long
        reads in redmapper workset
        creates `output_theWheel.csv` 
2. `analyzeWheel.py` [flux floor]                                 medium
        reads in `output_theWheel.csv`
        analyzes csv with mcmc, giving {alpha, beta, sigma}
        creates `mcmc_result_lnfxfloor[floor].txt`
        -> Can batch many with `batchAnalyzeWheel.py`             long
3. `mcmc_analyzer.py`                                             short
        reads in `mcmc_result_*.txt`
        combines into `mcmc_result_combined.dat`
4. `mcmc_plot_results.py`                                         short
        reads in `mcmc*combined.dat`
        plots results

