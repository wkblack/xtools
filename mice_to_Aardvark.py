#!/bin/python

# read in mice output from cosmohub
# output the galaxy file in a Aardvark-like style:
# ID,RA,DEC,TRA,TDEC,Z,HALOID,RHALO,NGALS,CENTRAL,M200,R200,
# PX,PY,PZ,VX,VY,VZ,TMAG_g,TMAG_r,TMAG_i,TMAG_z,TMAG_Y

fname = '/global/homes/w/wkblack/5559.fits'

def convert(f=fname,overwrite=False):
  fname_out = f.split('fit')[0] + 'csv'
  from os.path import isfile
  if isfile(fname_out) and not overwrite:
    print "%s already exists. Importing..." % fname_out
    import pandas as pd
    return pd.read_csv(fname_out)
  
  # converts mice output to dataframe
  from astropy.table import Table
  df = Table.read(f, format='fits').to_pandas()
  # ['unique_halo_id', 'unique_gal_id', 'ra_gal', 'dec_gal', 'z_cgal', 
  #  'xgal', 'ygal', 'zgal', 'flag_central', 'xhalo', 'yhalo', 'zhalo', 
  #  'gr_cos', 'mr_gal', 'lmhalo', 'nsats', 'des_asahi_full_g_true', 
  #  'des_asahi_full_g_abs_mag', 'des_asahi_full_r_abs_mag', 
  #  'des_asahi_full_r_true', 'des_asahi_full_i_abs_mag', 
  #  'des_asahi_full_i_true', 'des_asahi_full_z_abs_mag', 
  # 'des_asahi_full_z_true']
  df = df.rename(columns={"unique_halo_id": "HALOID", 
              "unique_gal_id": "ID", "ra_gal":"TRA","dec_gal":"TDEC",
              "z_cgal":"Z","xgal":"PX","ygal":"PY","zgal":"PZ",
              "flag_central":"CEN",
              'xhalo':"HALOPX", 'yhalo':"HALOPY", 'zhalo':"HALOPZ", 
              'gr_cos':"gmr", 'mr_gal':"TMAG_r_noEvolve", 
              'lmhalo':"log10M", 'nsats':"NSAT", 
              'des_asahi_full_g_abs_mag':"TMAG_g_abs", 
              'des_asahi_full_g_true':"TMAG_g", 
              'des_asahi_full_r_abs_mag':"TMAG_r_abs", 
              'des_asahi_full_r_true':"TMAG_r", 
              'des_asahi_full_i_abs_mag':"TMAG_i_abs", 
              'des_asahi_full_i_true':"TMAG_i", 
              'des_asahi_full_z_abs_mag':"TMAG_z_abs", 
              'des_asahi_full_z_true':"TMAG_z"})
  # *_?_abs_mag : absolute magnitude in des asahi full ?-band
  # *_?_true    : observed magnitude in des asahi full ?-band
  df['TMAG_Y']=0
  df.to_csv(fname_out,index=False)
  return df

def make_hdl(df):
  # read in complex dataframe, output Aardvark-like hdl
  present = ['HALOID','HALOPX','HALOPY','HALOPZ','RA','DEC','Z']
  absent = ['M200','R200','M200B','R200B','M500','R500','MVIR','RVIR',
            'M2500','R2500','VRMS','RS','JX','JY','JZ','SPIN','HALOVX',
            'HALOVY','HALOVZ','LUMTOT','LUM20','LBCG','NGALS','N18',
            'N19','N20','N21','N22','EDGE','HOST_HALOID','XOFF','VOFF',
            'LAMBDA','B_TO_A','C_TO_A','AX','AY','AZ','VIRIAL_RATIO']
  
  dv = df[df.CEN.values==0] # grab central galaxy
  dv = dv.rename(columns={"TRA":"RA","TDEC":"DEC"})
  dv['z']=dv['Z']
  
  for label in absent:
    dv[label]=0
  
  return dv.to_dict('index')[dv.index[0]] # returns central gal as dict

if __name__=='__main__':
  df = convert()
  hdl = make_hdl(df)
  
  # vet down galaxies 
  import galaxy_id as gid
  
  dist_df = gid.true_dist(df,hdl) # comoving Mpc/h
  
  dv = gid.vet_by_m_star(df,hdl)
  Ngal = len(dv)
  dist_dv = gid.true_dist(dv,hdl) # comoving Mpc/h
  
  dr = gid.select_ES0(dv,hdl)
  Nred = len(dr)
  dist_dr = gid.true_dist(dr,hdl) # comoving Mpc/h
  
  print "Red fraction Nred/Ngal = %i/%i ~ %g" \
          % (Nred,Ngal,1.0*Nred/Ngal)
  
  if 0: # make color--magnitude plots
    gid.color_plot(df,hdl,True)
    gid.color_plot(dv,hdl,True)
  
  if 0: # make a 3D plot
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(df.PX,df.PY,df.PZ,s=5,c=dist_df,edgecolor=None)
    ax.set_aspect('equal')
    plt.show()
  
  if 1: # make a radial profile
    print "radial profile for all galaxies"
    gid.radial_profile(df,hdl,steps_per_decade=10,use_r200=False)
    print "red sequence radial profile"
    gid.radial_profile(dr,hdl,steps_per_decade=10,use_r200=False)
  
  if 1: # check how Nred/Ngal changes with radial distance
    # start with no vetting, then logarithmically cut
    # we start with df, dist
    MAX=20; ratio=1.4142135624
    import numpy as np
    Ngal_list = np.zeros(MAX)
    Nred_list = np.zeros(MAX)
    dvet_list = np.zeros(MAX)
    mask_red = gid.ES0_mask(dv,hdl)
    print 'min dist:',min(dist_dv)
    for i in range(MAX):
      dvet_list[i] = max(dist_dv)/ratio**i
      mask = dist_dv<dvet_list[i]
      Ngal_list[i] = sum(mask)
      Nred_list[i] = sum(mask_red[mask])
    
    print dvet_list,Ngal_list,Nred_list
    
    from matplotlib import pyplot as plt
    plt.scatter(dvet_list,Ngal_list,36,c='g',label=r'$N_{{\rm gal}}$')
    plt.scatter(dvet_list,Nred_list,18,c='r',label=r'$N_{{\rm red}}$')
    plt.legend()
    plt.xscale('log'); plt.yscale('log')
    plt.xlabel(r'$r_{{\rm halo}}$'); plt.ylabel(r'$N$')
    plt.show() 
    
    plt.scatter(dvet_list,Nred_list/Ngal_list)
    plt.xlabel(r'$r_{{\rm halo}}$'); 
    plt.ylabel(r'$N_{{\rm red}}/N_{{\rm gal}}$')
    plt.xscale('log'); plt.yscale('log')
    plt.show() 
  
  print '~fin'
