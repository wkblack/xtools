#!/bin/python
# plot all halos in RA vs DEC

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
if __name__ == '__main__':
  # check input
  from sys import argv
  
  if len(argv) < 2: 
    print "Syntax: >>> [fits filenames]" 
    raise SystemExit 
  
  flist = argv[1:]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# other imports

from os.path import isfile
from os.path import splitext
from os.path import basename
from astropy.io import fits as pyfits
import matplotlib.pyplot as plt
import numpy as np
 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# function definitions 

def plot_mwd(RA,DEC,org=0,title='Mollweide projection', projection='mollweide'):
  ''' requires RA, DEC to be same length.
  RA takes values in [0,360), DEC in [-90,90],
  which represent angles in degrees.
  org is the origin of the plot, 0 or a multiple of 30 degrees in [0,360).
  title is the title of the figure.
  projection is the kind of projection: 'mollweide', 'aitoff', 'hammer', 'lambert'
  taken from balbuceosastropy.blogspot.com/2013/09/the-mollweide-projection.html
  '''
  x = np.remainder(RA+360-org,360) # shift RA values
  ind = x>180
  x[ind] -=360   # scale conversion to [-180, 180]
  
  tick_labels = np.array([150, 120, 90, 60, 30, 0, 330, 300, 270, 240, 210])
  if 1: # reverse the scale: East to the left (standard) 
    x=-x
  else: # have RA increasing to the right
    tick_labels = tick_labels[::-1]
  
  tick_labels = np.remainder(tick_labels+360+org,360)
  fig = plt.figure(figsize=(10, 5))
  ax = fig.add_subplot(111, projection=projection, axisbg ='LightCyan')
  ax.scatter(np.radians(x),np.radians(DEC),1)  # convert degrees to radians
  ax.set_xticklabels(tick_labels)     # we add the scale on the x axis
  ax.set_title(title)
  ax.title.set_fontsize(15)
  ax.set_xlabel("RA")
  ax.xaxis.label.set_fontsize(12)
  ax.set_ylabel("DEC")
  ax.yaxis.label.set_fontsize(12)
  ax.grid(True)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
if __name__ == '__main__':
  # main
  
  ra_total=np.array([])
  dec_total=np.array([])
  
  for fname in flist: 
    # accumulate points
    print "Processing %s" % fname
    if not isfile(fname): 
      print "Error: %s not recognized as file" % fname
      continue 
    shortname=splitext(basename(fname))[0]
    
    # scan in data from fits file
    hdulist = pyfits.open(fname) 
    data = hdulist[1].data
    ra = data["RA"] 
    dec = data["DEC"] 
    if 0: # print reports
      print "RA range: %g -> %g" % (min(ra),max(ra))
      print "DEC range: %g -> %g" % (min(dec),max(dec))
    
    # strip data  
    num=len(ra)/10
    ra_condensed=np.random.choice(ra,num)
    dec_condensed=np.random.choice(dec,num)
    
    ra_total = np.append(ra_total,ra_condensed)
    dec_total = np.append(dec_total,dec_condensed)
    
    if 0: # plot individual 
      plt.scatter(ra,dec,1)
      plt.xlabel("RA") 
      plt.ylabel("DEC") 
      name = "flat_" + shortname + ".png"
      plt.savefig(name) 
      print "Saved %s" % name
      
      plot_mwd(ra,dec) 
      name = "mwd_" + shortname + ".png"
      plt.savefig(name) 
      print "Saved %s" % name
  
  if 1: # plot full
    if 0: # shift to match Arya's data
      ra_shift = 90-22.5      # positive -> right
      dec_shift = 45-5+1.541  # positive -> up
      ra_total = np.remainder(ra_total+ra_shift,360)
      dec_total = dec_total+dec_shift
      if 0: # just print the ranges and exit 
        print "RA range: %g -> %g" % (min(ra_total),max(ra_total))
        print "DEC range: %g -> %g" % (min(dec_total),max(dec_total))
        raise SystemExit
    
    # plot 
    if 1: # make flat plot
      plt.scatter(ra_total,dec_total,1)
      plt.xlabel("RA") 
      plt.ylabel("DEC") 
      name = "flat.png"
      plt.savefig(name) 
      print "Saved %s" % name
    
    if 0: # make mwd plot
      plot_mwd(ra_total,dec_total) 
      name = "mwd.png"
      plt.savefig(name) 
      print "Saved %s" % name
