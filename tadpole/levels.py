# plot different magnitude cuts, to see how that changes cuts 

import red_dragon_base as rdb
np,plt = rdb.np,rdb.plt

from matplotlib import cm
cmap1 = 'cividis'
cw1 = cm.get_cmap(cmap1)
cmap2 = 'coolwarm'
cw2 = cm.get_cmap(cmap2)


if 1: # TODO: replace with for loop
  Z_i = .3
  dZ = .025
  
  mask_Z = (Z_i<=rdb.Z)*(rdb.Z<Z_i+dZ)
  g,r,i,z,Z = [tag[mask_Z] for tag in rdb.fields]
  xx = np.transpose([g-r,g-i,g-z,r-i,r-z,i-z])
  
  mi_range = np.array([min(i),max(i)]) # ~18 to ~22.6
  print('m_i range:',mi_range)
  
  # mi_vals = [21,21.5,20.5,20,20.5,19.5,19,19.5]
  dmi = .5
  mi_vals = np.linspace(mi_range[0]+dmi,mi_range[1],4)
  mi_vals = np.arange(mi_range[0]+dmi,mi_range[1],dmi) 
  mi_vals = np.flip(mi_vals)
  
  colors1 = cw1((mi_vals-mi_vals[0])/(mi_vals[-1]-mi_vals[0]))
  colors2 = cw2((mi_vals-mi_vals[0])/(mi_vals[-1]-mi_vals[0]))
  for ii,mi_max in enumerate(mi_vals[:-1]):
    # plt.subplot(101+10*len(mi_vals)+ii)
    title = r'$m_{i,{\rm max}} = %g$' % mi_max
    mi_curr = mi_max-dmi/2.
    # plt.title(title)
    # mask_mi = rdb.m_i[mask_Z] < mi_max
    mask_mi = (mi_max>i) * (i>=mi_vals[ii+1])
    X = xx[mask_mi,:]
    if len(X)<1:
      print('skipping') 
      continue
    clf = rdb.GaussianMixture(n_components=2)
    x_pred = clf.fit(X).predict(X).astype(bool)
    if np.median(i[mask_mi][x_pred])<np.median(i[mask_mi][~x_pred]):
      x_pred = ~x_pred
    print('counts:',np.sum(x_pred),np.sum(~x_pred))
    # plt.scatter(r-i,g-i,1,c=x_pred,alpha=.05,linewidths=0)
    plt.subplot(121)
    plt.scatter(X[:,3][~x_pred],X[:,1][~x_pred],1,color=colors2[ii],
                alpha=.5,linewidths=0)
    plt.scatter(X[:,3][x_pred],X[:,1][x_pred],1,color=colors1[ii],
                alpha=.5,linewidths=0,label=title)
    
    plt.subplot(122)
    plt.scatter(i[mask_mi][~x_pred],X[:,1][~x_pred],1,color=colors2[ii],
                alpha=.125,linewidths=0)
    plt.scatter([mi_curr],[np.median(X[:,1][~x_pred])],color='b')
    plt.scatter(i[mask_mi][x_pred],X[:,1][x_pred],1,color=colors1[ii],
                alpha=.125,linewidths=0,label=title)
    plt.scatter([mi_curr],[np.median(X[:,1][x_pred])],color='r')
    
  plt.subplot(121)
  plt.xlabel(r'$(r-i)$')
  plt.ylabel(r'$(g-i)$')
  plt.xlim(-.022,1.83)
  plt.ylim(.25,4.5)
  plt.gca().set_aspect('equal')
  # plt.legend()
  
  plt.subplot(122)
  plt.xlabel('$m_i$')
  plt.ylabel('$(g-i)$')
  plt.ylim(.25,4.5)
  
  plt.tight_layout()
  plt.show()

