"""
test sklearn PCA methods
see whether that makes things more clear
https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html
"""

import red_dragon_base as rdb
np,plt = rdb.np,rdb.plt

Z_ii = .6
dZ = .0001

mask_Z = (Z_ii <= rdb.Z) * (rdb.Z < Z_ii+dZ)
g,r,i,z,Z = [field[mask_Z] for field in rdb.fields]

print("vet contains %i components" % len(Z))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# test principle component analysis for color space

from sklearn.decomposition import PCA

X = np.transpose([g-r,r-i])
pca = PCA(n_components=2)
pca.fit(X)

print("for (g-r) and (r-i):")
print("explained variance ratio:",pca.explained_variance_ratio_)
print("singular values:",pca.singular_values_)








raise SystemExit


labels = ['g-r','g-i','g-z','r-i','r-z','i-z'] 


dZ = .005
z_vals = np.arange(.05,.7,dZ)
report = np.zeros((len(z_vals),6))
for ii,Z_ii in enumerate(z_vals):
  mask_Z = (Z_ii<rdb.Z) * (rdb.Z<Z_ii+.005)
  [g,r,i,z,Z] = [field[mask_Z] for field in rdb.fields]
  X = np.transpose([g-r,g-i,g-z,r-i,r-z,i-z])
  t = pca.fit_transform(X)
  it = pca.inverse_transform([[1,0,0,0,0,0]])
  print("%g" % Z_ii,it)
  report[ii] = it

plt.plot(z_vals,report); plt.legend(labels); plt.show()

maxvals = np.max(report,axis=1)
rel = [report[ii]/maxvals[ii] for ii in range(len(z_vals))]
plt.plot(z_vals,rel); plt.legend(labels); plt.show()




# high accuracy (S/N=10) limit for bands
# see table 1, row "Single-epoch Magnitude Limit" from 
# darkenergysurvey.org/wp-content/uploads/2018/01/DR1Release.pdf
lim_g,lim_r,lim_i,lim_z = 23.57, 23.34, 22.78, 22.1
mask_g,mask_r,mask_i,mask_z = g<lim_g, r<lim_r, i<lim_i, z<lim_z
tot = mask_g*mask_r*mask_i*mask_z # about a quarter of these have all bright bands
# at the lowest redshifts, all are above these limits
# at the highest redshifts, m_z is most above barrier

rep = np.zeros((len(z_vals),5))
for ii,Z_ii in enumerate(z_vals): # np.linspace(.05,.7-dZ,N_frames)):
  print("Processing %0.3g" % Z_ii)
  mask_Z = (Z_ii<rdb.Z) * (rdb.Z<Z_ii+.005)
  [g,r,i,z,Z] = [field[mask_Z] for field in rdb.fields]
  mask_g,mask_r,mask_i,mask_z = g<lim_g, r<lim_r, i<lim_i, z<lim_z
  tot = mask_g*mask_r*mask_i*mask_z
  rep[ii] = [sum(mask_g),sum(mask_r),sum(mask_i),sum(mask_z),sum(tot)]


plt.plot(z_vals,rep)
plt.legend(['g','r','i','z','tot'])
plt.show()




scores = np.zeros(len(z_vals))

# test similarity 
for ii,Z_ii in enumerate(z_vals):
  mask_Z = (Z_ii<rdb.Z) * (rdb.Z<Z_ii+.005)
  [g,r,i,z,Z] = [field[mask_Z] for field in rdb.fields]
  X = np.transpose([g-r,g-i,g-z,r-i,r-z,i-z])
  gmm = GaussianMixture(2).fit(X)
  mask_all = gmm.predict(X)
  X = np.transpose([g-i,g-z])
  gmm = GaussianMixture(2).fit(X)
  mask_two = gmm.predict(X)
  scores[ii] = rdb.score(mask_two,mask_all)
  print(Z_ii)

ml = scores < .5
scores[ml] = 1-scores[ml]
qtls = np.quantile(scores,rdb.sv)

plt.plot(z_vals,scores)
plt.fill_between([.045,.7],[qtls[1],qtls[1]],[qtls[3],qtls[3]],color='k',alpha=.25,linewidth=0)
plt.fill_between([.04,.7],[qtls[0],qtls[0]],[qtls[4],qtls[4]],color='k',alpha=.25,linewidth=0)
plt.plot([.04,.7],[qtls[2],qtls[2]],'k')
plt.xlim(.045,.7)
plt.ylim(None,1) # keep 100% as goal
plt.xlabel('Redshift')
plt.ylabel('Similarity')
plt.tight_layout()
plt.show()








