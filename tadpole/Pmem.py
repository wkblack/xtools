# test out P_mem marking of galaxies, rather than +/- 2 sigma cut

import red_dragon_base as rdb
import theta_tools as tt
from m_star_model import m_star_cutoff

np,plt = tt.np,rdb.plt
g,r,i,Z = rdb.m_g,rdb.m_r,rdb.m_i,rdb.Z


########################################################################
# test P_mem algorithm 

def normal(theta,x): 
  mean,scatter = theta
  return (2*np.pi*scatter**2)**-.5 * np.exp(-(x-mean)**2/(2*scatter**2))


def line(theta,m_i,z):
  a,b,s = theta
  return (m_i-m_star_cutoff(z,cutoff=0.2)) * a + b


def varying_normal(theta,m_i,COL,z):
  a,b,s = theta
  return normal((line(theta,m_i,z),s),COL)


def likelihood(theta,m_i,COL,z,col='red'): 
  # unpack parameters from theta
  fR,ar,br,lgsr,bb,lgsb = theta # lg = log_10
  if col=='red':
    return fR*varying_normal((ar,br,10.**lgsr),m_i,COL,z) 
  elif col=='blue':
    return (1-fR)*varying_normal((0,bb,10.**lgsb),m_i,COL,z)
  else:
    print("ERROR: invalid color")


def L_col(Z,bands,warnings=True):
  # grab parameters 
  g,r,i = bands
  # calculate P_mem for RS and BC
  # low redshift
  theta = tt.gen_theta(Z,col='g-r',warnings=True)
  L_red = likelihood(theta,i,g-r,Z,col='red')
  L_blue = likelihood(theta,i,g-r,Z,col='blue')
  # high redshift
  theta = tt.gen_theta(Z,col='r-i',warnings=True)
  mask_Z = Z>.425
  L_red[mask_Z] = likelihood(theta[:,mask_Z],i[mask_Z],
                             r[mask_Z]-i[mask_Z],Z[mask_Z],col='red')
  L_blue[mask_Z] = likelihood(theta[:,mask_Z],i[mask_Z],
                             r[mask_Z]-i[mask_Z],Z[mask_Z],col='blue')
  return L_red,L_blue


########################################################################
# grab a redshift slice

Z_i = .645
dZ = .005

mask_Z = (Z_i<=Z) * (Z<Z_i+dZ)
g,r,i,Z = [tag[mask_Z] for tag in [g,r,i,Z]]

if __name__=='__main__':
  # likelihoods
  L_red,L_blue = L_col(Z,[g,r,i],warnings=True)
  # sum likelihoods
  sum_red = np.sum(L_red)
  sum_blue = np.sum(L_blue)
  sum_tot = sum_red+sum_blue
  print("[sum red,sum blue]/(sum tot) = [%0.3g,%0.3g]/%0.3g" \
        % (sum_red,sum_blue,sum_tot))
  
  # masking
  mask_red = L_red>L_blue
  P_red = L_red/(L_red+L_blue)
  # NOTE: mathematically, P_red>.5 <-> mask_red
  P_blue = 1-P_red
  
  # printing
  N_red = sum(mask_red)
  N_blue = sum(~mask_red)
  print('N_red:',N_red)
  print('N_blue:',N_blue)
  sum_Pred = np.sum(P_red)
  print('sum P_red:',sum_Pred)
  print('total,len:',np.sum(P_red+P_blue),len(P_red))
  
  # diff analysis
  print("Errors:")
  print('sum red err:',((sum_red-N_red)/N_red))
  print('sum P_red err:',((sum_Pred-N_red)/N_red))
  # raise SystemExit
  
  # plot differences
  from matplotlib.colors import LinearSegmentedColormap as LSC
  nodes = np.copy(rdb.sigma_vals) # [-2s,-1s,.5,+1s,+2s] ~ [5,32,50,68,95]%
  nodes[0] = 0; nodes[-1] = 1; # required endpoints for mpl cmaps
  colors = ['blue','blue','green','red','red'] # for BC, GV, RS
  gx = LSC.from_list('glxy',list(zip(nodes, colors)))
  # create plot
  plt.suptitle('z|[%0.3g,%0.3g)' % (Z_i,Z_i+dZ))
  # multi-component model
  plt.subplot(131)
  plt.scatter(r-i,g-i,3,alpha=.01,c=L_red,linewidths=0,cmap=gx)
  plt.title(r'$L_{\rm red}$')
  plt.xlabel(r'$r-i$')
  plt.ylabel(r'$g-i$')
  xlim = plt.xlim(0,1.5)
  ylim = plt.ylim(0,5)
  # binary selection model
  plt.subplot(132)
  plt.scatter(r-i,g-i,3,alpha=.01,c=P_red,linewidths=0,cmap=gx)
  plt.title(r'$L_{\rm red}/(L_{\rm red}+L_{\rm blue})$')
  plt.xlabel(r'$r-i$')
  plt.xlim(xlim)
  plt.ylim(ylim)
  # Hao +/- 2 sigma model
  plt.subplot(133)
  mask_ES0 = tt.ES0_mask(Z,g,r,i,warnings=True)
  plt.scatter(r-i,g-i,3,alpha=.01,c=mask_ES0,linewidths=0,cmap=gx)
  plt.title(r'Hao: $\pm 2\sigma$')
  plt.xlabel(r'$r-i$')
  plt.xlim(xlim)
  plt.ylim(ylim)
  # ending commands
  # plt.tight_layout()
  plt.show()


