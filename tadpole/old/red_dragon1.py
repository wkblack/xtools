# testing whether we get a clear RS in 3-space

if 0:
  import voxel
  from mpl_toolkits.mplot3d import Axes3D
  
  LEN = 10**6
  g,r,i = [voxel.f[voxel.col_loc % col][:LEN] for col in ['g','r','i']]
  Z = voxel.f[voxel.Z_loc][:LEN]
  mask = (.2<Z)*(Z<.5)
  
  fig = voxel.plt.figure()
  ax = fig.add_subplot(111,projection='3d')
  ax.scatter((g-i)[mask],(r-i)[mask],i[mask],c=Z[mask],cmap='coolwarm')
  ax.set_xlabel(r'$(g-i)$')
  ax.set_ylabel(r'$(r-i)$')
  ax.set_zlabel(r'$m_i$')
  voxel.plt.show()


# Note: need thinner redshift bins; or just view as 2D projection of m_i


if 1: # thinner redshift bins
  from matplotlib import pyplot as plt
  import seaborn as sns
  import h5py
  import numpy as np
  
  print('loading colors and redshifts')
  with h5py.File('vetted.h5','r') as ds:
    m_g,m_i,m_r,m_z,Z = [ds[key][()] for key in ds.keys()]
  
  gmi,rmi = m_g-m_i,m_r-m_i
  print('loaded colors and redshifts')
  
  saving = True
  seaborn = False
  dZ = .01
  z_vals = np.arange(.3,.5+dZ,dZ)
  for ii in range(len(z_vals)): # len(z_vals)-1,len(z_vals)):
    print('Processing %i/%i' % ((ii+1),len(z_vals)))
    Zmin,Zmax = z_vals[ii],z_vals[ii]+dZ
    mask_ii = (Zmin<=Z)*(Z<Zmax)
    N_ii = len(Z[mask_ii])
    z_label = f'z|[{Zmin:0.3g},{Zmax:0.3g})'
    print(f'Galaxy count for {z_label}: {N_ii}')
    if not seaborn:
      plt.scatter(gmi[mask_ii],rmi[mask_ii],2,alpha=.01,linewidth=0,label=z_label)
      # plt.scatter(gmi[mask_ii],rmi[mask_ii],2,alpha=.5,linewidth=0,
      #             label=z_label,c=np.sqrt(m_i[mask_ii]-min(m_i[mask_ii]))/np.sqrt(24),cmap='bone')
      # plt.colorbar(label=r'$e^{m_i}$')
    else:
      # https://seaborn.pydata.org/generated/seaborn.kdeplot.html?highlight=kdeplot#seaborn.kdeplot
      sns.kdeplot(gmi[mask_ii],rmi[mask_ii])
    plt.title(z_label)
    plt.xlabel(r'$(g-i)$')
    plt.xlim(.5,2.5)
    plt.ylabel(r'$(r-i)$')
    plt.ylim(-1,0)
    # plt.legend()
    if not saving:
      plt.show()
    else:
      if seaborn:
        plt.savefig('gmi_rmi_%02i.png' % ii)
      else:
        plt.savefig('gmi_rmi_pts_%02i.png' % ii)
      plt.clf()
  
  print('~fin')
