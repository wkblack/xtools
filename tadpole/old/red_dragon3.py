# attempt to use scikitlearn to find peaks and distinguish the Gaussians

########################################################################
# imports 

from time import time
starttime = time()
print("Loading...")
import h5py
fname = 'vetted.h5'
with h5py.File('vetted.h5','r') as ds:
  " NOTE: vetted by L>0.2L_*, mu>13, Z|[.05,.7], mag != 0 or 99 "
  m_g,m_i,m_r,m_z,Z = [ds[key][()] for key in ds.keys()]
  gmi,rmi = m_g-m_i,m_r-m_i
print("Colors and redshifts loaded in %0.3g s" % (time()-starttime))

from matplotlib import pyplot as plt
import numpy as np
import sklearn

########################################################################
# vet by redshift, for this experiment 

case = 4

if case==1:
  Z_i = .1
  dZ = .005
elif case==2:
  Z_i = .15
  dZ = .001
elif case==3: 
  Z_i = .25
  dZ = .0005
elif case==4:
  dZ = .0005
  Z_i = .375 - dZ
elif case==5:
  Z_i = .375
  dZ = .0005
elif case==6:
  Z_i = .5
  dZ = .001
else:
  print("Undefined case")
  raise SystemExit

mask_Z = (Z_i<=Z)*(Z<=Z_i+dZ)
print("Using %i galaxies" % len(Z[mask_Z]))
label_Z = r'$z|[%0.4g,%0.4g]$' % (Z_i,Z_i+dZ)

Z_m,mg_m,mr_m,mi_m = [field[mask_Z] for field in [Z,m_g,m_r,m_i]]
gmi_m,rmi_m = mg_m - mi_m, mr_m - mi_m

########################################################################
# make predictions with SciKitLearn

from sklearn.mixture import GaussianMixture
X = np.transpose([rmi_m,gmi_m])
print('making x_pred')
clf = GaussianMixture(n_components=2) #,covariance_type='diag')
x_pred = clf.fit(X).predict(X)

# fix x_pred based on mean values! (invert mask if needed)
# makes x_pred True correspond to the red sequence
mask = x_pred.astype(bool)
if np.mean(gmi_m[mask]) < np.mean(gmi_m[~mask]):
  print('flipping x_pred') 
  x_pred = 1-x_pred

if 0: # plot mean and median, revealing skew
  plt.scatter(rmi_m,gmi_m,1,c=mask,cmap='cividis',alpha=.5,linewidths=0)
  for mask in [mask,~mask]:
    meds = np.median(rmi_m[mask]),np.median(gmi_m[mask])
    means = np.mean(rmi_m[mask]),np.mean(gmi_m[mask])
    plt.arrow(means[0],means[1],meds[0]-means[0],meds[1]-means[1],# color='y',
              width=.01,head_width=.035,length_includes_head=True)
  
  plt.show()
  assert(1==0)

print('x_pred created')

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# fit line between means, find transition point
means_rmi,means_gmi = np.transpose(clf.means_) # grab means
rmi_vals,gmi_vals = [np.linspace(means[0],means[1],1000) \
                     for means in [means_rmi,means_gmi]]
Y = np.transpose([rmi_vals,gmi_vals])
Y_pred = clf.predict(Y)
idx_transit = np.where(Y_pred>0)[0][0]
if idx_transit==0:
  idx_transit = np.where(Y_pred==0)[0][0]
transit = Y[idx_transit]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# fit line to divide the two groups

def fraction_missed(guess,actual): # score of zero is best
  assert(len(guess)==len(actual))
  return np.sum(np.abs(guess-actual))/len(guess)

score = fraction_missed

def cc_line(rmi_vals,slope=0,mu_rmi=transit[0],mu_gmi=transit[1]): 
  " line in color--color space to separate RS from BC "
  return mu_gmi + slope*(np.array(rmi_vals)-mu_rmi)

def es0_test(rmi_vals,gmi_vals,slope=0):
  return gmi_vals > cc_line(rmi_vals,slope)

# score es0_test 
max_slope = 5
slope_vals = np.linspace(-max_slope,+max_slope,51)
score_vals = np.array([score(es0_test(rmi_m,gmi_m,slope_ii),x_pred) \
                       for slope_ii in slope_vals])
if 1:
  plt.scatter(slope_vals,score_vals)
  plt.xlabel('slope')
  plt.ylabel('score')
  plt.yscale('log')
  plt.ylim(min(score_vals[score_vals>0])/1.5,min(1,1.5*max(score_vals)))
  plt.show()

min_loc = score_vals==min(score_vals) # where the best score happened
best_slope = np.mean(slope_vals[min_loc])

########################################################################
# plot results
import ES0_model
Z_med = np.median(Z_m)
mask_es0 = ES0_model.ES0_mask2(Z_med,mg_m,mr_m,mi_m)

if 0: 
  plt.figure(1)
  mi_vals = np.linspace(min(mi_m),max(mi_m),100)
  gmr_vals,wth = ES0_model.gmr_line(Z_med,mi_vals)
  plt.scatter(mi_m,mg_m-mr_m,1,c=mask_es0)
  plt.plot(mi_vals,gmr_vals)
  plt.xlabel(r'$m_i$')
  plt.ylabel(r'$(g-r)$')

plt.figure(figsize=(8,4.5))
colorings = [x_pred,mask_es0]
plotnums = [121,122]
cmaps = ['cividis','coolwarm']

for ii in [0,1]:
  plt.subplot(plotnums[ii])
  plt.scatter(rmi_m,gmi_m,1,c=colorings[ii],cmap=cmaps[ii],alpha=.5,linewidths=0)
  
  # plot up centers
  plt.scatter(means_rmi,means_gmi,100,marker='x',color='k')
  # plot up transition point
  plt.scatter(transit[0],transit[1],50,marker='x',color='r')
  # plot up line fit
  plt.plot([0,1],cc_line([0,1],best_slope),'r',linewidth=.5)
  
  plt.xlabel(r'$(r-i)$')
  plt.ylabel(r'$(g-i)$')

plt.suptitle(label_Z)
# plt.tight_layout()
plt.gcf().subplots_adjust(top=.927,bottom=.138,left=.09,right=.981,
                          hspace=.2,wspace=.211)
plt.show()

# find peaks with scikitlearn :) 

print('fin~')

