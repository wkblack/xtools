# check what's wrong with the RS estimator!!! 


########################################################################
# imports 

from time import time
starttime = time()
print("Loading...")
import h5py
fname = 'vetted.h5'
with h5py.File('vetted.h5','r') as ds:
  " NOTE: vetted by L>0.2L_*, mu>13, Z|[.05,.7], mag != 0 or 99 "
  m_g,m_i,m_r,m_z,Z = [ds[key][()] for key in ds.keys()]
  gmi,rmi = m_g-m_i,m_r-m_i
print("Colors and redshifts loaded in %0.3g s" % (time()-starttime))

from matplotlib import pyplot as plt
import numpy as np
import ES0_model
import sklearn

# set up sigma values
from scipy.special import erf
one_sigma = sixty_eight = erf(1/np.sqrt(2))
two_sigma = ninety_five = erf(2/np.sqrt(2))
sigma_vals = [1-two_sigma,1-one_sigma,.5,one_sigma,two_sigma]
def sigval(sig):
  if np.abs(sig)<1: # interpolate for sigma < 1
    return .5 + (one_sigma-.5)*sig
  report = erf(sig/np.sqrt(2))
  if sig<0: # negative sigma -> percentiles < .5
    report += 1
  return report

########################################################################
# step through redshift and cf model

dZ = .0005
for Z_i in np.linspace(.1,.375,4):
  # grab galaxies in this redshift bin
  label_Z = r'$z|[%0.4g,%0.4g]$' % (Z_i,Z_i+dZ)
  mask_Z = (Z_i<Z)*(Z<Z_i+dZ)
  Z_m,mg_m,mr_m,mi_m = [field[mask_Z] for field in [Z,m_g,m_r,m_i]]
  gmi_m,gmr_m,rmi_m = mg_m-mi_m, mg_m-mr_m, mr_m-mi_m
  
  # calculate RS
  Z_med = np.median(Z_m)
  mi_vals = np.linspace(min(mi_m),max(mi_m),100)
  gmr1,wth1 = ES0_model.gmr_line(Z_med,mi_vals)
  gmr2,wth2 = ES0_model.gmr_line(Z_med,mi_vals,ES0_model.HaoEtAl2010)
  mask_es0 = ES0_model.ES0_mask(Z_med,mg_m,mr_m,mi_m)
  
  # plot results
  plt.scatter(mi_m,mg_m-mr_m,1,c=mask_es0,cmap='coolwarm')
  plt.plot(mi_vals,gmr1)
  plt.plot(mi_vals,gmr2)
  plt.xlabel(r'$m_i$')
  plt.ylabel(r'$(g-r)$')
  plt.title(label_Z)
  plt.tight_layout()
  plt.show()
