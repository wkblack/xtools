# Red dragon 2
# an attempt to characterize the RS in color-color space
# looking here at whether the min mag gals are centered well
"""
  The question is whether the brightest galaxies are centered on their 
  respective Gaussians, or whether the RS slope throws things off. 
  
  It wasn't obvious, but the peaks seemed to be central, so 
  there doesn't seem to be a huge running with mag.
"""

########################################################################
# imports 

from time import time
starttime = time()
print("Loading...")
import h5py
fname = 'vetted.h5'
with h5py.File('vetted.h5','r') as ds:
  " NOTE: vetted by L>0.2L_*, mu>13, Z|[.05,.7], mag != 0 or 99 "
  m_g,m_i,m_r,m_z,Z = [ds[key][()] for key in ds.keys()]
  gmi,rmi = m_g-m_i,m_r-m_i
print("Colors and redshifts loaded in %0.3g s" % (time()-starttime))

from matplotlib import pyplot as plt
import numpy as np

from ES0_model import ES0_mask2

########################################################################

# define magnitude difference
mag = 100**.2 # apx 2.5

if __name__=='__main__':
  # grab redshift slice
  dZ = .0005
  sky = False # attempt to mimic sky image
  saving = True # save files
  N_frames = 100 # number of frames to display
  col_mag = True # color by magnitude
  
  # model for dZ_i: f(x) = A*e^(-a x)
  H = .35
  a = np.log(30)/(.5-.05)
  A = np.exp(.5*a)
  
  # for ii,Z_i in enumerate([.375]):
  for ii,Z_i in enumerate(np.linspace(.05,.7-dZ,N_frames)):
    dZ_i = dZ + dZ*A*np.exp(-a*Z_i)
    print("dZ_i=%0.3g" % dZ_i)
    mask_Z = (Z_i<Z)*(Z<Z_i + dZ_i)
    print("Using %i gals for set %i" % (len(Z[mask_Z]),ii+1))
    sorting = np.argsort(-m_i[mask_Z])
    m_i_s,gmi_s,rmi_s = [field[mask_Z][sorting] for field in [m_i,gmi,rmi]]
    m_g_s,m_r_s = [field[mask_Z][sorting] for field in [m_g,m_r]]
    
    # plot in CC space,
    # coloring min mag gals seperately 
    if col_mag: # color by magnitude
      coloring = m_i_s**3
      colormap = 'bone_r' if sky else 'rainbow_r'
      tpcy = 1
    else: # color by RS fit from my version of Hao
      Z_med = np.median(Z[mask_Z])
      coloring = ES0_mask2(Z_med,m_g_s,m_r_s,m_i_s)
      colormap = 'coolwarm'
      tpcy = .5
    # this sizing makes area of marker propto brightness on sky :)
    sizing = 50/mag**(m_i_s-min(m_i_s)) 
    plt.figure(figsize=(8,4.5))
    
    if 0: # make a triple plot
      plt.subplot(221,facecolor='k' if sky else None)
      plt.scatter(m_i_s,gmi_s,sizing,c=coloring,cmap=colormap,alpha=tpcy)
      # plt.xlabel(r'$m_i$')
      plt.ylabel(r'$(g-i)$')
      
      plt.subplot(223,facecolor='k' if sky else None)
      plt.scatter(m_i_s,rmi_s,sizing,c=coloring,cmap=colormap,alpha=tpcy)
      plt.xlabel(r'$m_i$')
      plt.ylabel(r'$(r-i)$')
      
      plt.subplot(122,facecolor='k' if sky else None)
    else: # add title for the current redshift 
      plt.title(r'Redshifts (%0.4g,%0.4g)' % (Z_i,Z_i+dZ_i))
    
    plt.xlim(.25,4.5) # plt.xlim(0,3)
    plt.ylim(-.022,1.83) # plt.ylim(-1.5,0)
    # always make this plot
    plt.scatter(gmi_s,rmi_s,sizing,c=coloring,cmap=colormap,alpha=tpcy)
    plt.xlabel(r'$(g-i)$')
    plt.ylabel(r'$(r-i)$')
    
    plt.tight_layout()
    if saving:
      plt.savefig('rd2_pics/cc%05i.png' % ii)
    else:
      plt.show()
    plt.clf()
