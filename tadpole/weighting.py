# idea: weight by magnitude, so sklearn clustering is improved. 

import red_dragon_base as rdb
np,plt = rdb.np,rdb.plt

# N_points = 100**(1/5) * (0.2L*(z)-m_i)
def N_points(Z,m_i):
  return np.ceil((100**.2)*(rdb.m_star_cutoff(Z,cutoff=0.2)-m_i)).astype(int)

Z,dZ=.5,.01 # overwrite Z
N_frames = 7
saving = True
for ii,Z in enumerate(np.linspace(.05,.7-dZ,5)):
  mask_Z = (Z<rdb.Z)*(rdb.Z<Z+dZ)
  
  zz,g,r,i = [tag[mask_Z] for tag in [rdb.Z,rdb.m_g,rdb.m_r,rdb.m_i]]
  
  # duplicate higher magnitude values:
  N = N_points(zz,i)
  
  if 0:
    plt.hist(N,bins=np.arange(13))
    plt.show()
  
  # make augmented dataset
  zz_A,g_A,r_A,i_A = [np.repeat(tag,N) for tag in [zz,g,r,i]]
  
  # make Gaussian Mixtures of the two and compare
  clf0,mask0 = rdb.GaussMix(r-i,g-i) # default "0"
  clfA,maskA = rdb.GaussMix(r_A-i_A,g_A-i_A) # augmented "A"
  
  
  if 0: # TODO: have it predict based on the original
    X = np.transpose([r-i,g-i])
    mask1 = clf.predict(X)
  # rdb.score_compare 
  # I'd like to shrink maskA down to the original size, 
  # so I can compare the two, but that's a difficult business,
  # so I'll hold off for now
  
  
  # make plots
  plt.figure(figsize=(9,9))
  plt.subplots_adjust(top=.75)
  plt.suptitle("Z|[%0.4g,%0.4g)" % (Z,Z+dZ))
  for jj,vals in enumerate(zip([r-i,r_A-i_A],[g-i,g_A-i_A],[mask0,maskA])):
    plt.subplot(121+jj)
    plt.scatter(vals[0],vals[1],1,c=vals[2],alpha=.01,linewidths=0)
    plt.xlim(-.022,1.83)
    plt.xlabel(r'$(r-i)$')
    plt.ylim(.25,4.5)
    plt.ylabel(r'$(g-i)$') # only for the first plot
    plt.gca().set_aspect('equal')
  
  plt.tight_layout()
  if saving:
    fname = 'gif_pics/weight_%03i.png' % ii
    plt.savefig(fname)
    plt.clf()
    print('Saved %s' % fname)
  else:
    plt.show()
  
