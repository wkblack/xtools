# Red Dragon

A package designed to select the red sequence within halos of the Buzzard Flock simulation

## Usage

```python
import red_dragon_base as rdb
rdb.analyze_sum(plot_fits=True)
```

## Contents

```
alt/  rd1_pics/  ES0_model.py     red_dragon3.py      vetted.h5
out/  rd2_pics/  m_star_model.py  red_dragon_base.py
```

    red_dragon_base.py
    * contains main 


## Overleaf paper

[link](https://www.overleaf.com/read/cbsmtdpsbpfy)

[readme support](https://www.makeareadme.com/)
