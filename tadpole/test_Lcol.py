import red_dragon_base as rdb
from theta_tools import L_col

np,plt = rdb.np,rdb.plt
fname = 'test_Lcol.npy'
creating = False

if creating:
  g,r,i,Z = rdb.m_g,rdb.m_r,rdb.m_i,rdb.Z
  dZ = .005
  zvals = np.arange(.05,.7,dZ)
  report = np.zeros((3,len(zvals)))
  report[0,:] = zvals + dZ/2
  
  for ii,Z_i in enumerate(zvals):
    print("Z:",Z_i)
    mask_Z = (Z_i<=Z) * (Z<Z_i+dZ)
    g_ii,r_ii,i_ii,Z_ii = [tag[mask_Z] for tag in [g,r,i,Z]]
    L_red,L_blue = L_col(Z_ii,[g_ii,r_ii,i_ii])
    report[1,ii] = np.sum(L_red>L_blue) # hard cut
    report[2,ii] = np.sum(L_red/(L_red+L_blue)) # real vals
  
  np.save(fname,report)
  print("Saved",fname)
  
else: # run analysis
  Z,Nred,sum_Pred = np.load(fname)
  Z_break = .54
  # plot counts vs redshift
  plt.subplot(211)
  plt.plot(Z,Nred,label=r'$N_{\rm red}$')
  plt.plot(Z,sum_Pred,label=r'$\sum P_{\rm red}$')
  plt.ylabel('Count')
  plt.yscale('log')
  plt.legend()
  ylim = plt.ylim()
  plt.plot([Z_break,Z_break],ylim,'grey',linewidth=.5) # plot transition line
  plt.ylim(ylim)
  # plot relative error
  plt.subplot(212)
  err = (sum_Pred-Nred)/Nred
  plt.plot(Z,err)
  plt.xlabel('Redshift')
  plt.ylabel('relative error')
  ylim = plt.ylim()
  plt.plot([Z_break,Z_break],ylim,'grey',linewidth=.5) # plot transition line
  plt.ylim(ylim)
  xlim = plt.xlim()
  plt.plot(xlim,[0,0],'k')
  qntls = np.quantile(err,rdb.sigma_vals)
  print('median error: %0.3g%%' % (100*qntls[2]))
  o = np.ones(2)
  plt.fill_between(xlim,o*qntls[1],o*qntls[3],alpha=.0625,color='b',linewidth=0)
  plt.fill_between(xlim,o*qntls[0],o*qntls[4],alpha=.0625,color='b',linewidth=0)
  plt.xlim(xlim)
  
  # display plot
  plt.tight_layout()
  plt.show()
