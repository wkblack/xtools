# show the difference between GMM+ and RD beta across redshift
import theta_tools as tt
import rd_beta
import red_dragon_base as rdb
np,plt,h5py = rdb.np,rdb.plt,rdb.h5py

fname_out = 'counting.h5'


def plot_diff(Z=.699,dZ=.001):
  mask_Z = (Z<=rdb.Z)*(rdb.Z<Z+dZ)
  title_Z = r'$z|[%0.3f,%0.3f)$' % (Z,Z+dZ)
  g,r,i,z,Z = [field[mask_Z] for field in rdb.fields]
  Z_med = np.median(Z)
  
  Pr_tt = tt.P_red(Z,[g,r,i])
  Pr_rd = rd_beta.P_red(Z,[g,r,i,z])
  P_reds = [Pr_tt,Pr_rd]
  titles = ['GMM+','RD beta']
  
  for ii,ct in enumerate(zip(P_reds,titles)):
    col,title = ct
    plt.subplot(1,2,1+ii)
    plt.title(title)
    plt.scatter(g-r,r-i,1,c=col,alpha=.02/Z_med,cmap=rdb.cmap_galaxy)
    plt.xlim(0,3)
    plt.xlabel(r'$(g-r)$')
    plt.ylim(.2,1.6)
    plt.ylabel(r'$(r-i)$')
  
  plt.suptitle(title_Z)
  plt.tight_layout()
  plt.show()


def save_vals(z_vals=[.1,.699],dZ=.001):
  output = np.zeros((len(z_vals),2))
  output_sum = np.zeros((len(z_vals),2))

  for ii in range(len(z_vals)):
    Z_ii = z_vals[ii]
    mask_Z = (Z_ii<=rdb.Z)*(rdb.Z<Z_ii+dZ)
    title_Z = r'$z|[%0.3f,%0.3f)$' % (Z_ii,Z_ii+dZ)
    print(title_Z)
    g,r,i,z,Z = [field[mask_Z] for field in rdb.fields]
    Z_med = np.median(Z)
    
    Pr_tt = tt.P_red(Z,[g,r,i])
    Pr_rd = rd_beta.P_red(Z,[g,r,i,z])
    P_reds = [Pr_tt,Pr_rd]
    output[ii] = [len(Z[p>.5]) for p in P_reds]
    output_sum[ii] = [np.sum(p) for p in P_reds]
  
  print('Saving output...')
  with h5py.File(fname_out,'w') as ds:
    ds.create_dataset('hard cut',data=output)
    ds.create_dataset('prob sum',data=output_sum)
    ds.create_dataset('z_vals',data=z_vals)
    ds.attrs['dZ'] = dZ


# analyze above data
names = ['GMM+','RD beta']

def analyze_vals(relative=names[0]):
  with h5py.File(fname_out,'r') as ds:
    hard_cut = ds['hard cut'][()]
    prob_sum = ds['prob sum'][()]
    z_vals = ds['z_vals'][()]
    dZ = ds.attrs['dZ']
  # plot data
  if relative==names[0]:
    plt.title(r'RD beta values / GMM+ values')
    plt.scatter(z_vals,hard_cut[:,1]/hard_cut[:,0],10,
                label=r'$P_{\rm red}>.5$')
    plt.scatter(z_vals,prob_sum[:,1]/prob_sum[:,0],10,
                label=r'$\sum P_{\rm red}$')
  elif relative==names[1]:
    plt.title(r'GMM+ values / RD beta values')
    plt.scatter(z_vals,hard_cut[:,0]/hard_cut[:,1],10,
                label=r'$P_{\rm red}>.5$')
    plt.scatter(z_vals,prob_sum[:,0]/prob_sum[:,1],10,
                label=r'$\sum P_{\rm red}$')
  else:
    print("Unset relative value")
    return
  # set axes
  plt.xlabel('Redshift')
  # plt.yscale('log') # for full counts
  plt.ylabel('Relative counts')
  # wrap it up
  plt.legend()
  plt.tight_layout()
  plt.show()


if __name__=='__main__':
  analyze_vals()


