# program to import all the core features of red dragon / tadpole analysis

########################################################################
# import statements
########################################################################
# time importation of colors
from time import time
starttime = time()
print("Loading...")
import h5py
fname = 'vetted.h5'
with h5py.File('vetted.h5','r') as ds:
  " NOTE: vetted by L>0.2L_*, mu>13, Z|[.05,.7], mag != 0 or 99 "
  m_g,m_i,m_r,m_z,Z = [ds[key][()] for key in ds.keys()]
  gmi,rmi = m_g-m_i,m_r-m_i
print("Colors and redshifts loaded in %0.3g s" % (time()-starttime))
fields = [m_g,m_r,m_i,m_z,Z]

# other imports
from sklearn.mixture import GaussianMixture
from ES0_model import ES0_mask2 as RS_mask
from matplotlib import pyplot as plt
from glob import glob
import numpy as np
import sklearn

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# define magnitude difference
mag = 100**.2 # apx 2.5

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up sigma values
from scipy.special import erf
one_sigma = sixty_eight = erf(1/np.sqrt(2))
two_sigma = ninety_five = erf(2/np.sqrt(2))
sv = sigma_vals = [1-two_sigma,1-one_sigma,.5,one_sigma,two_sigma]
def sigval(sig):
  " return approximate quantile for a given number of 'sigmas' away "
  if np.abs(sig)<1: # interpolate for sigma < 1
    return .5 + (one_sigma-.5)*sig
  report = erf(sig/np.sqrt(2))
  if sig<0: # negative sigma -> percentiles < .5
    report += 1
  return report

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# create galaxy colormap based on membership probability
# mark anything with >95% probability as the full color, else green
# (trying to make the green valley more noticable, small as it is) 

from matplotlib.colors import LinearSegmentedColormap as LSC
cnv = [0,sv[0],.5,sv[-1],1] # values for coloring the map (<5% or >95%)
cnc = ['blue','blue','green','red','red'] # for BC, GV, RS
cmap_galaxy = LSC.from_list('galaxy',list(zip(cnv,cnc)))



########################################################################
# function statements
########################################################################

def score(set1,set2): 
  " returns percent correctly guessed (zero is best score) "
  assert(len(set1)==len(set2))
  return np.sum(np.abs(set1-set2))/len(set1)


def cc_line(rmi_vals,slope,T_rmi,T_gmi): 
  " line in color--color space to separate RS from BC "
  return T_gmi + slope*(np.array(rmi_vals)-T_rmi)


def es0_test(rmi_vals,gmi_vals,slope,T_rmi,T_gmi):
  " use cc line to select red sequence "
  return gmi_vals > cc_line(rmi_vals,slope,T_rmi,T_gmi)


def skew_plot(rmi_vals,gmi_vals,mask,method=0):
  """ 
  plot skew from mean to median 
  method 0: pure mean to median comparison
  method 1: mean to low-mag mean comparison
  """
  plt.scatter(rmi_vals,gmi_vals,1,c=mask,cmap='cividis',alpha=.5,linewidths=0)
  for mask in [mask,~mask]:
    meds = np.median(rmi_vals[mask]),np.median(gmi_vals[mask])
    means = np.mean(rmi_vals[mask]),np.mean(gmi_vals[mask])
    plt.arrow(means[0],means[1],meds[0]-means[0],meds[1]-means[1],# color='y',
              width=.01,head_width=.035,length_includes_head=True)
  plt.show()


def GaussMix(rmi_vals,gmi_vals):
  """ 
  group input (r-i) and (g-i) into two independent Gaussians
  return fit (clf) and RS mask (higher mean labeled RS)
  """
  assert(len(rmi_vals)==len(gmi_vals))
  # make predictions with SciKitLearn
  X = np.transpose([rmi_vals,gmi_vals])
  clf = GaussianMixture(n_components=2)
  x_pred = clf.fit(X).predict(X)
  
  # fix x_pred based on mean values---invert mask if needed
  # makes x_pred True correspond to the red sequence
  mask = x_pred.astype(bool)
  if np.mean(gmi_vals[mask]) < np.mean(gmi_vals[~mask]):
    x_pred = 1-x_pred
  return clf,x_pred


outname_analyze_Z_temp = 'out/Z_%0.4f_to_%0.4f.h5'
outname_analyze_Z_glob = 'out/Z_*_to_*.h5'


def analyze_Z(Z_min,dZ=.005,printing=False,plotting=False):
  """ 
  read in redshift range, output:
  * transition points & slopes
  * plots
  """
  # mask by redshift
  mask_Z = (Z_min<=Z)*(Z<=Z_min+dZ)
  N = len(Z[mask_Z])
  if N<1:
    print("Error: No galaxies here")
    return -1
  elif printing: print("Using %i galaxies" % len(Z[mask_Z]))
  label_Z = r'$z|[%0.4g,%0.4g]$' % (Z_min,Z_min+dZ)
  # update colors to masked values
  Z_m, mg_m, mr_m, mi_m, gmi_m, rmi_m = [field[mask_Z] for field in \
                                         [Z, m_g, m_r, m_i, gmi, rmi]]
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # make predictions with SciKitLearn
  clf,x_pred = GaussMix(rmi_m,gmi_m)
  
  # fit line between means, find transition point
  means_rmi,means_gmi = np.transpose(clf.means_) # grab means
  rmi_vals,gmi_vals = [np.linspace(means[0],means[1],1000) \
                       for means in [means_rmi,means_gmi]]
  Y = np.transpose([rmi_vals,gmi_vals])
  Y_pred = clf.predict(Y)
  idx_transit = np.where(Y_pred>0)[0][0]
  if idx_transit==0:
    idx_transit = np.where(Y_pred==0)[0][0]
  transit = Y[idx_transit]
  
  # find optimal slope for this redshift range
  max_slope = 5
  slope_vals = np.linspace(-max_slope,+max_slope,501)
  score_vals = np.array([score(es0_test(rmi_m,gmi_m,slope_ii,\
                         transit[0],transit[1]),x_pred) \
                         for slope_ii in slope_vals])
  if plotting:
    plt.plot(slope_vals,score_vals,10)
    plt.title(label_Z)
    plt.xlabel('Slope')
    plt.ylabel('Score')
    plt.tight_layout()
    plt.show()
  
  min_loc = score_vals==min(score_vals) # where the best score happened
  best_slope = np.mean(slope_vals[min_loc])
  best_score = np.mean(score_vals[min_loc])
  
  # create output file
  outname = outname_analyze_Z_temp % (Z_min,(Z_min+dZ))
  with h5py.File(outname,'w') as ds:
    for label,dset in zip(['Z','m_g','m_r','m_i'],
                          [Z_m,mg_m,mr_m,mi_m]):
      ds.create_dataset(label,data=dset)
    for label,attr in zip(['T_rmi','T_gmi','slope','Z_min','dZ',
                           'count','rmi_means','gmi_means','score'],
                          [transit[0],transit[1],best_slope,Z_min,dZ,
                           len(Z_m),means_rmi,means_gmi,best_score]):
      ds.attrs[label] = attr
  
  return outname


def plot_score(Z_vals,score_vals,plot_quantiles=False):
  " plot a given set of scores, with the option to plot up quantiles "
  mn,mx = min(Z_vals)-.005,max(Z_vals)+.005
  plt.scatter(Z_vals,score_vals)
  plt.xlabel('Redshift')
  plt.xlim(mn,mx)
  plt.ylabel('Score')
  plt.ylim(min(score_vals[score_vals>0])/2,max(max(score_vals),.1))
  plt.yscale('log')
  if plot_quantiles:
    quantiles = np.quantile(score_vals,sigma_vals)
    plt.plot([mn,mx],[quantiles[2],quantiles[2]],color='k',alpha=.5)
    plt.fill_between([mn,mx],[quantiles[1],quantiles[1]],
                     [quantiles[3],quantiles[3]],color='k',alpha=.125,linewidth=0)
    plt.fill_between([mn,mx],[quantiles[0],quantiles[0]],
                     [quantiles[4],quantiles[4]],color='k',alpha=.125,linewidth=0)
  plt.tight_layout()
  plt.show()


def analyze_sum(plot_order=False,plot_fits=False,plot_scores=False,plot_quantiles=True):
  """
  Read in all output files matching outname_analyze_Z_glob
  and plot aggregate data trends
  
  * default (no arguments)
      aggregate data from all files (no analysis)
      returns Z_vals, T_rmi_vals, T_gmi_vals, slope_vals, score_vals
  * plot_order=False
      test different orders---how they look as they fit the data
      returns None
  * plot_fits=False
      see transition point (r-i) and (g-i) + slope parameter fits
      returns fit_Tr_params,fit_Tg_params,fit_sl_params
  * plot_scores=False (plot_quantiles=True)
      plot how well Tadpole worked for each z-bin (plot quantiles for score)
      returns score_vals
  """
  files = glob(outname_analyze_Z_glob)
  N = len(files)
  if N<1:
    print("Error: No matching files found")
  else:
    print("Processing %i files" % N)
  
  Z_vals,T_rmi_vals,T_gmi_vals,slope_vals,score_vals = np.zeros((5,N))
  
  for ii in range(N):
    with h5py.File(files[ii],'r') as ds:
      Z_vals[ii] = ds.attrs['Z_min']
      T_rmi_vals[ii] = ds.attrs['T_rmi']
      T_gmi_vals[ii] = ds.attrs['T_gmi']
      slope_vals[ii] = ds.attrs['slope']
      score_vals[ii] = ds.attrs['score']
  
  # sort values
  sorting = np.argsort(Z_vals)
  Z_vals,T_rmi_vals,T_gmi_vals,slope_vals = [field[sorting] for field \
                          in [Z_vals,T_rmi_vals,T_gmi_vals,slope_vals]]
  
  if plot_order: # try different orders of polynomials
    for order in range(1,5):
      # fit with polynomials
      plt.title("Order %i fit" % order)
      plt.scatter(Z_vals,T_rmi_vals,1,label='T_rmi')
      plt.scatter(Z_vals,T_gmi_vals,1,label='T_gmi')
      plt.scatter(Z_vals,slope_vals,1,label='slope')
      plt.xlabel('Redshift')
      plt.legend()
      plt.tight_layout()
      
      fit_Tr = np.poly1d(np.polyfit(Z_vals,T_rmi_vals,order))
      fit_Tg = np.poly1d(np.polyfit(Z_vals,T_gmi_vals,order))
      fit_sl = np.poly1d(np.polyfit(Z_vals,slope_vals,order))
      
      plt.plot(Z_vals,fit_Tr(Z_vals),'k-',
               Z_vals,fit_Tg(Z_vals),'k-',
               Z_vals,fit_sl(Z_vals),'k-',
               linewidth=.5)
    
      plt.show()
    return
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  elif plot_fits:
    # diplay values
    plt.scatter(Z_vals,T_rmi_vals,1,label='T_rmi')
    plt.scatter(Z_vals,T_gmi_vals,1,label='T_gmi')
    plt.scatter(Z_vals,slope_vals,1,label='slope')
    plt.xlabel('Redshift')
    plt.legend()
    plt.tight_layout()
    
    fit_Tr_params = np.polyfit(Z_vals,T_rmi_vals,4)
    fit_Tg_params = np.polyfit(Z_vals,T_gmi_vals,4)
    fit_sl_params = np.polyfit(Z_vals,slope_vals,3)
    fit_Tr = np.poly1d(fit_Tr_params)
    fit_Tg = np.poly1d(fit_Tg_params)
    fit_sl = np.poly1d(fit_sl_params)
    plt.plot(Z_vals,fit_Tr(Z_vals),'k-',
             Z_vals,fit_Tg(Z_vals),'k-',
             Z_vals,fit_sl(Z_vals),'k-',
             linewidth=.5)
    plt.show()
    return fit_Tr_params,fit_Tg_params,fit_sl_params
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  elif plot_scores:
    plot_score(Z_vals,score_vals,plot_quantiles)
    return score_vals
  else:
    return Z_vals, T_rmi_vals, T_gmi_vals, slope_vals, score_vals
  

# fit parameters from [.05,.7]:
# second order
_pars_Tr = np.array([1.65669321,-0.33813035,0.39498498])
_pars_Tg = np.array([-2.17803918,3.71382708,0.84166446])
# fourth order fit for transition points
_pars_Tr = np.array([-10.14059385,18.81298931,-10.0083629,2.38284624,0.21326387])
_pars_Tg = np.array([33.02754482,-48.45871365,21.48292047,-0.61795555,1.07272783])
# third order fit for slope
_pars_sl = np.array([102.77256477,-121.92528211,31.21894694,0.58896342])
# fit functions from fit parameters above
_fit_Tr = np.poly1d(_pars_Tr)
_fit_Tg = np.poly1d(_pars_Tg)
_fit_sl = np.poly1d(_pars_Tr)


def ES0_mask(Z,g,r,i):
  " Create Tadpole ES0_mask from parameter fits "
  half_dZ = .005/2. # shift due to binning
  return es0_test(r-i,g-i,_fit_sl(Z-half_dZ),
                  _fit_Tr(Z-half_dZ),
                  _fit_Tg(Z-half_dZ))


def test_mask(mask=RS_mask,printing=False,plotting=True,outname=None,col=None):
  """
  test_mask(mask=RS_mask,printing=False,plotting=True,outname=None,col=None)
  input mask(Z,m_g,m_r,m_i,[col])
  tests vs sklearn GaussianMixture bimodal clustering
  """
  # for each redshift bin, find the RS mask via GaussianMixture
  dZ = .005 # default redshift bin
  Z_vals = np.arange(.05,.7,dZ)
  score_vals = np.zeros(len(Z_vals))
  for ii,Z_i in enumerate(Z_vals):
    # mask by redshift
    mask_Z = (Z_i<=Z)*(Z<=Z_i+dZ)
    label_Z = r'$z|[%0.4g,%0.4g]$' % (Z_i,Z_i+dZ)
    if printing: 
      print(label_Z)
    # update colors to masked values
    Z_m, mg_m, mr_m, mi_m, mz_m, gmi_m, rmi_m = [field[mask_Z] \
                    for field in [Z, m_g, m_r, m_i, m_z, gmi, rmi]]
    
    # cf to input mask model
    clf,x_pred = GaussMix(rmi_m,gmi_m) # SciKitLearn prediction
    if col==None:
      RS_pred = mask(Z_m,mg_m,mr_m,mi_m)
    elif col==-1: 
      RS_pred = mask(Z_m,[mg_m,mr_m,mi_m])
    elif col==-2: 
      X = np.transpose([mg_m-mr_m,mg_m-mi_m,mg_m-mz_m,\
                        mr_m-mi_m,mr_m-mz_m,mi_m-mz_m])
      RS_pred = mask(Z_m,X)
    else:
      RS_pred = mask(Z_m,mg_m,mr_m,mi_m,col)
    
    score_vals[ii] = score(x_pred,RS_pred)
  
  if plotting: 
    plot_score(Z_vals,score_vals,plot_quantiles=True)
  if outname != None:
    np.save(outname,score_vals)
  return score_vals


def score_compare(scores,labels=['Tadpole','ECGMM+'],verts=[.375,.475],relative=False):
  """
  score_compare(scores,labels=['Tadpole','ECGMM+'],verts=[.375,.4525],relative=False):
  
  compare scores between different RS selectors (plot)
  
  Parameters
  ----------
  scores : array_like
      input scores to compare, e.g.  scores=[score0,score1]
      hardcoded to two values at the moment
  labels : plot labels
  verts : array_like
      plot vertical lines for marking things
  relative : boolean
      whether to plot values relative to each other
  """
  Z = np.arange(.0525,.7,.005)
  if relative:
    score0,score1 = scores
    label0,label1 = labels
    label_rel = '%s score / %s score' % (label0,label1)
    plt.scatter(Z,score0/score1,3)
    plt.ylabel(label_rel)
  else:
    for s,l in zip(scores,labels):
      plt.scatter(Z,s,3,label=l)
    plt.ylabel('Score')
    plt.legend()
  plt.xlim(.05,.7)
  plt.xlabel('Redshift')
  if verts != None:
    ylim = plt.ylim() # fix ylim to pre-line widths
    for vert in verts:
      plt.plot([vert,vert],ylim,color='grey',alpha=.5,linewidth=.5)
    plt.ylim(ylim) # re-assert ylim after adding lines
  plt.tight_layout()
  plt.show()


from theta_tools import ES0_mask as ES0_mask_tt
from m_star_model import m_star_cutoff

def gif_maker(N_frames=16,dZ=.005,sky=False,saving=True):
  """
  N_frames = 13  # number of frames to display
  dZ = .005      # redshift slice thickness
  sky = False    # attempt to mimic sky image
  saving = True  # save files
  """
  plt.rcParams.update({'figure.max_open_warning': 0})
  for ii,z in enumerate(np.linspace(.05,.7-dZ,N_frames)):
    mask_Z = (z<=Z)*(Z<z+dZ)
    sorting = np.argsort(-m_i[mask_Z])
    zz,g,r,i = [key[mask_Z][sorting] for key in [Z,m_g,m_r,m_i]]
    Z_med = np.median(zz)
    
    # set sizing: make area of marker propto brightness on sky :)
    min_i = 23.1 # cap redshift magnitude cutoff 0.2L*(z=.7)
    sizing = 2/mag**(i-m_star_cutoff(Z_med))
    ####################################################################
    plt.figure(figsize=(12,9)) # (8,4.5)
    plt.suptitle(r'z|[%0.5g,%0.5g)' % (z,z+dZ))
    plt.figtext(0.5,0.075,r'$(r-i)$')
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    plt.subplot(131) # color by magnitude
    coloring = i**3
    colormap = 'bone_r' if sky else 'rainbow_r'
    tpcy = 1
    plt.gca().set_facecolor('#16161d' if sky else 'w')
    
    plt.scatter(r-i,g-i,sizing,c=coloring,cmap=colormap,alpha=tpcy,linewidths=0)
    plt.ylabel(r'$(g-i)$') # only for the first plot
    plt.xlim(-.022,1.83)
    plt.ylim(.25,4.5)
    plt.gca().set_aspect('equal')
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    plt.subplot(132) # color by GaussianMixture
    clf,coloring = GaussMix(r-i,g-i)
    colormap = 'cividis_r'
    tpcy = .25
    
    plt.scatter(r-i,g-i,sizing,c=coloring,cmap=colormap,alpha=tpcy,linewidths=0)
    plt.xlim(-.022,1.83)
    plt.ylim(.25,4.5)
    plt.gca().set_aspect('equal')
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    plt.subplot(133) # color by RS fit from my version of Hao
    coloring = ES0_mask_tt(Z_med,g,r,i)
    colormap = 'coolwarm'
    tpcy = .5
    
    plt.scatter(r-i,g-i,sizing,c=coloring,cmap=colormap,alpha=tpcy,linewidths=0)
    plt.xlim(-.022,1.83)
    plt.ylim(.25,4.5)
    plt.gca().set_aspect('equal')
    
    # plt.tight_layout()
    plt.subplots_adjust(top=.95,bottom=.125,left=.02,right=1,hspace=0,wspace=0)
    if saving:
      fname = 'gif_pics/cc%03i.png' % ii
      plt.savefig(fname)
      plt.clf()
      print('Saved %s' % fname)
    else:
      plt.show()







# fin
