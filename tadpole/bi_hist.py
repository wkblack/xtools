import red_dragon_base as rdb
fields = [rdb.m_g,rdb.m_r,rdb.m_i,rdb.m_z,rdb.Z]
import theta_tools as tt
np,plt = rdb.np,rdb.plt

Z_min,Z_max = .05,.7
dZ = .005

saving = True
fname_template = 'gif_pics/bi_hist_%03i.png'

if saving: # execution phase
  Z_vals = np.arange(Z_min,Z_max,dZ)
else: # testing phase
  Z_vals = [.05,.375,.7-dZ] # ymax = [230, 3100, 3200]

bins = np.linspace(.25,4.5,250)
Z_label = r'$z|[%0.3f,%0.3f)$'

for ii,Z_ii in enumerate(Z_vals):
  # load colors for this redshift slice
  mask_Z = (Z_ii<=rdb.Z)*(rdb.Z<Z_ii+dZ)
  g,r,i,z,Z = [tag[mask_Z] for tag in fields]
  gmi = g-i
  
  mask_ES0 = tt.mask_red(Z,[g,r,i]) # binary
  
  # nbp = plt.hist([gmi[mask_ES0],gmi[~mask_ES0]],bins,stacked=True,density=True)
  nbp = plt.hist(gmi,bins,histtype='step',alpha=.75,color='black')
  nbp = plt.hist(gmi[~mask_ES0],bins,alpha=.75,color='blue')
  nbp = plt.hist(gmi[mask_ES0],bins,alpha=.75,color='red')
  plt.title(Z_label % (Z_ii,Z_ii+dZ))
  plt.xlabel(r'$(g-i)$')
  ymax = min(10000*Z_ii,4000)
  plt.ylim(0,ymax)
  plt.tight_layout()
  
  if not saving:
    plt.show()
  else:
    fname = fname_template % ii
    plt.savefig(fname)
    plt.clf()
    print('Saved %s for z|[%0.3f,%0.3f)' % (fname,Z_ii,Z_ii+dZ))
