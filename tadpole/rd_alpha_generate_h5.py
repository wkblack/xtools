# do multi-dimensional testing for color


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# imports
import red_dragon_base as rdb
np,plt,h5py = rdb.np,rdb.plt,rdb.h5py


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# start loop for saving

N_col = 6 # number of colors I'm using
N_fit = 3 # number of Gaussians I'm using to fit
fname_pars = 'out/pars_G%i.h5' % N_fit

dZ = .01
z_vals = [.1,.25,.4] # np.arange(.1,.699,dZ)
z_vals = np.arange(.1,.699,.05)
z_vals = np.arange(.05,.699,.01)
N_Z = len(z_vals)

ds_tags = ['z_vals','covariances','means','weights']
ds_attrs = ['N_col','N_fit','N_Z','dZ']
  

if __name__=='__main__':
  # create datafile
  ds = h5py.File(fname_pars,'w')
  
  covariances = np.zeros((N_Z,N_fit,N_col,N_col))
  means = np.zeros((N_Z,N_fit,N_col))
  weights = np.zeros((N_Z,N_fit))
  
  for ii in range(len(z_vals)):
    zmin,zmax = z_vals[ii],z_vals[ii]+dZ
    print("analyzing z|[%0.2f,%0.2f)" % (zmin,zmax))
    mask_Z = (zmin <= rdb.Z) * (rdb.Z < zmax)
    g,r,i,z,Z = [field[mask_Z] for field in rdb.fields]
    X = np.transpose([g-r,g-i,g-z,r-i,r-z,i-z])
    fit = rdb.GaussianMixture(N_fit).fit(X)
    # save variables from gmm fit for later reconstruction
    norms = np.linalg.norm(fit.means_,axis=1) # magnitude of col vec
    sorting = np.argsort(norms)
    covariances[ii] = fit.covariances_[sorting]
    means[ii] = fit.means_[sorting]
    weights[ii] = fit.weights_[sorting]
  
  for tag in ds_tags:
    exec(f"ds.create_dataset('{tag}',data={tag})")
  
  for tag in ds_attrs:
    exec(f"ds.attrs['{tag}'] = {tag}")
  
  print('Finished %s' % fname_pars)
  ds.close()
  
  print("~fin")
