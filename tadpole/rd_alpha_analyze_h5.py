# do multi-dimensional testing for color

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# imports
import red_dragon_base as rdb
np,plt,h5py = rdb.np,rdb.plt,rdb.h5py

import rd_alpha_generate_h5 as rdag
fname_in = rdag.fname_pars

import scipy
spline = scipy.interpolate.UnivariateSpline
interp1d = scipy.interpolate.interp1d

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# analysis

bool_pos_covar = True # enforce positive covariances (fixes high z!)
bool_plot_all = False # force all fit plots to show

# read back in output
with h5py.File(fname_in,'r') as ds:
  for tag in rdag.ds_tags:
    exec(f"{tag} = ds['{tag}'][()]")
  for attr in rdag.ds_attrs:
    exec(f"{attr} = ds.attrs['{attr}']")
    
if bool_pos_covar: # mask by demanding positive covariances
  mask_covar = np.array([np.all(covariances[ii]>0) for ii in range(N_Z)])
  if 1: # skip first two points which were outliers
    print("Skipping first two values")
    mask_covar[0] = False
    mask_covar[1] = False
  for tag in rdag.ds_tags:
    exec(f"{tag} = {tag}[mask_covar]")
  N_Z = len(z_vals)

# check symmetricity
err = 1e-16 # machine precision
maxval = np.max([[[[abs(covariances[ll,kk,ii,jj]-covariances[ll,kk,jj,ii])
         for ii in range(N_col)] for jj in range(N_col)] for kk in range(N_fit)] for ll in range(N_Z)]) 
if maxval > 2*err:
  print("Maximum covariance matrix asymmetry:",maxval)

print("Values loaded.")

labels_cols = [r'$(g-r)$',r'$(g-i)$',r'$(g-z)$',r'$(r-i)$',r'$(r-z)$',r'$(i-z)$']
colors = ['b','g','r']
labels_fit = ['BC','GV','RS']
Z_med = z_vals + dZ/2.

# for plotting, grab a sampling of points
zv = np.linspace(.05,.75,100)




########################################################################
# interpolation for means, covariances, weights

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# fitting functions


def tensor_polyfit(t,M,deg):
  """
  shape(t) = (i)
  shape(M) = (i,j,...)
  deg = degree of fitting polynomial 
  """
  flat_shape = (np.shape(M)[0],int(np.product(np.shape(M)[1:]))) # to vectorize
  return np.polyfit(t,M.reshape(flat_shape),deg)


def tensor_interpolate(t,P,tensor_shape=None):
  """
  shape(t) = (i)
  shape(P) = (deg+1,i)
  tensor_shape = shape to cast into at end: that of the original tensor
  """
  deg = np.shape(P)[0]
  y = np.sum([np.outer(np.array(t)**(deg-order-1),P[order]) \
              for order in range(deg)],axis=0)
  if tensor_shape is None: # or if tensor_shape is an int
    if np.shape(y)[0]==1:
      return y[0] # return flat array
    else:
      return y # don't modify anything; return flattened array
  else: # reshape to original array shape
    if hasattr(t,'__len__'):
      return y.reshape((len(t),*tensor_shape))
    else:
      return y.reshape(tensor_shape)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# poly fit for fit component weights

P_wghts = tensor_polyfit(Z_med,weights,4) # 5 behaves badly
spl_wghts = [spline(Z_med,weights[:,ii],s=.003)  for ii in range(N_fit)]

def get_weights(Z,method="spline",renormalize=True):
  if method=="polyfit":
    report = tensor_interpolate(Z,P_wghts) # linear, so no reshaping needed
  elif method=="spline":
    report = np.transpose([spl(Z) for spl in spl_wghts])
  else:
    print("ERROR: Undefined method '%s'" % method)
    raise SystemExit
  
  if renormalize: # enforce normalization (err < 3e-16)
    if hasattr(Z,'__len__'):
      s = np.sum(report,axis=1)
      err = np.max(np.abs(s-1))
      report /= np.outer(np.sum(report,axis=1),np.ones(3))
      report[:,2] = 1 - (report[:,0]+report[:,1])
    else:
      s = np.sum(report)
      err = np.abs(s-1)
      report /= np.sum(report) # renormalize
      report[2] = 1 - (report[0]+report[1]) # force sum == 1
    if(err>.05):
      print(f"Warning: heavy renormalization (max error={err:0.3g})")
  return report


if 0 or bool_plot_all: # plot weights of the components
  # `weights` has form [N_Z,N_fit] 
  wghts = get_weights(zv)
  for ii in range(N_fit):
    plt.scatter(Z_med,weights[:,ii],color=colors[ii])
    plt.plot(zv,wghts[:,ii],color=colors[ii])
  plt.legend(labels_fit)
  plt.xlabel('Redshift')
  plt.ylabel('Weight')
  plt.tight_layout()
  plt.show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# polynomial fit for C6 means

P_means = tensor_polyfit(Z_med,means,3)
f_means = interp1d(Z_med,means,axis=0,assume_sorted=True,fill_value='extrapolate')

def get_means(Z,method="interp"):
  """
  return the 6C 3G means as a function of redshift
  shape: (redshift (len(Z), component (N_fit), color (N_col))
  """
  if method=="polyfit":
    return tensor_interpolate(Z,P_means,(N_fit,N_col))
  elif method=="interp":
    return f_means(Z)
  else:
    print("ERROR: Undefined method '%s'" % method)
    raise SystemExit


if 0 or bool_plot_all: # plot means of the Gaussians
  mns = get_means(zv)
  for col in range(N_col):
    plt.subplot(231+col)
    for ii in range(N_fit):
      plt.scatter(Z_med,means[:,ii,col],10,color=colors[ii])
      plt.plot(zv,mns[:,ii,col],color=colors[ii])
    
    plt.ylabel(labels_cols[col])
    if col==0: plt.legend(labels_fit)
    if col==1: plt.title('GMM means')
    if col==4: plt.xlabel('Redshift')
  plt.show()


if 0 or bool_plot_all: # test fitting
  mns = get_means(zv)
  lbl_plt = ['Blue Cloud','Green Valley','Red Sequence']
  ymax = np.max(means) + .25
  for ii in range(3):
    plt.subplot(131+ii)
    plt.title(lbl_plt[ii])
    plt.plot(zv,mns[:,ii])
    [plt.scatter(Z_med,means[:,ii,jj],10) for jj in range(N_col)]
    plt.ylim(0,ymax)
    if ii==0: plt.legend(labels_cols,loc='upper left')
    if ii==1: plt.xlabel('Redshift')
  
  plt.tight_layout()
  plt.show()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# polynomial fit for covariance matrix
# the covariancess ought to stay symmetric, so I should enforce that! 

if bool_pos_covar:
  P_covar = tensor_polyfit(Z_med,np.log(covariances),3)
  f_covar = interp1d(Z_med,np.log(covariances),axis=0,assume_sorted=True,fill_value='extrapolate')
else:
  P_covar = tensor_polyfit(Z_med,covariances,3)
  f_covar = interp1d(Z_med,covariances,axis=0,assume_sorted=True,fill_value='extrapolate')

def get_covariances(Z,method="interp"):
  """ 
  return the 6C x 6C covariance matrix given redshift 
  has shape (len(Z),N_fit,N_col,N_col)
  """
  if method=="polyfit":
    report = tensor_interpolate(Z,P_covar,np.shape(covariances)[1:])
  elif method=="interp":
    report = f_covar(Z)
  else:
    print("ERROR: Undefined method '%s'" % method)
    raise SystemExit
  
  if bool_pos_covar:
    return np.exp(report)
  else:
    return report


if 0 or bool_plot_all: # plot covariances
  zv = np.linspace(.05,.75,100)
  covars = get_covariances(zv)
  plt.suptitle("Covariance Matrix")
  for kk in range(6):
    for ll in range(6):
      for jj in range(3):
        plt.subplot(6,6,1+6*kk+ll)
        plt.scatter(Z_med,covariances[:,jj,kk,ll],2)
        plt.plot(zv,covars[:,jj,kk,ll])
        plt.yscale('log')
  plt.subplots_adjust(top=.94,bottom=.054,left=.054,right=.976,hspace=.4,wspace=.4)
  plt.show()


########################################################################
# make RS selector


def gaussN(x,mu,Sigma):
  " return an N-dimensional Gaussian likelihood "
  D = len(x) # dimensionality of problem
  if len(np.shape(Sigma))>2:
    report = np.squeeze(np.expand_dims(x-mu,1) @ np.linalg.inv(Sigma) @ np.expand_dims(x-mu,2))
  else: # single case
    report = np.dot(x-mu,np.dot(np.linalg.inv(Sigma),x-mu))
  report = np.exp(-.5*report)
  report /= np.sqrt(np.abs(np.linalg.det(Sigma))*(2*np.pi)**D)
  return report


def likelihoods(Z,c):
  """
  shape(c) = [len(Z),N_col]      (i.e.: [[(g-r),...,(i-z)],[...],...]
  return shape: [len(Z),N_fit]
  """
  wghts = get_weights(Z) # [len(Z),N_fit] 
  mns = get_means(Z) # [len(Z),N_fit,N_col]
  covars = get_covariances(Z) # [N_col,N_col,len(Z),N_fit]
  if hasattr(Z,'__len__'):
    try: # try vectorized method
      L_BC = gaussN(c,mns[:,0],covars[:,0,:,:])
      L_GV = gaussN(c,mns[:,1],covars[:,1,:,:])
      L_RS = gaussN(c,mns[:,2],covars[:,2,:,:])
    except OverflowError: # but use for loop if necessary
      print("ERROR: Overflow! Using linear method")
      L_BC = [gaussN(c[ii],mns[ii,0],covars[ii,0,:,:]) for ii in range(len(Z))]
      L_GV = [gaussN(c[ii],mns[ii,1],covars[ii,1,:,:]) for ii in range(len(Z))]
      L_RS = [gaussN(c[ii],mns[ii,2],covars[ii,2,:,:]) for ii in range(len(Z))]
  else: # handle single redshift
      L_BC = gaussN(c,mns[0],covars[0,:,:])
      L_GV = gaussN(c,mns[1],covars[1,:,:])
      L_RS = gaussN(c,mns[2],covars[2,:,:])
    
  return np.transpose(wghts) * np.array([L_BC,L_GV,L_RS])


def P_red(Z,c):
  """ 
  inputs: (Z,c) 
  Z = redshift
  c = [g-r,g-i,g-z,r-i,r-z,i-z]
  shape(c) = [len(Z),N_colors]
  """
  L_BC,L_GV,L_RS = likelihoods(Z,c)
  return L_RS/(L_BC+L_GV+L_RS)


def ES0_mask(Z,c):
  return P_red(Z,c) > .5


########################################################################
# main method for checking snapshot of handiwork

if __name__=='__main__':
  print("Testing P_red")
  Z_ii = .2
  dZ = .02
  Z_min,Z_max = Z_ii,Z_ii+dZ
  mask_Z = (Z_min <= rdb.Z) * (rdb.Z < Z_max)
  g,r,i,z,Z = [field[mask_Z] for field in rdb.fields]
  X = np.transpose([g-r,g-i,g-z,r-i,r-z,i-z])
  print("Calculating P_red")
  prob_red = P_red(Z,X)
  # prob_red_single = P_red(np.median(Z),X) # TODO: get single redshift up and working!
  print("P_red calculated!")
  
  print("Plotting %i galaxies..." % len(Z))
  plt.scatter(r-i,g-i,1,alpha=.5,linewidth=0,c=prob_red,cmap=rdb.cmap_galaxy)
  plt.title(r'$z|[%0.3f,%0.3f)$' % (Z_min,Z_max))
  plt.xlabel(r'$(r-i)$')
  plt.ylabel(r'$(g-i)$')
  plt.clim(0,1) # 0 to 100% probability of being red
  plt.colorbar(label=r'$P_{\rm red}$')
  plt.tight_layout()
  plt.show()
  print("~fin")
