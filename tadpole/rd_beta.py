from sklearn.mixture import GaussianMixture
from glob import glob
import numpy as np
import h5py

########################################################################
# read in fields from an input h5 file 

fname_in_default = 'vetted.h5'
field_labels_in = ['m_g','m_r','m_i','m_z','redshift']

N_col = 6 # number of colors to use [g-r,g-i,g-z,r-i,r-z,i-z] 
N_fit = 3 # number of Gaussians to fit with 

def read_fields(fname=fname_in_default):
  with h5py.File(fname,'r') as ds:
    fields = [ds[field][()] for field in field_labels_in]
  return fields # m_g, m_r, m_i, m_z, redshift


########################################################################
# fit input colors with GaussianMixture 

ds_tags = ['z_vals','covariances','means','weights']
ds_attrs = ['N_col','N_fit','Z_min','Z_max','dZ','N_Z']
ds_fname = 'out/fit_pars_G%i.h5' % N_fit

def fit_h5(fname=fname_in_default,Z_min=.05,Z_max=.7,dZ=.01,printing=True,
           use_fname=None):
  """
  fit_h5(fname=fname_in_default,Z_min=.05,Z_max=.7,dZ=.01,printing=True,use_fname=None)
  """
  m_g,m_r,m_i,m_z,Z = read_fields(fname)
  z_vals = np.arange(Z_min,Z_max,dZ)
  N_Z = len(z_vals)
  # set up report names
  weights = np.zeros((N_Z,N_fit))
  means = np.zeros((N_Z,N_fit,N_col))
  covariances = np.zeros((N_Z,N_fit,N_col,N_col))
  # iterate through for each redshift bin
  for ii in range(len(z_vals)):
    z_min,z_max = z_vals[ii],z_vals[ii]+dZ
    if printing:
      print(f"Analyzing z|[{z_min:0.3f},{z_max:0.3f})")
    mask_Z = (z_min <= Z) * (Z < z_max)
    g,r,i,z = [band[mask_Z] for band in [m_g,m_r,m_i,m_z]]
    # fit with scikit-learn GaussianMixture; store fit variables
    c = np.transpose([g-r,g-i,g-z,r-i,r-z,i-z])
    if use_fname is None: # do fitting blindly
      fit = GaussianMixture(N_fit).fit(c)
    else: # use initial parameters based off of previous run
      Z_med = np.median(Z[mask_Z])
      if use_fname=='alpha': # use older rd alpha code (OVERWRITE THIS)
        fname = 'out/pars_G%i.h5' % N_fit
        if ii==0: # print warning /reminder before analyzing
          print("WARNING: Using red dragon alpha inputs")
          print("         This should be updated to be self-sufficient.")
        import rd_alpha_analyze_h5 as rdaa
        weights_ii = rdaa.get_weights(Z_med)
        means_ii = rdaa.get_means(Z_med)
      else:
        if use_fname is True:
          analyze_fit() # use default fname (ds_fname)
        else:
          analyze_fit(use_fname) # set up fit parameters
        weights_ii = get_weights(Z_med)
        means_ii = get_means(Z_med)
        # TODO: get covariances working!
      fit = GaussianMixture(N_fit,weights_init=weights_ii,means_init=means_ii).fit(c)
    
    # sort as {BC,GV,RS} via total color magnitude
    norms = np.linalg.norm(fit.means_,axis=1) # magnitude of col vec
    sorting = np.argsort(norms)
    # sort & save variables from gmm fit for later reconstruction
    covariances[ii] = fit.covariances_[sorting]
    means[ii] = fit.means_[sorting]
    weights[ii] = fit.weights_[sorting]
  
  with h5py.File(ds_fname,'w') as ds:
    for tag in ds_tags:
      exec(f"ds.create_dataset('{tag}',data={tag})")
    for tag in ds_attrs:
      exec(f"ds.attrs['{tag}'] = {tag}")
  
  if printing:
    print(f'Saved {ds_fname}')
  
  if use_fname is not None:
    clear_fit()


########################################################################
# analyze fit 

import scipy
spline = scipy.interpolate.UnivariateSpline
interp1d = scipy.interpolate.interp1d

interp_weights = None
interp_means = None
interp_covars = None

spline_weights = None
spline_means = None
spline_covars = None

polyfit_weights = None
polyfit_means = None
polyfit_covars = None


def analyze_fit(fname=ds_fname,softening=[.035,.035,.750]):
  """
  analyze_fit(fname=ds_fname,softening=[.035,.035,.750])
  fname:
    HDF5 file to read in for GaussianMixture fit parameters
  softening: 
    parameter in spline functions
    given for [weights,means,covariances]
  """
  # declare globals I'll be altering
  global interp_weights
  global interp_means
  global interp_covars
  global spline_weights
  global spline_means
  global spline_covars
  global polyfit_weights
  global polyfit_means
  global polyfit_covars
  # read in dataset
  with h5py.File(fname,'r') as ds:
    z_vals,covars,means,weights = [ds[tag][()] for tag in ds_tags]
    Z_med = z_vals + ds.attrs['dZ']/2.
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # alter globals
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # interpolations
  interp_weights,interp_means,interp_covars = [interp1d(Z_med,item,
                 axis=0,assume_sorted=True,fill_value='extrapolate') \
                 for item in [weights,means,covars]]
  # Splines
  spline_weights = [spline(Z_med,weights[:,ii],s=softening[0]) \
                    for ii in range(N_fit)]
  spline_means = [[spline(Z_med,means[:,ii,jj],s=softening[1]) \
                   for jj in range(N_col)] for ii in range(N_fit)]
  spline_covars = [[[spline(Z_med,np.log(covars[:,ii,jj,kk]),s=softening[2]) 
                     for kk in range(N_col)] for jj in range(N_col)] for ii in range(N_fit)]
  # TODO: set up spline & polyfit interpolation 
  return "Analyzed " + fname


def clear_fit():
  " remove fit parameters "
  # declare globals I'll be altering
  global interp_weights
  global interp_means
  global interp_covars
  global spline_weights
  global spline_means
  global spline_covars
  global polyfit_weights
  global polyfit_means
  global polyfit_covars
  # reset to None
  interp_weights = None
  interp_means = None
  interp_covars = None
  spline_weights = None
  spline_means = None
  spline_covars = None
  polyfit_weights = None
  polyfit_means = None
  polyfit_covars = None
  return "Cleared fit parameters"


def get_weights(Z,method='spline',fname=ds_fname,renormalize=True):
  " get_weights(Z,method='spline',fname=ds_fname) "
  if method=='interp':
    if interp_weights is None:
      analyze_fit()
    report = interp_weights(Z)
  elif method=='spline':
    if spline_weights is None:
      analyze_fit()
    report = np.transpose([spl(Z) for spl in spline_weights])
  else:
    print(f"ERROR: unset method '{method}'")
    return
  # fix so sum of weights is unity
  if renormalize:
    if hasattr(Z,'__len__'):
      s = np.sum(report,axis=1)
      err = np.max(np.abs(s-1))
      report /= np.outer(np.sum(report,axis=1),np.ones(3))
      report[:,2] = 1 - (report[:,0]+report[:,1])
    else:
      s = np.sum(report)
      err = np.abs(s-1)
      report /= np.sum(report) # renormalize
      report[2] = 1 - (report[0]+report[1]) # force sum == 1
    if(err>.05):
      print(f"Warning: heavy renormalization (max error={err:0.3g})")
  return report


def get_means(Z,method='interp',fname=ds_fname):
  " get_means(Z,method='interp',fname=ds_fname) "
  if method=='interp':
    if interp_means is None:
      analyze_fit()
    # otherwise, return component weights for given redshift(s)
    report = interp_means(Z)
  elif method=='spline':
    if spline_means is None:
      analyze_fit()
    report = np.transpose([[spline_means[ii][jj](Z) 
                 for ii in range(N_fit)] for jj in range(N_col)])
  else:
    print(f"ERROR: unset method '{method}'")
    return
  return np.array(report)


def get_covars(Z,method='interp',fname=ds_fname,force_symmetric=True,
               error_tolerance=2e-16):
  """
  get_covars(Z,method='interp',fname=ds_fname,force_symmetric=True,
               error_tolerance=2e-16)
  """
  if method=='interp':
    if interp_covars is None:
      analyze_fit()
    # otherwise, return component weights for given redshift(s)
    report = interp_covars(Z)
  elif method=='spline':
    if spline_covars is None:
      analyze_fit()
    # report = np.transpose([spl(Z) for spl in spline_covars])
    report = np.transpose([[[np.exp(spline_covars[ii][jj][kk](Z))
                for ii in range(N_fit)] for jj in range(N_col)] for kk in range(N_col)])
  else:
    print(f"ERROR: unset method '{method}'")
    return 
  
  if force_symmetric: # enforce symmetry
    maxerr = 0
    for ii in range(N_fit):
      for jj in range(ii):
        if hasattr(Z,'__len__'):
          err = np.max(np.abs(report[:,:,ii,jj]-report[:,:,jj,ii]))
          report[:,:,ii,jj] = report[:,:,jj,ii]
        else:
          err = np.max(np.abs(report[:,ii,jj]-report[:,jj,ii]))
          report[:,ii,jj] = report[:,jj,ii]
        if err > maxerr:
          maxerr = err
    if maxerr > error_tolerance:
      print(f"WARNING: Maximum error of {maxerr:0.3g} > {error_tolerance}")
  
  return report 


########################################################################
# RS selection

def gaussN(x,mu,Sigma):
  """
  gaussN(x,mu,Sigma)
  return an N-dimensional Gaussian likelihood
  """
  D = len(x) # dimensionality of problem
  if len(np.shape(Sigma))>2:
    report = np.squeeze(np.expand_dims(x-mu,1) @ np.linalg.inv(Sigma) @ np.expand_dims(x-mu,2))
  else: # single case
    report = np.dot(x-mu,np.dot(np.linalg.inv(Sigma),x-mu))
  report = np.exp(-.5*report)
  report /= np.sqrt(np.abs(np.linalg.det(Sigma))*(2*np.pi)**D)
  return report


def likelihoods(Z,c,notify=False):
  """
  likelihoods(Z,c)
  Z = redshift
  c = [m_g,m_r,m_i,m_z]
  return shape: [len(Z),N_fit]
  """
  g,r,i,z = c
  X = np.transpose([g-r,g-i,g-z,r-i,r-z,i-z])
  wghts = get_weights(Z) # [len(Z),N_fit] 
  mns = get_means(Z) # [len(Z),N_fit,N_col]
  covars = get_covars(Z) # [N_col,N_col,len(Z),N_fit]
  if hasattr(Z,'__len__'):
    try: # try vectorized method
      L_BC = gaussN(X,mns[:,0],covars[:,0,:,:])
      L_GV = gaussN(X,mns[:,1],covars[:,1,:,:])
      L_RS = gaussN(X,mns[:,2],covars[:,2,:,:])
    except OverflowError: # but use for loop if necessary
      if notify:
        print("ERROR: Overflow! Using linear method")
      L_BC = [gaussN(X[ii],mns[ii,0],covars[ii,0,:,:]) for ii in range(len(Z))]
      L_GV = [gaussN(X[ii],mns[ii,1],covars[ii,1,:,:]) for ii in range(len(Z))]
      L_RS = [gaussN(X[ii],mns[ii,2],covars[ii,2,:,:]) for ii in range(len(Z))]
  else: # handle single redshift
      L_BC = gaussN(X,mns[0],covars[0,:,:])
      L_GV = gaussN(X,mns[1],covars[1,:,:])
      L_RS = gaussN(X,mns[2],covars[2,:,:])
    
  L_BC,L_GV,L_RS = np.transpose(wghts) * np.array([L_BC,L_GV,L_RS])
  for L in [L_BC,L_GV,L_RS]:
    L[L<0] = 0 # ensure non-negativity
  return L_BC,L_GV,L_RS


def P_red(Z,c):
  """ 
  P_red(Z,c)
  Z = redshift
  c = [m_g,m_r,m_i,m_z]
  """ 
  L_BC,L_GV,L_RS = likelihoods(Z,c)
  report = np.copy(L_RS)
  report[report>0] /= L_BC[report>0] + L_GV[report>0] + L_RS[report>0]
  return report


def ES0_mask(Z,c):
  return P_red(Z,c) > .5


########################################################################
# plot parameter evolution 

from matplotlib import pyplot as plt
mpl_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

labels_cols = [r'$(g-r)$',r'$(g-i)$',r'$(g-z)$',
               r'$(r-i)$',r'$(r-z)$',r'$(i-z)$']
labels_fit_short = ['BC','GV','RS']
labels_fit_full = ['Blue Cloud','Green Valley','Red Sequence']
plot_colors = ['b','g','r']


def plot_weights(fname=ds_fname,method=None):
  " plot_weights(fname=ds_fname,method=None) "
  # read in dataset
  with h5py.File(fname,'r') as ds:
    z_vals,covars,means,weights = [ds[tag][()] for tag in ds_tags]
    Z_med = z_vals + ds.attrs['dZ']/2.
  # analyze dataset
  ymax = np.max(weights) + .05
  
  dz = .01
  if method is not None:
    # get plotting parameters
    zv = np.linspace(min(Z_med)-dz,max(Z_med)+dz,1000)
    if method==True: # set default
      method='spline'
    wghts = get_weights(zv,method=method)
  
  for ii in range(N_fit):
    plt.scatter(Z_med,weights[:,ii],10,color=plot_colors[ii],
                linewidth=0,label=labels_fit_short[ii])
    if method is not None:
      plt.plot(zv,wghts[:,ii],color=plot_colors[ii])
  
  plt.xlim(min(Z_med)-2*dz,max(Z_med)+2*dz)
  plt.xlabel(r'Redshift')
  plt.ylabel(r'Weight')
  plt.ylim(0,ymax)
  plt.legend()
  plt.tight_layout()
  plt.show()


def plot_means(fname=ds_fname,by_color=False,method=None):
  " plot_means(z_vals,means,by_color=False,method=None) "
  # read in dataset
  with h5py.File(fname,'r') as ds:
    z_vals,covars,means,weights = [ds[tag][()] for tag in ds_tags]
    Z_med = z_vals + ds.attrs['dZ']/2.
  # analyze dataset
  ymax = np.max(means) + .15
  
  if method is not None:
    # get plotting parameters
    dz = .01
    zv = np.linspace(min(Z_med)-dz,max(Z_med)+dz,1000)
    if method==True: # set default
      method='interp'
    mns = get_means(zv,method=method)
  
  if by_color: # (6)
    for col in range(N_col):
      plt.subplot(231+col)
      for ii in range(N_fit):
        plt.scatter(Z_med,means[:,ii,col],10,color=plot_colors[ii])
        if method is not None:
          plt.plot(zv,mns[:,ii,col],color=plot_colors[ii])
      
      plt.ylabel(labels_cols[col])
      if col==0: plt.legend(labels_fit_short)
      if col==1: plt.title('GMM means')
      if col==4: plt.xlabel('Redshift')
    plt.tight_layout()
    plt.show()
  else: # by component (3)
    for ii in range(N_fit):
      plt.subplot(131+ii)
      plt.title(labels_fit_full[ii])
      if method is not None:
        plt.plot(zv,mns[:,ii])
      [plt.scatter(Z_med,means[:,ii,jj],10) for jj in range(N_col)]
      plt.ylim(0,ymax)
      if ii==0: plt.legend(labels_cols,loc='upper left')
      if ii==1: plt.xlabel('Redshift')
    
    plt.tight_layout()
    plt.show()


def plot_covars(fname=ds_fname,method=None): 
  " plot_covars(fname=ds_fname,method=None) "
  # read in dataset
  with h5py.File(fname,'r') as ds:
    z_vals,covars,means,weights = [ds[tag][()] for tag in ds_tags]
    Z_med = z_vals + ds.attrs['dZ']/2.
  
  if method is not None:
    # get plotting parameters
    dz = .01
    zv = np.linspace(min(Z_med)-dz,max(Z_med)+dz,1000)
    if method==True: # set default
      method='interp'
    cvrs = get_covars(zv,method=method)
  
  plt.suptitle("Covariance Matrix")
  for kk in range(6):
    for ll in range(6):
      for jj in range(3):
        plt.subplot(6,6,1+6*kk+ll)
        if jj==0 and kk==5:
          plt.xlabel(labels_cols[ll])
        if jj==0 and ll==0:
          plt.ylabel(labels_cols[kk])
          
        plt.scatter(Z_med,covars[:,jj,kk,ll],2)
        if method is not None:
          plt.plot(zv,cvrs[:,jj,kk,ll])
        plt.yscale('log')
  plt.subplots_adjust(top=.94,bottom=.054,left=.054,right=.976,hspace=.4,wspace=.4)
  plt.show()


def plot_cf_weights(fnames=None):
  """
  read in list of files to compare
  """
  if fnames is None:
    fnames = sorted(glob('out/fit_pars*.h5'))
  ymax = 0
  for ii,fname in enumerate(fnames):
    with h5py.File(fname,'r') as ds:
      z_vals,covars,means,weights = [ds[tag][()] for tag in ds_tags]
      Z_med = z_vals + ds.attrs['dZ']/2.
    ymax = max(np.max(weights),ymax)
    plt.plot(Z_med,weights[:,0],label=fname,color=mpl_colors[ii])
    plt.plot(Z_med,weights[:,1],color=mpl_colors[ii])
    plt.plot(Z_med,weights[:,2],color=mpl_colors[ii])
  
  plt.xlabel('Redshift')
  plt.ylabel('Weights')
  plt.ylim(0,ymax + .05)
  plt.legend()
  plt.tight_layout()
  plt.show()

########################################################################
########################################################################

if __name__=='__main__':
  print("In main function...") 
  if 1:
    fit_h5(use_fname=True)
  else:
    old_fit = 'out/pars_G3.h5'
    analyze_fit(old_fit)
    print("TODO: check fit parameters!") 
    raise SystemExit
  print("~fin") 
