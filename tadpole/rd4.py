# look into whether a 3-component model makes sense at high redshifts

########################################################################
# imports 

from time import time
starttime = time()
print("Loading...")
import h5py
fname = 'vetted.h5'
with h5py.File('vetted.h5','r') as ds:
  " NOTE: vetted by L>0.2L_*, mu>13, Z|[.05,.7], mag != 0 or 99 "
  m_g,m_i,m_r,m_z,Z = [ds[key][()] for key in ds.keys()]
  gmi,rmi = m_g-m_i,m_r-m_i
print("Colors and redshifts loaded in %0.3g s" % (time()-starttime))

from matplotlib import pyplot as plt
import numpy as np
import sklearn

########################################################################

Z_i = .68
dZ = .001

Z_i = .35
dZ = .001

Z_i = .5
dZ = .001

mask_Z = (Z_i<=Z)*(Z<=Z_i+dZ)
print("Using %i galaxies" % len(Z[mask_Z]))
label_Z = r'$z|[%0.4g,%0.4g]$' % (Z_i,Z_i+dZ)

Z_m,mg_m,mr_m,mi_m = [field[mask_Z] for field in [Z,m_g,m_r,m_i]]
gmi_m,rmi_m = mg_m - mi_m, mr_m - mi_m

########################################################################
# make predictions with SciKitLearn

from sklearn.mixture import GaussianMixture
X = np.transpose([rmi_m,gmi_m])

NN = range(2,9)
bics = np.zeros(len(NN))

for i,N in enumerate(NN):
  gmm = GaussianMixture(n_components=N)
  x_pred = gmm.fit(X).predict(X)
  plt.subplot(1,len(NN),1+i)
  plt.scatter(rmi_m,gmi_m,1,c=x_pred,cmap='cividis',alpha=.5,linewidths=0)
  bics[i] = gmm.bic(X)
  plt.title("BIC=%g" % bics[i])

plt.tight_layout()
plt.show()

plt.scatter(NN,bics)
plt.xlabel('number of Gaussians fit')
plt.ylabel('BIC')
plt.show()
