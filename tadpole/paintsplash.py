" brute force algorithm to force high-mass halos to be red "
########################################################################
# imports

from matplotlib import pyplot as plt
import numpy as np
import h5py

import rd_alpha_analyze_h5 as rdaa

########################################################################
# setup

fnames = ['../test_patch.h5','../larger_test_patch.h5']
fname = fnames[1]

alter_tag = '_paintsplash'
outname = fname[:-3] + alter_tag + '.h5'

all_keys_out = ['ID','ra','dec',
                'mag_g','mag_r','mag_i','mag_z',
                'magerr_g','magerr_r','magerr_i','magerr_z',
                'flux_g', 'flux_i', 'flux_r', 'flux_z',
                'IVAR_g', 'IVAR_i', 'IVAR_r', 'IVAR_z',
                'Z','Z_cos','CENTRAL','haloid','M200']
keys = all_keys_out # THIS IS UNSTABLE

########################################################################
# read in dataset

print("Reading in dataset...")
with h5py.File(fname,'r') as ds:
  for key in keys:
    exec(f"{key} = ds['{key}'][()]")
print("Dataset read!")

########################################################################
# redden

mask_heavy = M200 > 1e14
print(f"Mask size (mu>14): {len(M200[mask_heavy])}/{len(M200)}")

# get central RS CC positions
mean_colors = rdaa.get_means(Z[mask_heavy])
RS = mean_colors[:,2,:] # [g-r,g-i,g-z,r-i,r-z,i-z]

# this isn't the best thing I could do, but it's quick & dirty.
mag_r_new = mag_g[mask_heavy] - RS[:,0] # g - (g-r) = r
mag_i_new = mag_g[mask_heavy] - RS[:,1] # g - (g-i) = i
mag_z_new = mag_g[mask_heavy] - RS[:,2] # g - (g-z) = z

if 0:
  r,i,z = [tag[mask_heavy] for tag in [mag_r,mag_i,mag_z]]
  plt.subplot(131)
  plt.title('r')
  plt.scatter(r,(mag_r_new-r)/r,1)
  
  plt.subplot(132)
  plt.title('i')
  plt.scatter(i,(mag_i_new-i)/i,1,label='new')
  plt.scatter(i,(mag_i_alt-i)/i,1,label='alt')
  
  plt.subplot(133)
  plt.title('z')
  plt.scatter(z,(mag_z_new-z)/z,1,label='new')
  plt.scatter(z,(mag_z_alt-z)/z,1,label='alt')
  plt.scatter(z,(mag_z_tri-z)/z,1,label='tri')
  
  plt.legend()
  plt.tight_layout()
  plt.show()

mag_g_alt = RS[:,0] + mag_r[mask_heavy] # (g-r) + r = g
mag_i_alt = mag_r[mask_heavy] - RS[:,3] # r - (r-i) = i
mag_z_alt = mag_r[mask_heavy] - RS[:,4] # r - (r-z) = z


if 0:
  g,r,i,z = [tag[mask_heavy] for tag in [mag_g,mag_r,mag_i,mag_z]]
  
  plt.subplot(141)
  plt.title('g')
  plt.scatter(g,(mag_g_alt-g)/g,1,label='alt',color='red')
  
  plt.subplot(142)
  plt.title('r')
  plt.scatter(r,(mag_r_new-r)/r,1,label='new',color='green')
  
  plt.subplot(143)
  plt.title('i')
  plt.scatter(i,(mag_i_alt-i)/i,1,label='alt',color='red')
  plt.scatter(i,(mag_i_new-i)/i,1,label='new',color='green')
  
  plt.subplot(144)
  plt.title('z')
  plt.scatter(z,(mag_z_alt-z)/z,1,label='alt',color='red')
  plt.scatter(z,(mag_z_new-z)/z,1,label='new',color='green')
  plt.legend()
  
  plt.tight_layout()
  plt.show()
  # from these plots, it's clear that the "alt" method is best. 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# fix fluxes
m_old = np.array([mag[mask_heavy] for mag in [mag_g,mag_r,mag_i,mag_z]])
m_new = np.array([mag_g_alt,mag_r[mask_heavy],mag_i_alt,mag_z_alt])
Delta_m = m_new-m_old

for ii,flux in enumerate([flux_g,flux_r,flux_i,flux_z]):
  flux[mask_heavy] = flux[mask_heavy]*100**(-Delta_m[ii]/5)
  

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# overwrite previous values
mag_g[mask_heavy] = mag_g_alt
mag_i[mask_heavy] = mag_i_alt
mag_z[mask_heavy] = mag_z_alt


########################################################################
# create output file

print('Creating output file...')
with h5py.File(outname,'w') as ds:
  for key in keys:
    exec(f"ds.create_dataset('{key}',data={key})")

print('~fin')
