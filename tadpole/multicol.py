# use many colors to constrain RS/BC
# RM1 = doi 10.1088/0004-637X/785/2/104
# looks like having >2 colors doesn't really change much. 

import red_dragon_base as rdb
np,plt = rdb.np,rdb.plt

if 1: # TODO: replace with for loop?
  Z_i = .6
  dZ = .05
  
  mask_Z = (Z_i<rdb.Z)*(rdb.Z<Z_i+dZ)
  Z = rdb.Z[mask_Z]
  g,r,i,z = [tag[mask_Z] for tag in [rdb.m_g,rdb.m_r,rdb.m_i,rdb.m_z]]
  
  
  if 0:
    plt.scatter(i,r-i,1,alpha=.125,c=i<np.mean(i))
    plt.show()
    raise SystemExit
  
  # get the multi-component estimate
  X = np.transpose([g-i]) # using RM1 colors
  clf = rdb.GaussianMixture(n_components=2)
  x_pred1 = clf.fit(X).predict(X)
  
  # get the two-component estimate
  clf,x_pred2 = rdb.GaussMix(r-i,g-i)
  
  # get the multi-component estimate
  X = np.transpose([g-r,r-i,i-z]) # using RM1 colors
  clf = rdb.GaussianMixture(n_components=2)
  x_pred3 = clf.fit(X).predict(X)
  
  if np.mean((r-i)[x_pred3]) < np.mean((r-i)[~x_pred3]):
    x_pred3 = 1 - x_pred3
  
  # use several more colors
  X = np.transpose([g-r,g-i,g-z,r-i,r-z,i-z])
  clf = rdb.GaussianMixture(n_components=2)
  x_pred4 = clf.fit(X).predict(X)
  
  if np.mean((r-i)[x_pred4]) < np.mean((r-i)[~x_pred4]):
    x_pred4 = 1 - x_pred4
  
  for ii,mask in enumerate([x_pred1,x_pred2,x_pred3,x_pred4]):
    plt.subplot(141+ii)
    plt.scatter(r-i,g-i,1,c=mask,cmap='coolwarm',alpha=.125,linewidths=0)
    plt.xlabel(r'$(r-i)$')
    plt.ylabel(r'$(g-i)$') # only for the first plot
    plt.xlim(-.022,1.83)
    plt.ylim(.25,4.5)
    plt.gca().set_aspect('equal')
  
  plt.show()

