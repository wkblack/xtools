# create list of pictures for different GaussianMixture models
# (to be made into a gif eventually for comparison)

import red_dragon_base as rdb
np,plt = rdb.np,rdb.plt

N_G = 3 # number of Gaussian components to model with

testing = False
ordering = True

if testing:
  z_vals = [.1,.35,.69] # for testing
else:
  z_vals = np.arange(.1,.699,.01) # for full run

cmap = 'coolwarm'
if N_G==3:
  cmap = rdb.cmap_galaxy

for Z_ii in z_vals: # for testing
  title = r'$z|[%0.2f,%0.2f)$' % (Z_ii,Z_ii+.01)
  fname = 'gif_pics/sorted_G%i_z%0.3f.png' % (N_G,Z_ii)
  print(title)
  mask_Z = (Z_ii<=rdb.Z) * (rdb.Z<Z_ii+.01)
  g,r,i,z,Z = [field[mask_Z] for field in rdb.fields]
  X = np.transpose([g-r,g-i,g-z,r-i,r-z,i-z]) # use all colors: C6
  fit = rdb.GaussianMixture(N_G).fit(X)
  
  # sort as {BC,GV,RS} via total color norm
  norms = np.linalg.norm(fit.means_,axis=1) # magnitude of col vec
  sorting = np.argsort(norms)
  mask = fit.predict(X)
  if ordering:
    # change masking to be sorted properly
    mask_sorted = np.zeros(np.shape(mask))
    for jj in range(N_G):
      mask_sorted[mask==sorting[jj]] = jj
    mask = mask_sorted
  # plt.scatter(r-i,g-i,1,alpha=.1,c=mask,cmap=cmap,linewidth=0) 
  plt.scatter(r-i,g-i,1,alpha=.01/Z_ii,c=mask,cmap=cmap,linewidth=0) 
  plt.xlabel(r'$(g-r)$')
  plt.ylabel(r'$(r-i)$')
  plt.title(title)
  plt.xlim(0,1.75)
  plt.ylim(0,4.5)
  plt.tight_layout()
  if testing:
    plt.show()
  else:
    print('\t\tSaving %s' % fname)
    plt.savefig(fname)
    plt.clf()

