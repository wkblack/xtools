########################################################################
# m*(z) function from Rykoff+14
########################################################################

import numpy as np

RykoffEtAl2014 = {
  "p1":[22.44,3.36,.273,-0.0618,-0.0227], # z<=.5 coefficients
  "p2":[22.94,3.08,-11.22,-27.11,-18.02], # z>.5 coefficients
  "lims":[.05,.5,.7], # lowest,transition,highest bounds for piecewise
  "doi":"10.1088/0004-637X/785/2/104",
  "url":"https://iopscience.iop.org/article/10.1088/0004-637X/785/2/104/pdf"
}

def _m_star_val(z,low=True,params=RykoffEtAl2014):
  # evaluate m* value for either high or low case
  p = params["p1"] if low else params["p2"]
  lnz = np.log(z)
  return p[0]+p[1]*lnz+p[2]*lnz**2+p[3]*lnz**3+p[4]*lnz**4

def m_star(z,params=RykoffEtAl2014,warnings=True):
  lims=params['lims']
  extrap_warn="WARNING: Extrapolating redshift! [m_star_model.m_star()]"
  low=z<=lims[1]
  if hasattr(z,'__len__') and len(z)>1:
    report = np.zeros(np.shape(z))
    report[low] = _m_star_val(z[low],True,params)
    report[~low] = _m_star_val(z[~low],False,params)
    if warnings and np.any(z<lims[0]) or np.any(z>lims[2]): 
      print(extrap_warn)
    return report
  else: # only one entry
    if low:
      if warnings and z<lims[0]: print(extrap_warn)
      return _m_star_val(z,True,params)
    else:
      if warnings and z>lims[2]: print(extrap_warn)
      return _m_star_val(z,False,params)

def L_star_shift(cutoff=0.2):
  # from definition of magnitude
  return -2.5*np.log10(cutoff)
  # where 0.4 -> 0.9949
  #   and 0.2 -> 1.7474

def m_star_cutoff(z,params=RykoffEtAl2014,warnings=True,cutoff=0.4):
  return m_star(z,params,warnings) + L_star_shift(cutoff)
