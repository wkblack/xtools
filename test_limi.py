import galaxy_id as gid
import numpy as np
from matplotlib import pyplot as plt

if __name__=='__main__':
  # variables
  z = np.linspace(.1,.3,100)
  limi = gid.limi(z) # i-band limit
  msp1 = gid.m_star(z)+1 # m star plus one
  err = (limi-msp1)/limi
  
  # plot
  plt.subplot(211)
  plt.plot(z,gid.limi(z),label=r'pyhao limi')
  plt.plot(z,gid.m_star(z)+1,label=r'$m^* + 1$')
  plt.ylabel(r'i-band magnitude $m_i$')
  plt.gca().axes.xaxis.set_ticklabels([])
  
  plt.subplot(212)
  plt.plot(z,err)
  plt.plot([.1,.3],[0,0],'k')
  plt.ylabel(r'relative error')
  plt.xlabel(r'Redshift $z$')
  
  plt.tight_layout()
  plt.show()
  print('~fin')
